<!--<script async>(function(s,u,m,o,j,v){j=u.createElement(m);v=u.getElementsByTagName(m)[0];j.async=1;j.src=o;j.dataset.sumoSiteId='2b38fac5c0b80d7cd97e6a7e0b0217daea5aa4b79da3cc87a07795b3e79cb1b0';v.parentNode.insertBefore(j,v)})(window,document,'script','//load.sumo.com/');</script> -->


<div class="slidercontainer container-fluid">
  <div class="row">
    <div class="col">

      <!-- carousel code -->
      <div id="carouselExampleIndicators" class="carousel slide">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
        </ol>
        <div class="carousel-inner ">

          <!-- first slide -->
          <div class="carousel-item text-center active">
            <img src="images/slider3.jpg" alt="Software Solution Company in India" class="img-responsive">
         
          </div>

          <!-- second slide -->
          <div class="carousel-item text-right">
            <img src="images/slider2.jpg" alt="Software Solution Company in India" class="img-responsive">
           
          </div>          


          <!-- third slide -->
          <!-- <div class="carousel-item ">
            <img src="images/slider4.jpg" alt="fourtek-banner" class="img-responsive">
          
          </div> -->
          <!-- four slide -->
          <div class="carousel-item ">
            <img src="images/slider1.jpg" alt="fourtek-banner" class="img-responsive">
           
          </div>
        </div>

        <!-- controls -->
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
   <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
      </div>

    </div>
  </div>
</div>

 <!--<div id="sticky">
<div class="container">
<div class="row">
  <div class="col-md-3"><i class="fa fa-phone effect-8"></i> +91-120-428-4526</div>
<div class="col-md-3"><a href="skype:arpit@fourtek.com?chat">
               <span><i class="fa fa-skype"></i></span>
               <span>arpit@fourtek.com</span></a></div>
<div class="col-md-3"><a href="#" target="_blank" class="external" rel="nofollow">
               <span><i class=" effect-8 fa fa-whatsapp"></i> WhatsApp</span>
               <span class="mb-show"></span>
               <span class="whatsup"><span class="mb-show">+91-956-XXXXXXX</span></span></a></div>
<div class="col-md-3"><a href="mailto:info@fourtek.com">
               <span><i class=" effect-8 fa fa fa-envelope-o"></i> </span>
               <span>info@fourtek.com</span></a></div>
</div>


</div>
</div-