<!-- DOTA site tag should be added  just before of the </body> tag --><!-- Start of DOTA site tag (dota.js) - Devnagri On The Air -->
  <!--set language code in 'lng' cookie variable-->
<script>
document.cookie = 'lng=hi';
</script>
<script type='text/javascript' src='https://dota.devnagri.com/static/js/dota.js'></script>  

<?php $base_url = "https://fourtek.com/";?>

<div class="topbar hiddentop">
<div class="container-fluid">
    <div class="row">

      <div class="col-md-12">
        <a href="javascript:;"><span><i class="fa fa-phone effect-8"></i> <strong>+91-9958104612</strong></span></a>
        <a href="javascript:;">
         <span><i class="fa fa-skype"></i></span>
         <span><strong>arpit@fourtek.com</strong></span></a>
          <a href="mailto:info@fourtek.com">
         <span><i class="effect-8 fa fa fa-envelope-o"></i> </span>
         <span><strong>info@fourtek.com</strong></span></a>
         <a href="javascript:;" class="get blinking">
         <span><i class="effect-8 fa fa fa-envelope-o"></i> </span>
         <span id="getblinking"><strong>Get a Quote</strong></span></a>
      </div>
     
     </div>
   </div>

</div>
<div class="navbar  navbar-expand-lg navbar-light" id="mainNav"> 
  <div class="container-fluid"> 
    <div class="col-lg-3 col-md-6 col-sm-6"><a href="<?php echo $base_url;?>" class="fourtek-logo"> <img src="images/fourtek-logo.png" alt="Fourtek IT Solutions- IT consulting company" /> </a> </div>  

    <div class="col-lg-9 col-md-4 col-sm-4" id='cssmenu'>
      <ul>
      <li><a href="about-us.php">About Us</a>
<ul class="tab">
          <li><a href="culture.php">Life@fourtek</a></li>
         </ul>
      </li>
  <li ><a href='javascript:void(0)'>Services</a>
        <ul class="tab">
         <li><a href="website-design-development-services.php"> Website Design & Development</a></li>
      <!-- <li><a href="web-developement.php">Web Development</a></li> -->
      <li><a href="mobile-app-development.php"> Mobile App Development</a></li>
      <li><a href="mobile-game-development.php"> Game Development</a></li>
      <li><a href="it-infrastructure-management-services.php">Infrastructure</a></li>
      <li><a href="digital-multilingual-marketing.php"> Digital Multilingual Marketing</a></li>
      <li><a href="mobile-banking-app-development.php"> Mobile Banking</a></li>
      <li><a href="android-app-development-company-in-india.php"> Android App Development</a></li>
      <li><a href="hybrid-mobile-application-development-in-noida-delhi.php">  Hybrid App Development</a></li>
        <li><a href="it-consulting.php">Consultancy</a></li>
           
           <li><a href="it-infrastructure-staffing-recruiting-services.php">Staffing</a></li> 
           <li><a href="react-native-app-development.php">React Native App Development</a></li>
           <li><a href="dating-app-development.php">Dating App Development</a></li>
           <li><a href="news-app-development.php">News App Development</a></li>   
           
        </ul>         
      </li>


<li ><a href='javascript:void(0)'>Digital Marketing</a>
        <ul class="tab">
        <li><a href="search-engine-optimization.php"> Search Engine Optimization (SEO)</a></li>
      <li><a href="social-media-optimization.php"> Social Media Optimization (SMO)</a></li>
      <li><a href="ppc-services.php"> PPC Advertising</a></li>
      <li><a href="online-reputation-management-services.php"> Online Reputation Management Service</a></li>
        </ul>         
      </li>

<li ><a href='javascript:void(0)'>Products</a>
        <ul class="tab">
     
        <!-- <li><a href="nvocc.php">Freight Forwarding / NVOCC</a></li> -->
        <li><a href="devnagri-online-translation-platform.php"> Devnagri</a></li>
        <li><a  href="#">Fourtek Smart</a>
<ul class="tab">
  <li><a href="https://www.fourtek.com/furniture-erp.php"> Furniture ERP</a></li>
    <li><a href="human-resources-management-software-solutions.php"> HRM Solutions</a></li>
        <li><a href="crm-software-services-india.php"> Customer Relationship Management</a></li>
        <li><a href="erp-software-solutions.php"> ERP Solutions</a> </li>
</ul>

        </li>
        <li><a target="_blank" href="https://academy.fourtek.com/"> Fourtek Academy</a></li>
        </ul>         
      </li>
       <li><a href="blog">Blog</a></li>
 <li><a href="career.php">Career</a></li>
       
        <li><a href="contact-us.php">Contact</a></li>


      


    </ul>
  </div>
  
  <div class="clearfix"></div>    
</div>

<div id="page-content-wrapper"></div>

<!--sidebar-menu-->
<div class="mob-menu hidden-lg" id="wrapper">
  <button type="button" class="hamburger is-closed" data-toggle="offcanvas"> <span class="hamb-top"></span> <span class="hamb-middle"></span> <span class="hamb-bottom"></span> </button>
  <div class="overlay sidebar-overlap"></div>
  
  <!-- Sidebar -->
  <nav class="navbar-fixed-top" id="sidebar-wrapper" role="navigation">
    <ul class="nav sidebar-nav">
      <li><a href="about-us.php"><strong>About Us</strong></a></li>
       <li><a href="culture.php"><strong>Life@fourtek</strong></a></li>
      <hr/>

      <li class="isDisabled"><a href="javascript:;"><strong>Services</strong></a></li>
      <li><a href="website-design-development-services.php"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Website Design & Development</a></li>
      <!-- <li><a href="web-developement.php">Web Development</a></li> -->
      <li><a href="mobile-app-development.php"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Mobile App Development</a></li>
      <li class="d-xl-none"><a href="it-infrastructure-management-services.php"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Infrastructure</a></li>
      <li><a href="mobile-game-development.php"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Game Development</a></li>
      <li><a href="digital-multilingual-marketing.php"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Digital Multilingual Marketing</a></li>
      <li><a href="mobile-banking-app-development-in-india.php"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Mobile Banking</a></li>
      <li><a href="android-app-development-company-in-india.php"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Android App Development</a></li>
      <li><a href="hybrid-mobile-application-development-in-noida-delhi.php"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Hybrid App Development</a></li>
      <li class="d-xl-none"><a href="it-consulting-services-solutions.php"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Consultancy</a></li>
      
      <li class="d-xl-none"><a href="it-infrastructure-staffing-recruiting-services.php"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Staffing</a></li> 
      <li class="d-xl-none"><a href="react-native-app-development.php"><i class="fa fa-angle-double-right" aria-hidden="true"></i> React Native App Development</a></li>
           <li class="d-xl-none"><a href="dating-app-development.php"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Dating App Development</a></li>
           <li class="d-xl-none"><a href="news-app-development.php"><i class="fa fa-angle-double-right" aria-hidden="true"></i> News App Development</a></li>  
      

      <!-- <li class="isDisabled"><a href="javascript:;"><strong>Marketing</strong></a></li> -->

      <li><a href="digital-marketing-company-in-noida-delhi.php"><strong> Digital Marketing</strong></a></li>
         <li><a href="search-engine-optimization.php"><i class="fa fa-angle-double-right" aria-hidden="true"></i>  Search Engine Optimization (SEO)</a></li>
      <li><a href="social-media-optimization.php"><i class="fa fa-angle-double-right" aria-hidden="true"></i>  Social Media Optimization (SMO)</a></li>
      <li><a href="ppc-services.php"><i class="fa fa-angle-double-right" aria-hidden="true"></i>  PPC Advertising</a></li>
      <li><a href="online-reputation-management-services.php"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Online Reputation Management Service</a></li>


      <hr/>
      <li class="isDisabled"><a href="#"><strong>Products</strong></a>
        <li><a href="https://www.fourtek.com/furniture-erp.php"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Furniture ERP</a></li>
         <li><a href="human-resources-management-software-solutions.php"><i class="fa fa-angle-double-right" aria-hidden="true"></i>  HRM Solutions</a></li>
        <li><a href="crm-software-services-india.php"><i class="fa fa-angle-double-right" aria-hidden="true"></i>  Customer Relationship Management</a></li>
        <li><a href="erp-software-solutions.php"><i class="fa fa-angle-double-right" aria-hidden="true"></i>  ERP Solutions</a></li>
        <!-- <li><a href="nvocc.php">Freight Forwarding / NVOCC</a></li> -->
        <li><a href="devnagri-online-translation-platform.php"><i class="fa fa-angle-double-right" aria-hidden="true"></i>  Devnagri</a></li>


        <hr/>

        <li><a href="our-clients.php"><strong>Our Clients</strong></a></li>



        <li><a href="career.php"><strong>Career</strong></a></li>
       
        <li><a href="contact-us.php"><strong>Contact</strong></a></li>
      </ul>
    </nav>
    <!-- /#sidebar-wrapper --> 
  </div>
  <!-- /#wrapper --> 

  
</div>
<!--free-consulation-form-->

<!-- <div class="free-consult element element-1">
 <img src="images/aa.png"> <a href="free-consultation"> Free Consultation</a>
</div> -->




<div class="sticky1 footer-stiky" > 
  <div class="container">
    <div class="row">
      <div class="col-md-3">
        <a href="javascript:;"><span><i class="fa fa-phone effect-8"></i> <strong>+91-9958104612</strong></span></a>
      </div>
      <div class="col-md-3">
        <a href="skype:arpit@fourtek.com?chat">
         <span><i class="fa fa-skype"></i></span>
         <span><strong>arpit@fourtek.com</strong></span></a>
       </div>
       <div class="col-md-3">
        <a href="mailto:info@fourtek.com">
         <span><i class="effect-8 fa fa fa-envelope-o"></i> </span>
         <span><strong>info@fourtek.com</strong></span></a>
       </div>
     </div>
   </div>
 </div>






