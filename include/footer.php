
<!--Start of Zopim Live Chat Script
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="//v2.zopim.com/?3fIRwv7NugdMCUTckwdnZvKoC9muhOeI";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");


</script>-->


 <div id="feedback">
    <div id="feedback-form" style='display:none;' class="">
      <h2>Leave a Message</h2>
     <form class="form" role="form"  action="" method="POST">
      <div id="ResponseDiv"></div>
  <div class="form-group">
      <input type="text" class="form-control1 " name="name"  required="" placeholder="Your Name *" value="<?php //echo $_POST['name']?>" >
  </div>
  <div class="form-group">
      <input type="email" class="form-control1 " name="email" required="" placeholder="Your Email *" value="<?php //echo $_POST['email']?>" >
  </div>
  <div class="form-group">
      <input type="phone" class="form-control1 " name="mobile" required="" placeholder="Your Phone Number *" minlength="10" maxlength="15" pattern="^[0-9]*$"  value="<?php //echo $_POST['mobile']?>">
  </div>


  <div class="form-group">
  <select class="form-control1"  name="consultation" required>
    <option value="">Please Select Consultation *</option>
  <option value="Consultation Interest" <?php if($_POST['consultation'] == 'Consultation Interest'){ echo 'selected="selected"';}?>>Consultation Interest</option>
  <option value="Website Designing" <?php if($_POST['consultation'] == 'Website Designing'){ echo 'selected="selected"';}?>>Website Designing</option>
  <option value="Website Development" <?php if($_POST['consultation'] == 'Website Development'){ echo 'selected="selected"';}?>>Website Development</option>
  <option value="Application Development" <?php if($_POST['consultation'] == 'Application Development'){ echo 'selected="selected"';}?>>Application Development </option>
  <option value="Game Development" <?php if($_POST['consultation'] == 'Game Development'){ echo 'selected="selected"';}?>>Game Development</option>
  <option value="Digital Marketing" <?php if($_POST['consultation'] == 'Digital Marketing'){ echo 'selected="selected"';}?>>Digital Marketing</option>
  <option value="SEO" <?php if($_POST['consultation'] == 'SEO'){ echo 'selected="selected"';}?>>SEO</option>
  <option value="SEM" <?php if($_POST['consultation'] == 'SEM'){ echo 'selected="selected"';}?>>SEM</option>
  <option value="Content Marketing" <?php if($_POST['consultation'] == 'Content Marketing'){ echo 'selected="selected"';}?>>Content Marketing</option>
  <option value="Email Marketing" <?php if($_POST['consultation'] == 'Email Marketing'){ echo 'selected="selected"';}?>>Email Marketing</option>
  <option value="Social Media Management" <?php if($_POST['consultation'] == 'Social Media Management'){ echo 'selected="selected"';}?>>Social Media Management</option>
  </select>
  </div>
  <div class="form-group">
      <input type="text" class="form-control1 " name="company" required="" placeholder="Your Company name *" value="<?php //echo $_POST['company']?>">
  </div>


  <div class="form-group">
      <textarea class="form-control1 " name="comment" rows="2" placeholder="Additional Information / Comments"><?php //echo $_POST['comment']?></textarea>
  </div>
 
  <div class="row">
  <div class="col-md-12">
    <div class="form-group">
  <div class="g-recaptcha" data-sitekey="6LcfIYMUAAAAAGI1bmHjGaNkvJo4xnzzijcTarIe"></div>
</div>
  </div>
  <div class="col-md-12 send-messages">
  <div class="form-group">
            
            <button type="button" id="lets" name="submit" class="btn-contact">Send Message</button>

        </div>
  </div>

<div class="clearfix"></div>

</div>
      </form>

    </div>
    <div id="feedback-tab"><i class=" fa fa fa-envelope-o"></i> Let's Connect</div>
  </div>

<!--End of Zopim Live Chat Script-->
    <!-- Footer -->
        <footer class="footer">

          <div class="callUsk">
            <div class="container">  
                
              <div class="row">
                <div class="col-md-8">
                   <div class="newsletter">
                       <p>Call now at +91-9958104612 | Get Started now 0120-4151299 </p>
                   </div>                
                </div>

                <div class="col-md-4">
                   <div class="certificated"> <img src="images/google-partner.jpg" /> </div>                
                </div>                
              </div>              

            </div>             
          </div>
       
       <!-- <hr class="footer-hr"> -->
       <div class="container">
            <div class="footer-content row">
            
                <div class="col-md-3 col-sm-12"> 
                  <strong>FOURTEK</strong>
                  <ul>
                      <li><a href="about-us.php">About Us</a></li>
                      <li><a href="blog">Blog</a></li>
                      <li><a href="career.php">Careers</a></li>
                      <!-- <li><a href="#">Show Case</a></li> -->
                      <li><a href="terms-conditions.php">Terms & Conditions</a></li>
                      <li><a href="privacy-policy.php">Privacy Policy</a></li>
					  <li><a href="our-clients.php">Our Clients</a></li>
                  </ul>
                </div>

                <div class="col-md-3 col-sm-12"> 
                  <strong>MAJOR SERVICES</strong>
                  <ul>
                      <li><a href="php-website-development-company.php">PHP Website Development</a></li>
                      
                      <li><a href="custom-magento-development.php">Magento Development</a></li>
                      <li><a href="custom-ecommerce-website-design-development.php">Custom E-commerce Development</a></li>
                      <li><a href="ios-app-development-services-in-india.php">iOS App Development</a></li>
                        <li><a href="react-native-app-development.php">React Native App Development</a></li>
           <li><a href="dating-app-development.php">Dating App Development</a></li>
           <li><a href="news-app-development.php">News App Development</a></li> 
                  </ul>
                </div>

                <div class="col-md-3 col-sm-12"> 
                  <strong>COMPANY</strong>
                  <ul>
                   <li><a href="custom-web-development.php">Custom Website Services</a></li>
                       <li><a href="website-redesigning-service-company-in-noida.php">Website Re-Design</a></li>
                      <li><a href="website-design-development-service-company-in-delhi.php">Web Design & Development</a></li>
                      <li><a href="ecommerce-website-development-company-in-india.php">E-commerce Development</a></li>
                      <li><a href="mobile-apps-development-companies-in-india.php">Mobile App Development</a></li>
                      <li><a href="digital-marketing-company-in-noida-delhi.php">Online Marketing</a></li>
                      <li><a href="mobile-banking-app-development-in-india.php">Mobile Banking</a></li>
                  </ul>
                </div>

                <div class="col-md-3 col-sm-12 footer-address"> 
                  <strong>CONTACT</strong>
                  <ul>
                      <li><i class="fa fa-map-marker" aria-hidden="true"></i>
  Fourtek IT Solutions Pvt. Ltd. <br/> B86, Sector 60, Noida ( Uttar Pradesh) 201301 India</li>
                      <li><i class="fa fa-phone" aria-hidden="true"></i> +91-9958104612, 0120-4151299</li>
                      <li><i class="fa fa-envelope" aria-hidden="true"></i> <a href="mailto:info@fourtek.com">info@fourtek.com</a></li>
                  </ul>
                </div>
             </div>
           </div>

                
            <div class="copyright">
              <div class="container-fluid">
                <div class="row">
                  <div class="col-md-9 col-sm-12 copy">  
                     <p>Copyright © 2020 <span>Fourtek It Solutions</span> all right reserved</p>
                  </div>
                 

                 <div class="col-md-3 col-sm-12 text-right footer-social">

                 <div class="social-media-footer">
                   <a href="https://www.facebook.com/fourtek-235547968001/" target="_blank"><i class="fa fa-facebook"></i></a>
                   <!-- <a href="https://plus.google.com/u/0/105141360718909317231" target="_blank"><i class="fa fa-google-plus"></i></a> -->
                   <a href="https://twitter.com/Fourtek" target="_blank"><i class="fa fa-twitter"></i></a>
                   <a href="https://www.instagram.com/fourtek_india/" target="_blank"><i class="fa fa-instagram"></i></a>
                   <a href="https://www.linkedin.com/company/fourtekitsolution/" target="_blank"><i class="fa fa-linkedin"></i></a>
                  </div>


               </div>
              </div>
             </div>
           </div>

          <div class="footer-short">
             <ul>
               <li><a href="tel:01204284526"><i class="fa fa-phone"></i></a></li>
               <li><a href="index.php"><i class="fa fa-home"></i></a></li>
               <li><a href="mailto:info@fourtek.com"><i class="fa fa-envelope"></i></li>
             </ul>
          </div>

        </footer>

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/jquery.easing.min.js"></script>
    <script src="js/owl.carousel.js"></script>
    <script src="js/vitality.js"></script>
    <script src="js/slider.js"></script>
    <script src="js/wow.js"></script>
<script>
  $( "#rst" ).click(function() {    
  $( "#bnrst" ).trigger( "click" );      
});
  
$(document).ready(function() {
$(".tab").click(function () {
    $(".tab").removeClass("active");
    $(this).addClass("active");   
});
});
</script>
    <script>
        document.addEventListener('DOMContentLoaded', function () {
            var timer = new PsgTimer({
                selector: '#firstTimer',
                currentDateTime: Date.UTC(2019, 0, 26, 12,),
                endDateTime: 'UTC+5:30 31.01.2019 18:00:00',
                multilpeBlocks: true,
                animation: 'fade',
                labels: {
                    days: 'Days',
                    hours: 'Hours',
                    minutes: 'minutes',
                    seconds: 'seconds'
                },
                callbacks: {
                    onInit: function () {
                        console.log('Hello world!');
                    }
                }
            });

           
         
        })
    </script>
<script src="js/jquery.psgTimer.js"></script>
 <script>
      $(function () {

        $('#lets').on('click', function (e) {
          

          e.preventDefault();

          $.ajax({
            type: 'post',
            url: 'ajax-form',
            data: $('form').serialize(),
            success: function (response) {
              document.getElementById("ResponseDiv").innerHTML= response; 
              
            }
          });

        });

      });
    </script> 
    


<script>
    (function($) {
    $.fn.menumaker = function(options) {  
     var cssmenu = $(this), settings = $.extend({
       format: "dropdown",
       sticky: false
     }, options);
     return this.each(function() {
       $(this).find(".button").on('click', function(){
         $(this).toggleClass('menu-opened');
         var mainmenu = $(this).next('ul');
         if (mainmenu.hasClass('open')) { 
           mainmenu.slideToggle().removeClass('open');
         }
         else {
           mainmenu.slideToggle().addClass('open');
           if (settings.format === "dropdown") {
             mainmenu.find('ul').show();
           }
         }
       });
       cssmenu.find('li ul').parent().addClass('has-sub');
    multiTg = function() {
         cssmenu.find(".has-sub").prepend('<span class="submenu-button"></span>');
         cssmenu.find('.submenu-button').on('click', function() {
           $(this).toggleClass('submenu-opened');
           if ($(this).siblings('ul').hasClass('open')) {
             $(this).siblings('ul').removeClass('open').slideToggle();
           }
           else {
             $(this).siblings('ul').addClass('open').slideToggle();
           }
         });
       };
       if (settings.format === 'multitoggle') multiTg();
       else cssmenu.addClass('dropdown');
       if (settings.sticky === true) cssmenu.css('position', 'fixed');
    resizeFix = function() {
      var mediasize = 1000;
         if ($( window ).width() > mediasize) {
           cssmenu.find('ul').show();
         }
         if ($(window).width() <= mediasize) {
           cssmenu.find('ul').hide().removeClass('open');
         }
       };
       resizeFix();
       return $(window).on('resize', resizeFix);
     });
      };
    })(jQuery);
    
    (function($){
    $(document).ready(function(){
    $("#cssmenu").menumaker({
       format: "multitoggle"
    });
    });
    })(jQuery);
    
    </script>
<script type="text/javascript">
	  //custom js

	$(document).ready(function () {
	  var trigger = $('.hamburger'),
		  overlay = $('.overlay'),
		 isClosed = false;

		trigger.click(function () {
		  hamburger_cross();      
		});

		function hamburger_cross() {

		  if (isClosed == true) {          
			overlay.hide();
			trigger.removeClass('is-open');
			trigger.addClass('is-closed');
			isClosed = false;
		  } else {   
			overlay.show();
			trigger.removeClass('is-closed');
			trigger.addClass('is-open');
			isClosed = true;
		  }
	  }
	  
	  $('[data-toggle="offcanvas"]').click(function () {
			$('#wrapper').toggleClass('toggled');
	  }); 
	});


	$(window).scroll(function(){
	  var sticky = $('.mob-menu'),
		  scroll = $(window).scrollTop();

	  if (scroll >= 100) sticky.addClass('fixed-menu');
	  else sticky.removeClass('fixed-menu');
	});



	wow = new WOW(
	  {
		animateClass: 'animated',
		offset:       100,
		callback:     function(box) {
		  console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
		}
	  }
	);
	wow.init();
	/*document.getElementById('moar').onclick = function() {
	  var section = document.createElement('section');
	  section.className = 'section--purple wow fadeInDown';
	  this.parentNode.insertBefore(section, this);
	};*/
    </script>
	
	
<script>
 
$(document).ready(function(){
    $("#testimonial-slider").owlCarousel({
        items:6,
        itemsDesktop:[1000,6],
        itemsDesktopSmall:[979,6],
        itemsTablet:[768,3],
        pagination:false,
        navigation:true,
        navigationText:["",""],
        autoPlay:true
    });
});	
</script>
	
	
<script> 
$(document).ready(function(){
    $("#paytm-slider").owlCarousel({
        items:1,
        itemsDesktop:[1000,1],
        itemsDesktopSmall:[979,1],
        itemsTablet:[768,1],
        pagination:false,
        navigation:false,
        navigationText:["",""],
        autoPlay:true
    });
});	
</script>	
	
	
<script> 
$(document).ready(function(){
    $("#facebook-slider").owlCarousel({
        items:1,
        itemsDesktop:[1000,1],
        itemsDesktopSmall:[979,1],
        itemsTablet:[768,1],
        pagination:false,
        navigation:false,
        navigationText:["",""],
        autoPlay:true
    });
});	
</script>	
	
<script> 
$(document).ready(function(){
    $("#phonepay-slider").owlCarousel({
        items:1,
        itemsDesktop:[1000,1],
        itemsDesktopSmall:[979,1],
        itemsTablet:[768,1],
        pagination:false,
        navigation:false,
        navigationText:["",""],
        autoPlay:true
    });
});	
</script>

<script> 
$(document).ready(function(){
    $("#testimonial-solutions").owlCarousel({
        items:4,
        itemsDesktop:[1000,1],
        itemsDesktopSmall:[979,1],
        itemsTablet:[768,1],
        pagination:false,
        navigation:false,
        navigationText:["",""],
        autoPlay:true
    });
}); 
</script>
<script> 
$(document).ready(function(){
    $("#clienttestimonial").owlCarousel({
        items:1,
        itemsDesktop:[1000,1],
        itemsDesktopSmall:[979,1],
        itemsTablet:[768,1],
        pagination:true,
        navigation:false,
        navigationText:["",""],
        autoPlay:true
    });
}); 


</script>

<script> 
$(document).ready(function(){
    $("#testimonial-products").owlCarousel({
        items:1,
        itemsDesktop:[1000,1],
        itemsDesktopSmall:[979,1],
        itemsTablet:[768,1],
        pagination:false,
        navigation:true,
        navigationText:["",""],
        autoPlay:true
    });
}); 


</script>
<script>
$(window).scroll(function(){
  var sticky = $('.sticky1'),
      scroll = $(window).scrollTop();

  if (scroll >= 450) sticky.addClass('fixed11');
  else sticky.removeClass('fixed11');
});

</script>

<script type="text/javascript">
  $(function() {
  $("#feedback-tab").click(function() {
    $("#feedback-form").toggle("fade");
  });
  $("#getblinking").click(function() {
    $("#feedback-form").toggle("fade");
  });

  /*$("#feedback-form form").on('submit', function(event) {
    var $form = $(this);
    $.ajax({
      type: $form.attr('method'),
      url: $form.attr('action'),
      data: $form.serialize(),
      success: function() {
        $("#feedback-form").toggle("fade").find("textarea").val('');
      }
    });
    event.preventDefault();
  });*/
});

</script>
