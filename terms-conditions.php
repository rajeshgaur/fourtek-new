<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="At Fourtek, the data collected by us is secured under the copyright and intellectual property laws. User must read the given terms & conditions before using any part of the website">

    <title>Terms and condition-Fourtek</title>
    <link rel="canonical" href="https://www.fourtek.com/terms-conditions">
    <link rel="shortcut icon" type="image/icon" href="images/fav.ico" />
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Pattaya|Poppins:300,300i,400" rel="stylesheet">
    <link href="css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
    <link href="css/animate.min.css" rel="stylesheet" type="text/css">
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link href="css/responsive.css" rel="stylesheet" type="text/css">
    <?php include "google-code.php";?>
  </head>


<style>
  header{background: url(images/termsconditions.jpg) !important; background-size: cover !important;  background-repeat: no-repeat;background-attachment: fixed !important;}
</style>


  <body id="page-top" class="inner-page">
      <?php include 'include/menu.php' ; ?>
  
    <header class="masthead video">
      <div class="container h-100">
        <div class="row h-100">
          <div class="col-12 my-auto text-center text-white">          
            <h1 class="wow fadeInDown">Terms & Conditions</h1>
            <p>We assure you that we will use the collected data unbiasedly and keep it protected for any kind of misuse and other clients/resources as well.</p>
          </div>
        </div>
      </div>
    </header>

<section class="breadcrumb-block">
<div class="container">
  <div class="breadcrumb">
    <a class="breadcrumb-item" href="index.php">Home</a>
    <span class="breadcrumb-item active">Terms & Conditions</span>
  </div>
</div>
</section>


    <!-- About Section -->
    <section class="about-sections">
      <div class="container">

        <h2><span>General </span>Terms & Conditions</h2>
		<span class="line-blue"></span>
        <p>We state that each and every material that is posted on our website is the sole property of us (Fourtek IT Solutions Pvt. Ltd). The entire data collected by us is secured under the copyright and intellectual property laws. We authorize no one to copy, transfer, use, exploit or distribute our property for any non-commercial or commercial purpose. Nobody is authorized for using our logo and name for any kind of promotion purpose as well.</p>       
        <p>We also do not authorize you to transmit any of content on our website that is obscene, unlawful, harassing, defamatory, or pornographic, that encourage misconduct. It will be considered as a criminal offense done at your end.</p>
        <p>We hold all the rights for modifying or revising our terms and conditions and services without issuing any prior notice. We can also change product pricing, specifications, terms, and warranties.</p>

		<br />

		 <h2><span>Breach of </span> Terms & Conditions</h4>
		 <span class="line-blue"></span>
         <p>Due to any reason, if you fail to conform to the terms and conditions or our agreement, then all the rights granted to you by us will end with self-termination. By agreeing with the term and conditions, you will be responsible to protect data of Fourtek IT Solutions Pvt. Ltd. along with its subsidiaries, affiliates, business partners, employees, licensors, joint-ventures, and third-party data services.
</p>
        
		<br />
        <h2><span>Disclaimer </span> Regarding Warranty</h4>
		<span class="line-blue"></span>
        <p>We may change or revise our policy disclaimer at any time. We are not responsible for any non-infringement and, if you keep using our website without having a look at them and face any sort of damage, we will not be liable for the same. Your decision of continuing use of our website will be purely considered as your agreement to the applied modifications.</p>
          
       

       


      </div>
    </section>

 




   <?php include 'include/footer.php' ;?>

  </body>
</html>
