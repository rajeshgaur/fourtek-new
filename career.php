<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Give your dream wings, connect with Fourtek & be a part of India's most trusted software company. Apply online.">
    <meta name="keywords" content="job openings , job vacancy">
    <title>Job Openings, Vacancy - Fourtek</title>
	  <link rel="canonical" href="https://www.fourtek.com/career">
    <link rel="shortcut icon" type="image/icon" href="images/fav.ico" />
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Pattaya|Poppins:300,300i,400" rel="stylesheet">
    <link href="css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
    <link href="css/animate.min.css" rel="stylesheet" type="text/css">
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link href="css/responsive.css" rel="stylesheet" type="text/css">
	
	<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <?php include "google-code.php";?>
  </head>

<h1 style="display:none;"> job openings</h1>
<h2 style="display:none;">job vacancy</h2>
  <style>
.nojob .fa {font-size: 35px;vertical-align: middle;margin-right: 10px;}
.nojob {font-size: 22px;font-weight: 600;color: #af1f25;}
header{background: url(images/career-bannner.jpg) !important; background-size: cover !important;  background-repeat: no-repeat;background-attachment: fixed !important;}
</style>
<?php
session_start();
error_reporting(0);
require_once("admin/functions/user_list.php");
include "pagination_function.php";
$db = new Database();
$limitt = 6;
$adjacents = 2;
?>
<body id="page-top" class="inner-page">
  <?php include 'include/menu.php'; ?>  
<header class="masthead video">
      <div class="container h-100">
        <div class="row h-100">
          <div class="col-12 my-auto text-center text-white">          
            <h1 class="wow fadeInDown">Not Only "Perks", Enjoy "Fourtek Experience"</h1>
            <p>We believe that no person can spring any movement individually. It takes the effort of the entire team behind building something Big. Therefore, we craft together, learn together and play together to serve our world with real-time business, out-of-the-box solutions, and market-moving information.</p>
          </div>
        </div>
      </div>
</header>
<section class="breadcrumb-block">
<div class="container">
  <div class="breadcrumb">
    <a class="breadcrumb-item" href="index.php">Home</a>
    <span class="breadcrumb-item active">Career</span>
  </div>
</div>
</section>
    <!-- About Section -->
<section class="about-sections about-career">
    <div class="container">
        <div class="wow fadeIn text-center">
          <h2><span>We’re Raising the Bar</span> – Are You Ready to Join the Squad?</h2><br/>
        </div>
       <div class="row careeer-desc"> 
        <div class="col-sm-6">
        <img src="images/career-img.jpg" alt="" class="img-fluid">
        </div>
        <div class="col-sm-6">
           <p>Fourtek is no less than a platform where your passion meets your profession. We set you free from all the barriers and let you explore your highest potentials. We believe in grow hands in hands and therefore we offer the equal opportunity to every member of our Fourtek family to grow and excel. We don’t only offer “perks”; we offer Fourtek experience.</p>
		   <p> You will not only get to live up to your passion but also be part of our fun activities. We keep our out-of-the-box thinkers motivated. We together work hard and party harder.</p>
        </div>
        <div class="col-sm-12 mailtotext">
         <p>We always welcome the energetic and enthusiastic souls who love to pursue their passion and turn that into their profession. If you also want to be a part of our squad, drop us your updated resume at <a href="mailto:hr@fourtek.com">hrd@fourtek.com</a> or you may apply for the latest opening directly here.</p>
        </div>
     </div> 
  </div>
</section>
<section>
  <div class="container">
     <div class="row">
      <div class="col-sm-12 jobs-title">
        <div class="title-name shadow-sm p-3 mb-5 bg-white rounded">RECENT JOBS</div>
<?php
$result = $db->selectdata("job_career", "where 1");
if (!empty($result)) {
    ?>
<div class="table-responsive">
    <table id="example" class="table table-striped table-bordered" style="width:100%">
    <thead>
        <tr>
            <th>S.No</th>
            <th>Job Title</th>
            <th>Posted Dates</th>
            <th>No Of Opening</th>
            <th>Action</th>            
        </tr>
    </thead>
  <?php  $i=1;
    while ($row = $result->fetch_assoc()) {
        ?>
    <tbody>
            <tr>
                <td><?php echo $i; ?></td>
                <td><?php echo $row['title']; ?></td>
                <td><?php $date = $row['post_date'];
        $Dates = strtotime($date);
        echo date('d-m-Y', $Dates); ?></td>
				        <td><?php echo $row['num_of_post']; ?></td>
                <td> <a href="career-detail.php?id=<?php echo $row['id']; ?>#career-details">View</a> </td>
            </tr>
    </tbody>
        <?php
        $i++;
    } ?>
  </table>
</div>
<?php
} else {
        ?>
<div class="col-md-12 nojob"> <i class="fa fa-frown-o"></i> Currently there is no Opening </div>
<?php
    } ?>
       </div>
       <br/> <br/> <br/>
    </div>
    </div>
  </section>  
  <br/><br/><br/><br/>
<?php include 'include/footer.php';?>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>














   
  </body>
</html>
