<?php error_reporting(1); 
require_once "../vendor/autoload.php";
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

if(isset($_POST['submit']))
{
  //$_POST=array();

$name = $_POST['name'];
$visitor_email = $_POST['email'];
$phone = $_POST['phone'];
$message = $_POST['message'];

//Create a new PHPMailer instance 
$mail = new PHPMailer;
$mail->isSMTP();
$mail->SMTPDebug = 0;
$mail->Host = 'smtp.gmail.com';
$mail->Port = 587;
$mail->SMTPSecure = 'tls';
$mail->SMTPAutoTLS = false;
$mail->SMTPAuth = true;
$mail->Username = "adwords@fourtek.com";
$mail->Password = "Fourtek@0101";
$mail->setFrom($visitor_email, 'Fourtek : Smart Request For Demo');
$mail->addAddress('himanshu.fourtek@gmail.com');
$mail->addAddress('arpit@fourtek.com');
$mail->addAddress('nakul@fourtek.com');
$mail->addAddress('sharmagaurav@fourtek.com');
$mail->Subject = 'Fourtek : Smart Request For Demo';

$mail->msgHTML('<html><head> </head><body> <table border="1" cellpadding="5">
        <tr> <td colspan="2"><strong>Your Name</strong></td>
          <td colspan="2">'.$_POST["name"].'</td>
        </tr>
        <tr>
          <td colspan="2"><strong>Your Email</strong></td>
          <td colspan="2">'.$_POST["email"].'</td>
        </tr>
        <tr>
          <td colspan="2"><strong>Your Phone</strong></td>
          <td colspan="2">'.$_POST["phone"].'</td>
        </tr>       
        <tr>
          <td colspan="2"><strong>Comment</strong></td>
          <td colspan="2">'.$_POST["message"].'</td>
        </tr>
      </table>
      </body>
      </html>');
  $errmsg = '';
  $succmsg = '';
      if (!$mail->send()) {
        $errmsg = "Mailer Error: " . $mail->ErrorInfo;
       } else {  
        $succmsg = "<div class='success-msg' style='color:green;'>Your Message has been Sent Successfully.</div>";
      /*  $form_data = array(
        'name' => $_POST['name'],
        'email' => $_POST['email'],
        'mobile' => $_POST['mobile'],
        'comment' => $_POST['comment']
          );
        $result = $db->insert_data('contact_us_form', $form_data);*/ 
       }
    } 


?>

<!doctype html>
<html lang="zxx">
    
<!-- Mirrored from templates.envytheme.com/pluck/default/index-3.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 15 Oct 2019 12:15:42 GMT -->
<head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap Min CSS -->
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <!-- Animate Min CSS -->
        <link rel="stylesheet" href="assets/css/animate.min.css">
        <!-- FontAwesome Min CSS -->
        <link rel="stylesheet" href="assets/css/fontawesome.min.css">
        <!-- Owl Carousel Min CSS -->
        <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
        <!-- Odometer Min CSS -->
        <link rel="stylesheet" href="assets/css/odometer.min.css">
        <!-- Magnific Min CSS -->
        <link rel="stylesheet" href="assets/css/magnific-popup.min.css">
        <!-- Style CSS -->
        <link rel="stylesheet" href="assets/css/style.css">
		<!-- Resposive CSS -->
        <link rel="stylesheet" href="assets/css/responsive.css">

        <title>Fourtek Smart ERP</title>

		<link rel="shortcut icon" type="image/icon" href="https://www.fourtek.com/images/fav.ico" />
    </head>

    <body data-spy="scroll" data-offset="70">

        <!-- Start Preloader Area -->
        <div class="preloader">
            <div class="spinner">
                <div class="double-bounce1"></div>
                <div class="double-bounce2"></div>
            </div>
        </div>
        <!-- End Preloader Area -->
        
        <!-- Start Navbar Area -->
        <nav class="navbar navbar-expand-lg navbar-style-one navbar-light bg-light">
            <div class="container">
                <a class="navbar-brand" href="index.php">
                    <img src="assets/img/logo.jpg" alt="Fourtek Smart ERP">
                    <img src="assets/img/logo.jpg" alt="Fourtek Smart ERP">
                </a>

                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item"><a class="nav-link" href="#Introduction">Introduction</a></li>
                        <li class="nav-item"><a class="nav-link" href="#modules">Our Modules</a></li>
                        <li class="nav-item"><a class="nav-link" href="#features">Features</a></li>
                        <li class="nav-item"><a class="nav-link" href="#Benefits">Benefits</a></li>
                        <li class="nav-item"><a class="nav-link" href="#Deployment">Deployment</a></li>
                        <li class="nav-item"><a class="nav-link" href="#FAQs">FAQs</a></li>
                        
                    </ul>

                    <ul class="others-option">
                        <li><a href="#contact" class="btn btn-primary">Request for demo</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- End Navbar Area -->

        <!-- Start Main Banner -->
        <div id="Introduction" class="saas-banner">
            <div class="effect"></div>

            <div class="d-table">
                <div class="d-table-cell">
                    <div class="container">
                        <div class="saas-banner-content">
                            <h1>The complete solution for furniture manufacturers and exporters!</h1>
                            
                            
                        </div>
                    </div>
                </div>
            </div>

            <div id="particles-js"></div>

            <div class="saas-banner-image"><img src="assets/img/laptop1.png" alt="image"></div>
        </div>
        <!-- End Main Banner -->

        <!-- Start Services Area -->
        <section id="about" class="services-area ptb-100">
            <div class="container">
                <div class="saas-section-title">
                    <h2>About Fourtek Smart ERP</h2>
                    <div class="bar"></div>
                    <p>When you do any business it is necessary to keep a track of all your work flow and process to ultimately know the profiest and losses for your business. To do all these many businesses have some form of enterprise solution in their company like excel and few customized ERP. But slowly with changes in business requirements and competition to give best output by managing all business processes. FourtekSmart manages all critical business functions in any kind of business.
</p>
                </div>

 <div class="row">
  <div class="col-lg-6 col-md-6"><p>FourtekSmart ERP for furniture industry is useful for small as well as large furniture industries. Furniture industry is present worldwide and have unique requirements of ERPs. The ERP for furniture industry would be required to keep a track of all the clients, vendors, suppliers, productions, purchase, exports, BOM, etc. 
</p></div>
  <div class="col-lg-6 col-md-6">
  <p>FourtekSmart ERP helps the Furniture industry to give a quick response for any situation & product recalls. This ERP for furniture Industry is helpful to maintain the list of all and unused used raw materials, how many products are developed and sold, the details of customer and vendors, total amount expended an earned etc. it’s beneficial  to improve the profit by cutting the unnecessary expended and also by getting the new opportunity. 
</p>
  </div>
  
  
 </div>


                
            </div>

            
        </section>
        <!-- End Services Area -->
<!-- Start Pricing Area -->
        <section id="modules" class="pricing-area ptb-100 pt-0">
            <div class="container">
                <div class="saas-section-title">
                    <h2>Our Modules</h2>
                    <div class="bar"></div>
                    
                </div>

                <div class="row">
                    <div class="services-slides owl-carousel owl-theme">
                        <div class="col-lg-12 col-md-12">
                            <div class="single-services">
                                
                                <h3>Bill of Materials (BOM)</h3>
                                <p>BOM is an important part of your quality and control systems. Managing BOM through your ERP system will improve inventory flow, material BOM planning, and ultimately, strategic advantage for your business.</p>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12">
                            <div class="single-services">
                               
                                <h3>Marketing & Sales</h3>
                                <p>It designed to support the sales order processing system, control daily activities like prospecting, and manage contacts. This system produces sales forecasting, identifies advertising channels, and helps to maintain competitive pricing scales.</p>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12">
                            <div class="single-services">
                               
                                <h3>Order</h3>
                                <p>Order processing is the most important function in order management. As soon as the sales order is generated, resource allocation begins to procure raw material from inventory stock.</p>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12">
                            <div class="single-services">
                              
                                <h3>Planning</h3>
                                <p>It helps companies implement resource planning by integrating all of the processes it needs to run a company with a single system.</p>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12">
                            <div class="single-services">
                                
                                <h3>Purchase</h3>
                                <p>From procurement to quoting, from ordering to receipt and invoicing, the purchase module helps you through all.</p>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12">
                            <div class="single-services">
                               <h3>Gate</h3>
                                <p>This module is very user-friendly, that even your security personnel can be easily trained to make quick entries.</p>
                            </div>
                        </div>
						
						<div class="col-lg-12 col-md-12">
                            <div class="single-services">
                               <h3>Store</h3>
                                <p>This system designed to empower businesses to manage stock and keep tracking each material issue. </p>
                            </div>
                        </div>
						
						<div class="col-lg-12 col-md-12">
                            <div class="single-services">
                               <h3>Production</h3>
                                <p>It allows manufacturers to store, retrieve, and revise every formula that is being used in business, and define processes, equipment, stages, etc., as well.</p>
                            </div>
                        </div>
						<div class="col-lg-12 col-md-12">
                            <div class="single-services">
                               <h3>Export</h3>
                                <p>This involves planning, execution, control, and monitoring trust and collaboration among supply chain partners and much more.</p>
                            </div>
                        </div>
						<div class="col-lg-12 col-md-12">
                            <div class="single-services">
                               <h3>Reports</h3>
                                <p> Get every kind of reports of various stages and processes to review the progress.</p>
                            </div>
                        </div>
						
						
                    </div>
                </div>
            </div>
        </section>
        <!-- End Pricing Area -->
        <!-- Start Features Area -->
        <section id="features" class="features-area-two bg-f7fafd ptb-100">
            <div class="container">
                <div class="saas-section-title">
                    <h2>Our Features</h2>
                    <div class="bar"></div>
                    
                </div>
<div class="row align-items-center">
                    <div class="col-lg-6 col-md-12">
                        <div class="features-content">
                            <div class="row">
                                <div class="col-lg-6 col-md-6">
                                    <div class="box">
                                        <i class="far fa-hand-point-up"></i>

                                        Drag and Drop
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-6">
                                    <div class="box">
                                        <i class="far fa-gem"></i>

                                        Fully Customizable
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-6">
                                    <div class="box">
                                        <i class="fas fa-fighter-jet"></i>

                                        App Integration
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-6">
                                    <div class="box">
                                        <i class="fas fa-lock"></i>

                                        Private and Security
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-6">
                                    <div class="box">
                                        <i class="fas fa-magic"></i>

                                        High Quality
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-6">
                                    <div class="box">
                                        <i class="fas fa-location-arrow"></i>

                                        Easy To Use
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-6">
                                    <div class="box">
                                        <i class="fas fa-compress"></i>

                                        Pixel Precision
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-6">
                                    <div class="box">
                                        <i class="fas fa-cloud"></i>

                                        Cloud Service
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-6">
                                    <div class="box">
                                        <i class="fas fa-edit"></i>

                                        Vector Editing
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-6">
                                    <div class="box">
                                        <i class="fas fa-cogs"></i>

                                        Latest Technology
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-12">
                        <div class="features-image">
                            <img src="assets/img/main-pic.png" class="wow fadeInUp" data-wow-delay="0.6s" alt="main-pic">
                            <img src="assets/img/circle-shape.png" class="rotate-image rotateme" alt="circle">
                        </div>
                    </div>
                </div>
                <!--<div class="row align-items-center">
                    <div class="col-lg-12 col-md-12">
                        <div class="features-content">
                            <div class="row">
                                <div class="col-lg-6 col-md-6">
                                    <div class="box">
                                       Customized to your Business Requirements: Although the basic operation model is standard in every industry vertical, there are exceptions that make yours' unique. We limit the scope of the implementation to the module(s) needed to meet your operational goals and customize to meet your precise business requirements.
                                    </div>
                                </div>

                                 <div class="col-lg-6 col-md-6">
                                    <div class="box">
                                       
Solution within your Budget: Our ERP specializes and caters to business requirements of small and medium scale enterprises (SME). Consequently, low cost of ownership and maintenance is the defining element of our product. The modules, routines and reports are customized to meet your precise business requirements. Quality is our hallmark and we guarantee the satisfaction of our clients.
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-6">
                                    <div class="box">
                                       Go Live in 90 Days: Smart back end programming allows for quick configuration and deployment of new modules. Our ERP can be deployed in less than 90 days - in an era where ERP solutions take 6-12 months to go live.
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-6">
                                    <div class="box">
                                        
Advanced Analytics: Features such as Predefined Reports, Pivot Tables for data analysis, Report Writer for customized reports, Document Designer for preferred layout, and Multi-Company to maintain accounts of your subsidiary, help the management team organize data points and access advanced analytical functions to track key metrics.
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-6">
                                    <div class="box">
                                       Built-in controls: Our ERP also allows configuration of appropriate validation and controls to minimize costs and optimize productivity. For example, expense limitations, customized approval workflows, audit tracking on transactions.
                                    </div>
                                </div>

                                 <div class="col-lg-6 col-md-6">
                                    <div class="box">
                                    <b>User Friendly</b>: Customizable dashboard and favorites window for your ease-in-use.
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-6">
                                    <div class="box">
                                    Client Server Architecture Technology: The 3-tier architecture provides high level security in terms of data management and grouping access rights. High data processing speed provides the ultimate working experience.
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-6">
                                    <div class="box">
                                      Location Independent Application: Multi-location data synchronization allows updated information at any given point of time.
                                    </div>
                                </div>

                              

                            </div>
                        </div>
                    </div>

                   
                </div>-->
            </div>
        </section>
        <!-- End Features Area -->

        <!-- Start New Features Update Area -->
        <section id="Benefits" class="new-features-update edge ptb-100">
            <div class="container">
                <div class="saas-section-title">
                    <h2>Benefits</h2>
                    <div class="bar"></div>
                   
                </div>
<div class="services-inner-area">
				<div class="container-fluid">
					<div class="row h-100 justify-content-center align-items-center">
						<div class="col-lg-6 col-md-12">
							<div class="services-image">
								<img src="assets/img/laptop.png" alt="image">
							</div>
						</div>

						<div class="col-lg-6 col-md-12">
							<div class="services-inner-content">
								<div class="services-item">
									<div class="icon">
										<i class="fa fa-check"></i>
									</div>
									<h3>Streamline your processes</h3>
									
								</div>

								<div class="services-item">
									<div class="icon">
										<i class="fa fa-check"></i>
									</div>
									<h3>Monitor your costing</h3>
									
								</div>

								<div class="services-item">
									<div class="icon">
										<i class="fa fa-check"></i>
									</div>
									<h3>Reduce the Dependency on others</h3>
									
								</div>
								
								<div class="services-item">
									<div class="icon">
										<i class="fa fa-check"></i>
									</div>
									<h3>Reporting and tracking allows you manage delivery time</h3>
									
								</div>
								
								<div class="services-item">
									<div class="icon">
										<i class="fa fa-check"></i>
									</div>
									<h3>Cost reduction</h3>									
								</div>
								<div class="services-item">
									<div class="icon">
										<i class="fa fa-check"></i>
									</div>
									<h3>Flexibility</h3>									
								</div>
								<div class="services-item">
									<div class="icon">
										<i class="fa fa-check"></i>
									</div>
									<h3>Provide competitive advantage</h3>									
								</div>
							</div>
						</div>
					</div>
				</div>
            </div>
               
            </div>
        </section>
        <!-- End New Features Update Area -->

        <!-- Start Fun Facts Area -->
		<section id="Deployment" class="funfacts-area-three ptb-100 pt-0">
			<div class="container">
                <div class="saas-section-title">
                    <h2>Deployment</h2>
                    <div class="bar"></div>
                   <p>Deployment options of Fourtek Smart ERP</p>
                </div>

				

				<div class="contact-cta-box">
					<h3>With a FourtekSmart ERP solution, you can put your business considerations first and do what is best for your business. You can choose to deploy your business software:</h3>
					<ol>
					<li>In house (with in your premises)</li>
<li>In the cloud</li>
<li>Or both using a hybrid model that combines cloud and on-premises</li>
					</ol>
					
				</div>
				<div class="contact-cta-box">
					<h3>In house:</h3>
					<p>With an in house deployment, some organizations feel that they have more control over their property, can rely on their own IT departments, are concerned with privacy, are willing to make a large initial investment to keep ongoing costs down and wish to provide their own security. They may have already invested in hardware and networks.</p>
					
				</div>
				<div class="contact-cta-box">
					<h3>In the cloud:</h3>
					<p>The cloud offers benefits such as flexibility, ease of access, lower up-front costs, quality IT maintenance and security.</p>
<p>The cloud is getting a lot of attention. However, this does not mean that the cloud is right for all companies and industries. For example, think about a situation where the latency inherent in cloud access is not acceptable. In a manufacturing environment, for instance, a real-time control application might need to be located very close to the robots it controls.</p>
					
				</div>
				<div class="contact-cta-box">
					<h3>Hybrid Model:</h3>
					<p>A growing number of businesses are choosing a hybrid deployment model that incorporates the cloud but retains an on-premises architecture. This allows organizations to take advantage of the cloud’s capabilities while keeping their most sensitive data behind the firewall.</p>
					
				</div>
			</div>

           
		</section>
        <!-- End Fun Facts Area -->
        
        <!-- Start Feedback Area -->
        
        <!-- End Feedback Area -->

         

        <!-- Start FAQ Area -->
        <section id="FAQs" class="faq-area ptb-100  bg-f7fafd">
            <div class="container">
                <div class="saas-section-title">
                    <h2>Frequently Asked Question</h2>
                    <div class="bar"></div>
                    <p>Some FAQs help you to choose right ERP</p>
                </div>

                <div class="faq">
                    <ul class="accordion">
                        <li class="accordion-item">
                            <a class="accordion-title active" href="javascript:void(0)">What are the benefits of an ERP System?  <i class="fas fa-plus"></i></a>
                           <div class="accordion-content show">The benefits derived from ERP can far outweigh the costs of the system, providing that the system is selected carefully and is appropriate for your company from a feature, cost, and technology standpoint. Some of the benefits realized are:</p>
							<ol>
							<li>A single integrated system</li>
<li> Streamlining processes and workflows</li>
<li>Reduce redundant data entry and processes</li>
<li>Establish uniform processes that are based on recognized best business practices</li>
<li>Information sharing across departments</li>
<li> Improved access to information</li>
<li> Improved workflow and efficiency</li>
<li> Improved customer satisfaction based on improved on-time delivery, increased quality, shortened delivery times</li>
<li>Reduced inventory costs resulting from better planning, tracking and forecasting of requirements</li>
<li> Turn collections faster based on better visibility into accounts and fewer billing and/or delivery errors</li>
<li> Decrease in vendor pricing by taking better advantage of quotation comparison and tracking vendor performance</li>
<li> Track actual costs of activities and perform activity based costing</li>
<li> Provide a consolidated picture of sales, inventory and receivables</li>
<li> An ERP system provides the solid operational backbone manufacturers and distributors need to improve the volume of production and fulfillment of orders while reducing costs. By optimizing your manufacturing and distribution operations with ERP, you'll also be able to focus on new business opportunities.</li>
							</ol>
							</div>
                        </li>
                        
                        <li class="accordion-item">
                            <a class="accordion-title" href="javascript:void(0)">Do I need a new ERP? <i class="fas fa-plus"></i></a>
                           <div class="accordion-content"> <p >Below are the indications that you need a new ERP now:</p>
							<ol>
							<li>Increasing customer complaints about poor service</li>
<li> Using Manual and multiple systems for the same task</li>
<li> Failing to get accurate, up-to-date information for decision-making</li>
<li> Using handwritten notes as it's easier to jot it down than use the current system</li>
<li> Having to enter the same data into more than one system</li>
<li> Inability to expand the business due to limitations of current information system</li>
<li> Lost or corrupted data</li>
<li> Dependency on others for completion of your task</li>
							</ol></div>
                        </li>
                        
                        <li class="accordion-item">
                            <a class="accordion-title" href="javascript:void(0)">How to choose right ERP? <i class="fas fa-plus"></i></a>
                           <div class="accordion-content"> <ol>
							<li> Cost Benefit Analysis:The cost of ERP implementation versus the benefit derived post implementation is a critical factor for consideration. An ERP should help overcome the cost and continue to pay dividends for years to come.</li>
<li> Business Specific Requirements Implementation: Although the basic operation model is standard in every industry vertical, there are exceptions that make yours' unique. An ERP should account for and cater to each exception and meet the precise requirements of your business.</li>
<li> Unrealistic Implementation Expectations: ERP sales representatives often understate the level of resources required to close the deal. Therefore, many companies fail to budget adequate time, money, resources and external consulting support to make the project successful.</li>
<li> Modules Selection: An ERP consists of multiple modules. However, a business should limit the scope of the implementation to the module(s) needed to meet its operational goals. An attempt to overachieve can lead to failure.</li>
<li> Cost of Implementation: ERP implementation can be a long and expensive process. Businesses should identify the deliverables and relevant cost implications for development and deployment of various modules along with the timeline. Cost of ownership of the required hardware system is a onetime expense, however the cost of upgradation and maintenance should be carefully considered as well. Additionally, salaries of IT managers and trained operators will remain a recurring cost.</li>
<li> Employee Feedback on the ERP: Your employees will be the key stakeholders impacted by the ERP implementation. Consequently, employees from every department should preview the product to ensure their respective requirements and concerns are addressed.</li>
<li> Selection of the ERP Vendor: Technology and business process implementation are generally the pillars to short list ERP vendors. However, the credibility of the vendor and testimonials to back the service quality should not be discounted.</li>
<li> Long Term Planning: Although the primary criteria for ERP selection will be the current business requirements, future enhancements and requirements should also be considered and relevant provision should be maintained in the ERP.</li>
<li> Post Implementation Support: A lot of issues arise once an organization starts using the implemented ERP system. Unless the issues are addressed appropriately and in a timely manner, the ERP cannot function smoothly.
</li>
							</ol>
							</div>
                        </li>
                        
                        
                    </ul>
                </div>
            </div>
        </section>
        <!-- End FAQ Area -->

        <!-- Start Free Trial Area -->
        <section class="free-trial-area ptb-100 pt-0">
            <div class="container">
                <div class="free-trial-content">
                    <img src="assets/img/man-and-women.png" alt="image">

                    <h3>Start your 30 days FREE <br>trials today!</h3>
                    <p>Experience best-selling ERP .One stop solution for your industry. Try Now! Buy Now!</p>
                    <a href="#contact" class="btn btn-primary">try it free</a>

                </div>
            </div>
        </section>
        <!-- End Free Trial Area -->

        <!-- Start Blog Area -->
       
        <!-- End Blog Area -->

        <!-- Start Partner Area -->
        
        <!-- End Partner Area -->

        <!-- Start Contact Area -->
        <section id="contact" class="contact-area ptb-10">
            <div class="container">
                <div class="saas-section-title">
                    <h2>Request for demo</h2>
                    <div class="bar"></div>
                   
                </div>

                <div class="contact-form">
                    <div class="row align-items-center">
                        <div class="col-lg-6 col-md-12">
                            <div class="contact-info-box">
                                <p>Our 24/7 support team is here to help you and make sure our product is up to date.</p>

                                <span>Would love to hear from you. Call us directly at:

                                    <a href="call:+91-989-9846-974">+91-9958104612</a>
                                    <a href="call:+9120-4151299">+91-120-4151299</a>

                                   
                                </span>
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-12">
                          <p style="color:red;"><?php echo $errmsg;?></p>
                          <p style="color:green;"><?php echo $succmsg;?></p>
                            <form method="post" name="myform">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12">
                                        <div class="form-group">
                                            <label>Name*</label>
                     <input id="name" type="text" class="form-control" required data-error="Please enter your name" name="name">
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-md-12">
                                        <div class="form-group">
                                            <label>Email*</label>
                                            <input id="email" type="email" class="form-control" required data-error="Please enter your email" name="email">
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12">
                                        <div class="form-group">
                                            <label>Mobile*</label>
                                            <input id="mobile" type="text" class="form-control" required data-error="Please enter your mobile number" name="phone">
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-md-12">
                                        <div class="form-group">
                                            <label>Message*</label>
                                            <textarea id="message" class="form-control" cols="30" rows="4" required data-error="Write your message" name="message"></textarea>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-md-12">
                                        <input type="submit" class="btn btn-primary" name="submit" value="Send Message">
                                        <div id="msgSubmit" class="h3 text-center hidden"></div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Contact Area -->

        <!-- Start Subscribe Area -->
       
        <!-- End Subscribe Area -->

        <!-- Start Footer Area -->
        <footer class="footer-area bg-fffcf4">
            

            <div class="copyright-area">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-7 col-md-7">
                            <p><i class="far fa-copyright"></i> 2019 Fourtek Smart ERP. all right reserved Developed by<a href="#">Fourtek IT Solution</a>. </p>
                        </div>

                        <div class="col-lg-5 col-md-5">
                            <ul>
                                <li><a href="https://www.facebook.com/Fourtek/" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="https://twitter.com/Fourtek" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="https://www.instagram.com/fourtek_india/" target="_blank"><i class="fab fa-instagram"></i></a></li>
                                <li><a href="https://www.linkedin.com/company/fourtekitsolutions" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

           
        </footer>
        <!-- End Footer Area -->

        <div class="go-top"><i class="fas fa-angle-up"></i></div>

        <!-- jQuery Min JS -->
        <script src="assets/js/jquery.min.js"></script>
        <!-- Popper Min JS -->
        <script src="assets/js/popper.min.js"></script>
        <!-- Bootstrap Min JS -->
        <script src="assets/js/bootstrap.min.js"></script>
        <!-- Parallax Min JS -->
        <script src="assets/js/parallax.min.js"></script>
        <!-- Appear Min JS -->
        <script src="assets/js/jquery.appear.min.js"></script>
        <!-- Odometer Min JS -->
        <script src="assets/js/odometer.min.js"></script>
        <!-- Owl Carousel Min JS -->
        <script src="assets/js/owl.carousel.min.js"></script>
        <!-- Magnific Popup Min JS -->
        <script src="assets/js/jquery.magnific-popup.min.js"></script>
        <!-- MixItUp Min JS -->
        <script src="assets/js/mixitup.min.js"></script>
        <!-- Particles Min JS -->
        <script src="assets/js/particles.min.js"></script>
        <!-- WOW Min JS -->
        <script src="assets/js/wow.min.js"></script>
        <!-- Form Validator Min JS -->
        <script src="assets/js/form-validator.min.js"></script>
        <!-- Contact Form Min JS -->
        <script src="assets/js/contact-form-script.js"></script>
        <!-- ajaxChimp Min JS -->
        <script src="assets/js/jquery.ajaxchimp.min.js"></script>
        <!-- Main JS -->
        <script src="assets/js/main.js"></script>
    </body>

<!-- Mirrored from templates.envytheme.com/pluck/default/index-3.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 15 Oct 2019 12:15:54 GMT -->
</html>