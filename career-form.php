<?php ob_start();
error_reporting(0);
if ($_GET['job'] == '') {
 header('Location: career-detail.php');
}
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require 'vendor/autoload.php';
$mail = new PHPMailer;
require_once("admin/functions/user_list.php");
$db = new Database();
if (isset($_POST['submit'])) {

  //$_POST=array();

  if (isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])) {
        //your site secret key
    $secret = '6LcfIYMUAAAAAJVBnF2cjuzCdSremj4GUx8fRJGY';
        //get verify response data
    $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
    $responseData = json_decode($verifyResponse);
    if ($responseData->success) {
      $target_dir = "upload/";
      $ran_name = md5(uniqid()) . basename($_FILES["fileAttach"]["name"]);
      $target_file = $target_dir . $ran_name;
      $file_ext = strtolower(end(explode('.', $_FILES['fileAttach']['name'])));
      $expensions= array("pdf","doc");
      $upload = 1;
      if (in_array($file_ext, $expensions)=== false) {
        $msg="extension not allowed, please choose a pdf or doc file.";
        $upload = 0;
      }
      $visitor_email = $_POST['fromEmail'];
            $mail->SMTPDebug = 0;                                       // Enable verbose debug output
            $mail->isSMTP();                                            // Set mailer to use SMTP
            $mail->Host       = 'smtp.gmail.com';  // Specify main and backup SMTP servers
            $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
            $mail->Username   = 'adwords@fourtek.com';                     // SMTP username
            $mail->Password   = 'Fourtek@0101';                               // SMTP password
            $mail->SMTPSecure = 'tls';                                  // Enable TLS encryption, `ssl` also accepted
            $mail->Port       = 587;
            $mail->AddAttachment($_FILES['fileAttach']['tmp_name'], $_FILES['fileAttach']['name']);
            $mail->setFrom($visitor_email, 'Fourtek Contact Us');
            $mail->addAddress('hrd@fourtek.com');
            $mail->addAddress('apurva@fourtek.com');
            $mail->isHTML(true);
            $mail->Subject = 'Career';
            $mail->isHTML(true);
            $mail->msgHTML('<html><head> </head><body> <table border="1" cellpadding="5">
        <tr> <td colspan="2"><strong>First Name</strong></td>
        <td colspan="2">'.$_POST["fromName"].'</td>
        </tr>
        <tr> <td colspan="2"><strong>Last Name</strong></td>
        <td colspan="2">'.$_POST["lname"].'</td>
        </tr>
        
        <tr>
        <td colspan="2"><strong>Mobile</strong></td>
        <td colspan="2">'.$_POST["mobile"].'</td>
        </tr>
        <tr>
        <td colspan="2"><strong>Email Id</strong></td>
        <td colspan="2">'.$_POST["fromEmail"].'</td>
        </tr>
        <tr>
        <td colspan="2"><strong>Post for Apply</strong></td>
        <td colspan="2">'.$_POST["post_apply"].'</td>
        </tr>       
        <tr>
        <td colspan="2"><strong>Total Experience</strong></td>
        <td colspan="2">'.$_POST["experience"].'</td>
        </tr>
        <tr>
        <td colspan="2"><strong>CTC</strong></td>
        <td colspan="2">'.$_POST["ctc"].'</td>
        </tr>
        <tr>
        <td colspan="2"><strong>Address 2</strong></td>
        <td colspan="2">'.$_POST["address-two"].'</td>
        </tr>
        </table>
        </body>
        </html>');
            
      if (!$mail->send()) {
        $errmsg = "Mailer Error: " . $mail->ErrorInfo;
      } else {
        move_uploaded_file($_FILES["fileAttach"]["tmp_name"], $target_file);
        $formsData = array(
          'first_name' => $_POST['fromName'],
          'last_name' => $_POST['lname'],
          'post_apply' => $_POST['post_apply'],
          'total_exp' => $_POST['experience'],
          'email' => $_POST['fromEmail'],
          'mobile_number' => $_POST['mobile'],          
          'address_one' => $_POST['ctc'],
          'address_two' => $_POST['address-two'],
          'resume_name' => $ran_name
        );
        $result = $db->insert_data('job_apply', $formsData);
        if ($result) {
                    //header('Location: career.php');
          $msg = "Thank You. You have successfully applied for the job.";
        }
      }
   } else {
      $errmsg = 'Robot verification failed, please try again.';
    }
  } else {
    $errmsg = 'Please click on the reCAPTCHA box.';
  }
} else {
  $errmsg = '';
  $succmsg = '';
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <title>Fourtek</title>
  <link rel="shortcut icon" type="image/icon" href="images/fav.ico" />
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Pattaya|Poppins:300,300i,400" rel="stylesheet">
  <link href="css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
  <link href="css/animate.min.css" rel="stylesheet" type="text/css">
  <link href="css/style.css" rel="stylesheet" type="text/css">
  <link href="css/responsive.css" rel="stylesheet" type="text/css">
  <?php include "google-code.php";?>
</head>
<style>
  header{background: url(images/career-detail-banner.jpg) !important; background-size: cover !important;  background-repeat: no-repeat;background-attachment: fixed !important;}
</style>
<body id="page-top" class="inner-page">
  <?php include 'include/menu.php' ; ?>  
  <header class="masthead video">
    <div class="container h-100">
      <div class="row h-100">
        <div class="col-12 my-auto text-center text-white">          
          <h1>WE ARE HIRING</h1>
          <p>A formal career path and development process exists in Fourtek IT Solutions Pvt Ltd. All career paths have an underlying management structure. Promotions within each career path are aligned with the needs of the organization. </p>
        </div>
      </div>
    </div>
  </header>
  <section class="breadcrumb-block">
    <div class="container">
      <div class="breadcrumb">
        <a class="breadcrumb-item" href="index.php">Home</a>
        <span class="breadcrumb-item active">Career</span>
      </div>
    </div>
  </section>


  <section id="career-form" class="recent-job career-forms">
    <div class="container">

      <p style="color:red;"><?php echo $errmsg;?></p>
      <p style="color:green;" ><?php echo $msg;?></p>
      <div class="row">
        <div class="col-sm-12 col-md-12">
          <div class="card ">
           <h5 class="personal-title">Personal Information</h5>
           <div class="card-body">
             <br/> 
             <form action="" method="post" enctype="multipart/form-data">  
               <div class="row">   

                 <div class="col-sm-12 col-md-6 form-group"> 
                   <span class="field-name">First Name*</span>                
                   <input type="text" name="fromName" class="form-control" placeholder="First Name*" required="" />
                 </div>

                 <div class="col-sm-12 col-md-6 form-group"> 
                   <span class="field-name">Last Name*</span>                
                   <input type="text" name="lname" class="form-control" placeholder="Last Name*" required="" />
                 </div>
                 <div class="col-sm-12 col-md-6 form-group">
                   <span class="field-name">Mobile*</span>                 
                   <input type="phone" name="mobile" minlength="10" maxlength="15"  pattern="^[0-9]*$" class="form-control" placeholder="Mobile*" required="" />
                 </div> 
                 <div class="col-sm-12 col-md-6 form-group"> 
                   <span class="field-name">Email Id*</span>                
                   <input type="email" name="fromEmail" class="form-control" placeholder="Email Id*" required="" />
                 </div> 

                 <div class="col-sm-12 col-md-6 form-group">
                   <span class="field-name">Post for Apply*</span>                 
                   <input type="text" name="post_apply" class="form-control" placeholder="Post for Apply*" value="<?php echo $_GET['job'];?>" readonly/>
                 </div>               

                 <div class="col-sm-12 col-md-6 form-group"> 
                   <span class="field-name">Total Experience*</span>                
                   <input type="text" name="experience" class="form-control" placeholder="Total Experience*" required="" />
                 </div>

               <!--<div class="col-sm-12 col-md-6 form-group"> 
                   <span class="field-name">City</span>                
                   <input type="text" name="city" class="form-control" placeholder="City*" required="" />
               </div>

               <div class="col-sm-12 col-md-6 form-group"> 
                   <span class="field-name">State</span>                
                   <input type="text" name="state" class="form-control" placeholder="State*" required="" />
               </div> 

               <div class="col-sm-12 col-md-6 form-group">
                   <span class="field-name">Address 1</span>                 
                   <input type="text" name="address" class="form-control" placeholder="Address*" required="" />
                 </div> -->

                 <div class="col-sm-12 col-md-6 form-group">
                   <span class="field-name">Current Location*</span>                 
                   <input type="text" name="address-two" class="form-control" placeholder="Current Location*" required="" />
                 </div>
                 <div class="col-sm-12 col-md-6 form-group">
                   <span class="field-name">CTC*</span>                 
                   <input type="text" name="ctc" class="form-control" placeholder="CTC*" required="" />
                 </div>

                 <style>
                   .apply-btns {
                    text-align: right;
                    margin: 18px 0;
                  }
                  .apply-btns input[type="submit"] {
                    padding: 8px 35px;
                    border-radius: 4px;
                    border: none;
                    font-weight: 800;
                  }               
                  .uplodbutton input[type="file"] {
                    height: auto;
                    margin: 0px;
                    padding: 10px 10px;
                    width: 100%;
                  }              
                </style>

                <div class="col-sm-12 col-md-12 form-group uplodbutton">
                 <span class="field-name">Upload CV*</span>                 
                 <input type="file" name="fileAttach" class="form-control creer-frm" required="" />
               </div>


             </div>  
             <div class="row">
              <div class="col-md-6">
                <div class="g-recaptcha" data-sitekey="6LcfIYMUAAAAAGI1bmHjGaNkvJo4xnzzijcTarIe"></div>
              </div>
              <div class="col-md-6" style="text-align:right;">
                <div class="apply-btns">
                  <input type="submit" name="submit" class="btn btn-primary" value="Apply">
                </div>
              </div>

              <div class="clearfix"></div>

            </div>
            <br/>

            
          </div>
        </form>
        <br/>
      </div>
    </div>




  </div>
  <br/> <br/> <br/> <br/> 
</div>
</section>


<?php include 'include/footer.php' ;?>

</body>
</html>