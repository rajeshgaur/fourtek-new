<?php
//session_start();
/*
if(!$_SESSION['user_id']){
  header('location:index.php');
}
*/
include "header.php";
require_once("admin/functions/user_list.php");
include "pagination_function.php";
$limitt = 6;
$adjacents = 2;
  
//include "sidebar-menu.php";
?>

<!-- Blog Area Start Here -->
        <div class="single-services-area blog-page-area">
            <div class="container">
                <div class="row">
                  <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                        <div class="blog-page-content">
                        	<?php
                            $db = new Database();
                            $target_dir = "admin/blog-images/";
                            $total_rows = 6;
                            $total_pages = ceil($total_rows / $limitt);
                            if (isset($_GET['page']) && $_GET['page'] != "") {
                                $page = $_GET['page'];
                                $offset = $limitt * ($page-1);
                            } else {
                                $page = 1;
                                $offset = 0;
                            }
                            $result = $db->selectdata("blog", "where 1 ORDER BY id DESC LIMIT $offset, $limitt");
                            $resultrows = $db->selectdata("blog", "where 1");
                            //$total_records = mysqli_num_rows($resultrows);
                            
                            if (!empty($result)) {
                                while ($row = $result->fetch_assoc()) {
                                    $blog_url = $row['blog_url'];
                                    $image_url = $target_dir . $row['images'];
                                    $blog_id = $row['id'];
                                    $user_id = $row['user_id'];
                                    $createDate = $row['create_date'];
                                    $datee = explode(" ", $createDate);
                                    $date = $datee[0];
                                    $timestamp = strtotime($date); ?>
								
								
			<div class="single-blog padding-top">
				<div class="media">
				  <div class="pull-left">                                    
					<a href="post-details.php?id=<?php echo $row['id']?>"><img src="<?php echo $image_url; ?>" alt=""></a>                          
					<div class="details">
						<a href="post-details.php?id=<?php echo $row['id']?>"><i class="fa fa-link" aria-hidden="true"></i></a>
					</div>
					<div class="date">
						<ul>
							<li><?php echo date(" j,  F ", $timestamp); ?> <span> <?php echo date("Y", $timestamp); ?></span></li>                                        
						</ul>
					</div>
				  </div>
				 <?php
                    $query = "SELECT COUNT(id) AS total FROM blog_comment WHERE blog_id = '".$blog_id."' AND status = 1 ";
                                    $result_query = $db->select($query);
                                    $resultt = $result_query->fetch_assoc(); ?>
				  <div class="media-body">
					<h4 class="media-heading"><a href="post-details.php?id=<?php echo $row['id']?>"><?php echo $row['title']; ?></a></h4>
					<?php
                    $query_user = "SELECT username FROM admin WHERE id = '".$user_id."' ";
                                    $user_query = $db->select($query_user);
                                    $result_user = $user_query->fetch_assoc(); ?>
					<p><span>by</span> <?php echo $result_user['username']; ?> <span>  &nbsp;&nbsp;  <?php echo $resultt['total']; ?></span> <?php if ($resultt['total'] > 1) {
                                        echo "comments";
                                    } else {
                                        echo "comment";
                                    } ?></p>
					<div class="blog-content">
						<p><?php $description = $row['description'];
                                    $limit = 200;
                                    if (strlen($description)<=$limit) {
                                        echo $description;
                                    } else {
                                        $text = substr($description, 0, $limit) .'...';
                                        echo $text;
                                    } ?></p>
						<div class="read-more">
							<a href="post-details.php?id=<?php echo $row['id']?>">Read More</a>
						</div>                                        
					</div>                                    
				  </div>
				</div>
			</div><!--single-blog padding-top-->
		
		<?php
                                }
                            } ?>
		
		<?php

              if ($total_pages <= (1+($adjacents * 2))) {
                  $start = 1;
                  $end   = $total_pages;
              } else {
                  if (($page - $adjacents) > 1) {
                      if (($page + $adjacents) < $total_pages) {
                          $start = ($page - $adjacents);
                          $end   = ($page + $adjacents);
                      } else {
                          $start = ($total_pages - (1+($adjacents*2)));
                          $end   = $total_pages;
                      }
                  } else {
                      $start = 1;
                      $end   = (1+($adjacents * 2));
                  }
              }
    if ($total_pages > 1) {
        ?>
			  <div class="text-center">
				  <ul class="pagination pagination-sm justify-content-center">
					<!-- Link of the first page -->
					<li class='page-item <?php ($page <= 1 ? print 'disabled' : '')?>'>
					  <a class='page-link' href='<?php echo $base_url; ?>blog.php?page=1'><<</a>
					</li>
					<!-- Link of the previous page -->
					<li class='page-item <?php ($page <= 1 ? print 'disabled' : '')?>'>
					  <a class='page-link' href='<?php echo $base_url; ?>blog.php?page=<?php ($page>1 ? print($page-1) : print 1)?>'><</a>
					</li>
					<!-- Links of the pages with page number -->
					<?php for ($i=$start; $i<=$end; $i++) {
            ?>
					<li class='page-item <?php ($i == $page ? print 'active' : '')?>'>
					  <a class='page-link' href='<?php echo $base_url; ?>blog.php?page=<?php echo $i; ?>'><?php echo $i; ?></a>
					</li>
					<?php
        } ?>
					<!-- Link of the next page -->
					<li class='page-item <?php ($page >= $total_pages ? print 'disabled' : '')?>'>
					  <a class='page-link' href='<?php echo $base_url; ?>blog.php?page=<?php ($page < $total_pages ? print($page+1) : print $total_pages)?>'>></a>
					</li>
					<!-- Link of the last page -->
					<li class='page-item <?php ($page >= $total_pages ? print 'disabled' : '')?>'>
					  <a class='page-link' href='<?php echo $base_url; ?>blog.php?page=<?php echo $total_pages; ?>'>>>                      
					  </a>
					</li>
				  </ul>
			 </div>
			  
       <?php
    } ?>
			  </div>
            </div> 			
          </div>
        </div>        


                <div class="col-lg-3 col-md-md-3 col-sm-3 col-xs-12">
                        <div class="sidebar-area">
                            <div class="single-sidebar padding-top">
                               <?php
                $dynamic_field = $db->selectdata('dynamic_field', "WHERE 1");

                if ($dynamic_field) {
                    $dynamic = $dynamic_field->fetch_assoc();
                }

             if (!empty($dynamic['blog_add_first_img'])) {
                 $imagee = $dynamic['blog_add_first_img']; ?>
                  <a href="<?php echo $base_url; ?>register.php"><img src="<?php echo $base_url; ?>admin/uploads/<?php echo $imagee?>" class="img-responsive"> </a>
                <?php
             } ?>
                               <!-- <img src="img/blog-sale.jpg" class="img-responsive"/> -->
                            </div>
                            <div class="single-sidebar padding-top">
                                <h2>Recent Post</h2>
                                 <ul class="recent-post">
            <?php
            $start_from = 0;
            $limit = 5;
            $result = $db->selectdata("blog", "where 1 ORDER BY id DESC LIMIT $start_from, $limit");
            while ($row = $result->fetch_assoc()) {
                ?>
      <li><a href="post-details.php?id=<?php echo $row['id']?>"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> <?php echo $row['title']; ?> </a>
      <p>
            <?php $description = $row['description'];
                $limit = 30;
                if (strlen($description)<=$limit) {
                    echo $description;
                } else {
                    $text = substr($description, 0, $limit) .'...';
                    echo $text;
                } ?>
            </p></li>
            <?php
            } ?>
            </ul>
                            </div>
                            <div class="single-sidebar padding-top">
                                <h2>Categories</h2>
                      <ul>
                        <?php
                        $category = $db->selectdata("category", "where 1");
                        if (!empty($category)) {
                            while ($categoryrow = $category->fetch_assoc()) {
                                ?>
                          <li><a href="<?php echo $base_url?>category_list.php?id=<?php echo $categoryrow['id']?>"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i><?php echo $categoryrow['cat_name']?></a></li>
                          <?php
                            }
                        } else {
                            echo "No Categories";
                        } ?>
                      </ul>
                            </div>
                            <div class="single-sidebar padding-top">
                            
                               <?php if (!empty($dynamic['blog_add_scnd_img'])) {
                            $imagee = $dynamic['blog_add_scnd_img']; ?>
                  <a href="<?php echo $base_url; ?>meetup.php"><img src="<?php echo $base_url; ?>admin/uploads/<?php echo $imagee?>" class="img-responsive"> </a>
               <?php
                        }?>
                       
                            </div>
                            <div class="single-sidebar padding-top">
                            <h2>Social Links</h2><br>
                            <div class="bolg-sociala">
                                <ul>
                                    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
                                   
                                </ul>
                            </div>
                    
                         </div>
                        </div>
                    </div>







                </div>
            </div>
        </div>
        <!-- Blog Area End Here -->
<?php
    
include "footer.php"; ?>
