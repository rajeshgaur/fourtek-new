<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
<title>Top Mobile App Development,Digital Marketing Company USA, UK, Canada, India</title>
<meta name="description" content="Fourtek is one of the topmost mobile app development and digital marketing companies in USA, UK, Canada, India.  For any project,services visit Fourtek now.">
<meta name="keywords" content="Top Mobile App Development company, Mobile App Development company USA, Mobile App Development company UK, Mobile App Development company Canada, Digital Marketing Company USA, Digital Marketing Company UK, Digital Marketing Company Canada">
<link rel="canonical" href="https://www.fourtek.com/">
<meta http-equiv="content-language" content="en-gb" />
<meta name="DC.title" content="Fourtek - Mobile App Development company, Agency in Delhi/Noida | Digital Marketing Agency in India" />
<meta name="geo.region" content="IN-UP" />
<meta name="geo.placename" content="Noida" />
<meta name="geo.position" content="28.603208;77.367205" />
<meta name="ICBM" content="28.603208, 77.367205" />
<meta name="robots" content="index, follow" />
<meta name="p:domain_verify" content="f52410c9206369cad3c2e055263a4d40"/>
<META NAME="Revisit-After" CONTENT="10 days" />
<META NAME="Copyright" CONTENT="Copyright 2020. Fourtek IT Solution Pvt Ltd" />
<META NAME="GOOGLEBOT" CONTENT="NOODP" />
<META NAME="Slurp" CONTENT="NOODP" />
<META NAME="msnbot" CONTENT="NOODP" />
<META NAME="ROBOTS" CONTENT="NOYDIR" />
<META NAME="Slurp" CONTENT="NOYDIR" />

<link rel="shortcut icon" type="image/icon" href="images/fav.ico" />
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="css/popins.css" rel="stylesheet" type="text/css">
<link href="css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
<link href="css/animate.min.css" rel="stylesheet" type="text/css">
<link href="css/style.css" rel="stylesheet" type="text/css">
<link href="css/slider.css" rel="stylesheet" type="text/css">
<link href="css/custom-bootstrap-margin-padding.css" rel="stylesheet" type="text/css">
<link href="css/responsive.css" rel="stylesheet" type="text/css">
<?php include "google-code.php";
error_reporting(0);
?>
</head>
<body id="page-top">
<!-- DOTA site tag should be added  just before of the </body> tag --><!-- Start of DOTA site tag (dota.js) - Devnagri On The Air -->
  <!--set language code in 'lng' cookie variable-->
<!--<script>
document.cookie = 'lng=hi';
</script>
<script type='text/javascript' src='https://dota.devnagri.com/static/js/dota.js'></script>-->
<!-- End of DOTA site tag (dota.js) - Devnagri On The Air -->
<!--<script type="application/ld+json">
{
  "@context" : "http://schema.org",
  "@type" : "Organization",
  "name" : "Fourtek",
 "url" : "https://www.fourtek.com/",
 "sameAs" : [
   "https://twitter.com/Fourtek",
   "https://www.facebook.com/Fourtek/",
   "https://www.instagram.com/fourtek_india/"
   "https://www.linkedin.com/company/fourtekitsolution/"
   ],
  "address": {
    "@type": "PostalAddress",
    "streetAddress": "B86, Sector 60",
    "addressRegion": "Noida",
    "postalCode": "201301 ",
    "addressCountry": "IN"
  }
}
</script>-->
  <h1 style="display:none;"> Top Mobile App Development company</h1>
  <h2 style="display:none;">Top Digital Marketing Company</h2>
  <?php include 'include/menu.php' ; ?>
  <?php include 'include/header.php';
  require_once("admin/functions/user_list.php");
  $db = new Database();
  ?>
  <!-- About Section -->
  <section class="page-section" id="about">
    <div class="container">
      <div class="text-center about-us">
        <h2  data-wow-iteration="1" data-wow-duration="0.15" class="wow fadeInDown">Your Reliable Digital Business <br> Partner Rendering Sterling Solutions</h2>
        <p data-wow-iteration="3" data-wow-duration="0.15" class="wow pulse  mb-0">With 10 years of mastership in website building, Fourtek is a perfect blend of seasoned professionals that gets your business ideology perfectly. Our comprehensive range of digital solutions is not just empowering businesses to outperform, but also building their brand’s reputation in the online world with a robust & strategic approach.
        </p>
      </div>
      <div class="row">
        <div class="col-md-4 solutionsSection">
          <div class="title text-left">
            <h2><span>Our </span> Solutions</h2>
            <span class="text-line"></span>
          </div>  
          <p>The wide range of our result-driven solutions bringing greater experiences and helping us in acquiring maximum contentment with a long-lasting relationship with our global clients.</p>
        </div>
        <div class="col-md-8">
          <div id="testimonial-solutions" class="owl-carousel">
            <div class="testimonial">
              <div class="pic">
                <a href="it-infrastructure-staffing-recruiting-services"><img src="images/solutions1.jpg" alt=""><strong>Staffing</strong></a>
              </div>
            </div>       
            <div class="testimonial">
              <div class="pic">
                <a href="enterprise-resource-planning-software-solution"><img src="images/solutions2.jpg" alt=""> <strong>Customized ERP</strong></a>
              </div>
            </div>                   
            <div class="testimonial">
              <div class="pic">
                <a href="it-infrastructure-management-services"> <img src="images/solutions3.jpg" alt=""><strong>Infrastructure</strong></a>
              </div>
            </div>                     
            <div class="testimonial">
              <div class="pic">
                <a href="it-consulting-services-solutions"><img src="images/solutions4.jpg" alt=""><strong>Consulting</strong></a>
              </div>
            </div>                     
            <div class="testimonial">
              <div class="pic">
                <a href="javascript:;" id="foo"><img src="images/solutions5.jpg" alt="">
                  <strong>Engineering</strong></a>
                </div>
              </div>                                          
            </div>            
          </div>
        </div>
      </div>
    </section>
    <!-- Call to Action -->
    <section id="counter" class="inductry-rating">
      <div class="container wow fadeIn">
        <div class="row">
          <div class="col-md-12 mx-auto">
            <h4 class="wow fadeInUpBig">Inspiration Strikes Anywhere & We Make That A Reality</h4>
            <p>Our capability of transforming your ideas into a reality with intellectually has led us a huge success.</p>
          </div>
        </div>
      </div>     
      <hr class="stat-line">
      <div class="container wow fadeIn">
        <div class="row">
          <div class="col-md-12 mx-auto">

           <div class="container stat-count">
            <div class="row"> 

              <div class="col-md-2 col-sm-6 col-xs-12 number">
                <!--<i class="fourtek-heart"></i>-->
                <span class="counter-value" data-count="10">0</span> <span class="counter">+</span>
                <p>YEARS IN BUSINESS</p>
              </div>
              <div class="col-md-2 col-sm-6 col-xs-12 number">
                <!--<i class="fourtek-coffe"></i>-->
                <span class="counter-value" data-count="100">0</span> <span class="counter">+</span>
                <p>IT PROFESSIONALS</p>
              </div>
              <div class="col-md-4 col-sm-6 col-xs-12 number iso-cer">
                <!--<i class="fourtek-star"></i>-->
                <span class="counter-value-iso wow fadeInDown">ISO 9001 CERTIFIED</span>
                <p class="wow fadeInUp">NASSCOM & STPI ACCREDITATION</p>
              </div>
              <div class="col-md-2 col-sm-6 col-xs-12 number">
                <!--<i class="fourtek-cup"></i>-->
                <span class="counter-value" data-count="11000">0</span> <span class="counter">+</span>
                <p>CLIENTS WORLDWIDE</p>
              </div>
              <div class="col-md-2 col-sm-6 col-xs-12 last">
               <!-- <i class="fourtek-cup"></i>-->
               <span class="counter-value" data-count="15000">0</span> <span class="counter">+</span>
               <p>PROJECTS EXECUTED</p>
             </div>
           </div>
         </div>

       </div>
     </div>
   </div>
 </section>
 <!--our-product-->

 <section id="ourproduct" class="padd80">
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <div id="testimonial-products" class="owl-carousel">
          <div class="testimonial">
            <img src="images/ourproducts_1.jpg" alt="">
            <div class="row">
              <div class="col-md-4">Human Resource Management</div>
              <div class="col-md-8">
                <p>At present, in this cut-throat market, each and every kind of business has to face the issues in bringing down the operating costs while delivering more value... <a href="human-resources-management-software-solutions">learn more</a></p>
              </div>
            </div>
          </div>
          <div class="testimonial"><img src="images/ourproducts_2.jpg" alt="">
            <div class="row">
              <div class="col-md-4">Customer Relationship Management</div>
              <div class="col-md-8">
                <p>Enhance your visibility into cross-selling opportunities with our best-tailored solutions, matching with your business requirements... <a href="crm-software-services-india">learn more</a></p>
              </div>
            </div>
          </div>
          <div class="testimonial"><img src="images/ourproducts_3.jpg" alt="">
            <div class="row">
              <div class="col-md-4">Enterprise Resource Planning</div>
              <div class="col-md-8">
                <p>Aligning your people and processes with the new technologies by offering our Smart ERP Solutions that will not only help you enhance your performance...<a href="enterprise-resource-planning-software-solution">learn more</a></p>
              </div>
            </div>
          </div>
          <div class="testimonial"><img src="images/ourproducts_4.jpg" alt="">
            <div class="row">
              <div class="col-md-4">Devnagri</div>
              <div class="col-md-8">
                <p>At Fourtek, we apprehend that today technology has imbibed in every phase of our life. It is transforming multiple facets of our lives and industries as well... <a href="devnagri-online-translation-platform">learn more</a></p>
              </div>
            </div>
          </div>
          <div class="testimonial"><img src="images/ourproducts_5.jpg" alt="">
            <div class="row">
              <div class="col-md-4">Mobile banking</div>
              <div class="col-md-8">
                <p>The banking app development has led the world of banking and finance towards a entire revolutionary phase where ...<a href="mobile-banking-app-development">learn more</a></p>
              </div>
            </div>
          </div>
        </div> 
      </div>
      <div class="col-md-4 title text-right mt-80">
        <h2><span>Our</span> Products</h2>
        <span class="text-line"></span><br/><br/>
        <p class="mb-0 text111">Being a reliable software solution company in India, prolificacy is not just the part of our services. Each of our product is carved out with a blend of innovation, brilliance, and creativity. Coupled with agile methodologies and cutting-edge technology, our products bring out the most satisfactory results for our client's business. </p>        
      </div>        
    </div>
  </div>
</section>
<section id="case-study" class="case-study">
  <div class="container">
    <div class="title text-center">
      <h2><span>Case</span> Studies</h2>
      <span class="text-line"></span>
    </div>
    <div class="row">
      <?php $db = new Database();
      $result = $db->selectdata("case_study", "WHERE 1");
      
      if (!empty($result)) {
        while ($row = $result->fetch_assoc()) {
          ?>                  
          <div class="col-lg-4 col-md-12">
            <div class="post">                           
              <div class="img-post">
                <?php $caseId = $row['id'];
                if($caseId == 1){
                  $caseurl = 'prestige-case-study-details-fourtek';
                }elseif($caseId == 2){
                  $caseurl = 'greyorange-case-study-details-fourtek';
                }elseif($caseId == 3){
                  $caseurl = 'cerc-case-study-details-fourtek';
                }?>
                <a href="<?php echo $caseurl?>" target="_blank"><img src="admin/uploads/<?php echo $row['bimage']?>" class="img-fluid" alt="img-post" /></a>
              </div>
              <div class="infos-post">
                <h3><a href="javascript:;"><?php echo $row['title']?></a></h3>                                
                <?php $content = $row['about_client'];
                $limit = 155;
                if (strlen($content)<=$limit) {
                  echo $content;
                } else {
                  $text = substr($content, 0, $limit) .'...';
                  echo $text;
                } 
                
                ?>
                <br>

                <a href="<?php echo $caseurl;?>" class="btn learn-btn">Learn More</a>
              </div>
            </div>
          </div>
          <?php
        }
      }?>                    
    </div>
  </div>

</section>
<section class="clientstestimonial">
 <div class="container">

  <div class="title text-center">
    <h2><span>Our </span> Testimonial</h2>
    <span class="text-line"></span>
  </div>      

  <div class="row">
   <div class="col-md-12">
     <div id="clienttestimonial" class="owl-carousel">
          <div class="testimonial">
            <p> "Fourtek is a very nice company, I had a great experience with them for my product.<br>
I love the way they handle a client."
<br><span>- Umesh Chandra -</span></p>
           
          </div>
           <div class="testimonial">
          <p> "I am really thankful to you for the excellent job you have done for our Digital Marketing Platform.<br> I am very happy with the results of SEO, AdWords and Social Media. Really Appreciate your Hard Work."
<br><span>- Paras Vohra -</span></p>
           
          </div>

  <div class="testimonial">
          <p> "Best SEO and Social Media service provider. Good Knowledge in digital marketing platform.<br>Thank you Fourtek"
<br><span>- Shailendra Pandey -</span></p>
           
          </div>

 
 

<div class="testimonial">
          <p> "The digital marketing team is quite amazing.<br> Had a wonderful experience with Fourtek"
<br><span>- Rahul Sharma -</span></p>
           
          </div>

<div class="testimonial">
          <p> "Very professional yet personal touch to your queries and requirements.<br> Highly recommendable Web Design & Development Company."
<br><span>- Swati Saini -</span></p>
           
          </div>



        </div>         
</div>
</div>
</div>      
</section>



<section class="ourclients">
 <div class="container">

  <div class="title text-center">
    <h2><span>Our </span> Clients</h2>
    <span class="text-line"></span>
  </div>      

  <div class="row">
   <div class="col-md-12">
    <div id="testimonial-slider" class="owl-carousel">
     <div class="testimonial">
      <div class="pic">
       <img src="images/client-logo/irctc-logo.jpg" alt="">
     </div>
   </div>

   <div class="testimonial">
    <div class="pic">
     <img src="images/client-logo/cerc-logo.jpg" alt="">
   </div>
 </div>

 <div class="testimonial">
  <div class="pic">
   <img src="images/client-logo/prestige-logo.jpg" alt="">
 </div>
</div>                     
<div class="testimonial">
  <div class="pic">
   <img src="images/client-logo/grey-orange-logo.jpg" alt="">
 </div>
</div>                     
<div class="testimonial">
  <div class="pic">
   <img src="images/client-logo/chantwork-logo.jpg" alt="">
 </div>
</div>                                       
<div class="testimonial">
  <div class="pic">
   <img src="images/client-logo/skipper-logo.jpg" alt="">
 </div>
</div>                     

<div class="testimonial">
  <div class="pic">
   <img src="images/client-logo/ease-my-match-logo.jpg" alt="">
 </div>
</div>    


<div class="testimonial">
  <div class="pic">
    <img src="images/client-logo/irctc-logo.jpg" alt="">
  </div>
</div>

<div class="testimonial">
  <div class="pic">
    <img src="images/client-logo/cerc-logo.jpg" alt="">
  </div>
</div>                   
<div class="testimonial">
  <div class="pic">
    <img src="images/client-logo/prestige-logo.jpg" alt="">
  </div>
</div>                     
<div class="testimonial">
  <div class="pic">
    <img src="images/client-logo/grey-orange-logo.jpg" alt="">
  </div>
</div>                     
<div class="testimonial">
  <div class="pic">
    <img src="images/client-logo/chantwork-logo.jpg" alt="">
  </div>
</div>                                       
<div class="testimonial">
  <div class="pic">
    <img src="images/client-logo/skipper-logo.jpg" alt="">
  </div>
</div> 
<div class="testimonial">
  <div class="pic">
    <img src="images/client-logo/ease-my-match-logo.jpg" alt="">
  </div>
</div>
</div>          
</div>
</div>
</div>      
</section>


<div class="clearfix"></div>
<?php include 'include/footer.php' ;?>
<!-- <script type="text/javascript">
$('.Single').each(function () {
  var $this = $(this);
  jQuery({ Counter: 0 }).animate({ Counter: $this.text() }, {
    duration: 5000,
    easing: 'swing',
    step: function () {
      $this.text(Math.ceil(this.Counter));
    }
  });
});
</script> -->
<script>
  var a = 0;
  $(window).scroll(function() {

    var oTop = $('#counter').offset().top - window.innerHeight;
    if (a == 0 && $(window).scrollTop() > oTop) {
      $('.counter-value').each(function() {
        var $this = $(this),
        countTo = $this.attr('data-count');
        $({
          countNum: $this.text()
        }).animate({
          countNum: countTo
        },

        {

          duration: 2000,
          easing: 'swing',
          step: function() {
            $this.text(Math.floor(this.countNum));
          },
          complete: function() {
            $this.text(this.countNum);
            //alert('finished');
          }

        });
      });
      a = 1;
    }

  });
</script>
</body>
</html> 
