<?php
/**
 * The template for displaying comments
 *
 * The area of the page that contains both current comments
 * and the comment form.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if (post_password_required()) {
    return;
}
?>
<style>.vr_darkgray_wrapper{background-color: #f5f5f5;}</style>
<div class="vr_darkgray_wrapper vr_bottompadder50">
		<div class="container">
			<div class="row">
				<?php if (have_comments()) : ?>
		<?php twentyfifteen_comment_nav(); ?>

		<ol class="comment-list">
			<?php
                wp_list_comments(array(
                    'style'       => 'ol',
                    'short_ping'  => true,
                    'avatar_size' => 56,
                ));
            ?>
		</ol><!-- .comment-list -->

		<?php twentyfifteen_comment_nav(); ?>

	<?php endif; // have_comments()?>

	<?php
        // If comments are closed and there are comments, let's leave a little note, shall we?
        if (! comments_open() && get_comments_number() && post_type_supports(get_post_type(), 'comments')) :
    ?>
		<p class="no-comments"><?php _e('Comments are closed.', 'twentyfifteen'); ?></p>
	<?php endif; ?>
				<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
					  <?php comment_form(); ?>
				</div>
			</div>
		</div>
	</div>

	

	










<!-- .comments-area -->
<!--------<div class="vr_darkgray_wrapper vr_toppadder100 vr_bottompadder100">
		
		
		
		
		<div class="container">
			<div class="row">
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
					<div class="vr_testimonail_img">
						<div class="image_wrapper">
							<div class="image">		
								<img src="<?php echo get_template_directory_uri();?>/images/1.jpg" alt="" />
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
					  <form action="#" method="post"padding-top: 100px;>
					 <div class=" col-sm-12"> <h5>Leave a Reply</h5>
					<hr></div>
                        <div class="form-group col-sm-6"> 
						    <label>Name* </label>
							<input type="text" name="uname" class="form-control" placeholder="Name"  required  />  
						</div>    
						
						
						<div class="form-group col-sm-6"> 
						    <label>Email Id*</label>
							<input type="text" name="email" class="form-control" placeholder="Email Id"  id="email"   required />  
						</div>   
						 
						
						   
						<div class="form-group col-sm-12"> 
						    <label>Website</label>
							<input type="text" name="website" class="form-control" placeholder="Website" />  
						</div>   
						
						
						<div class="form-group col-sm-12"> 
						    <label>Comment</label>
							  <textarea  class="form-control" style="height: 165px;">
							  </textarea>
						</div> 
						<div class="col-sm-12">
						  <input type="button" name="btn" class="btn blog-btn" value="Post Comment" style="background-color: #102229; color: #fff;" />
						</div>
                  	 </form>
				</div>
			</div>
		</div>
	</div>----->
	
	