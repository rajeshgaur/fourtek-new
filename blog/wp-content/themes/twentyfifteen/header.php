<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width">
	 <meta charset="UTF-8">
  <meta name="description"  content="Fourtek Tech Blogs are based on rising technology, digital world & latest tech trends. Increase your knowledge with Fourtek Tech Blogs."/>
    <meta name="keywords" content="top technology blogs, best technology blogs, technology blogs, best tech blogs, top tech blogs, latest technology blogs">
  <link rel="canonical" href="http://www.fourtek.com/blog/">
    <meta name="author"  content=""/>
    <meta name="MobileOptimized" content="320">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
<?php //$catID = the_category_ID();?>
<?php if (is_home()) {
    ?>
<title>Top Tech Blogs - Fourtek Blog</title> 
<?php
} ?>
<?php if (is_archive()) {
        ?> <title><?php  echo single_cat_title() ?></title><?php
    } else {
    ?> <title> <?php  echo get_the_title(); ?></title> <?php
} ?>

<h1 style="display:none;">best technology blogs</h1>
<h2 style="display:none;">top technology blogs</h2>

   <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri();?>/css/main.css" />
		 <link href='<?php echo get_template_directory_uri();?>/stylesheets/jquery.lighter.css' rel='stylesheet' type='text/css'>
      <link href='<?php echo get_template_directory_uri();?>/stylesheets/sample.css' rel='stylesheet' type='text/css'> 
       <script src='https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.js' type='text/javascript'></script>
      <script src='<?php echo get_template_directory_uri();?>/javascripts/jquery.lighter.js' type='text/javascript'></script> 
    <link rel="shortcut icon" type="image/icon" href="<?php echo get_template_directory_uri();?>/images/fav.ico" />
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

	
    <link href="<?php echo get_template_directory_uri();?>/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Pattaya|Poppins:300,300i,400" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri();?>/css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo get_template_directory_uri();?>/css/owl.carousel.css" rel="stylesheet" type="text/css">
    <link href="<?php echo get_template_directory_uri();?>/css/animate.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo get_template_directory_uri();?>/css/style.css" rel="stylesheet" type="text/css">
    <link href="<?php echo get_template_directory_uri();?>/css/slider.css" rel="stylesheet" type="text/css">
    <link href="<?php echo get_template_directory_uri();?>/css/custom-bootstrap-margin-padding.css" rel="stylesheet" type="text/css">
    <link href="<?php echo get_template_directory_uri();?>/css/responsive.css" rel="stylesheet" type="text/css">

	<?php wp_head();
$base_url = "https://www.fourtek.com/";
  ?>
	 <!-- Start of HubSpot Embed Code -->
    <script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/5797952.js"></script>
    <!-- End of HubSpot Embed Code -->
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-107580501-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-107580501-1');
</script>
</head>
<body <?php body_class(); ?> id="page-top">

<div class="navbar fixed-top navbar-expand-lg navbar-light" id="mainNav"> 
  <div class="container-fluid"> 
  <div class="col-lg-3 col-md-6 col-sm-6"><a href="<?php echo $base_url; ?>" class="fourtek-logo"> <img src="<?php echo get_template_directory_uri();?>/images/fourtek-logo.png" alt="fourtek logo" /> </a> </div>  
  
  <div class="col-lg-9 col-md-4 col-sm-4" id='cssmenu'>
    <ul>
         <li><a href="<?php echo $base_url;?>about-us">About Us</a>

<ul class="tab">
        <li><a href="<?php echo $base_url;?>culture">Life@fourtek</a></li></ul>
         </li>
  <li ><a href='javascript:void(0)'>Services</a>
        <ul class="tab">
         <li><a href="<?php echo $base_url;?>website-design-development-service-company-in-delhi"> Website Design & Development</a></li>
      <!-- <li><a href="web-developement.php">Web Development</a></li> -->
      <li><a href="<?php echo $base_url;?>mobile-apps-development-companies-in-india"> Mobile App Development</a></li>
      <li><a href="<?php echo $base_url;?>mobile-game-development-company-india"> Game Development</a></li>
      <li><a href="<?php echo $base_url;?>digital-multilingual-marketing-company-noida-india"> Digital Multilingual Marketing</a></li>
      <li><a href="<?php echo $base_url;?>mobile-banking-app-development-in-india"> Mobile Banking</a></li>
      <li><a href="<?php echo $base_url;?>android-app-development-company-in-india"> Android App Development</a></li>
      <li><a href="<?php echo $base_url;?>hybrid-mobile-application-development-in-noida-delhi">  Hybrid App Development</a></li>
        <li><a href="<?php echo $base_url;?>it-consulting-services-solutions">Consultancy</a></li>
           <li><a href="<?php echo $base_url;?>it-infrastructure-management-services">Infrastructure</a></li>
           <li><a href="<?php echo $base_url;?><?php echo $base_url;?>it-infrastructure-staffing-recruiting-services">Staffing</a></li>   
           <li class="d-xl-none" style="display:block !important;"><a href="<?php echo $base_url;?>mobile-banking-app-development-in-india"> Mobile Banking</a></li>
        </ul>         
      </li>


<li ><a href='javascript:void(0)'>Digital Marketing</a>
        <ul class="tab">
        <li><a href="<?php echo $base_url;?>top-seo-service-in-noida-delhi"> Search Engine Optimization (SEO)</a></li>
      <li><a href="<?php echo $base_url;?>smo-services-company-in-noida"> Social Media Optimization (SMO)</a></li>
      <li><a href="<?php echo $base_url;?>ppc-service-company-in-noida"> PPC Advertising</a></li>
      <li><a href="<?php echo $base_url;?>online-reputation-management-services-company-delhi"> Online Reputation Management Service</a></li>
        </ul>         
      </li>

<li ><a href='javascript:void(0)'>Products</a>
        <ul class="tab">
       <li><a href="<?php echo $base_url;?>human-resources-management-software-solutions"> HRM Solutions</a></li>
        <li><a href="<?php echo $base_url;?>crm-software-services-india"> Customer Relationship Management</a></li>
        <li><a href="<?php echo $base_url;?>enterprise-resource-planning-software-solution"> ERP Solutions</a></li>
        <!-- <li><a href="nvocc.php">Freight Forwarding / NVOCC</a></li> -->
        <li><a href="<?php echo $base_url;?>devnagri-online-translation-platform"> Devnagri</a></li>
        </ul>         
      </li>
       <li><a href="<?php echo $base_url;?>/blog">Blog</a></li>
 <li><a href="career">Career</a></li>
        
        <li><a href="<?php echo $base_url;?>contact-us">Contact</a></li>


      <li><a href="javascript:;" id="getblinking" class=" ">
<i class="effect-8 fa fa fa-envelope-o"></i> Get a Quote</a></li>
               
         
          
          
        </ul>
  </div>
  
  <div class="clearfix"></div>    
  </div>
  
<div id="page-content-wrapper"></div>

<!--sidebar-menu-->
<div class="mob-menu hidden-lg" id="wrapper">
  <button type="button" class="hamburger is-closed" data-toggle="offcanvas"> <span class="hamb-top"></span> <span class="hamb-middle"></span> <span class="hamb-bottom"></span> </button>
  <div class="overlay sidebar-overlap"></div>
  
  <!-- Sidebar -->
  <nav class="navbar-fixed-top" id="sidebar-wrapper" role="navigation">
    <ul class="nav sidebar-nav">
      <li><a href="<?php echo $base_url;?>about-us"><strong>About Us</strong></a></li>
      <li><a href="<?php echo $base_url;?>culture">Life@fourtek</a></li>
      <hr/>
	  
      <li class="isDisabled"><a href="javascript:;"><strong>Services</strong></a></li>
      <li><a href="<?php echo $base_url;?>website-design-development-service-company-in-delhi"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Website Design & Development</a></li>
      <!-- <li><a href="web-developement.php">Web Development</a></li> -->
      <li><a href="<?php echo $base_url;?>mobile-apps-development-companies-in-india"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Mobile App Development</a></li>
      <li><a href="<?php echo $base_url;?>mobile-game-development-company-india"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Game Development</a></li>
      <li><a href="<?php echo $base_url;?>digital-multilingual-marketing-company-noida-india"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Digital Multilingual Marketing</a></li>
      <li class="d-xl-none"><a href="<?php echo $base_url;?>it-consulting-services-solutions"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Consultancy</a></li>
      <li class="d-xl-none"><a href="<?php echo $base_url;?>it-infrastructure-management-services"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Infrastructure</a></li>
      <li class="d-xl-none"><a href="<?php echo $base_url;?>it-infrastructure-staffing-recruiting-services"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Staffing</a></li> 
	  
	  
    <!-- <li class="isDisabled"><a href="javascript:;"><strong>Marketing</strong></a></li> -->
    <li><a href="<?php echo $base_url;?>digital-marketing-company-in-noida-delhi"><strong> Digital Marketing</strong></a></li>		 
		 <li><a href="<?php echo $base_url;?>top-seo-service-in-noida-delhi"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Search Engine Optimization (SEO)</a></li>
		 <li><a href="<?php echo $base_url;?>smo-services-company-in-noida"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Social Media Optimization (SMO)</a></li>
		 <li><a href="<?php echo $base_url;?>ppc-service-company-in-noida"><i class="fa fa-angle-double-right" aria-hidden="true"></i> PPC Advertising</a></li>
		 <li><a href="<?php echo $base_url;?>online-reputation-management-services-company-delhi"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Online Reputation Management Service</a></li>
	  
	  
      <hr/>
      <li class="isDisabled"><a href="#"><strong>Products</strong></a>
      <li><a href="<?php echo $base_url;?>human-resources-management-software-solutions"><i class="fa fa-angle-double-right" aria-hidden="true"></i> HRM Solutions</a></li>
      <li><a href="<?php echo $base_url;?>crm-software-services-india"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Customer Relationship Management</a></li>
      <li><a href="<?php echo $base_url;?>enterprise-resource-planning-software-solution"><i class="fa fa-angle-double-right" aria-hidden="true"></i> ERP Solutions</a></li>
      <!-- <li><a href="nvocc.php">Freight Forwarding / NVOCC</a></li> -->
      <li><a href="<?php echo $base_url;?>devnagri-online-translation-platform"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Devnagri</a></li>
      
      <hr/>
      
      <li><a href="<?php echo $base_url;?>our-clients"><strong>Our Clients</strong></a></li>
      
      
       
      <li><a href="<?php echo $base_url;?>career"><strong>Career</strong></a></li>
      <li><a href="<?php echo $base_url;?>contact-us"><strong>Contact 1</strong></a></li>
      <li><a href="javascript:;" class="get blinking">
<span><i class="effect-8 fa fa fa-envelope-o"></i> </span>
<span id="getblinking"><strong>Get a Quote</strong></span></a></li>
    </ul>
  </nav>
  <!-- /#sidebar-wrapper --> 
</div>
<!-- /#wrapper --> 

  
</div>
<!--free-consulation-form-->

<!-- <div class="free-consult element element-1">
 <img src="<?php echo get_template_directory_uri();?>/images/aa.png"> <a href="<?php echo $base_url;?>free-consultation"> Free Consultation</a>
</div> -->
<div class="sticky1 footer-stiky" > 
  <div class="container">
    <div class="row">
      <div class="col-md-3">
        <i class="fa fa-phone effect-8"></i> +91-9958104612
      </div>
      <div class="col-md-3">
        <a href="skype:arpit@fourtek.com?chat">
         <span><i class="fa fa-skype"></i></span>
         <span>arpit@fourtek.com</span></a>
       </div>
       <div class="col-md-3">
        <a href="mailto:info@fourtek.com">
         <span><i class=" effect-8 fa fa fa-envelope-o"></i> </span>
         <span>info@fourtek.com</span></a>
       </div>
     </div>
   </div>
 </div>


