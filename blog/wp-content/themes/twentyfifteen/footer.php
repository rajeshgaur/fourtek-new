<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
$base_url = "https://www.fourtek.com/";
?>

  <!--Start of Zopim Live Chat Script-->
<!---<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="//v2.zopim.com/?3fIRwv7NugdMCUTckwdnZvKoC9muhOeI";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>-->
<!--End of Zopim Live Chat Script-->
<!--End of Zopim Live Chat Script-->
    <!-- Footer --> 
   
    
	<footer class="footer">
    <div id="feedback">
    <div id="feedback-form" style='display:none;' class="">
      <h2>Leave a Message</h2>
      <?php //echo do_shortcode('[contact-form-7 id="601" title="Contact form 1"]');?>
      <form class="form" role="form"  action="" method="POST">
      <div id="ResponseDiv"></div>
  <div class="form-group">
      <input type="text" class="form-control1 " name="name"  required="" placeholder="Your Name *" value="<?php //echo $_POST['name']?>" >
  </div>
  <div class="form-group">
      <input type="email" class="form-control1 " name="email" required="" placeholder="Your Email *" value="<?php //echo $_POST['email']?>" >
  </div>
  <div class="form-group">
      <input type="phone" class="form-control1 " name="mobile" required="" placeholder="Your Phone Number *" minlength="10" maxlength="15" pattern="^[0-9]*$"  value="<?php //echo $_POST['mobile']?>">
  </div>


  <div class="form-group">
  <select class="form-control1"  name="consultation" required>
    <option value="">Please Select Consultation *</option>
  <option value="Consultation Interest" <?php if($_POST['consultation'] == 'Consultation Interest'){ echo 'selected="selected"';}?>>Consultation Interest</option>
  <option value="Website Designing" <?php if($_POST['consultation'] == 'Website Designing'){ echo 'selected="selected"';}?>>Website Designing</option>
  <option value="Website Development" <?php if($_POST['consultation'] == 'Website Development'){ echo 'selected="selected"';}?>>Website Development</option>
  <option value="Application Development" <?php if($_POST['consultation'] == 'Application Development'){ echo 'selected="selected"';}?>>Application Development </option>
  <option value="Game Development" <?php if($_POST['consultation'] == 'Game Development'){ echo 'selected="selected"';}?>>Game Development</option>
  <option value="Digital Marketing" <?php if($_POST['consultation'] == 'Digital Marketing'){ echo 'selected="selected"';}?>>Digital Marketing</option>
  <option value="SEO" <?php if($_POST['consultation'] == 'SEO'){ echo 'selected="selected"';}?>>SEO</option>
  <option value="SEM" <?php if($_POST['consultation'] == 'SEM'){ echo 'selected="selected"';}?>>SEM</option>
  <option value="Content Marketing" <?php if($_POST['consultation'] == 'Content Marketing'){ echo 'selected="selected"';}?>>Content Marketing</option>
  <option value="Email Marketing" <?php if($_POST['consultation'] == 'Email Marketing'){ echo 'selected="selected"';}?>>Email Marketing</option>
  <option value="Social Media Management" <?php if($_POST['consultation'] == 'Social Media Management'){ echo 'selected="selected"';}?>>Social Media Management</option>
  </select>
  </div>
  <div class="form-group">
      <input type="text" class="form-control1 " name="company" required="" placeholder="Your Company name *" value="<?php //echo $_POST['company']?>">
  </div>


  <div class="form-group">
      <textarea class="form-control1 " name="comment" rows="2" placeholder="Additional Information / Comments"><?php //echo $_POST['comment']?></textarea>
  </div>
 
  <div class="row">
  <div class="col-md-12">
    <div class="form-group">
  <!-- <div class="g-recaptcha" data-sitekey="6LcfIYMUAAAAAGI1bmHjGaNkvJo4xnzzijcTarIe"></div> -->
</div>
  </div>
  <div class="col-md-12 send-messages">
  <div class="form-group">
            
            <button type="button" id="lets" name="submit" class="btn-contact">Send Message</button>

        </div>
  </div>

<div class="clearfix"></div>

</div>
      </form>
    </div>
    <div id="feedback-tab"><i class=" fa fa fa-envelope-o"></i> Let's Connect</div>
  </div>
          <div class="container">  
              
             <div class="newsletter">
                 <p>Call now at 0120-4284526, 0120-4151299 and Get Started</p>
             </div> 
          </div> 
       
       <hr class="footer-hr">
       <div class="container">
            <div class="footer-content row">
            
                <div class="col-md-3 col-sm-12"> 
                  <strong>FOURTEK</strong>
                  <ul>
                      <li><a href="<?php echo $base_url;?>about-us">About Us</a></li>
                      <li><a href="<?php echo $base_url;?>blog">Blog</a></li>
                      <li><a href="<?php echo $base_url;?>career">Careers</a></li>
                      <!-- <li><a href="#">Show Case</a></li> -->
                      <li><a href="<?php echo $base_url;?>terms-conditions">Terms & Conditions</a></li>
                      <li><a href="<?php echo $base_url;?>privacy-policy">Privacy Policy</a></li>
					  <li><a href="<?php echo $base_url;?>our-clients">Our Clients</a></li>
                  </ul>
                </div>

                <div class="col-md-3 col-sm-12"> 
                  <strong>MAJOR SERVICES</strong>
                  <ul>
                      <li><a href="<?php echo $base_url;?>custom-website-development-services-india">Custom Website Services</a></li>
                      <li><a href="<?php echo $base_url;?>php-website-development-company-india">PHP Website Development</a></li>
                      <li><a href="<?php echo $base_url;?>website-redesigning-service-company-in-noida">Website Re-Design</a></li>
                      <li><a href="<?php echo $base_url;?>custom-magento-development-services-india">Magento Development</a></li>
                      <li><a href="<?php echo $base_url;?>custom-ecommerce-website-design-development">Custom E-commerce Development</a></li>
                      <li><a href="<?php echo $base_url;?>ios-app-development-services-in-india">iOS App Development</a></li>
                  </ul>
                </div>

                <div class="col-md-3 col-sm-12"> 
                  <strong>COMPANY</strong>
                  <ul>
                      <li><a href="<?php echo $base_url;?>website-design-development-service-company-in-delhi">Web Design & Development</a></li>
                      <li><a href="<?php echo $base_url;?>ecommerce-website-development-company-in-india">E-commerce Development</a></li>
                      <li><a href="<?php echo $base_url;?>mobile-apps-development-companies-in-india">Mobile App Development</a></li>
                      <li><a href="<?php echo $base_url;?>digital-marketing-company-in-noida-delhi">Online Marketing</a></li>
                  </ul>
                </div>

                <div class="col-md-3 col-sm-12 footer-address"> 
                  <strong>CONTACT</strong>
                  <ul>
                      <li>Fourtek IT Solutions Pvt. Ltd. <br/> B86, Sector 60, Noida ( Uttar Pradesh) 201301 India</li>
                      <li>0120-4284526, 0120-4151299</li>
                      <li><a href="mailto:info@fourtek.com">info@fourtek.com</a></li>
                  </ul>
                </div>
             </div>
           </div>

                
            <div class="copyright">
              <div class="container-fluid">
                <div class="row">
                  <div class="col-md-6 col-sm-12 copy">  
                     <p>Copyright © 2020 <span>Fourtek It Solutions</span> all right reserved</p>
                  </div>

                 <div class="col-md-6 col-sm-12 text-right footer-social">

                 <div class="social-media-footer">
                   <a href="https://www.facebook.com/fourtek-235547968001/" target="_blank"><i class="fa fa-facebook"></i></a>
                   <!-- <a href="https://plus.google.com/u/0/105141360718909317231" target="_blank"><i class="fa fa-google-plus"></i></a> -->
                   <a href="https://twitter.com/Fourtek" target="_blank"><i class="fa fa-twitter"></i></a>
                   <a href="https://www.instagram.com/fourtek_india/" target="_blank"><i class="fa fa-instagram"></i></a>
                   <a href="https://www.linkedin.com/company/fourtek?trk=biz-companies-cym" target="_blank"><i class="fa fa-linkedin"></i></a>
                  </div>


               </div>
              </div>
             </div>
           </div>

          <div class="footer-short">
             <ul>
               <li><a href="tel:01204284526"><i class="fa fa-phone"></i></a></li>
               <li><a href="index.php"><i class="fa fa-home"></i></a></li>
               <li><a href="mailto:info@fourtek.com"><i class="fa fa-envelope"></i></li>
             </ul>
          </div>

        </footer>
       

    
    <script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.easing.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/owl.carousel.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/vitality.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/slider.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/wow.js"></script>


<?php wp_footer(); ?>
<script>
      $(function () {

        $('#lets').on('click', function (e) {
          

          e.preventDefault();
          $.ajax({
            type: 'post',
            dataType: "text",
            url: '<?php echo get_template_directory_uri(); ?>/ajax-form.php',
            data: $('form').serialize(),
            success: function (response) {
              document.getElementById("ResponseDiv").innerHTML= response; 
              
            }
          });

        });

      });
    </script>

<script>
  $(function() {
  $("#feedback-tab").click(function() {
    $("#feedback-form").toggle("fade");
  });
  $("#getblinking").click(function() {
    $("#feedback-form").toggle("fade");
  });

  /*$("#feedback-form form").on('submit', function(event) {
    var $form = $(this);
    $.ajax({
      type: $form.attr('method'),
      url: $form.attr('action'),
      data: $form.serialize(),
      success: function() {
        $("#feedback-form").toggle("fade").find("textarea").val('');
      }
    });
    event.preventDefault();
  });*/
});

</script>

	<script type="text/javascript">
 
$(window).scroll(function(){
  var sticky = $('.sticky1'),
      scroll = $(window).scrollTop();

  if (scroll >= 450) sticky.addClass('fixed11');
  else sticky.removeClass('fixed11');
});

</script>


<script type="text/javascript">
$(window).scroll(function(){
    if ($(window).scrollTop() >= 300) {
        $('.blog-menu-main').addClass('fixed-blog-menu');
    }
    else {
        $('.blog-menu-main').removeClass('fixed-blog-menu');
    }
});


$( document ).ready(function() {
$('a[href*="#"]')
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
      && 
      location.hostname == this.hostname
    ) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top-75
        }, 1000, function() {
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) { 
            return false;
          }
        });
      }
    }
  });
});


    //custom js

  $(document).ready(function () {
    var trigger = $('.hamburger'),
      overlay = $('.overlay'),
     isClosed = false;

    trigger.click(function () {
      hamburger_cross();      
    });

    function hamburger_cross() {

      if (isClosed == true) {          
      overlay.hide();
      trigger.removeClass('is-open');
      trigger.addClass('is-closed');
      isClosed = false;
      } else {   
      overlay.show();
      trigger.removeClass('is-closed');
      trigger.addClass('is-open');
      isClosed = true;
      }
    }
    
    $('[data-toggle="offcanvas"]').click(function () {
      $('#wrapper').toggleClass('toggled');
    }); 
  });

   
  </script>
	</body>
</html>
