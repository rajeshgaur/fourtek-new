<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>


<style>
.test img{width:100% !important;height:350px !important;    border: 1px solid #848484;    text-align: center;}
.vr_article_page, .vr_single_project{background:none;}
.vr_articledata p {line-height: 25px; margin-left: 0; width: 100%; }
body , p{color: #000;}
.vr_blog_data h4, .vr_blog_data h4 a{color: #000;}
.vr_blog_data p{ color: #333;}
.vr_blog_data span a {color: #000;}
.vr_blog_data span:after{ background-color: #150f0f;}
.widget { background-color: #f5f5f5;}
.widget ul li{ padding: 10px 10px; position: relative;padding-right: 0px; border-bottom: 1px solid #ddd;}
.widget ul li a {color: #707171;font-size: 14px;font-weight: bold;}
.widget ul li:before{background:none;}
.vr_darkgray_wrapper{background-color: #f5f5f5;}
.vr_blog_data ol li { color: #333; font-weight: 500; line-height: 27px;}
.vr_blog_data ol li strong{color:#333}
.vr_readmore, .vr_readmore:hover {color: #d81414;}
.widget .widget-title {background-color: #00BCD4;font-size: 20px; padding: 10px 10px;  border: none;}
.widget .widget-title:after{background:none;}
.nav-links {margin-left: 10px;margin-top: 2px;}
.screen-reader-text {display: none;}

</style>


<div style="clear:both;"></div>
	<div class="vr_portfolio vr_article_page vr_blog">
		<div class="container">
			<div class="row">
				<div class="vr_portfolio vr_blog_sidebar">

					<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 vr_toppadder100">
						
						<?php
                        global $query_string;

                        query_posts($query_string . '&posts_per_page=5');
                        if (have_posts()) : ?>
						<?php while (have_posts()) : the_post(); ?> 
						<div class="vr_blogcontent">
							<div class="vr_blog_img">
								<div class="vr_blog_img_inner ff">
									<?php //the_post_thumbnail(); ?>
									<img src="<?php the_post_thumbnail_url('full'); ?>" class="img-responsive" alt="" style="width:100%; height:380px;">
									<div class="vr_blog_overlay">
										<div class="vr_blog_overlay_content">
											<a href="<?php the_permalink() ?>">Read Article</a>
										</div>
									</div>
								</div>
							</div>
						    <div class="vr_blog_data">
								<!--<h4><a href="<?php the_permalink() ?>"><?php the_archive_title();?></a></h4>--->
<h4><a href="<?php the_permalink() ?>"><?php  echo get_the_title(); ?></a></h4>
<?php //echo do_shortcode("[wp_social_sharing social_options='facebook,twitter,googleplus,linkedin' twitter_username='Fourtek' facebook_text='Facebook' twitter_text='Twitter' googleplus_text='Google+' linkedin_text='Linkedin' icon_order='f,t,g,l,p,x,r' show_icons='0' before_button_text='' text_position='' social_image='']");?>
								<span><a href="#"><?php the_field('date'); ?></a></span>
								<p><?php $little_excerpt = substr($post->post_content, 0, 300); echo $little_excerpt; ?>
<?php //echo $post->post_content?></p>
								<a href="<?php the_permalink() ?>" class="vr_readmore">read more</a>
							</div>
						</div>
						
						
					         	<?php endwhile;?>
					         	<?php // Previous/next page navigation.
                                
                                // If no content, include the "No posts found" template.
                                else :
                                get_template_part('content', 'none');
                                endif; ?>


                             <div class="col-sm-12">
                             	<?php the_posts_pagination(array(
                                'prev_text'          => __('Previous', 'twentyfifteen'),
                                'next_text'          => __('Next', 'twentyfifteen'),
                                'before_page_number' => '<span class="meta-nav screen-reader-text">' . __('') . ' </span>',
                                )); ?>
                             </div>

					</div>
					
					<div class="col-lg-4 col-md-4 col-sm-4 co-xs-12 vr_toppadder100">
						<!-- <div class="widget widget-share">
							<ul>
								<li><label>share on : </label></li>
								<li><i class="fa fa-share-alt"></i></li>
								 <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
								<li><a href="https://twitter.com/Fourtek"><i class="fa fa-twitter"></i></a></li>
								<li><a href="https://www.linkedin.com/company/fourtek?trk=biz-companies-cym"><i class="fa fa-linkedin"></i></a></li>
								<li><a href="https://www.facebook.com/fourtek-235547968001/"><i class="fa fa-facebook"></i></a></li> 
								
							</ul>
						</div> -->
						<div class="">
							<?php dynamic_sidebar('Widget Area'); ?>
						
						</div>
						<!--<div class="widget widget_tag_cloud">
								<?php dynamic_sidebar('Tag'); ?>
							
						</div>-->
						<!--<div class="widget widget_portfolio">
							<h4 class="widget-title wow fadeIn">Latest Projects</h4>
							<ul>
							<?php $posts = get_posts('category=16&posts=4'); foreach ($posts as $post) {
                                    ?>
								<li><a href="#"><?php the_post_thumbnail(array( 100, 100)); ?></a></li>
								
							<?php
                                } ?>
							</ul>
						</div>-->
					</div>
					
					
					
					
				</div>
				
				
				
			</div>
		</div>


<?php get_footer(); ?>
