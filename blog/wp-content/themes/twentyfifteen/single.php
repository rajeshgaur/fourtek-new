<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
<style>
.test img{width:100% !important ;height:350px !important;    border: 1px solid #848484;    text-align: center;}
.vr_articledata p {line-height: 25px; margin-left: 0; width: 100%; }
body , p{color: #000;}
.vr_blog_data h4, .vr_blog_data h4 a{color: #000;}
.vr_articledata p { color: #333;}
.vr_articledata p img{width:100%;}
.vr_blog_data span a {color: #000;}
.vr_blog_data span:after{ background-color: #150f0f;}
.widget { background-color: #f5f5f5;}
.widget ul li{ padding: 10px 10px; position: relative;padding-right: 0px; border-bottom: 1px solid #ddd;}
.widget ul li a {color: #707171;font-size: 14px;font-weight: bold;}
.widget ul li:before{background:none;}
.vr_darkgray_wrapper{background-color: #f5f5f5;}
.widget .widget-title {background-color: #00BCD4;font-size: 20px; padding: 10px 10px;  border: none;}
.widget .widget-title:after{background:none;}
.vr_articledata ul {
    padding: 0 18px;
}
.vr_articledata ul li {
    list-style: circle;
    padding: 13px 0;
    color: #423c3c;
    font-weight: 600;
    text-align: justify;
}
.vr_articledata p strong {
    color: #00bcd4;
}
</style>
  <div style="clear:both;"></div>
	<!--<div class="vr_transprent_wrapper" style="background-color:#102229;" >
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<h1 style="color:#fff;text-align:center;    margin-bottom: 25px;">Blog</h1>
				</div>
			</div>
		</div>
	</div>-->

    <div class="vr_portfolio_wrapper vr_blog vr_article_page" style="padding-top: 29px;    background: none;">
		<div class="container">
			<div class="row">
				<div class="vr_portfolio vr_blog_single  vr_toppadder40">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="row">
				<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
						<div class="row">
						<div class="vr_articlediv vr_toppadder30 col-lg-12 col-md-12 test ">
						
							<!--<img src="<?php //echo get_template_directory_uri();?>/images/blog-slider.jpg" class="img-responsive pull-right img-responsive" alt="">--->
							<?php the_post_thumbnail(array('class'=>'img-responsive pull-right img-responsive')); ?>
							
						</div>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-right">
								<div class="vr_blog_data vr_articledata">
									<h4><?php the_title(); ?></h4>
									<span><a href="#"><?php the_field('date'); ?></a></span>
									<p class="vr_bottompadder20"><?php echo $post->post_content ?></p>
									</div>
							</div>
						</div>
				</div>
			<div class="col-lg-4 col-md-4 col-sm-4 co-xs-12 vr_toppadder40">
			<div class="widget widget-share">
							<ul>
								<li><label>share on : </label></li>
								<li><i class="fa fa-share-alt"></i></li>
								<?php //echo do_shortcode("[wp_social_sharing social_options='facebook,twitter,googleplus,linkedin' twitter_username='Fourtek' facebook_text='Facebook' twitter_text='Twitter' googleplus_text='Google+' linkedin_text='Linkedin' icon_order='f,t,g,l,p,x,r' show_icons='0' before_button_text='' text_position='' social_image='']");?>
							</ul>
			</div>	
			<div class="">
							<?php dynamic_sidebar('Widget Area'); ?>
							
						</div>	
			
			</div>	
		<?php
        // Start the loop.
        while (have_posts()) : the_post();

            /*
             * Include the post format-specific template for the content. If you want to
             * use this in a child theme, then include a file called called content-___.php
             * (where ___ is the post format) and that will be used instead.
             */
            //get_template_part( 'content', get_post_format() );

            // If comments are open or we have at least one comment, load up the comment template.
            if (comments_open() || get_comments_number()) :
                comments_template();
            endif;

            // Previous/next post navigation.
            

        // End the loop.
        endwhile;
        ?>

		</div>
			</div>
		</div>
	</div>
	</div>
	

<?php get_footer(); ?>
