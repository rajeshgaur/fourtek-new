

<div class="topbar hiddentop">
<div class="container-fluid">
    <div class="row">

      <div class="col-md-12">
        <a href="javascript:;"><span><i class="fa fa-phone effect-8"></i> <strong>+91-9958104612</strong></span></a>
        <a href="skype:arpit@fourtek.com?chat">
         <span><i class="fa fa-skype"></i></span>
         <span><strong>arpit@fourtek.com</strong></span></a>
          <a href="mailto:info@fourtek.com">
         <span><i class="effect-8 fa fa fa-envelope-o"></i> </span>
         <span><strong>info@fourtek.com</strong></span></a>
         <a href="#" class="get blinking">
         <span><i class="effect-8 fa fa fa-envelope-o"></i> </span>
         <span><strong>Get a Quote</strong></span></a>
      </div>
     
     </div>
   </div>

</div>

<div class="navbar  navbar-expand-lg navbar-light" id="mainNav"> 
  <div class="container-fluid"> 
  <div class="col-lg-8 col-md-6 col-sm-6"><a href="index.php" class="fourtek-logo"> <img src="images/fourtek-logo.png" alt="fourtek logo" /> </a> </div>  
  
  <div class="col-lg-4 col-md-4 col-sm-4" id='cssmenu'>
    <ul>
          <li class='active'><a href='javascript:void(0)'>Services</a>
            <ul>
              <li><a href="javascript:void(0)">Engineering</a>
                <ul>
                 <li><a href="website-design-service.php">Website Design & Development</a></li>
                  <li><a href="mobile-application.php">Mobile App Development</a></li>                    
                  <li><a href="digital-marketing.php">Digital Marketing</a></li>
                  <li><a href="game-development.php">Game Development</a></li> 
                  <li><a href="digital-multilingual-marketing.php"> Digital Multilingual Marketing</a></li> 
                  <li><a href="sales-force-development.php">Sales Force</a></li>          
                </ul>
              </li>
    
            <!-- <li><a href="customized-erp.php">Customized ERP</a></li> -->
               <li><a href="consulting-solutions.php">Consultancy</a></li>
               <li><a href="infrastructure.php">Infrastructure</a></li>
               <li><a href="staffing.php">Staffing</a></li>       
              
            </ul> 
          </li>  
		  
               
          <li><a href='javascript:void(0)'>Products</a>
            <ul>
              <li><a href="hrm-service.php">HRM Solutions</a></li>
             <li><a href="customer_relationship_management.php">Customer Relationship Management</a></li>
             <li><a href="enterprise-resource-planning-software-solution.php">ERP Solutions</a></li>
             <!-- <li><a href="nvocc.php">Freight Forwarding / NVOCC</a></li> -->
             <li><a href="devnagri.php">Devnagri</a></li>
            </ul>         
          </li>
          
          
        </ul>
  </div>
  
  <div class="clearfix"></div>    
  </div>
  
<div id="page-content-wrapper"></div>

<!--sidebar-menu-->
<div class="mob-menu" id="wrapper">
  <button type="button" class="hamburger is-closed" data-toggle="offcanvas"> <span class="hamb-top"></span> <span class="hamb-middle"></span> <span class="hamb-bottom"></span> </button>
  <div class="overlay sidebar-overlap"></div>
  
  <!-- Sidebar -->
  <nav class="navbar-fixed-top" id="sidebar-wrapper" role="navigation">
    <ul class="nav sidebar-nav">
      <li><a href="about-us.php"><strong>About Us</strong></a></li>
      <hr/>
	  
      <li class="isDisabled"><a href="javascript:;"><strong>Services</strong></a></li>
      <li><a href="website-design-service.php"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Website Design & Development</a></li>
      <!-- <li><a href="web-developement.php">Web Development</a></li> -->
      <li><a href="mobile-application.php"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Mobile App Development</a></li>
      <li><a href="game-development.php"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Game Development</a></li>
      <li><a href="digital-multilingual-marketing.php"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Digital Multilingual Marketing</a></li>
      <li class="d-xl-none"><a href="consulting-solutions.php"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Consultancy</a></li>
      <li class="d-xl-none"><a href="infrastructure.php"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Infrastructure</a></li>
      <li class="d-xl-none"><a href="staffing.php"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Staffing</a></li> 
	  
	  
    <li class="isDisabled"><a href="javascript:;"><strong>Marketing</strong></a></li>

		 <li><a href="digital-marketing.php"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Digital Marketing</a></li>
		 <li><a href="seo-service.php"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Search Engine Optimization (SEO)</a></li>
		 <li><a href="smo-service.php"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Social Media Optimization (SMO)</a></li>
		 <li><a href="PPC-Service.php"><i class="fa fa-angle-double-right" aria-hidden="true"></i> PPC Advertising</a></li>
		 <li><a href="online-Reputation-Management-Service.php"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Online Reputation Management Service</a></li>
	  
	  
      <hr/>
      <li class="isDisabled"><a href="#"><strong>Products</strong></a>
      <li><a href="hrm-service.php"><i class="fa fa-angle-double-right" aria-hidden="true"></i> HRM Solutions</a></li>
      <li><a href="customer_relationship_management.php"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Customer Relationship Management</a></li>
      <li><a href="enterprise-resource-planning-software-solution.php"><i class="fa fa-angle-double-right" aria-hidden="true"></i> ERP Solutions</a></li>
      <!-- <li><a href="nvocc.php">Freight Forwarding / NVOCC</a></li> -->
      <li><a href="devnagri.php"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Devnagri</a></li>
      
      <hr/>
      
      <li><a href="our-clients.php"><strong>Our Clients</strong></a></li>
      
      
       
      <li><a href="career.php"><strong>Career</strong></a></li>
      <li><a href="contact-us.php"><strong>Contact</strong></a></li>
    </ul>
  </nav>
  <!-- /#sidebar-wrapper --> 
</div>
<!-- /#wrapper --> 

  
</div>
<!--free-consulation-form-->

<div class="free-consult element element-1">
 <img src="images/aa.png"> <a href="free-consultation.php"> Free Consultation</a>
</div>
        








