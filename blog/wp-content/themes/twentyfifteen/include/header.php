


<div class="slidercontainer container-fluid">
  <div class="row">
    <div class="col">

      <!-- carousel code -->
      <div id="carouselExampleIndicators" class="carousel slide">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner skyblue">

          <!-- first slide -->
          <div class="carousel-item active">
            <img src="images/background.jpg" alt="fourtek-banner" class="img-responsive">
            <div class="carousel-caption d-md-block slider-content">
            <h1 data-animation="animated bounceInLeft">Fourtek</h1>
              <p class="masthead-subtitle" data-animation="animated bounceInRight">Help business to start & help them to stay <br />
step a head of the competition</p>
              <a href="./about-us.php" class="btn-fourtek" data-animation="animated zoomInUp">learn more</a>
            </div>
          </div>

          <!-- second slide -->
          <div class="carousel-item">
            <img src="images/background.jpg" alt="fourtek-banner" class="img-responsive">
            <div class="carousel-caption d-md-block slider-content">
              <h3 class="icon-container" data-animation="animated bounceInDown"><span class="fa fa-heart"></span></h3>
              <h1 data-animation="animated bounceInLeft">Allegiance</h1>
              <p class="masthead-subtitle" data-animation="animated bounceInRight">Inventing Great Experiences That Deliver Contentment With Enhanced ROI</p>
             <a href="./about-us.php" class="btn-fourtek" data-animation="animated zoomInUp">learn more</a>
            </div>
          </div>

          <!-- third slide -->
          <div class="carousel-item skyblue">
            <img src="images/background.jpg" alt="fourtek-banner" class="img-responsive">
            <div class="carousel-caption d-md-block slider-content">
            <h1 data-animation="animated bounceInLeft">Covenant</h1>
              <p class="masthead-subtitle" data-animation="animated bounceInRight">The Rat Race Is For The Rest, Fourtek Focuses On Delivering Better Than The Best</p>
            <a href="./about-us.php" class="btn-fourtek" data-animation="animated zoomInUp">learn more</a>
            </div>
          </div>
        </div>

        <!-- controls -->
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
   <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
      </div>

    </div>
  </div>
</div>