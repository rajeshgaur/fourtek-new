(function($){
  "use strict";

	// on ready function
	jQuery(document).ready(function($) {
   		var $this = $(window);

		// Carousel Slider
		$('.carousel').carousel({
			  interval: 10000
		});
		// wow js
			var wow = new WOW(
			  {
				animateClass: 'animated',
				offset:       100
			  }
			);
		wow.init();
		// Star Rating
			$('.starType').raty({
				cancel   : true,
				half     : true,
				starType : 'i',
				score: 3
			});

		// Portfolio-About Home slider
		var owl = $(".vr_home_slider .owl-carousel");
		owl.owlCarousel({
		items:1,
		autoplay:false,
		loop:true,
		dots:false,
		nav: true,
		slideSpeed : 3000,
		animateIn: 'fadeIn',
		animateOut: 'fadeOut',
		navText:['<img src="images/arrow_left.png">','<img src="images/arrow_right.png">']
		});
		// Index2 Page Portfolio and About-us Slider
		var owl1 = $(".vr_home2 .owl-carousel");
		owl1.owlCarousel({
		items:1,
		autoplay:false,
		loop:true,
		dots:true,
		mouseDrag: false,
		nav: true,
		slideSpeed : 3000,
		animateIn: 'fadeIn',
		animateOut: 'fadeOut',
		navText:['<img src="images/arrow_left.png">','<img src="images/arrow_right_green.png">'],
		responsive: {
			0: {
				navText:['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>']
			}
		}
		});
		//Article Page gallery
		var owl2 = $(".article_gallery .owl-carousel");
		  owl2.owlCarousel({
			items:1,
			autoplay:false,
			loop:true,
			dots:false,
			nav: true,
			slideSpeed : 3000,
			navText:['<img src="images/arrow-left.png">','<img src="images/arrow-right.png">']
		});
		// our article
		var owl3 = $(".vr_our_articleslider .owl-carousel");
		owl3.owlCarousel({
			items:2,
			autoplay:false,
			loop:true,
			dots:false,
			mouseDrag: false,
			nav: true,
			navText:['<svg width="100px" height="100px" viewBox="0 0 99.000000 99.000000" preserveAspectRatio="xMidYMid meet"><metadata>Created by potrace 1.10, written by Peter Selinger 2001-2011</metadata><g transform="translate(0.000000,99.000000) scale(0.100000,-0.100000)"fill="#000000" stroke="none"><path d="M247 742 l-247 -247 248 -248 247 -247 248 248 247 247 -248 248 -247 247 -248 -248z m481 -479 l-233 -233 -232 232 -233 233 232 232 233 233 232 -232 233 -233 -232 -232z"/><path d="M419 544 c-40 -40 -70 -77 -66 -81 4 -4 37 22 73 58 l65 65 64 -65 c39 -40 67 -62 72 -56 5 6 -22 40 -64 81 l-71 71 -73 -73z"/></g></svg>','<svg width="100px" height="100px" viewBox="0 0 99.000000 99.000000" preserveAspectRatio="xMidYMid meet"><metadata> Created by potrace 1.10, written by Peter Selinger 2001-2011 </metadata><g transform="translate(0.000000,99.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none"><path d="M247 742 l-247 -247 248 -248 247 -247 248 248 247 247 -248 248 -247 247 -248 -248z m481 -479 l-233 -233 -232 232 -233 233 232 232 233 233 232 -232 233 -233 -232 -232z"/><path d="M362 521 c2 -5 33 -40 69 -79 l67 -70 73 74 c40 40 70 77 66 81 -4 4 -37 -22 -73 -58 l-66 -66 -60 64 c-55 57 -85 78 -76 54z"/></g></svg> '],
			responsive: {
				0: {
					items: 1,
					nav: true,
					navText:['<img src="images/arrow-left.png">','<img src="images/arrow-right.png"> ']
				},
				600: {
					items: 2,
					nav: true
				},
				1000: {
					items: 2,
					nav: true
				}
			}
		});
		// client
		var owl4 = $(".vr_client_wrapper .owl-carousel");
		owl4.owlCarousel({
			items:3,
			autoplay:false,
			loop:true,
			mouseDrag: false,
			dots:false,
			nav: true,
			navText:['<svg width="100px" height="100px" viewBox="0 0 99.000000 99.000000" preserveAspectRatio="xMidYMid meet"><metadata>Created by potrace 1.10, written by Peter Selinger 2001-2011</metadata><g transform="translate(0.000000,99.000000) scale(0.100000,-0.100000)"fill="#000000" stroke="none"><path d="M247 742 l-247 -247 248 -248 247 -247 248 248 247 247 -248 248 -247 247 -248 -248z m481 -479 l-233 -233 -232 232 -233 233 232 232 233 233 232 -232 233 -233 -232 -232z"/><path d="M419 544 c-40 -40 -70 -77 -66 -81 4 -4 37 22 73 58 l65 65 64 -65 c39 -40 67 -62 72 -56 5 6 -22 40 -64 81 l-71 71 -73 -73z"/></g></svg>','<svg width="100px" height="100px" viewBox="0 0 99.000000 99.000000" preserveAspectRatio="xMidYMid meet"><metadata> Created by potrace 1.10, written by Peter Selinger 2001-2011 </metadata><g transform="translate(0.000000,99.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none"><path d="M247 742 l-247 -247 248 -248 247 -247 248 248 247 247 -248 248 -247 247 -248 -248z m481 -479 l-233 -233 -232 232 -233 233 232 232 233 233 232 -232 233 -233 -232 -232z"/><path d="M362 521 c2 -5 33 -40 69 -79 l67 -70 73 74 c40 40 70 77 66 81 -4 4 -37 -22 -73 -58 l-66 -66 -60 64 c-55 57 -85 78 -76 54z"/></g></svg> '],
			responsive: {
				0: {
					items: 1,
					nav: true
				},
				600: {
					items: 3,
					nav: true
				},
				1000: {
					items: 3,
					nav: true
				}
			}
		});

		// Portfolio Page slider
		var owl5 = $(".vr_portfolio .owl-carousel");
		owl5.owlCarousel({
		items:1,
		autoplay:false,
		loop:true,
		dots:false,
		mouseDrag: false,
		nav: true,
		slideSpeed : 3000,
		animateIn: 'flipInX',
		navText:['<svg width="100px" height="100px" viewBox="0 0 99.000000 99.000000" preserveAspectRatio="xMidYMid meet"><metadata>Created by potrace 1.10, written by Peter Selinger 2001-2011</metadata><g transform="translate(0.000000,99.000000) scale(0.100000,-0.100000)"fill="#fff" stroke="none"><path d="M247 742 l-247 -247 248 -248 247 -247 248 248 247 247 -248 248 -247 247 -248 -248z m481 -479 l-233 -233 -232 232 -233 233 232 232 233 233 232 -232 233 -233 -232 -232z"/><path d="M419 544 c-40 -40 -70 -77 -66 -81 4 -4 37 22 73 58 l65 65 64 -65 c39 -40 67 -62 72 -56 5 6 -22 40 -64 81 l-71 71 -73 -73z"/></g></svg>','<svg width="100px" height="100px" viewBox="0 0 99.000000 99.000000" preserveAspectRatio="xMidYMid meet"><metadata> Created by potrace 1.10, written by Peter Selinger 2001-2011 </metadata><g transform="translate(0.000000,99.000000) scale(0.100000,-0.100000)" fill="#fff" stroke="none"><path d="M247 742 l-247 -247 248 -248 247 -247 248 248 247 247 -248 248 -247 247 -248 -248z m481 -479 l-233 -233 -232 232 -233 233 232 232 233 233 232 -232 233 -233 -232 -232z"/><path d="M362 521 c2 -5 33 -40 69 -79 l67 -70 73 74 c40 40 70 77 66 81 -4 4 -37 -22 -73 -58 l-66 -66 -60 64 c-55 57 -85 78 -76 54z"/></g></svg> ']
		});
		// portfolio traingle silder in responsive
		var owl6 = $(".vr_portfolio_responsive_slider .owl-carousel");
		owl6.owlCarousel({
		items:1,
		autoplay:false,
		loop:true,
		dots:false,
		mouseDrag: false,
		nav: true,
		slideSpeed : 3000,
		//animateIn: 'flipInX',
		navText:['<svg width="100px" height="100px" viewBox="0 0 99.000000 99.000000" preserveAspectRatio="xMidYMid meet"><metadata>Created by potrace 1.10, written by Peter Selinger 2001-2011</metadata><g transform="translate(0.000000,99.000000) scale(0.100000,-0.100000)"fill="#fff" stroke="none"><path d="M247 742 l-247 -247 248 -248 247 -247 248 248 247 247 -248 248 -247 247 -248 -248z m481 -479 l-233 -233 -232 232 -233 233 232 232 233 233 232 -232 233 -233 -232 -232z"/><path d="M419 544 c-40 -40 -70 -77 -66 -81 4 -4 37 22 73 58 l65 65 64 -65 c39 -40 67 -62 72 -56 5 6 -22 40 -64 81 l-71 71 -73 -73z"/></g></svg>','<svg width="100px" height="100px" viewBox="0 0 99.000000 99.000000" preserveAspectRatio="xMidYMid meet"><metadata> Created by potrace 1.10, written by Peter Selinger 2001-2011 </metadata><g transform="translate(0.000000,99.000000) scale(0.100000,-0.100000)" fill="#fff" stroke="none"><path d="M247 742 l-247 -247 248 -248 247 -247 248 248 247 247 -248 248 -247 247 -248 -248z m481 -479 l-233 -233 -232 232 -233 233 232 232 233 233 232 -232 233 -233 -232 -232z"/><path d="M362 521 c2 -5 33 -40 69 -79 l67 -70 73 74 c40 40 70 77 66 81 -4 4 -37 -22 -73 -58 l-66 -66 -60 64 c-55 57 -85 78 -76 54z"/></g></svg> ']
		});
		// Portfolio Page slider
		var owl7 = $("#vr_portfolio_slider .owl-carousel");
		owl7.owlCarousel({
		items:1,
		autoplay:true,
		loop:true,
		dots:false,
		nav: true,
		slideSpeed : 3000,
		navText:['<svg width="100px" height="100px" viewBox="0 0 99.000000 99.000000" preserveAspectRatio="xMidYMid meet"><metadata>Created by potrace 1.10, written by Peter Selinger 2001-2011</metadata><g transform="translate(0.000000,99.000000) scale(0.100000,-0.100000)"fill="#fff" stroke="none"><path d="M247 742 l-247 -247 248 -248 247 -247 248 248 247 247 -248 248 -247 247 -248 -248z m481 -479 l-233 -233 -232 232 -233 233 232 232 233 233 232 -232 233 -233 -232 -232z"/><path d="M419 544 c-40 -40 -70 -77 -66 -81 4 -4 37 22 73 58 l65 65 64 -65 c39 -40 67 -62 72 -56 5 6 -22 40 -64 81 l-71 71 -73 -73z"/></g></svg>','<svg width="100px" height="100px" viewBox="0 0 99.000000 99.000000" preserveAspectRatio="xMidYMid meet"><metadata> Created by potrace 1.10, written by Peter Selinger 2001-2011 </metadata><g transform="translate(0.000000,99.000000) scale(0.100000,-0.100000)" fill="#fff" stroke="none"><path d="M247 742 l-247 -247 248 -248 247 -247 248 248 247 247 -248 248 -247 247 -248 -248z m481 -479 l-233 -233 -232 232 -233 233 232 232 233 233 232 -232 233 -233 -232 -232z"/><path d="M362 521 c2 -5 33 -40 69 -79 l67 -70 73 74 c40 40 70 77 66 81 -4 4 -37 -22 -73 -58 l-66 -66 -60 64 c-55 57 -85 78 -76 54z"/></g></svg> ']
		});
		// single project slider
		var owl8 = $(".vr_other_project_slider .owl-carousel");
		owl8.owlCarousel({
			items:2,
			autoplay:false,
			loop:true,
			dots:false,
			nav: true,
			navText:['<svg width="100px" height="100px" viewBox="0 0 99.000000 99.000000" preserveAspectRatio="xMidYMid meet"><metadata>Created by potrace 1.10, written by Peter Selinger 2001-2011</metadata><g transform="translate(0.000000,99.000000) scale(0.100000,-0.100000)"fill="#fff" stroke="none"><path d="M247 742 l-247 -247 248 -248 247 -247 248 248 247 247 -248 248 -247 247 -248 -248z m481 -479 l-233 -233 -232 232 -233 233 232 232 233 233 232 -232 233 -233 -232 -232z"/><path d="M419 544 c-40 -40 -70 -77 -66 -81 4 -4 37 22 73 58 l65 65 64 -65 c39 -40 67 -62 72 -56 5 6 -22 40 -64 81 l-71 71 -73 -73z"/></g></svg>','<svg width="100px" height="100px" viewBox="0 0 99.000000 99.000000" preserveAspectRatio="xMidYMid meet"><metadata> Created by potrace 1.10, written by Peter Selinger 2001-2011 </metadata><g transform="translate(0.000000,99.000000) scale(0.100000,-0.100000)" fill="#fff" stroke="none"><path d="M247 742 l-247 -247 248 -248 247 -247 248 248 247 247 -248 248 -247 247 -248 -248z m481 -479 l-233 -233 -232 232 -233 233 232 232 233 233 232 -232 233 -233 -232 -232z"/><path d="M362 521 c2 -5 33 -40 69 -79 l67 -70 73 74 c40 40 70 77 66 81 -4 4 -37 -22 -73 -58 l-66 -66 -60 64 c-55 57 -85 78 -76 54z"/></g></svg> '],
			responsive: {
				0: {
					items: 1,
					nav: true
				},
				600: {
					items: 2,
					nav: true
				},
				1000: {
					items: 2,
					nav: true
				}
			}
		});
		//smoothscroll
		smoothScroll.init({
			speed: 3000,
			easing: 'easeInOutCubic'
		});
		// This example adds a duration to the tweens so they are synced to the scroll position
		var controller = $.superscrollorama({
			triggerAtCenter: true,
			playoutAnimations: true
		});
		// amount of scrolling over which the tween takes place (in pixels)
		var scrollDuration = 0;
		// individual element tween examples
		controller.addTween('.vr_left_small', TweenMax.to( $('.vr_left_small'), 3, {left:-100+"px", ease: Power4.easeInOut, y: 0 }), scrollDuration);

		controller.addTween('.vr_right_small', TweenMax.to( $('.vr_right_small'), 3, {right:-100+"px", ease: Power4.easeInOut, y: 0 }), scrollDuration);

		controller.addTween('.vr_left', TweenMax.to( $('.vr_left'), 3, {left:-100+"px", ease: Power4.easeInOut, y: 0 }), scrollDuration);

		controller.addTween('.vr_right', TweenMax.to( $('.vr_right'), 3, {right:-100+"px", ease: Power4.easeInOut, y: 0 }), scrollDuration);


		// only for firefox browser
		var FIREFOX = /Firefox/i.test(navigator.userAgent);

		if (FIREFOX) {
		document.getElementById("skillchart_mozilla").style.display="block";
		document.getElementById("skillchart").style.display="none";

		var circle1 = Circles.create({
        id: 'circles-1',
        value: 75,
        radius: 100,
        number: 75,
        text: '75%',
        width: 7,
        colors: ["#f5f5f5", "#11232a"],
        duration: 900
		});
		var circle2 = Circles.create({
        id: 'circles-2',
        value: 54,
        radius: 100,
        number: 54,
        text: '54%',
        width: 7,
        colors: ["#f5f5f5", "#11232a"],
        duration: 900
		});
		var circle3 = Circles.create({
        id: 'circles-3',
        value: 90,
        radius: 100,
        number: 90,
        text: '90%',
        width: 7,
        colors: ["#f5f5f5", "#11232a"],
        duration: 900
		});
		var circle4 = Circles.create({
        id: 'circles-4',
        value: 85,
        radius: 100,
        number: 85,
        text: '85%',
        width: 7,
        colors: ["#f5f5f5", "#11232a"],
        duration: 900
		});
		}
		// only for safari browser
		var isSafari = (/Safari/.test(navigator.userAgent)) && (/Apple Computer/.test(navigator.vendor));

		// testimonial
		if (isSafari) {
		 var owl9 = $(".vr_testimonial_content .owl-carousel");
		owl9.owlCarousel({
			items:1,
			autoplay:false,
			loop:true,
			dots:true,
			mouseDrag: false,
			nav: true,
            slideSpeed:  500,
			animateIn: 'fadeIn',
			animationDelay: 15000,
			animateOut: 'fadeOut',
			navText:['<svg width="100px" height="100px" viewBox="0 0 99.000000 99.000000" preserveAspectRatio="xMidYMid meet"><metadata>Created by potrace 1.10, written by Peter Selinger 2001-2011</metadata><g transform="translate(0.000000,99.000000) scale(0.100000,-0.100000)"fill="#000000" stroke="none"><path d="M247 742 l-247 -247 248 -248 247 -247 248 248 247 247 -248 248 -247 247 -248 -248z m481 -479 l-233 -233 -232 232 -233 233 232 232 233 233 232 -232 233 -233 -232 -232z"/><path d="M419 544 c-40 -40 -70 -77 -66 -81 4 -4 37 22 73 58 l65 65 64 -65 c39 -40 67 -62 72 -56 5 6 -22 40 -64 81 l-71 71 -73 -73z"/></g></svg>','<svg width="100px" height="100px" viewBox="0 0 99.000000 99.000000" preserveAspectRatio="xMidYMid meet"><metadata> Created by potrace 1.10, written by Peter Selinger 2001-2011 </metadata><g transform="translate(0.000000,99.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none"><path d="M247 742 l-247 -247 248 -248 247 -247 248 248 247 247 -248 248 -247 247 -248 -248z m481 -479 l-233 -233 -232 232 -233 233 232 232 233 233 232 -232 233 -233 -232 -232z"/><path d="M362 521 c2 -5 33 -40 69 -79 l67 -70 73 74 c40 40 70 77 66 81 -4 4 -37 -22 -73 -58 l-66 -66 -60 64 c-55 57 -85 78 -76 54z"/></g></svg> ']
		});
   }
   else{
	   var owl10 = $(".vr_testimonial_content .owl-carousel");
		owl10.owlCarousel({
			items:1,
			autoplay:false,
			loop:true,
			dots:true,
			mouseDrag: false,
			nav: true,
            slideSpeed:  500,
			animateIn: 'fadeIn',
			navText:['<svg width="100px" height="100px" viewBox="0 0 99.000000 99.000000" preserveAspectRatio="xMidYMid meet"><metadata>Created by potrace 1.10, written by Peter Selinger 2001-2011</metadata><g transform="translate(0.000000,99.000000) scale(0.100000,-0.100000)"fill="#000000" stroke="none"><path d="M247 742 l-247 -247 248 -248 247 -247 248 248 247 247 -248 248 -247 247 -248 -248z m481 -479 l-233 -233 -232 232 -233 233 232 232 233 233 232 -232 233 -233 -232 -232z"/><path d="M419 544 c-40 -40 -70 -77 -66 -81 4 -4 37 22 73 58 l65 65 64 -65 c39 -40 67 -62 72 -56 5 6 -22 40 -64 81 l-71 71 -73 -73z"/></g></svg>','<svg width="100px" height="100px" viewBox="0 0 99.000000 99.000000" preserveAspectRatio="xMidYMid meet"><metadata> Created by potrace 1.10, written by Peter Selinger 2001-2011 </metadata><g transform="translate(0.000000,99.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none"><path d="M247 742 l-247 -247 248 -248 247 -247 248 248 247 247 -248 248 -247 247 -248 -248z m481 -479 l-233 -233 -232 232 -233 233 232 232 233 233 232 -232 233 -233 -232 -232z"/><path d="M362 521 c2 -5 33 -40 69 -79 l67 -70 73 74 c40 40 70 77 66 81 -4 4 -37 -22 -73 -58 l-66 -66 -60 64 c-55 57 -85 78 -76 54z"/></g></svg> ']
		});
   }
	var width= $(window).width();
	if(width >= 1200){
		if (isSafari) {
		$(".vr_slider_img").addClass("vr_slider_img_safari");
		$(".vr_slider_img").css("top", "59%");
		$(".vr_slider_img").css("left", "29%");
		$(".vr_slider_img").css("width", "150px");
		$(".vr_slider_img").css("height", "150px");
		$(".vr_team_img").css("margin-top", "55px");
		$(".vr_testimonial_data span").css("padding-bottom", "20px");
		$(".vr_mainmenu ul.nav li ul.submenu").css("webkitTransition", "none");
		$(".vr_mainmenu ul.nav li").on("hover",function() {
			$('ul', this).css("opacity", 1);
		});
		}
		}

	if(width >= 992 && width <= 1199){
		if (isSafari) {
			$(".vr_slider_img").css("top", "52%");
			$(".vr_slider_img").css("left", "29%");
			$(".vr_slider_img").css("width", "120px");
			$(".vr_slider_img").css("height", "120px");
			$(".vr_team_detail").css("margin", "-60px 44px 44px");
			$(".vr_team_img").css("marginTop", "41px");
			$(".vr_slider_img").addClass("vr_slider_img_safari");
			$(".vr_mainmenu ul.nav li ul.submenu").css("webkitTransition", "none");
		$(".vr_mainmenu ul.nav li").on("hover",function() {
			$('ul', this).css("opacity", 1);
		});
		}
	}
	
	if(width >= 768 && width <= 991){
		if (isSafari) {
			$(".vr_slider_img").addClass("vr_slider_img_safari");
			$(".vr_slider_img").css("top", "37%");
			$(".vr_slider_img").css("left", "39.5%");
			$(".vr_slider_img").css("width", "100px");
			$(".vr_slider_img").css("height", "100px");
		}
	}
	if(width >= 767 && width <= 769){
		if (isSafari) {
			$("#vr_container").css("width", "auto");
		}

	}
	//contact form submition
		$("#send_btn").on("click", function() {
        var e = $("#ur_name").val();
        var t = $("#ur_mail").val();
        var r = $("#msg").val();
        $.ajax({
            type: "POST",
            url: "ajaxmail.php",
            data: {
                username: e,
                useremail: t,
                mesg: r
            },
            success: function(n) {
                var i = n.split("#");
                if (i[0] == "1") {
                    $("#ur_name").val("");
                    $("#ur_mail").val("");
                    $("#msg").val("");
                    $("#err").html(i[1])
                } else {
                    $("#ur_name").val(e);
                    $("#ur_mail").val(t);
                    $("#msg").val(r);
                    $("#err").html(i[1])
                }
            }
        })
		});
	
	});

})();
