<?php get_header();

?>
<div style="clear:both;"></div>


<style>
.blogs-1 {min-height: 384px; padding: 15px ;}
.blogs-1 img {height: 155px;border:1px solid #e8e8e8;}
.main h2 a{padding: 0 14px;font-size: 26px;color: #af1f25;text-transform: uppercase; margin-bottom: 23px;}
.blogs-1 h5 { color: #36bba5; margin-top: 30px;}
</style>


<div class="container">
<div class="blog-banner">
	<img src="<?php bloginfo('template_url'); ?>/images/blog_banner.jpg" alt="" class="img-responsive">
   <h1>The future of work:<br/> How to navigate the augmented workforce.</h1>
</div>
</div>

<section class="menu-blog">
<div class="container">
	<div class="main" >
 <section class="scroll-1"> 

 <h2><a href="javascript:;">Latest  &nbsp; <i class="fa fa-angle-right" aria-hidden="true"></i></a></h2>
			    <?php $the_query = new WP_Query( 'posts_per_page=3' ); 
			while ($the_query -> have_posts()) : $the_query -> the_post();?>
			    <div class="col-md-4 col-sm-12">
				  <div class="col-sm-12 blogs-1">
				  <p style="font-size: 12px;">POSTED ON : <?php the_time('l, F jS, Y') ?></p>
				  <a href="<?php the_permalink(); ?>">
				  	<img src="<?php the_post_thumbnail_url('full');?>" class="img-responsive"/>
				  	<?php //the_post_thumbnail('post-thumbnail', array( 'class'=> "img-responsive")); ?> </a>
				      <h5><a href="<?php the_permalink(); ?>"><?php echo substr(strip_tags($post->post_title), 0, 42)."..."; ?></a></h5>
					  <p><?php echo substr(strip_tags($post->post_content), 0, 100)."..."; ?></p>
					  <p><a href="<?php the_permalink(); ?>">Read more</a></p>
				</div>
							
				</div>
<?php endwhile;wp_reset_postdata();?>
 </section>
</div>
<br>
<br>
<br>

	<div class="blog-menu-main scroll-navi stick" id="menu-toggle">
		<a href="#website-development">Website Development</a>
		<a href="#mobile-app-development">Mobile App Development</a>
		<a href="#game-development">Game Development</a>
		<a href="#digital-marketing">Digital Marketing</a>
		<a href="#salesforce-development">Salesforce Development</a>
		<a href="#e-commerce">E-commerce</a>
	</div>




<div class="main" id="website-development">
 <section class="scroll-1"> 

 <h2><a href="<?php echo get_site_url(); ?>/category/website-development/">Website Development  &nbsp; <i class="fa fa-angle-right" aria-hidden="true"></i></a></h2>
			    <?php $posts = get_posts('category=43&numberposts=3');
                foreach ($posts as $post) {
                    ?>
			    <div class="col-md-4 col-sm-12">
				  <div class="col-sm-12 blogs-1">
				  <p style="font-size: 12px;">POSTED ON : <?php the_time('l, F jS, Y') ?></p>
				  <a href="<?php the_permalink(); ?>"><img src="<?php the_post_thumbnail_url('full');?>" class="img-responsive"/></a>
				      <h5><a href="<?php the_permalink(); ?>"><?php echo substr(strip_tags($post->post_title), 0, 42)."..."; ?></a></h5>
					  <p><?php echo substr(strip_tags($post->post_content), 0, 100)."..."; ?></p>
					  <p><a href="<?php the_permalink(); ?>">Read more</a></p>
				</div>
							
				</div>
<?php
                } ?>
 </section>
</div>


<div class="main" id="mobile-app-development">
 <section class="scroll-1"> 
 <h2><a href="<?php echo get_site_url(); ?>/category/mobile-app-development">Mobile App Development  &nbsp; <i class="fa fa-angle-right" aria-hidden="true"></i></a></h2>
<?php $posts = get_posts('category=34&numberposts=3');
foreach ($posts as $post) {
    ?>
			    <div class="col-md-4 col-sm-12">
				  <div class="col-sm-12 blogs-1">
				  <p style="font-size: 12px;">POSTED ON : <?php the_time('l, F jS, Y') ?></p>
				<a href="<?php the_permalink(); ?>">  <img src="<?php the_post_thumbnail_url('full');?>" class="img-responsive"/> </a>
				      <h5><a href="<?php the_permalink(); ?>"><?php echo substr(strip_tags($post->post_title), 0, 42)."..."; ?></a></h5>
					  <p><?php echo substr(strip_tags($post->post_content), 0, 100)."..."; ?></p>
					  <p><a href="<?php the_permalink(); ?>">Read more</a></p>
				</div>
							
				</div>
				  <?php
} ?>
 </section>
</div>


<div class="main" id="game-development">
 <section class="scroll-1"> 

 <h2><a href="<?php echo get_site_url(); ?>/category/game-development">Game Development  &nbsp; <i class="fa fa-angle-right" aria-hidden="true"></i></a></h2>
			    <?php $posts = get_posts('category=24&numberposts=3');
foreach ($posts as $post) {
    ?>
			    <div class="col-md-4 col-sm-12">
				  <div class="col-sm-12 blogs-1">
				  <p style="font-size: 12px;">POSTED ON : <?php the_time('l, F jS, Y') ?></p>
				  <a href="<?php the_permalink(); ?>"><img src="<?php the_post_thumbnail_url('full');?>" class="img-responsive"/></a>
				      <h5><a href="<?php the_permalink(); ?>"><?php echo substr(strip_tags($post->post_title), 0, 42)."..."; ?></a></h5>
					  <p><?php echo substr(strip_tags($post->post_content), 0, 100)."..."; ?></p>
					  <p><a href="<?php the_permalink(); ?>">Read more</a></p>
				</div>
							
				</div>
				  <?php
} ?>
				  
				  

 </section>
</div>

<div class="main" id="digital-marketing">
 <section class="scroll-1"> 

 <h2><a href="<?php echo get_site_url(); ?>/category/digital-marketing">Digital Marketing  &nbsp; <i class="fa fa-angle-right" aria-hidden="true"></i></a></h2>
			    <?php $posts = get_posts('category=23&numberposts=3');
foreach ($posts as $post) {
    ?>
			    <div class="col-md-4 col-sm-12">
				  <div class="col-sm-12 blogs-1">
				  <p style="font-size: 12px;">POSTED ON : <?php the_time('l, F jS, Y') ?></p>
				<a href="<?php the_permalink(); ?>"> <img src="<?php the_post_thumbnail_url('full');?>" class="img-responsive"/> </a>
				      <h5><a href="<?php the_permalink(); ?>"><?php echo substr(strip_tags($post->post_title), 0, 42)."..."; ?></a></h5>
					  <p><?php echo substr(strip_tags($post->post_content), 0, 100)."..."; ?></p>
					  <p><a href="<?php the_permalink(); ?>">Read more</a></p>
				</div>
							
				</div>
				  <?php
} ?>
				  
				  

 </section>
</div>

<div class="main" id="salesforce-development">
 <section class="scroll-1"> 
 <h2><a href="<?php echo get_site_url(); ?>/category/saleaforce-development">Salesforce Development  &nbsp; <i class="fa fa-angle-right" aria-hidden="true"></i></a></h2>
			    <?php $posts = get_posts('category=44&numberposts=3');
foreach ($posts as $post) {
    ?>
			    <div class="col-md-4 col-sm-12">
				  <div class="col-sm-12 blogs-1">
				  <p style="font-size: 12px;">POSTED ON : <?php the_time('l, F jS, Y') ?></p>
				 <a href="<?php the_permalink(); ?>"> <img src="<?php the_post_thumbnail_url('full');?>" class="img-responsive"/> </a>
				      <h5><a href="<?php the_permalink(); ?>"><?php echo substr(strip_tags($post->post_title), 0, 42)."..."; ?></a></h5>
					  <p><?php echo substr(strip_tags($post->post_content), 0, 100)."..."; ?></p>
					  <p><a href="<?php the_permalink(); ?>">Read more</a></p>
				</div>
							
				</div>
<?php
} ?>
				  
				  

 </section>
</div>
<div class="main" id="e-commerce">
 <section class="scroll-1"> 
 <h2><a href="<?php echo get_site_url(); ?>/category/e-commerce">E-commerce  &nbsp; <i class="fa fa-angle-right" aria-hidden="true"></i></a></h2>
			    <?php $posts = get_posts('category=30&numberposts=3');
foreach ($posts as $post) {
    ?>
			    <div class="col-md-4 col-sm-12">
				  <div class="col-sm-12 blogs-1">
				  <p style="font-size: 12px;">POSTED ON : <?php the_time('l, F jS, Y') ?></p>
				 <a href="<?php the_permalink(); ?>"> <img src="<?php the_post_thumbnail_url('full');?>" class="img-responsive"/></a>
				      <h5><a href="<?php the_permalink(); ?>"><?php echo substr(strip_tags($post->post_title), 0, 42)."..."; ?></a></h5>
					  <p><?php echo substr(strip_tags($post->post_content), 0, 100)."..."; ?></p>
					  <p><a href="<?php the_permalink(); ?>">Read more</a></p>
				</div>
							
				</div>
<?php
} ?>
</section>
</div>
<div style="clear:both;"></div>
</div>
</section>	
<div style="clear:both;"></div><br/>
<?php get_footer(); ?>


