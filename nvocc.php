<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">

    <title>Fourtek</title>
    <link rel="shortcut icon" type="image/icon" href="images/fav.ico" />
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Pattaya|Poppins:300,300i,400" rel="stylesheet">
    <link href="css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
    <link href="css/animate.min.css" rel="stylesheet" type="text/css">
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link href="css/responsive.css" rel="stylesheet" type="text/css">
    <?php include "google-code.php";?>
  </head>


<style>
  header{background: url(images/hrm-service-banner.jpg) !important; background-size: cover !important;  background-repeat: no-repeat;background-attachment: fixed !important;}
</style>


  <body id="page-top" class="inner-page">
      <?php include 'include/menu.php' ; ?>
  
    <header class="masthead video">
      <div class="container h-100">
        <div class="row h-100">
          <div class="col-12 my-auto text-center text-white">          
            <h1>Freight Forwarding / NVOCC</h1>
            <p>Fourtek employee management system allows companies to centralize confidential employee information and define access permissions to authorized personnel to ensure that employee information is both secure and accessible.</p>           
          </div>
        </div>
      </div>
    </header>

<section class="breadcrumb-block">
<div class="container">
  <div class="breadcrumb">
    <a class="breadcrumb-item" href="index.php">Home</a>
    <span class="breadcrumb-item active">Freight Forwarding</span>
  </div>
</div>
</section>


<section class="hrm-sections">
      <div class="container">

     <div class="row">         
        <div class="col-sm-6">
            <div class="">
               <h2>Automate your leave management</h2>
                 <span class="line-blue"></span>
               <br/>
            </div>
            <p><strong>Are you still relying on Excel to track your employee leave records? Is your current leave application and approval process too confusing?</strong></p>

           <p>Fourtek Hrm leave management system allows you to stop all time-off abuses and implement company-wide leave policy. You can add your own leave types, make exceptions to individual employees and groups using leave rules. Also it supports leave accrual and carry forwarding to next leave period.</p>
        </div>
        <div class="col-sm-6"><img src="images/hrm-img-1.jpg" alt=""> </div>
     </div>

     <div class="row">  
      <div class="col-sm-6"><img src="images/hrm-img-2.jpg" alt=""> </div>       
        <div class="col-sm-6">
            <div class="">
               <h2>Easy Payroll Processing</h2>
                 <span class="line-blue"></span>
               <br/>
            </div>
            <p>Fourtek can now cover your company payroll too.
Create payroll reports required for third party payroll processing applications or create payroll rules to calculate your company payroll via IceHrm.</p><p>
We support multiple pay periods, payroll groups for applying only selected payroll processing rules to selected employee groups and exporting payroll reports.</p>
        </div>
       
     </div>

   </div>
</section>

  <section class="request-section" style="background:url(images/service-bg.jpg); background-attachment: fixed;">
    <div class="container">
        <h2>Let’s start something great together !</h2>
        <p>Send us your requirements and we'll get back to you with an outline on prices, timeframe and expectations</p>
        <a href="#" class="btn-fourtek">Request a Quote</a> 
       
    </div>
  </section>

<section class="business-process">
    <div class="container">

      <div class="process-title">
       <h2> Freight Forwarding / NVOCC</h2>
        <p>It's important to keep your employees up to date by providing training. IceHrm makes it easy for you to manage training courses and sessions in your company. You can define training sessions which your employees can subscribe. Also, you can track the progress and the completion of the training.</p>
       </div>

       <div class="row">
           <div class="col-md-6 col-sm-12">
            <ul class="hrm-list">

               <li>
                  <h4>Track Every Bit of Time Spent</h4>
                  <p>With the help of IceHrm Time sheets module employees to update their own time sheets and send those for approval to supervisors. The attendance module will keep track of employee punch-in and punch-out times.</p>
               </li>

               <li>
                  <h4>Expenses Management</h4>
                  <p>Expense claim management is a lot easier with icehrm expense module with increased visibility a proper approval process. IceHrm supports up to 4 levels of approval for Expense module.
Also aprroved expenses can be imported in to payroll and can be used to add expense claims to employee salary.</p>
               </li>

               <li>
                  <h4>Recruitment and Applicant Tracking</h4>
                  <p>Fourtek Recruitment Module can help you to find the ideal candidate for your job
It not only provides an internal job portal for the company but also it lets you manage your interview process smoothly.</p>
               </li>

               <li>
                  <h4>Custom Forms</h4>
                  <p>HR Departments need to collect various employee information time to time. In this case, you are covered with Fourtek.
With Fourtek you can create custom forms and send these forms to selected employees so they can fill and send it for approval.</p>
               </li>

             </ul> 
         </div>

            <div class="col-md-6 col-sm-12"><br/>
              <img src="images/hrm-img-3.jpg" class="img-fluid" alt="">
           </div>
                                                             
       </div>  
    </div>
  </section>


   <?php include 'include/footer.php' ;?>
   
  </body>
</html>
