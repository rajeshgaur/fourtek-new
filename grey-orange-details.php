<!DOCTYPE html>
<html lang="en">
 
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <title> Case Studies </title>
    <link rel="shortcut icon" type="image/icon" href="images/fav.ico" />
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Pattaya|Poppins:300,300i,400" rel="stylesheet">
    <link href="css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
    <link href="css/animate.min.css" rel="stylesheet" type="text/css">
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link href="css/responsive.css" rel="stylesheet" type="text/css">
    <?php include "google-code.php";?>
  </head>


<style>
  header{background: url(images/greyorange-bg.jpg) !important; background-size: cover !important;  background-repeat: no-repeat;background-attachment: fixed !important; background-position: bottom;}
</style>


  <body id="page-top" class="inner-page">
      <?php include 'include/menu.php'; ?>
  
    <header class="masthead video">
      <div class="container h-100">
        <div class="row h-100">
          <div class="col-12 my-auto text-center text-white">          
            <h1 class="wow fadeInDown">Grey Orange</h1>
            <p>GreyOrange is a multinational technology company that designs, manufactures and deploys advanced robotics systems for automation in warehouses, distribution and fulfillment centers. </p>

            <p> <a href="javascript:;" data-toggle="modal" id="bnrst" data-target="#exampleModal" class="btn-fourtek wow fadeInUpBig">Request a Quote</a></p>
          </div>
        </div>
      </div>
    </header>

    <section class="breadcrumb-block">
    <div class="container">
      <div class="breadcrumb">
        <a class="breadcrumb-item" href="index.php">Home</a>
        <span class="breadcrumb-item active">Grey Orange</span>
      </div>
    </div>
    </section>


    <section class="padd_80">
      <div class="container">
        <div class="row">
          <div class="col-md-9">
            <div class="slider"><img src="images/greyorange-slider.jpg" class="img-responsive"></div>

            <div class="bs-example">
                <ul class="nav nav-tabs" id="myTab">
                    <li><a data-toggle="tab" href="#sectionA" class="active">About Client</a></li>
                    <li><a data-toggle="tab" href="#sectionB">Project Overview</a></li>
                    <li><a data-toggle="tab" href="#sectionC">Identifying Problem Statement</a></li>
                    <li><a data-toggle="tab" href="#sectionD">Higher Success Rate</a></li>
                </ul>
                <div class="tab-content">
                    <div id="sectionA" class="tab-pane active">
                        <h3>Grey Orange</h3>
                        <p>GreyOrange is one of the leading multinational technology companies, catering to the needs of a broad spectrum of industries with its innovative robotics and supply chain automation solutions. Since its inception in 2011, the company is helping its clients by making them ready for designing, manufacturing and deploying advanced robotics systems for automation in the warehouses, fulfillment and distribution centers. The organization is purely focused towards bringing advancement in the way logistics and supply chain processes are being optimized in the world. With having headquarter in Singapore, the company has also expanded its legs successfully in Japan, Germany, USA, and India.</p>
                    </div>

                    <div id="sectionB" class="tab-pane">
                        <h3>Project Overview</h3>
                        <p>One of the leading robotic automation solutions provider-GreyOrange wanted us to design and develop an appealing and high-performing website that can drive increased conversions and better ROI. The organization wanted us to develop the site with a content management system and reduce the bounce rate. Also, they wanted to mark their online presence with a strong and strategic approach. And, this is how we started working on developing an alluring website for them, designing better UI, creating a robust identity and promoting them in the online world.</p>
                    </div>

                    <div id="sectionC" class="tab-pane">

<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
  <div class="panel panel-default">
    <div class="panel-heading active" role="tab" id="headingOne">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          What were the demands of the project?
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">
        <p>GreyOrange is into manufacturing and deploying robotics and supply chain automation solutions, catering to the needs of multiple clients from a different industry background. Being in this technology era, it is quite impossible to promote yourself and expand your business without having an online identity. Hence, in order to expand their business and prove their prominence, a website was significantly required and so was its promotion.</p>

        <p>When we approached GreyOrange, the company needed a website with captivating graphics, alluring designs, and functionality. In the lack of the online presence, the company was facing problem in expanding their customer base and approaching more potential consumers. Also, they wanted us to expand their business in other countries. And, since robotic automation is the most demanding and trendy realm, the development of brilliant automation system is having a quite tough competition in the market.</p>

        <p>So, GreyOrange wanted to lead the market with having a robust and enchanting online presence. Not only they required a website coupled with the latest technology but also, wanted us to promote them and help them expand their business reach amongst customers.</p>
      </div>
    </div>
  </div>

  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingTwo">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">What were the challenges?</a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
      <div class="panel-body">
        <h2>Challenges (overview): Introduction</h2>
        <p>Finding the exact problem was mandatory before delivering any kind of solution so, we took the help of our experts. Then, we came to know that not only they wanted us to develop a website that is based on the latest technology but also packed with brilliant features and functionalities. Apart from that, they were striving to expand their business in other countries as well. During that, they found China to be their one of the major competitors. The Organization wanted us to build their brand name and maintain its reputation.</p>

        <strong>Major Challenges</strong>
        <ul> 
          <li>Robust & appealing website design & development</li>
          <li>Region wise job search option implementation</li>
          <li>Active/Inactive job posting options insertion</li>
          <li>Boost their website’s traffic</li>
          <li>Building their name in the online market</li>
          <li>Maintaining their online reputation</li>
          <li>Enhance their leads and sales</li>
          <li>Boost Their Website’s Ranking in the SERPs</li>
        </ul>

      </div>
    </div>
  </div>

  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingThree">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          What We Created?
        </a>
      </h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">
        <p>We gathered all the business requirements from GreyOrange. We built strategy and then started developing their website using Laravel technology. We used all its dynamic and standard features and functionality. Along with that, we played with trendy graphics and alluring designs to give the website an appealing touch. We also developed a content management system (CMS) for them. We made the security stronger of the website, helped them reducing the bounce rate of the website, creating brilliant UI and UX and boosting their brand identity.</p>
        <p>The freshly fabricated website is helping our clients to engage customers in a large number and retaining them as potential ones. Also, the traffic of the website has increased and users are getting relevant information and spending a good time on the same, resulting in reduced bounce rate. The visitors can easily navigate to the targeted pages. Also, the CMS development solution created by us is helping them to drive more traffic and generate higher conversions.</p>
        <p>Apart from web development and CMS development solutions, we also offered them the best digital marketing services, which proved to be very useful in expanding their business in the world. </p>

        <br>

        <div class="row">
          <div class="col-md-4">
            <strong>Web Development Features</strong>
            <ul>
              <li>Use of Laravel Framework For Developing Website</li>
            </ul>            
          </div>
          <div class="col-md-4">
            <strong>UI Design</strong>
            <ul>
              <li>Elimination of all distracting elements</li>
              <li>Usage of Perfect blend of font, images & graphics</li>
              <li>Quick selection and easy checkout</li>
              <li>Making the flow quick and easy</li>
            </ul>            
          </div>
          <div class="col-md-4">
            <strong>Search Options</strong>
            <ul>
              <li>Dynamic search features</li>
            </ul>            
          </div>          
        </div>

        <div class="row">
          <div class="col-md-4">
            <strong>Job Search Option</strong>
            <ul>
              <li>Region Wise Job Search Option</li>
              <li>Active/Inactive Job Posting</li>
            </ul>            
          </div>
          <div class="col-md-4">
            <strong>Multilingual Feature</strong>
            <ul>
              <li>Multilingual features added to CMS (German+Japanese)</li>
            </ul>            
          </div>
          <div class="col-md-4">
            <strong>Security Features</strong>
            <ul>
              <li>Prevent Fake Queries</li>
            </ul>            
          </div> 
          <div class="col-md-12">
            <strong>Digital Marketing Services</strong>
            <ul>
              <li>Google AdWords</li>
              <li>Directory submission</li>
              <li>Image submission</li>
              <li>Social bookmarking</li>
              <li>Press release</li>
              <li>Article submission</li>
              <li>Blog commenting</li>
            </ul>            
          </div>                    
        </div>        

      </div>
    </div>
  </div>
</div>


                    </div>     


                    <div id="sectionD" class="tab-pane">
                     
                      <p>Post-deployment of the project, the organization gained a higher customer engagement and visitors’ attention. We didn’t only give them a vigorous web presence but also helped them in boosting their position in the search engine result pages. We gave them a robust identity in the online market and maintained it well. Due to this, they experienced a great hike in the traffic rates. Also, they got a brilliant hike in conversion rates. They got enhanced leads and sales as well. Visitors, who were leaving the website within a few seconds, started spending time on the website, which further reduced the bounce rate. We also made available the dynamic search and region wise job search section available which enables the visitors to look for the jobs as per the country, region or area. This also helped them to get enhanced ROI within a short duration. Due to all these reasons, they found us worth the investment as didn’t only bring them solution through providing them brilliant web-CMS development and internet marketing services but also help them to achieve their business goals and customer contentment.  </p>
                    </div>


                </div>
            </div>




          </div>
          <div class="col-md-3">
            <div class="rightsection">
              <h2>Quick Services</h2>
              <ul>
                <li> <a href="javascript:void(0)">Website Design & Development</a> </li>
                <li> <a href="javascript:void(0)">Mobile App Development</a> </li>
                <li> <a href="javascript:void(0)">Game Development</a> </li>
                <li> <a href="javascript:void(0)">Digital Multilingual Marketing</a> </li>              
              </ul>
            </div>

            <div class="rightsection">
              <h2> Digital Marketing</h2>
              <ul>  
                <li> <a href="javascript:void(0)">Search Engine Optimization (SEO)</a> </li>
                <li> <a href="javascript:void(0)">Social Media Optimization (SMO)</a> </li>
                <li> <a href="javascript:void(0)">PPC Advertising</a> </li>
                <li> <a href="javascript:void(0)">Online Reputation Management Service</a> </li>
              </ul>
            </div>

            <div class="rightsection">
              <h2> Products</h2>
              <ul>  
                <li> <a href="javascript:void(0)">HRM Solutions</a> </li>
                <li> <a href="javascript:void(0)">Customer Relationship Management</a> </li>
                <li> <a href="javascript:void(0)">ERP Solutions</a> </li>
                <li> <a href="javascript:void(0)">Devnagri</a> </li>
              </ul>
            </div>


            .
          </div>
        </div>
      </div>
    </section>









  <?php include "request-form.php";?>
 <?php include 'include/footer.php'; ?>

<script type="text/javascript">
$(document).ready(function(){ 
    $("#myTab li:eq(4) a").tab('show');
});
</script>


<script>
 $('.panel-collapse').on('show.bs.collapse', function () {
    $(this).siblings('.panel-heading').addClass('active');
  });

  $('.panel-collapse').on('hide.bs.collapse', function () {
    $(this).siblings('.panel-heading').removeClass('active');
  });  
</script>



</body>
</html>


