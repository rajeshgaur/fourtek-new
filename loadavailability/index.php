<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="Fourtek, a leading IT consulting solutions provider in Noida, helping businesses in creating digital footprints. As a Top IT Software solutions company, it is offering reliable services.">
<title>IT Consulting, Software Solution Company, Delhi/Noida, India</title>
	<link rel="shortcut icon" type="image/icon" href="https://www.fourtek.com/images/fav.ico" />
	<style>
	.map-container{
overflow:hidden;
padding-bottom:56.25%;
position:relative;
height:0;
}
.map-container iframe{
left:0;
top:0;
height:100%;
width:100%;
position:absolute;
}
.map-container .img-fluid.logo {
    margin-top: 2%;
    max-width: 40%;
    margin-bottom: 2%;
    display: none !important;
}

.map-container .logoarea{position: absolute;
    z-index: 9999;
    width: 100%;
    background: #fff;
    height: 80px;
    top: 0;
    left: 20px;
    padding-top: 20px;}


@media (min-width: 992px){
.container {
    max-width: 960px;
}}
@media (min-width: 768px){
.container {
    max-width: 720px;
}}
@media (min-width: 576px){
.container {
    max-width: 540px;
}}
@media (min-width: 1200px){
.container {
    max-width: 1140px;
}}
.container {
    width: 100%;
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
}
	</style>
	
</head>
<body>
<div id="map-container" class="z-depth-1-half map-container" style="height: 100%">
<div class="logoarea">
<div class="container">
<a href="index.php" class="fourtek-logo"> <img src="https://www.fourtek.com/images/fourtek-logo.png" alt="Fourtek IT Solutions- IT consulting company"> </a>
</div>
</div>
  <iframe src="https://www.pier2pier.com/loadcalc/" frameborder="0"
    style="border:0" allowfullscreen></iframe>
</div>
</body>
</html>