<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="We are a 2009 founded company. We have a 100+ IT professionals team that delivered 2500+ mobile apps,5000+ e-commerce websites successfully.">
    <title>About - Fourtek</title>
    <link rel="canonical" href="https://www.fourtek.com/about-us">
    <link rel="shortcut icon" type="image/icon" href="images/fav.ico" />
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Pattaya|Poppins:300,300i,400" rel="stylesheet">
    <link href="css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
    <link href="css/animate.min.css" rel="stylesheet" type="text/css">
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link href="css/responsive.css" rel="stylesheet" type="text/css">
    <?php include "google-code.php";?>

  </head>


<style>
  header{background: url(images/about-bg.jpg) !important; background-size: cover !important;  background-repeat: no-repeat;background-attachment: fixed !important;}
</style>


  <body id="page-top" class="inner-page">
      <?php include 'include/menu.php' ; ?>
  
    <header class="masthead video">
      <div class="container h-100">
        <div class="row h-100">
          <div class="col-12 my-auto text-center text-white">          
            <h1 class="wow fadeInDown">About Us</h1>
            <p><strong>Discovering, Designing & Deploying Experience That Matters </strong><br>
            With more 10 years of experience, we are producing ‘just extraor
dinary’ experiences for brands that are as H.O.T (High Order Thinking) as they are efficacious. </p>
          </div>
        </div>
      </div>
    </header>

<section class="breadcrumb-block">
<div class="container">
  <div class="breadcrumb">
    <a class="breadcrumb-item" href="index.php">Home</a>
    <span class="breadcrumb-item active">About Us</span>
  </div>
</div>
</section>


    <!-- About Section -->
    <section class="about-sections">
      <div class="container">
        <h2><span>Our Story</span> – The Road So Far!</h2><br/>

      
       <div class="row"> 
        <div class="col-lg-6 col-md-12 wow fadeInLeft"><img src="images/about-img.jpg" alt="Fourtek-Digital Marketing Company in Delhi" class="img-fluid"> </div>
        <div class="col-lg-6 col-md-12">
           <p> <strong>Fourtek has been in the industry of Information Technology for more than 10 years, holding a core motive to make a change in the world by providing solutions perfectly crafted with intelligence and creativity.  Since its origin in 2009, the company has been awarded by many prestigious awards and successfully achieved multiple milestones. Bestowed with pure professionalism and the strong foothold in the industry, the company is gradually yet constantly spreading its legs across the world with leaving the rat race for the rest and striving to become better than the best!</strong></p>

           <p>Started from scratch, now we’re a group of 50+ young spirited, energetic and experienced professionals, who do everything they believe in. Our pool of trained and talented intellectuals works as ONE and think bigger to drive innovation and spice up our client’s brands. Over the years we have set up the long-lasting relationship with multiple brands and has established ourselves as a leading digital marketing company in Delhi. Our determination and dedication towards our work have established us as one of the legendary web solutions providers in the industry.</p>
        </div>

         <div class="col-sm-12"> 
           <h2><span>Our Motto</span> </h2>

		   <span class="line-blue"></span>
       <h5 style="color: #2d5387;">Do Good. Encourage!</h5>
           <p>We serve our people with solutions that actually performs for them and bring Good results for them. By extending our personal limits to outperform on expectations, we encourage our people to break the chains and step out of their comfort zone so that they may also grow with us and achieve their goals.</p>

			<br />
		   
           <h2> <span> Our Motivation Sources </span>  </h2>
		   <span class="line-blue"></span>
       <h5 style="color: #2d5387;">Work. Rest. Travel. Explore!</h5>
           <p>We passionately believe that only self-motivated souls can bring out the change in the world. So, we don’t remain a step back to encourage our team. We work, we travel, we eat good, we make friends, we take long walks and we fill up the enthusiasm, energy with the spirit of adventure in our people.</p>
         </div> 
     </div> 
      </div>
    </section>


  <section class="our-business">
    <div class="container">
        <h2>Our Business Philosophy</h2>
		
        <p>With our strong and unparalleled tech-skills and experience packed with inventive ideas and creativity, we offer the best Omni-solutions that make us the preferred choice amongst our customers.</p>

       <div class="row">
           <div class="col-md-3 col-sm-12 block-business">
              <img src="images/person-red.png"  alt=""> 
              <h5>Customer Satisfaction</h5>
              <p>We have acquired success in building long-term relationships with our clients by providing result-oriented solutions designed and developed with user-centric strategy.</p>
           </div>
           <div class="col-md-3 col-sm-12 block-business">
              <img src="images/prize.png"  alt=""> 
              <h5>Award Winning Performance</h5>
              <p>We also offer state-of-the-art web development services in Noida that perform well in the online world and drive the outstanding outcomes with enhanced ROI within a shorter span.</p>
           </div>
           <div class="col-md-3 col-sm-12 block-business">
              <img src="images/clock.png"  alt=""> 
              <h5>Timely Delivery</h5>
              <p>We transform your idea into reality, add our intellectuality, creativity & deliver the brilliant mobile apps development India and other solutions on time to prevent any last minute hassle.</p>
           </div>
           <div class="col-md-3 col-sm-12 block-business">
              <img src="images/search.png"  alt=""> 
              <h5>Quality Assurance</h5>
              <p>We ensure that you get the highest quality in each and every one of our solutions customized to match with your businesses’ expectations and requirements.</p>
           </div>                                  
       </div>  
    </div>
  </section>

<section class="about-sections business-descirption">
    <div class="container">

      <h2><span> Our </span> Leaders</h2><br/>
      <div class="row">    
	  
      <div class="col-sm-12 col-md-4 wow fadeInLeft" data-wow-duration="500ms">
        <div class="team_img">
          <img src="images/nakul.jpg" alt="" class="img-fluid">
            <div class="team_overlay text-center">
              <div class="team_overlay_top ">
                <h5>Nakul Kundra</h5>
                <p>Managing Director - Fourtek</p>
                <a href="#" class="tab" data-toggle="modal" data-target="#myModal">View Profile</a>
              </div>
            </div>
         </div>      
       </div>

      <div class="col-sm-12 col-md-4 wow fadeInLeft" data-wow-duration="1000ms">
        <div class="team_img">
          <img src="images/himanshu.jpg" alt="" class="img-fluid">
            <div class="team_overlay text-center">
              <div class="team_overlay_top ">
                <h5>Himanshu sharma</h5>
                <p>Chief Executive Officer</p>
                <a href="#" class="tab" data-toggle="modal" data-target="#myModal1">View Profile</a>
              </div>
            </div>
         </div>      
       </div>

      <div class="col-sm-12 col-md-4 wow fadeInLeft" data-wow-duration="1500ms">
        <div class="team_img">
          <img src="images/arpit.png" alt="" class="img-fluid">
            <div class="team_overlay text-center">
              <div class="team_overlay_top ">
                <h5>Arpit Sharma</h5>
                <p>Chief Development Officer</p>
                <a href="#" class="tab" data-toggle="modal" data-target="#myModal2">View Profile</a>
              </div>
            </div>
         </div>      
       </div>

    </div>
    </div>
  </section>



<div class="team_info_modal modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body member_info">
		<h4 class="modal-title">Nakul Kundra <span> (Chief Marketing Officer)</span></h4>
        <p>With having more than a decade of experience in the Technology & Marketing industry, Nakul Kundra is handling the Marketing and Finance department as CMO at Fourtek. Being the think-tank of the organization, he is taking care of all the finances and marketing operations. He constantly strives to keep his critical thinking skills sharp that help the team to perform well beyond the expectation.  Being a people person, he loves to connect with people and explore new things and technologies. Also, he can be found trying out new cuisines and traveling his way around the world.
 
 </p>
		<strong>Follow Us On :</strong>
		<ul>
		  <li><a href="https://in.linkedin.com/in/nakulfourtek" target="_blank" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
		  <li><a href="https://twitter.com/nakulkundra" target="_blank" title="Twitter"><i class="fa fa-twitter"></i></a></li>
		</ul>
		<button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
    </div>
  </div>
</div>


<div class="team_info_modal modal" id="myModal1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body member_info">
		<h4 class="modal-title">Himanshu sharma <span> (Chief Executive Officer)</span></h4>
        <p>Holding more than 9 years of experience as an entrepreneur, Himanshu Sharma is working as the CEO at Fourtek. Being Six Sigma Green Belt achiever and PMP (Project Management Certified Professional), he is responsible to take care of all the business operations including marketing, business strategy, Business intelligence and more. Having passion and love for technology, he constantly strives to bring unique and efficacious solutions in the market. When he is not busy handling business operations, he can be found playing video games, cricket and cooking as well. </p>
		<strong>Follow Us On :</strong>
		<ul>
		  <li><a href="https://www.linkedin.com/in/fourtek/" target="_blank" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
		  <li><a href="https://twitter.com/h_fourtek" target="_blank" title="Twitter"><i class="fa fa-twitter"></i></a></li>
		</ul>
		<button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>	    
    </div>
  </div>
</div>

<div class="team_info_modal modal" id="myModal2">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body member_info">
		<h4 class="modal-title">Arpit Sharma <span> (Chief Development Officer)</span></h4>
        <p>With 07 years of experience in Engineering & Marketing Industry, Arpit Sharma is currently working as Chief Development Officer at Fourtek. Initially, he started his career as an IT Engineer with core knowledge of PHP, Dot Net & Objective C. Gradually he enhanced his marketing skills as well while working with a renowned Japanese company OneDish. He explored the international market & learned multiple tactics. And now, he is using his intellectuality, sharpness & experience to take Fourtek on the edge of success. He is leading the marketing team and also taking care of the development team. From the initial step to the end process, he regularly checks up that everything goes as planned.  </p>
		<strong>Follow Us On :</strong>
		<ul>
		  <li><a href="https://in.linkedin.com/in/arpit-sharma-51884b50" target="_blank" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
		  <li><a href="javascript:void(0)" target="_blank" title="Twitter"><i class="fa fa-twitter"></i></a></li>
		</ul>
		<button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
    </div>
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
  </div>
</div>





   <?php include 'include/footer.php' ;?>

  </body>
</html>
