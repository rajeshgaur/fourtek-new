<?php error_reporting(1); 
require_once "../vendor/autoload.php";
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

if(isset($_POST['submit']))
{
  //$_POST=array();

$name = $_POST['name'];
$visitor_email = $_POST['email'];
$phone = $_POST['phone'];
$message = $_POST['message'];

//Create a new PHPMailer instance 
$mail = new PHPMailer;
$mail->isSMTP();
$mail->SMTPDebug = 0;
$mail->Host = 'smtp.gmail.com';
$mail->Port = 587;
$mail->SMTPSecure = 'tls';
$mail->SMTPAutoTLS = false;
$mail->SMTPAuth = true;
$mail->Username = "adwords@fourtek.com";
$mail->Password = "Fourtek@0101";
$mail->setFrom($visitor_email, 'Fourtek : Appfutura Get in Touch');
$mail->addAddress('himanshu.fourtek@gmail.com');
$mail->addAddress('arpit@fourtek.com');
$mail->addAddress('nakul@fourtek.com');
$mail->addAddress('sharmagaurav@fourtek.com');
$mail->Subject = 'Fourtek : Appfutura Get in Touch';

$mail->msgHTML('<html><head> </head><body> <table border="1" cellpadding="5">
        <tr> <td colspan="2"><strong>Your Name</strong></td>
          <td colspan="2">'.$_POST["name"].'</td>
        </tr>
        <tr>
          <td colspan="2"><strong>Your Email</strong></td>
          <td colspan="2">'.$_POST["email"].'</td>
        </tr>
        <tr>
          <td colspan="2"><strong>Your Phone</strong></td>
          <td colspan="2">'.$_POST["phone"].'</td>
        </tr>       
        <tr>
          <td colspan="2"><strong>Comment</strong></td>
          <td colspan="2">'.$_POST["message"].'</td>
        </tr>
      </table>
      </body>
      </html>');
  $errmsg = '';
  $succmsg = '';
      if (!$mail->send()) {
        $errmsg = "Mailer Error: " . $mail->ErrorInfo;
       } else {  
        $succmsg = "<div class='success-msg' style='color:green;'>Your Message has been Sent Successfully.</div>";
      /*  $form_data = array(
        'name' => $_POST['name'],
        'email' => $_POST['email'],
        'mobile' => $_POST['mobile'],
        'comment' => $_POST['comment']
          );
        $result = $db->insert_data('contact_us_form', $form_data);*/ 
       }
    } 


?>
<!DOCTYPE html>
<html class="wide wow-animation" lang="en">
  <head>
    <!-- Site Title-->
    <title>IT Consulting, Software Solution Company, Delhi/Noida, India</title>
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
   <link rel="icon" href="https://www.fourtek.com/images/fav.ico" type="image/x-icon">
    <!-- Stylesheets-->
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=PT+Sans:300,400,500,700%7CRaleway:700">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/style.css">
    <!--[if lt IE 10]>
    <div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;"><a href="https://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <script src="js/html5shiv.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <!-- Page Loader-->
    <div id="page-loader">
      <div class="page-loader-logo">
        <div class="brand">
          <div class="brand__name"><img src="images/logo-135x47.png" alt="" width="135" height="47"/>
          </div>
        </div>
      </div>
      <div class="page-loader-body">
        <div id="loadingProgressG">
          <div class="loadingProgressG" id="loadingProgressG_1"></div>
        </div>
      </div>
    </div>
    <!-- Page-->
    <div class="page">
      <!-- Page Header-->
      <header class="page-header section">
        <!-- RD Navbar-->
        <div class="rd-navbar-wrap">
          <nav class="rd-navbar rd-navbar-creative" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-sm-device-layout="rd-navbar-fixed" data-md-layout="rd-navbar-fullwidth" data-md-device-layout="rd-navbar-fixed" data-lg-device-layout="rd-navbar-fullwidth" data-lg-layout="rd-navbar-fullwidth" data-stick-up-clone="false" data-md-stick-up-offset="150px" data-lg-stick-up-offset="60px" data-md-stick-up="true" data-lg-stick-up="true">
            
            <div class="rd-navbar-main-outer">
              <div class="rd-navbar-main">
                <!-- RD Navbar Panel -->
                <div class="rd-navbar-panel">
                  <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar-nav-wrap"><span></span></button>
                  <!-- RD Navbar Brand-->
                  <div class="rd-navbar-brand"><a class="brand" href="https://www.fourtek.com/" target="_blank">
                      <div class="brand__name"><img src="images/logo-135x47.png" alt="" width="135" height="47"/>
                      </div></a></div>
                </div>
                <!-- RD Navbar Nav -->
                <div class="rd-navbar-nav-wrap">
                 
                  <!-- RD Navbar Nav-->
                  <ul class="rd-navbar-nav">
                    <li class="active"><a href="#home">Home</a>
                    </li>
                    <li><a href="#about">About Us</a>
                    </li>
                    <li><a href="#portfolio">Portfolio</a>
                    </li>
                    <li><a href="#services">Our Services</a>
                    </li>
                    <li><a href="#clients">Clients</a>
                    </li>
                    
                    <li><a href="#contacts">Contacts</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </nav>
        </div>
      </header>

      <!-- Swiper-->
      <section class="section section-layout-1" id="home" data-type="anchor">
        <div class="swiper-container swiper-slider swiper-slider_style-2" data-loop="true" data-autoplay="5000" data-simulate-touch="false" data-slide-effect="fade">
          <div class="swiper-wrapper">
            <div class="swiper-slide" data-slide-bg="images/slider-slide-1-1464x660.jpg">
              <div class="swiper-slide-caption">
                <div class="shell">
                  <div class="range range-center">
                    <div class="cell-sm-11 cell-md-10 cell-lg-12">
                      <h1 data-caption-animate="fadeInUpSmall"><span class="custom-line">Mobile App</span><br> <span>Development</span>
                      </h1>
                      <div class="object-decorated" style="width:100%">
                        <h4 data-caption-animate="fadeInRightSmall" data-caption-delay="550">We'll help you get your message to your clientby <br>placing advertising in the best print magazines <br>and newspapers in the country!</h4>
                      </div><a class="button button-primary" href="https://www.fourtek.com/mobile-app-development" target="_blank" data-caption-animate="fadeInUpSmall" data-caption-delay="850">learn more</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="swiper-slide" data-slide-bg="images/slider-slide-2-1464x660.jpg">
              <div class="swiper-slide-caption">
                <div class="shell">
                  <div class="range range-center">
                    <div class="cell-sm-11 cell-md-10 cell-lg-12">
                      <h1 data-caption-animate="fadeInUpSmall"><span class="custom-line">Website Design</span> <br><span>& Development</span>
                      </h1>
                       <div class="object-decorated" style="width:100%">
                        <h4 data-caption-animate="fadeInRightSmall" data-caption-delay="550">We'll help you get your message to your clientby <br>placing advertising in the best print magazines <br>and newspapers in the country!</h4>
                      </div><a class="button button-primary" href="https://www.fourtek.com/website-design-development-services" target="_blank" data-caption-animate="fadeInUpSmall" data-caption-delay="850">learn more</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
          <div class="swiper-pagination"></div>
        </div>
      </section>
      <!-- Experience since 1999-->
      <section class="section section-md bg-white" id="about" data-type="anchor">
        <div class="bg-gray-4">
          <div class="shell-fluid shell-condensed">
            <div class="range range-30 range-md-reverse">
              <div class="cell-sm-12 cell-md-6">
                <div class="section-md shell-fluid-cell">
                  <div class="box-centered box-width-1 box-custom">
                    <h2><span>Experience</span><span class="object-decorated object-decorated_inline" style="max-width: 125px;"><span class="heading-5">since 2009</span></span></h2>
                    <p>Fourtek has been in the industry of Information Technology for more than 10 years, holding a core motive to make a change in the world by providing solutions perfectly crafted with intelligence and creativity. With 10 years of mastership in website building, Fourtek is a perfect blend of seasoned professionals that gets your business ideology perfectly. Our comprehensive range of digital solutions is not just empowering businesses to outperform, but also building their brand’s reputation in the online world with a robust & strategic approach</p>
                    <div class="group-md group-middle button-group"><a class="button button-darker" href="#contacts">get a quote</a>
                     
                    </div>
                  </div>
                </div>
              </div>
              <div class="cell-sm-12 cell-md-6 reveal-flex">
                <div class="thumb-video"><img class="thumb-video__image" src="images/video-preview-962x465.jpg" alt="" width="962" height="465"/>
                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- Counters-->
      <section class="section parallax-container bg-gray-dark" data-parallax-img="images/parallax-3.jpg">
        <div class="parallax-content">
          <div class="section-md text-center">
            <div class="shell shell-wide">
              <ul class="list-blocks">
                <li class="list-blocks__item">
                  <div class="list-blocks__item-inner">
                    <article class="box-counter-modern">
                      <div class="box-counter-modern__wrap">
                        <div class="counter" data-zero="true">10</div><span class="big">+</span>
                      </div>
                      <p class="box-counter-modern__title">YEARS IN BUSINESS</p>
                    </article>
                  </div>
                </li>
                <li class="list-blocks__item">
                  <div class="list-blocks__item-inner">
                    <article class="box-counter-modern">
                      <div class="box-counter-modern__wrap">
                        <div class="counter" data-zero="true">300</div><span class="big">+</span>
                      </div>
                      <p class="box-counter-modern__title">IT PROFESSIONALS</p>
                    </article>
                  </div>
                </li>
                <li class="list-blocks__item">
                  <div class="list-blocks__item-inner">
                    <article class="box-counter-modern">
                      <div class="box-counter-modern__wrap"> <div class="counter" data-zero="true">500</div><span class="big">+</span>
                    </div>
                      <p class="box-counter-modern__title">CLIENTS WORLDWIDE</p>
                    </article>
                  </div>
                </li>
                <li class="list-blocks__item">
                  <div class="list-blocks__item-inner">
                    <article class="box-counter-modern">
                      <div class="box-counter-modern__wrap">
                        <div class="counter" data-zero="true">1500 </div><span class="big">+</span>
                      </div>
                      <p class="box-counter-modern__title">PROJECTS EXECUTED</p>
                    </article>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </section>

      <!-- Our Projects-->
      <section class="section section-md bg-white" id="portfolio" data-type="anchor">
        <div class="shell shell-wide">
          <div class="range range-md-center">
            <div class="cell-lg-11">
              <!-- Section Header-->
              <div class="section__header">
                <h2>Our Portfolio</h2>
                <div class="section__header-element"><a class="link link-arrow" href="https://www.fourtek.com/our-clients" target="_blank"><span>See All Projects</span></a></div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">

<ul class="related-products-carousel owl-carousel owl-theme">
                    <li class="slide-item"><figure class="image text-center"><center><img src="images/portfolio/1.jpg" alt="" class="imageauto"></center></figure></li>
                   <li class="slide-item"><figure class="image text-center"><center><img src="images/portfolio/2.jpg" alt="" class="imageauto"></center></figure></li>
                   <li class="slide-item"><figure class="image text-center"><center><img src="images/portfolio/3.jpg" alt="" class="imageauto"></center></figure></li>
                   <li class="slide-item"><figure class="image text-center"><center><img src="images/portfolio/4.jpg" alt="" class="imageauto"></center></figure></li>
                   <li class="slide-item"><figure class="image text-center"><center><img src="images/portfolio/5.jpg" alt="" class="imageauto"></center></figure></li>
                   <li class="slide-item"><figure class="image text-center"><center><img src="images/portfolio/6.jpg" alt="" class="imageauto"></center></figure></li>
                   <li class="slide-item"><figure class="image text-center"><center><img src="images/portfolio/7.jpg" alt="" class="imageauto"></center></figure></li>
                   <li class="slide-item"><figure class="image text-center"><center><img src="images/portfolio/8.jpg" alt="" class="imageauto"></center></figure></li>
                   <li class="slide-item"><figure class="image text-center"><center><img src="images/portfolio/9.jpg" alt="" class="imageauto"></center></figure></li>
                   <li class="slide-item"><figure class="image text-center"><center><img src="images/portfolio/10.jpg" alt="" class="imageauto"></center></figure></li>
                   <li class="slide-item"><figure class="image text-center"><center><img src="images/portfolio/11.jpg" alt="" class="imageauto"></center></figure></li>
                   <li class="slide-item"><figure class="image text-center"><center><img src="images/portfolio/12.jpg" alt="" class="imageauto"></center></figure></li>
                   
                </ul>

        
      </div></div>
      </section>
      <!-- Stocks Rating-->
      <section class="section section-lg bg-white">
        <div class="shell shell-wide">
          <div class="range range-md-center">
            <div class="cell-lg-11">
              <div class="range range-sm-center range-md-left range-md-middle range-50">
                <div class="cell-sm-10 cell-md-5 cell-lg-5">
                  <h2>Why Choose Us</h2>
                  <p>Fourtek IT Solutions Pvt. Ltd. is committed to helping its clients reach their goals, to personalizing their development experiences, to providing an innovative solution, and to making a difference. </p>
<p>Our strong sense of identification with client projects means that we are constantly striving to provide solutions, even for issues they aren’t yet aware of. To this end, we adopt a progressive approach to technology and marketing techniques.  </p>
                
                </div>
                <div class="cell-sm-10 cell-md-7 cell-xl-6">
                  <center><img src="images/rightimage.jpg"></center>

                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
     

      <!-- Our production-->
      <section class="section section-md bg-white" id="services" data-type="anchor">
        <div class="shell">
          <div class="tabs-custom tabs-custom-init tabs-inline" id="tabs-production">
            <!-- Section Header-->
            <div class="section__header">
              <h2>Our Services</h2>
              <div class="section__header-element">
                <!-- Nav tabs-->
                <div class="navigation-custom">
                  <button class="button navigation-custom__toggle" data-custom-toggle=".navigation-custom__content" data-custom-toggle-hide-on-blur="true">Filter</button>
                  <div class="navigation-custom__content">
                    <ul class="nav nav-tabs">
                      <li class="active"><a href="#tabs-production-1" data-toggle="tab"> Website Design & Development</a></li>
                      <li><a href="#tabs-production-2" data-toggle="tab"> Mobile App Development</a></li>
                      <li><a href="#tabs-production-3" data-toggle="tab"> Digital Marketing</a></li>
                      <li><a href="#tabs-production-4" data-toggle="tab">Custom ERP</a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <!-- Tab panes-->
            <div class="tab-content">
              <div class="tab-pane fade in active" id="tabs-production-1">
                <div class="layout-horizontal layout-horizontal_md-reverse">
                  <div class="layout-horizontal__main">
                    <h4> Website Design & Development</h4>
                   <p>Web design & development industry is growing faster. All over the world, there are millions of websites running online and thousands of websites are getting developed every day that are intensifying the competition. Due to which, entrepreneurs are facing difficulties in getting right brand recognition in the online world. As a matter of fact, if you want to make your business site noticeable, it should be developed with innovative ideas, brilliant layouts, and cutting-edge technologies.</p>
                    <a class="button button-primary" href="https://www.fourtek.com/website-design-development-services" target="_blank">Read More</a>
                  </div>
                  <div class="layout-horizontal__aside">
                   
                    <div class="slick-image"><img src="images/websitedesign.jpg" alt=""/>
                          </div>
                   
                  </div>
                </div>
              </div>
              <div class="tab-pane fade" id="tabs-production-2">
                <div class="layout-horizontal layout-horizontal_md-reverse">
                  <div class="layout-horizontal__main">
                    <h4>Mobile App Development</h4>
                    <p>In this technology-driven world, mobile applications are not only transforming the way we live but also changing the multiple facets of businesses. Every day, a large number of applications are getting downloaded and used by millions of people. This ultimate growth of this industry is encouraging entrepreneurs, like you to invest in this realm for expanding business and enhancing its reach.</p>
                    <a class="button button-primary" href="https://www.fourtek.com/mobile-app-development" target="_blank">Read More</a>
                  </div>
                  <div class="layout-horizontal__aside">
                     <div class="slick-image"><img src="images/mobileapp.jpg" alt=""/>
                          </div>
                  </div>
                </div>
              </div>
              <div class="tab-pane fade" id="tabs-production-3">
                <div class="layout-horizontal layout-horizontal_md-reverse">
                  <div class="layout-horizontal__main">
                    <h4>Digital Marketing</h4>
                    <p>Internet market has seen the influence of English in promotional and non-promotional activities. But, since the modernization of internet customers belonging to different lingual groups have risen potentially and now service providers are moving towards multilingual digital marketing. At Fourtek, we are nailing the odds and striving to be the best digital multilingual marketing company in delhi/ Noida. </p>
                    <a class="button button-primary" href="https://www.fourtek.com/digital-multilingual-marketing" target="_blank">Read More</a>
                  </div>
                  <div class="layout-horizontal__aside">
                     <div class="slick-image"><img src="images/digitalmarketing.jpg" alt=""/>
                          </div>
                  </div>
                </div>
              </div>
              <div class="tab-pane fade" id="tabs-production-4">
                <div class="layout-horizontal layout-horizontal_md-reverse">
                  <div class="layout-horizontal__main">
                    <h4>Custom ERP</h4>
                    <p>It integrates all the facets of operations (including planning, distribution, invoice tracking, purchase management, manufacturing, warehouse management, logistics, software etc.) Also, it assists enterprises in bringing significantly effective results. Therefore, multiple organizations are hiring ERP software solutions providers so that they can take their business to the next level of success.</p>
                     <a class="button button-primary" href="https://www.fourtek.com/erp-software-solutions" target="_blank">Read More</a>
                  </div>
                  <div class="layout-horizontal__aside">
                  <div class="slick-image"><img src="images/erp.jpg" alt=""/>
                          </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
     
      <!-- Clients-->
      <section class="section section-md bg-white text-center" id="clients" data-type="anchor">
        <div class="shell shell-wide">
          <div class="range range-md-center">
            <div class="cell-lg-11">
              <!-- Section Header-->
              <div class="section__header">
                <h2>Clients</h2>
                <div class="section__header-element"><a class="link link-arrow" href="https://www.fourtek.com/our-clients" target="_blank"><span>View More</span></a></div>
              </div>
            </div>
          </div>
        </div>
        <div class="shell-fluid">
          <div class="range range-condensed range-md-reverse range-flex">
            <div class="cell-md-6  bg-image" style="background-image: url(images/parallax-5.jpg);">
              <div class="section-variant-1">
                <!-- Owl Carousel-->
                <div class="owl-carousel owl-carousel_style-1" data-items="1" data-autoplay="true" data-dots="true" data-nav="true" data-stage-padding="0" data-loop="true" data-margin="30" data-mouse-drag="false">
                  <div class="item">
                    <!-- Quote light-->
                    <article class="quote-light">
                     
                      <div class="quote-light__main">
                        <p class="q">"The digital marketing team is quite amazing.<br> Had a wonderful experience with Fourtek"
</p>
                        <ul class="quote-light__meta">
                          <li><span class="cite">- Rahul Sharma -</span></li>
                          
                        </ul>
                      </div>
                    </article>
                  </div>
                  <div class="item">
                    <!-- Quote light-->
                    <article class="quote-light">
                     
                      <div class="quote-light__main">
                        <p class="q">"Best SEO and Social Media service provider. Good Knowledge in <br>digital marketing platform.Thank you Fourtek"

</p>
                        <ul class="quote-light__meta">
                          <li><span class="cite">- Zini Sharma -</span></li>
                         
                        </ul>
                      </div>
                    </article>
                  </div>
                  <div class="item">
                    <!-- Quote light-->
                    <article class="quote-light">
                     
                      <div class="quote-light__main">
                        <p class="q">"I am very happy with the results of SEO, AdWords and Social Media.<br> Really Appreciate your Hard Work."
</p>
                        <ul class="quote-light__meta">
                          <li><span class="cite">- Paras Vohra -</span></li>
                          
                        </ul>
                      </div>
                    </article>
                  </div>
                </div>
              </div>
            </div>
            <div class="cell-md-6">
              <div class="link-block-group">
<div class="owl-carousel owl-carousel_style-1" data-items="1" data-autoplay="true" data-dots="true" data-nav="true" data-stage-padding="0" data-loop="true" data-margin="30" data-mouse-drag="false">
                  <div class="item">
                    <!-- Quote light-->
                   <a class="link-block" href="#"><img src="images/brand-1.png" alt="" width="216" height="102"/></a><a class="link-block" href="#"><img src="images/brand-2.png" alt="" width="216" height="102"/></a>
                   <a class="link-block" href="#"><img src="images/brand-3.png" alt="" width="216" height="102"/></a><a class="link-block" href="#"><img src="images/brand-4.png" alt=""width="216" height="102"/></a>
                  </div>
                 
                  <div class="item">
                    <!-- Quote light-->
                    <a class="link-block" href="#"><img src="images/brand-5.png" alt="" width="216" height="102"/></a><a class="link-block" href="#"><img src="images/brand-6.png" alt="" width="216" height="102"/></a>
                   <a class="link-block" href="#"><img src="images/brand-7.png" alt="" width="216" height="102"/></a><a class="link-block" href="#"><img src="images/brand-8.png" alt=""width="216" height="102"/></a>
                  </div>
                </div>



                </div>
            </div>
          </div>
        </div>
      </section>
      
      
      <!-- Get in Touch-->
      <section class="section section-lg bg-white" id="contacts" data-type="anchor">
        <div class="shell">
          <div class="layout-bordered">
            <div class="layout-bordered__main text-center">
              <div class="layout-bordered__main-inner">
                <h2>Get in Touch</h2>
                <p>We are available 24/7. You can also use our quick contact form to ask a question about our services and projects.</p>
                <!-- RD Mailform-->
                <p style="color:red;"><?php echo $errmsg;?></p>
         <p style="color:green;"><?php echo $succmsg;?></p>
                <form  name="myemail" data-form-output="form-output-global" data-form-type="contact" method="post">
                  <div class="range range-sm-bottom range-20">
                    <div class="cell-sm-6">
                      <div class="form-wrap">
                        <input class="form-input" id="contact-first-name" type="text" name="name" data-constraints="@Required">
                        <label class="form-label" for="contact-first-name">First name</label>
                      </div>
                    </div>
                    <div class="cell-sm-6">
                      <div class="form-wrap">
                        <input class="form-input" id="contact-phone" type="text" name="phone" data-constraints="@Numeric @Required">
                        <label class="form-label" for="contact-phone">Phone</label>
                      </div>
                    </div>
                    <div class="cell-xs-12">
                      <div class="form-wrap">
                        <label class="form-label" for="contact-message">Your Message</label>
                        <textarea class="form-input" id="contact-message" name="message" data-constraints="@Required"></textarea>
                      </div>
                    </div>
                    <div class="cell-sm-6">
                      <div class="form-wrap">
                        <input class="form-input" id="contact-email" type="email" name="email" data-constraints="@Email @Required">
                        <label class="form-label" for="contact-email">E-mail</label>
                      </div>
                    </div>
                    <div class="cell-sm-6">
                      <input class="button button-block button-primary" type="submit" name="submit" value="Send Message">
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <div class="layout-bordered__aside">
              <div class="layout-bordered__aside-item">
                <p class="heading-8">Get social</p>
                <ul class="list-inline-xs">
                  <li><a class="icon icon-sm icon-default fa fa-facebook" href="https://www.facebook.com/Fourtek/" target="_blank"></a></li>
                  <li><a class="icon icon-sm icon-default fa fa-twitter" href="https://twitter.com/Fourtek" target="_blank"></a></li>
                  <li><a class="icon icon-sm icon-default fa fa-instagram" href="https://www.instagram.com/fourtek_india/" target="_blank"></a></li>
                  <li><a class="icon icon-sm icon-default fa fa-linkedin" href="https://www.linkedin.com/company/fourtekitsolutions" target="_blank"></a></li>
                </ul>
              </div>
              <div class="layout-bordered__aside-item">
                <p class="heading-8">Phone</p>
                <div class="unit unit-horizontal unit-spacing-xxs">
                  <div class="unit__left"><span class="icon icon-sm icon-primary material-icons-local_phone"></span></div>
                  <div class="unit__body"><a href="tell:+919899846974">+91-989-9846-974</a></div>
                </div>
              </div>
              <div class="layout-bordered__aside-item">
                <p class="heading-8">E-mail</p>
                <div class="unit unit-horizontal unit-spacing-xxs">
                  <div class="unit__left"><span class="icon icon-sm icon-primary mdi mdi-email-outline"></span></div>
                  <div class="unit__body"><a href="mailtto:info@fourtek.com">info@fourtek.com</a></div>
                </div>
              </div>
              <div class="layout-bordered__aside-item">
                <p class="heading-8">Address</p>
                <div class="unit unit-horizontal unit-spacing-xxs">
                  <div class="unit__left"><span class="icon icon-sm icon-primary material-icons-location_on"></span></div>
                  <div class="unit__body">Fourtek IT Solutions Pvt. Ltd.<br>
B86, Sector 60, Noida <br>( Uttar Pradesh) 201301 India</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- RD Google Map-->
      <section class="section">
        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14011.731390119505!2d77.3660175!3d28.6017913!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x9f1b0b68a91d0992!2sFourtek%20IT%20Solutions%20Private%20Limited!5e0!3m2!1sen!2sin!4v1573196902371!5m2!1sen!2sin" width="100%" height="350" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
      </section>
      <!-- Page Footer-->
      <footer class="footer-corporate">
        
        <div class="footer-corporate__aside bg-gray-base text-center">
          <div class="shell shell-fluid shell-condensed">
            <div class="range range-20 range_xl-ten footer-corporate__range">
              <div class="cell-sm-8 cell-xl-6 footer-corporate__aside-column text-sm-left">
                <!-- Rights-->
                <p class="rights">Copyright &copy; 2019 <a href="#" target="_blank">Fourtek IT Solutions</a>. all right reserved
                </p>
              </div>
              <div class="cell-sm-4 cell-xl-4 footer-corporate__aside-column text-sm-right">
                <ul class="list-inline-xxs">
                  
                  <li><a class="icon icon-xs icon-style-modern fa fa-facebook" href="https://www.facebook.com/Fourtek/" target="_blank"></a></li>
                  <li><a class="icon icon-xs icon-style-modern fa fa-twitter" href="https://twitter.com/Fourtek" target="_blank"></a></li>
                  <li><a class="icon icon-xs icon-style-modern fa fa-instagram" href="https://www.instagram.com/fourtek_india/" target="_blank"></a></li>
                  <li><a class="icon icon-xs icon-style-modern fa fa-linkedin" href="https://www.linkedin.com/company/fourtekitsolutions" target="_blank"></a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </footer>

    </div>
    <!-- Global Mailform Output-->
    <div class="snackbars" id="form-output-global"></div>
    <!-- Javascript-->
    <script src="js/core.min.js"></script>
    <script src="js/script.js"></script>
  <script src="js/jquery.film_roll.js"></script> 
  <script type="text/javascript">// Generated by CoffeeScript 1.6.3
if ($('.related-products-carousel').length) {
    $('.related-products-carousel').owlCarousel({
      loop:true,
      margin:30,
      nav:true,
      smartSpeed: 500,
      autoplay: true,
      navText: [ '<span class="flaticon-next-1"></span>', '<span class="flaticon-next-1"></span>' ],
      responsive:{
        0:{
          items:1
        },
        600:{
          items:1
        },
        768:{
          items:2
        },
        1024:{
          items:3
        },
        1280:{
          items:3
        }
      }
    });       
  }

</script>

  
  </body>
</html>