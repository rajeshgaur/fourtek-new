<!DOCTYPE html>
<html lang="en">
 
<head>
<?php
error_reporting(0);
require_once("admin/functions/user_list.php");
$db = new Database();
$case_Id = 1;
$result = $db->selectdata("case_study", "WHERE id=$case_Id");
if (!empty($result)) {
    $row = $result->fetch_assoc();
}?>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="One of India’s largest kitchen appliances companies- Prestige was having a website a with bygone UI and was experiencing increased bounce rate.">
    <title>Prestige Case Study - Fourtek Work Report</title>
    <link rel="shortcut icon" type="image/icon" href="images/fav.ico" />
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Pattaya|Poppins:300,300i,400" rel="stylesheet">
    <link href="css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
    <link href="css/animate.min.css" rel="stylesheet" type="text/css">
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link href="css/responsive.css" rel="stylesheet" type="text/css">
    <?php include "google-code.php";?>
  </head>
  <body id="page-top" class="inner-page">
      <?php include 'include/menu.php'; ?>  
 <header class="masthead video" style="background: url('admin/uploads/<?php echo $row["bimage"];?>') !important;background-size: cover !important;
background-repeat: no-repeat;
background-attachment: fixed !important;">
  <div class="container h-100">
    <div class="row h-100">
      <div class="col-12 my-auto text-center text-white">          
        <h1 class="wow fadeInDown"><?php echo $row['title']?></h1>
        <p><?php echo $row['banner-text']?></p>
        <p><a href="javascript:;" data-toggle="modal" id="bnrst" data-target="#exampleModal" class="btn-fourtek wow fadeInUpBig">Request a Quote</a></p>
      </div>
    </div>
  </div>
</header>
    <section class="breadcrumb-block">
    <div class="container">
      <div class="breadcrumb">
        <a class="breadcrumb-item" href="index.php">Home</a>
        <span class="breadcrumb-item active"><?php echo $row['title']?></span>
      </div>
    </div>
    </section>
<section class="padd_80">
<div class="container">
  <div class="row">
    <div class="col-md-9">
      <!-- <div class="slider"><img src="admin/uploads/<?php echo $row['pimage'];?>" class="img-responsive"></div> -->
<div class="bs-example">
  <!-- <ul class="nav nav-tabs" id="myTab">
      <li><a data-toggle="tab" href="#sectionA" class="active">About Client</a></li>
      <li><a data-toggle="tab" href="#sectionB">Project Overview</a></li>
      <li><a data-toggle="tab" href="#sectionC">Identifying The Problem Statement</a></li>
      <li><a data-toggle="tab" href="#sectionD">Client Benefits</a></li>
  </ul> -->
<div class="caseneheading">
<h3>About Client</h3>
<div id="sectionA" class="tab-pane active">
<?php echo $row['about_client']; ?>
</div>
<div id="sectionB" class="tab-pane">
<?php echo $row['overview']; ?>
</div>
<div id="sectionC" class="tab-pane">
<?php echo $row['statment'];?>
</div>  
<div id="sectionD" class="tab-pane">
  <?php echo $row['benifit'];?></div>
</div>
</div>
</div>
<div class="col-md-3">
<div class="rightsection">
  <h2>Quick Services</h2>
  <ul>
    <li> <a href="website-design-development-service-company-in-delhi">Website Design & Development</a> </li>
    <li> <a href="mobile-apps-development-companies-in-india">Mobile App Development</a> </li>
    <li> <a href="mobile-game-development-company-india">Game Development</a> </li>
    <li> <a href="digital-multilingual-marketing-company-noida-india">Digital Multilingual Marketing</a> </li> 
  </ul>
</div>
<div class="rightsection">
  <a href="digital-marketing-company-in-noida-delhi"><h2> Digital Marketing</h2></a>
  <ul>  
    <li><a href="top-seo-service-in-noida-delhi">Search Engine Optimization (SEO)</a></li>
    <li><a href="smo-services-company-in-noida">Social Media Optimization (SMO)</a></li>
    <li> <a href="ppc-service-company-in-noida">PPC Advertising</a> </li>
    <li> <a href="online-reputation-management-services-company-delhi">Online Reputation Management Service</a> </li>
  </ul>
</div>
<div class="rightsection">
  <h2> Products</h2>
  <ul>  
    <li><a href="human-resources-management-software-solutions">HRM Solutions</a></li>
    <li><a href="crm-software-services-india">Customer Relationship Management</a></li>
    <li><a href="enterprise-resource-planning-software-solution">ERP Solutions</a></li>
    <li><a href="devnagri-online-translation-platform">Devnagri</a></li>
  </ul>
</div>
</div>
</div>
</div>
</section>
  <?php include "request-form.php";?>
 <?php include 'include/footer.php'; ?>
<script type="text/javascript">
$(document).ready(function(){ 
    $("#myTab li:eq(4) a").tab('show');
});
</script>
<script>
 $('.panel-collapse').on('show.bs.collapse', function () {
    $(this).siblings('.panel-heading').addClass('active');
  });
  $('.panel-collapse').on('hide.bs.collapse', function () {
    $(this).siblings('.panel-heading').removeClass('active');
  });  
</script>
</body>
</html>
