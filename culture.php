<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="We celebrate each and every festival at our headquarters. We bring happiness to our employees faces through games, quiz & many other activities.">
  <title>Culture - Fourtek</title>
  <link rel="canonical" href="https://www.fourtek.com/culture">
  <link rel="shortcut icon" type="image/icon" href="images/fav.ico" />
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Pattaya|Poppins:300,300i,400" rel="stylesheet">
  <link href="css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
  <link href="css/animate.min.css" rel="stylesheet" type="text/css">
  <link href="css/style.css" rel="stylesheet" type="text/css">
  <link href="css/responsive.css" rel="stylesheet" type="text/css">
  <link href="css/custom-bootstrap-margin-padding.css" rel="stylesheet" type="text/css">

  <?php include "google-code.php";?>
</head>


<style>
  header{background: url(images/culturepage-banner.jpg) !important; background-size: 100% !important;  background-repeat: no-repeat;background-attachment: fixed !important;}

  .infro-service{background: url(images/bg-service.jpg) !important; background-size: cover !important;  background-repeat: no-repeat;background-attachment: fixed !important;}
</style>


<body id="page-top" class="inner-page">
  <?php include 'include/menu.php' ; ?>

  <header class="masthead video">
    <div class="container h-100">
      <div class="row h-100">
        <div class="col-12 my-auto text-center text-white">
          <h1 class="wow fadeInDown">Join The Coded Wisdom Of Fourtek Culture</h1>
          <p>We weave the culture artistically and focus on the continuous pursuit of building the best, most talented and the happiest team we possibly can. </p>
        </div>
      </div>
    </div>
  </header>

  <section class="breadcrumb-block">
    <div class="container">
      <div class="breadcrumb">
        <a class="breadcrumb-item" href="index.php">Home</a>
        <span class="breadcrumb-item active">Culture Page</span>
      </div>
    </div>
  </section>



  <section>
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-4 p-0"><img src="images/blood-donation-camp1.jpg" class="img-fluid"></div>
        <div class="col-md-4 p-0">
          <div class="csrtext">
            <h3>Fourtek CSR</h3>
            <p>With a core motive to contribute to the advancement of the society, Fourtek has organized different events for social services including donation of the pads to underprivileged women - as pad is for hygienic safety of women, clothes donation to kerala flood victims, keto donation (fundraising for surgery), donation to NGO for children education and food donation for underprivileged children.</p>

          </div>
        </div>
        <div class="col-md-4 p-0"><img src="images/blood-donation-camp2.jpg" class="img-fluid"></div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row">
        <div class="col-md-4 p-0">
          <div class="csrtext colour2">
            <h3>Rewards & Recognition</h3>
            <p>We believe that this is the finest way to say “you are our rockstar”. Every person needs encouragement and appreciation for achieving the greater good in their life. Hence, we at Fourtek, motivate our family to push their limits, overcome their fears, break the rules and attain their unrivaled goals. We abet them not to be the same but even better.</p>

          </div>
        </div>
        <div class="col-md-4 p-0"><img src="images/rewardsrecognition1.jpg" class="img-fluid"></div>
        <div class="col-md-4 p-0"><img src="images/rewardsrecognition2.jpg" class="img-fluid"></div>
      </div>

      <div class="row imgborder">
        <div class="col-md-2 p-0"><img src="images/rewardsrecognition3.jpg" class="img-fluid"></div>
        <div class="col-md-2 p-0"><img src="images/rewardsrecognition4.jpg" class="img-fluid"></div>
        <div class="col-md-2 p-0"><img src="images/rewardsrecognition5.jpg" class="img-fluid"></div>
        <div class="col-md-2 p-0"><img src="images/rewardsrecognition6.jpg" class="img-fluid"></div>
        <div class="col-md-2 p-0"><img src="images/rewardsrecognition7.jpg" class="img-fluid"></div>
        <div class="col-md-2 p-0"><img src="images/rewardsrecognition8.jpg" class="img-fluid"></div>
        <div class="col-md-2 p-0"><img src="images/rewardsrecognition9.jpg" class="img-fluid"></div>
        <div class="col-md-2 p-0"><img src="images/rewardsrecognition10.jpg" class="img-fluid"></div>
        <div class="col-md-2 p-0"><img src="images/rewardsrecognition11.jpg" class="img-fluid"></div>
        <div class="col-md-2 p-0"><img src="images/rewardsrecognition12.jpg" class="img-fluid"></div>
        <div class="col-md-2 p-0"><img src="images/rewardsrecognition13.jpg" class="img-fluid"></div>
        <div class="col-md-2 p-0"><img src="images/rewardsrecognition14.jpg" class="img-fluid"></div>
      </div>









      <div class="row">
        <div class="col-md-8 p-0">
          <div class="fourtekgallery">
            <h3>Fun With Fourtek</h3>
            <p>We respect every belief, faith, custom, ritual, religion, and language. We understand that actions may not bring happiness but happiness doesn’t come without actions. Therefore, we work with immense focus and passion. We put together all the bits and pieces and create the art that inspires others. Along with working with full dedication, we know how to enjoy every single moment. We find the joy in the smallest pieces and bring fun at workplace. We celebrate every festival and cherish every moment to live up life at Fourtek to the fullest.</p>
          </div>
        </div>
        <div class="col-md-4 p-0">
          <div class="filter-button-group" role="group" aria-label="filterImages">

            <button type="button" class="is-checked" data-filter="*"><i class="fa fa-link"></i></button>

            <button type="button" class="btn active" data-filter=".fourtekpicnic"> <img src="images/c-icon1.png">
              Picnic </button>

            <button type="button" class="btn" data-filter=".games"> <img src="images/c-icon2.png"> Games </button>

            <button type="button" class="btn" data-filter=".fgt"> <img src="images/c-icon3.png"> Fourtek Got Talent
            </button>

            <button type="button" class="btn" data-filter=".celebratingfestivals"> <img src="images/c-icon4.png">
              Celebrating festivals </button>

          </div>
        </div>
      </div>

      <div class="grid">

        <div class="grid-sizer col-xs-12 col-sm-6 col-md-4 col-lg-4"></div>

        <div class="col-xs-12 col-sm-6 col-md-2 grid-item celebratingfestivals p-0">
          <img class="thumbnail img-fluid" src="images/g17.jpg" alt="">
          <strong>Diwali Festival</strong>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-2 grid-item games p-0">
          <img class="thumbnail img-fluid" src="images/g11.jpg" alt="">
          <strong>Tower building game</strong>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-2 grid-item games p-0">
          <img class="thumbnail img-fluid" src="images/g10.jpg" alt="">
          <strong>Tower building game</strong>
        </div>


        <!-- Fourtek Got Talent -->
        <div class="col-xs-12 col-sm-6 col-md-2 grid-item fgt p-0">
          <img class="thumbnail img-fluid" src="images/g12.jpg" alt="">
          <strong>singing competition winner</strong>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-2 grid-item fgt p-0">
          <img class="thumbnail img-fluid" src="images/g13.jpg" alt="">
          <strong>singing competition winner</strong>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-2 grid-item fgt p-0">
          <img class="thumbnail img-fluid" src="images/g14.jpg" alt="">
          <strong>piano competition winner</strong>
        </div>

        <!-- Celebrating festivals -->
        <div class="col-xs-12 col-sm-6 col-md-2 grid-item celebratingfestivals p-0">
          <img class="thumbnail img-fluid" src="images/g15.jpg" alt="">
          <strong>christmas celebration</strong>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-2 grid-item celebratingfestivals p-0">
          <img class="thumbnail img-fluid" src="images/g16.jpg" alt="">
          <strong>yoga day</strong>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-2 grid-item celebratingfestivals p-0">
          <img class="thumbnail img-fluid" src="images/g18.jpg" alt="">
          <strong>independence day celebration</strong>
        </div>

        <!-- Picnic -->
        <div class="col-xs-12 col-sm-6 col-md-2 grid-item fourtekpicnic p-0">
          <img class="thumbnail img-fluid" src="images/g1.jpg" alt="">
          <strong>back to being kids again</strong>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-2 grid-item fourtekpicnic p-0">
          <img class="thumbnail img-fluid" src="images/g2.jpg" alt="">
          <strong>Fourtek day out</strong>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-2 grid-item fourtekpicnic p-0">
          <img class="thumbnail img-fluid" src="images/g3.jpg" alt="">
          <strong>picnic</strong>
        </div>


        <!-- Games -->
        <div class="col-xs-12 col-sm-6 col-md-2 grid-item celebratingfestivals p-0">
          <img class="thumbnail img-fluid" src="images/g4.jpg" alt="">
          <strong>CHRISTMAS CELEBRATION</strong>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-2 grid-item games p-0">
          <img class="thumbnail img-fluid" src="images/g5.jpg" alt="">
          <strong>Games</strong>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-2 grid-item celebratingfestivals p-0">
          <img class="thumbnail img-fluid" src="images/g6.jpg" alt="">
          <strong>CHRISTMAS CELEBRATION</strong>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-2 grid-item celebratingfestivals p-0">
          <img class="thumbnail img-fluid" src="images/g7.jpg" alt="">
          <strong>CHRISTMAS CELEBRATION</strong>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-2 grid-item celebratingfestivals p-0">
          <img class="thumbnail img-fluid" src="images/g8.jpg" alt="">
          <strong>Secret Santa</strong>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-2 grid-item games p-0">
          <img class="thumbnail img-fluid" src="images/g9.jpg" alt="">
          <strong>Tower building game</strong>
        </div>
      </div>




    </div>

  </section>





  <?php include 'include/footer.php' ;?>


  <script src="https://npmcdn.com/isotope-layout@3.0/dist/isotope.pkgd.min.js"></script>

  <script>
    //need this to deactivate lightbox on small screens
    $(document).ready(function() {

      lightboxOnResize();

    });

    $(window).resize(function() {
      lightboxOnResize();

    });

    //***ISOTOPE***
    // init Isotope
    var $grid = $('.grid').isotope({
      itemSelector: '.grid-item',
      layoutMode: 'masonry'
    });

    // filter items on button click
    $('.filter-button-group').on('click', 'button', function() {
      var filterValue = $(this).attr('data-filter');
      $grid.isotope({
        filter: filterValue
      });
    });



    // change is-checked class on buttons
    $('.btn-group').each(function(i, buttonGroup) {
      var $buttonGroup = $(buttonGroup);
      $buttonGroup.on('click', 'button', function() {
        $buttonGroup.find('.is-checked').removeClass('is-checked');
        $(this).addClass('is-checked');
      });
    });


    function lightboxOnResize() {

      if ($(window).width() < 768) {

        $('a[rel="prettyPhoto[portfolio]"]')
          .removeAttr('rel')
          .addClass('lightboxRemoved');

        $('a.lightboxRemoved').click(function(event) {
          event.preventDefault();
          console.log("test");
        });
        // $("a[rel='prettyPhoto[portfolio]']").unbind('click');

      } else {

        $('a.lightboxRemoved').attr('rel', 'prettyPhoto[portfolio]').removeClass("lightboxRemoved");
        $("a[rel='prettyPhoto[portfolio]']").prettyPhoto({
          theme: "light_square",
        });

      }
    }
  </script>





</body>

</html>