<?php
session_start();
if (!$_SESSION['user_id']) {
    header('location:index.php');
}
include "header.php";
require_once("functions/user_list.php");
include "sidebar-menu.php";

$id = $_REQUEST['id'];

?>
  
<div id="content">
<div id="content-header">
  <div id="breadcrumb"> <a href="dashboard.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
  <a href="contact_us_form_list.php" class="tip-bottom">Users list</a> 
  <a href="#" class="current">View contact</a> 
</div>
  <!--<h1>Common Form Elements</h1>-->
</div>
<div class="container-fluid">
  <!--<hr>-->
  <?php if (isset($suc_msg)) {
    echo $suc_msg;
} $suc_msg = ''; ?>
  <?php if (isset($err_msg)) {
    echo $err_msg;
} $err_msg = ''; ?>
  <div class="row-fluid">
    <div class="span12">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>View blog</h5>
        </div>

        <?php $db = new Database();
        $result = $db->selectdata("contact_us_form", "where id = '$id'");
        while ($row = $result->fetch_array()) {
            ?>

        <div class="widget-content nopadding">
          <form action="" method="post" class="form-horizontal">
            <input type="hidden" name="table_name" value="blog" />
            <div class="control-group">
              <label class="control-label">User Name* :</label>
              <div class="controls">
       <?php if ($row['name']) {
                echo $row['name'];
            } ?>
               
              </div>
            </div> 
             <div class="control-group">
              <label class="control-label">Email* :</label>
              <div class="controls">
                 <?php if ($row['email']) {
                echo $row['email'];
            } ?>
                 
              </div>
            </div> 
            <div class="control-group">
              <label class="control-label">Mobile* :</label>
              <div class="controls">
                 <?php if ($row['mobile']) {
                echo $row['mobile'];
            } ?>
                 
              </div>
            </div> 
          <div class="control-group">
              <label class="control-label">Comment :</label>
              <div class="controls">
                 <?php if ($row['comment']) {
                echo $row['comment'];
            } ?>
                 
              </div>
            </div> 

            
            


           
          </form>
        </div>
        <?php
        } ?>
      </div>
      
    </div>
    
  </div>
 
</div></div>
<!--Footer-part-->
<div class="row-fluid">
  <div id="footer" class="span12"> 2013 &copy; Matrix Admin. Brought to you by <a href="http://themedesigner.in">Themedesigner.in</a> </div>
</div>
  
<!--end-Footer-part-->
<script src="js/jquery.min.js"></script> 
<script src="js/jquery.ui.custom.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/bootstrap-colorpicker.js"></script> 
<script src="js/bootstrap-datepicker.js"></script> 
<script src="js/jquery.toggle.buttons.js"></script> 
<script src="js/masked.js"></script> 
<script src="js/jquery.uniform.js"></script> 
<script src="js/select2.min.js"></script> 
<script src="js/matrix.js"></script> 
<script src="js/matrix.form_common.js"></script> 
<script src="js/wysihtml5-0.3.0.js"></script> 
<script src="js/jquery.peity.min.js"></script> 
<script src="js/bootstrap-wysihtml5.js"></script> 

 
<script>
	$('.textarea_editor').wysihtml5();
</script>
</body>
</html>