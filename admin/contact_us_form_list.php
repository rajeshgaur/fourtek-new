 <!DOCTYPE html>
 <html> 
 <body>
 <?php
session_start();
if (!$_SESSION['user_id']) {
    header('location:index.php');
}
include "header.php";
require_once("functions/user_list.php");
include "sidebar-menu.php";
$db = new Database();
if (!empty($_REQUEST['id'])) {
    $id = $_REQUEST['id'];
    $db = new Database();
    $result = $db->delete('contact_us_form', "WHERE id = '$id'");
    if (!empty($result)) {
        $suc_msg = "Record deleted successfully";
    } else {
        $err_msg = "Please try again";
    }
}
?>
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="dashboard.php" title="Go to Home" class="tip-bottom">
      <i class="icon-home"></i> Home</a>
    <a href="javascript:;" class="current">Users Listing</a> 
    </div>   
  </div>
  <div class="container-fluid">  
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>Users Listing</h5>          
          </div>
          <div class="widget-content nopadding table-responsive">          
            <div class="table-responsive">
            <table class="table table-bordered data-table">
              <thead>
                <tr>                  
                  <th>S.no</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Mobile</th>                 
                  <th>Comments</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
              <?php
              $result = $db->selectdata('contact_us_form', 'WHERE 1');
              $i=1;
              if (!empty($result)) {
                  while ($row = $result->fetch_array()) {
                      ?>
               <tr class="gradeX" id="changeme<?php echo $i; ?>">                 
                  <td class=""><?php echo $i; ?></td>
                  <td><?php echo $row['name']; ?></td>
                  <td><?php echo $row['email']; ?></td>
                  <td><?php echo $row['mobile']; ?></td>                  
                  <td><?php //echo $row['comment'];?>
                  <?php $description = $row['comment'];
                      $limit = 50;
                      if (strlen($description)<=$limit) {
                          echo $description;
                      } else {
                          $text = substr($description, 0, $limit) .'...';
                          echo $text;
                      } ?></td>
                  <td> <a href="contact-us-view.php?id=<?php echo $row['id']; ?>">View</a> | <a href="javascript:void();" onclick="doConfirm('<?php echo $row['id']; ?>');">Delete</a></td>
                
                </tr>
                <?php $i++;
                  }
              }?>
              </tbody>
            </table>
          </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<script>
    function doConfirm(id)
    {

        var ok = confirm("Are you sure to Delete?")
        if (ok)
        {
            location.href='contact_us_form_list.php?id='+id;

        }
    }
</script>

<script src="js/jquery.min.js"></script> 
<script src="js/jquery.ui.custom.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/jquery.uniform.js"></script> 
<script src="js/select2.min.js"></script> 
<script src="js/jquery.dataTables.min.js"></script> 
<script src="js/matrix.js"></script> 
<script src="js/matrix.tables.js"></script>
</body>
</html>