<?php
session_start();
if (!$_SESSION['user_id']) {
    header('location:index.php');
}
include "header.php";
require_once("functions/user_list.php");
include "sidebar-menu.php";
if (!empty($_REQUEST['id'])) {
    $id = $_REQUEST['id'];
    $db = new Database();
    $result = $db->delete('job_career', "WHERE id = '$id'");
    if (!empty($result)) {
        $suc_msg = "Record deleted successfully";
    } else {
        $err_msg = "Please try again";
    }
}
?>
<div id="content">
  <div id="content-header"> 
    <div id="breadcrumb"> 
      <a href="dashboard.php" title="Go to Home" class="tip-bottom">
        <i class="icon-home"></i> Home</a> 
        <a href="blog-list.php" class="tip-bottom">Job requirement</a> 
      <a href="#" class="current">Job list</a> 
  </div>
  <div class="container-fluid">    
    <div class="row-fluid">
      <div class="span12">
      <?php if (isset($_SESSION['suc_msg'])) {
    echo $_SESSION['suc_msg'];
    $_SESSION['suc_msg'] = '';
} ?>
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>Job listing</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>S.no</th>
                  <th>Title</th>
                  <th>Description</th>                  
                  <th>Post Date</th>                  
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
              <?php $db = new Database();
              $result = $db->selectdata("job_career", "where 1 ORDER BY id DESC");
              if (!empty($result)) {
                  $i=1;
                  while ($row = $result->fetch_assoc()) {
                      ?>
                <tr class="gradeX">
                  <td><?php echo $i; ?></td>
                  <td><?php echo $row['title']; ?></td>
                  <td><?php //echo $row['short_desc'];?>
                <?php $description = $row['description'];
                      $limit = 50;
                      if (strlen($description)<=$limit) {
                          echo $description;
                      } else {
                          $text = substr($description, 0, $limit) .'...';
                          echo $text;
                      } ?></td>
                  <td><?php echo $row['post_date']; ?></td>
                  <td><a href="job-edit.php?id=<?php echo $row['id']; ?>">Edit</a> | <a href="job-view.php?id=<?php echo $row['id']; ?>">View</a> | <a href="javascript:void();" onclick="doConfirm('<?php echo $row['id']; ?>');">Delete</a></td>
                </tr>
              <?php $i++;
                  }
              }
               ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
    function doConfirm(id)
    {

        var ok = confirm("Are you sure to Delete?")
        if (ok)
        {
            location.href='job-list.php?id='+id;

        }
    }
</script>
<!--Footer-part-->
<div class="row-fluid">
  <!--<div id="footer" class="span12"> 2013 &copy; Matrix Admin. Brought to you by <a href="http://themedesigner.in">Themedesigner.in</a> </div>-->
</div>
<!--end-Footer-part-->
<script src="js/jquery.min.js"></script> 
<script src="js/jquery.ui.custom.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/jquery.uniform.js"></script> 
<script src="js/select2.min.js"></script> 
<script src="js/jquery.dataTables.min.js"></script> 
<script src="js/matrix.js"></script> 
<script src="js/matrix.tables.js"></script>
</body>
</html>