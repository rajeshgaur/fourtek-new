<?php
session_start();
if (!$_SESSION['user_id']) {
    header('location:index.php');
}
include "header.php";
require_once("functions/user_list.php");
include "sidebar-menu.php";

$id = $_REQUEST['id'];

?>

<div id="content">
<div id="content-header">
  <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
   <a href="#" class="tip-bottom">Users</a>
   <a href="#" class="current">View user</a> </div>
  <!--<h1>Common Form Elements</h1>-->
</div>
<div class="container-fluid">
  <!--<hr>-->
  
  <div class="row-fluid">
    <div class="span12">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>View user</h5>
        </div>

        <?php $db = new Database();
        $query = "SELECT * from `users_profile` WHERE `user_id` = ".$id;
        $result = $db->select($query);
        while ($row = $result->fetch_array()) {
            ?>

        <div class="widget-content nopadding">
          <form action="" method="post" class="form-horizontal">
            <div class="control-group">
              <label class="control-label">First Name :</label>
              <div class="controls">
                <?php echo $row['firstname']; ?>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Last Name :</label>
              <div class="controls">
                <?php echo $row['lastname']; ?>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Email :</label>
              <div class="controls">
                <?php echo $row['email']; ?>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Age :</label>
              <div class="controls">
                <?php echo $row['age']; ?>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Height :</label>
              <div class="controls">
                <?php echo $row['height']; ?>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Sex :</label>
              <div class="controls">
                <?php echo $row['sex']; ?>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Religion :</label>
              <div class="controls">
                <?php echo $row['religion']; ?>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Caste :</label>
              <div class="controls">
                <?php echo $row['caste']; ?>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Subcaste :</label>
              <div class="controls">
                <?php echo $row['subcaste']; ?>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Country :</label>
              <div class="controls">
                <?php echo $row['country']; ?>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">State :</label>
              <div class="controls">
                <?php echo $row['state']; ?>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">District :</label>
              <div class="controls">
                <?php echo $row['district']; ?>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Martial Status :</label>
              <div class="controls">
                <?php echo $row['maritalstatus']; ?>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Profile Created By :</label>
              <div class="controls">
                <?php echo $row['profilecreatedby']; ?>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Education :</label>
              <div class="controls">
                <?php echo $row['education']; ?>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Education Sub :</label>
              <div class="controls">
                <?php echo $row['education_sub']; ?>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Body Type :</label>
              <div class="controls">
                <?php echo $row['body_type']; ?>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Physical Status :</label>
              <div class="controls">
                <?php echo $row['physical_status']; ?>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Drink :</label>
              <div class="controls">
                <?php echo $row['drink']; ?>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Mother Tongue :</label>
              <div class="controls">
                <?php echo $row['mothertounge']; ?>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Color :</label>
              <div class="controls">
                <?php echo $row['colour']; ?>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Weight :</label>
              <div class="controls">
                <?php echo $row['weight']; ?>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Blood Group :</label>
              <div class="controls">
                <?php echo $row['blood_group']; ?>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Diet :</label>
              <div class="controls">
                <?php echo $row['diet']; ?>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Smoke :</label>
              <div class="controls">
                <?php echo $row['diet']; ?>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Date of Birth :</label>
              <div class="controls">
                <?php echo $row['dateofbirth']; ?>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Occupation :</label>
              <div class="controls">
                <?php echo $row['occupation']; ?>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Occupation Desc :</label>
              <div class="controls">
                <?php echo $row['occupation_descr']; ?>
              </div>
              </div>
              <div class="control-group">
              <label class="control-label">Annual Income :</label>
              <div class="controls">
                <?php echo $row['annual_income']; ?>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Father Occupation :</label>
              <div class="controls">
                <?php echo $row['fathers_occupation']; ?>
              </div>
              </div>
              <div class="control-group">
              <label class="control-label">Mother Occupation :</label>
              <div class="controls">
                <?php echo $row['mothers_occupation']; ?>
              </div>
            </div>

            <div class="control-group">
              <label class="control-label">Brother :</label>
              <div class="controls">
                <?php echo $row['no_bro']; ?>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Sister :</label>
              <div class="controls">
                <?php echo $row['no_sis']; ?>
              </div>
            </div>
            

            <div class="control-group">
              <label class="control-label">About me</label>
              <div class="controls">
                <?php echo $row['aboutme']; ?>
              </div>
            </div>
            
          </form>
        </div>
        <?php
        } ?>
      </div>
      
    </div>
    
  </div>
 
</div></div>
<!--Footer-part-->
<div class="row-fluid">
  <div id="footer" class="span12"> 2013 &copy; Matrix Admin. Brought to you by <a href="http://themedesigner.in">Themedesigner.in</a> </div>
</div>
<!--end-Footer-part--> 
<script src="js/jquery.min.js"></script> 
<script src="js/jquery.ui.custom.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/bootstrap-colorpicker.js"></script> 
<script src="js/bootstrap-datepicker.js"></script> 
<script src="js/jquery.toggle.buttons.js"></script> 
<script src="js/masked.js"></script> 
<script src="js/jquery.uniform.js"></script> 
<script src="js/select2.min.js"></script> 
<script src="js/matrix.js"></script> 
<script src="js/matrix.form_common.js"></script> 
<script src="js/wysihtml5-0.3.0.js"></script> 
<script src="js/jquery.peity.min.js"></script> 
<script src="js/bootstrap-wysihtml5.js"></script> 
<script>
	$('.textarea_editor').wysihtml5();
</script>
</body>
</html>