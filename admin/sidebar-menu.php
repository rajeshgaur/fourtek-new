<!--sidebar-menu-->
<?php error_reporting(0); ?>
<div id="sidebar"><a href="#" class="visible-phone"><i class="icon icon-home"></i> Dashboard</a>
<ul>
<!-- start dashboard menu -->
<?php
$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
?>
<li <?php if (strpos($actual_link, 'dashboard.php') !== false) {
    echo 'class="active"';
} ?>><a href="index.php"><i class="icon icon-home"></i> <span>Dashboard</span></a> </li>
<!-- end dashboard menu -->

<!-- start Job requirment menu -->
<?php if (strpos($actual_link, 'add-job.php') !== false || strpos($actual_link, 'job-list.php') !== false || strpos($actual_link, 'job-edit.php') !== false) {
    $class = 'class="submenu open"';
} else {
    $class = 'class="submenu"';
} ?>
<li <?php echo $class; ?>> 
<a href="#">
<i class="icon icon-th-list"></i><span>Job requirment</span>  
</a>
<ul>
<li <?php if (strpos($actual_link, 'add-job.php') !== false) {
    echo 'class="active"';
} ?>><a href="add-job.php">Add job</a></li>
<li <?php if (strpos($actual_link, 'job-list.php') !== false) {
    echo 'class="active"';
} ?>><a href="job-list.php">Job list</a></li>
</ul>
</li>
<!-- end Job requirment menu -->

<!-- start case study menu -->
<?php if (strpos($actual_link, 'add-job.php') !== false || strpos($actual_link, 'job-list.php') !== false || strpos($actual_link, 'job-edit.php') !== false) {
    $class = 'class="submenu open"';
} else {
    $class = 'class="submenu"';
} ?>
<li <?php echo $class; ?>> 
<a href="#">
<i class="icon icon-th-list"></i><span>Case Study</span>  
</a>
<ul>
<li <?php if (strpos($actual_link, 'case-study') !== false) {
    echo 'class="active"';
} ?>><a href="case-study">Add case study</a></li>
<li <?php if (strpos($actual_link, 'case-study-list') !== false) {
    echo 'class="active"';
} ?>><a href="case-study-list.php">case study list</a></li>
</ul>
</li>
<!-- end case study menu -->

<!-- start digital marketing package menu -->
<li <?php if (strpos($actual_link, 'add_dynamic_field.php') !== false) {
    echo 'class="active"';
} ?>><a href="add_dynamic_field.php"><i class="icon icon-th-list"></i> <span>Digital marketing package</span></a> </li>
<!-- end digital marketing package menu -->

<!-- start digital marketing contact list menu -->
  <li <?php if (strpos($actual_link, 'user_contact_list.php') !== false) {
    echo 'class="active"';
} ?>><a href="user_contact_list.php"><i class="icon icon-th-list"></i> <span>Digital marketing contact list</span></a> </li>
  <!-- end digital marketing contact menu list menu -->

<!-- start contact user list menu -->
  <li <?php if (strpos($actual_link, 'contact_us_form_list.php') !== false) {
    echo 'class="active"';
} ?>><a href="contact_us_form_list.php"><i class="icon icon-th-list"></i> <span>Contact us form user list</span></a> </li>
  <!-- end contact user list menu -->

<!-- start job apply user list menu -->
  <li <?php if (strpos($actual_link, 'job_apply_user_list.php') !== false) {
    echo 'class="active"';
} ?>><a href="job_apply_user_list.php"><i class="icon icon-th-list"></i> <span>Job apply user list</span></a> </li>
  <!-- end job apply user list menu -->
</ul>
</div>
<!--sidebar-menu-->
