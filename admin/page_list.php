<?php
session_start();
if (!$_SESSION['user_id']) {
    header('location:index.php');
}
include "header.php";
require_once("functions/user_list.php");
include "sidebar-menu.php";

if (!empty($_REQUEST['id'])) {
    $id = $_REQUEST['id'];
    $db = new Database();
    $result = $db->delete('pages', "WHERE id = '$id'");
    if (!empty($result)) {
        $suc_msg = "Record deleted successfully";
    } else {
        $err_msg = "Please try again";
    }
}
?>

<div id="content">
  <div id="content-header">
  <div id="breadcrumb"> <a href="dashboard.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="page_list.php" class="tip-bottom">Pages</a> 
      <a href="#" class="current">Page list</a>
  </div>
  <div class="container-fluid">   
    <div class="row-fluid">
      <div class="span12">        
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>Page listing</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>S.no</th>
                  <th>Title</th>
                  <th>Description</th>
                  <th>Image</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>  
              <?php
              $db = new Database();
              $result = $db->selectdata("pages", "where 1");
              if (!empty($result)) {
                  $i=1;
                  while ($row = $result->fetch_assoc()) {
                      $imagee = $row['images'];
                      if (empty($imagee)) {
                          $no_image = "No Image";
                      } else {
                          $no_image = '<img src="uploads/'.$imagee.'"  width="100px;" height="100px;" >';
                      } ?>
                <tr class="gradeX">
                  <td><?php echo $i; ?></td>
                  <td><?php echo $row['title']; ?></td>
                  <td><?php //echo $row['description'];?>
                  <?php $description = $row['description'];
                      $limit = 50;
                      if (strlen($description)<=$limit) {
                          echo $description;
                      } else {
                          $text = substr($description, 0, $limit) .'...';
                          echo $text;
                      } ?>
                  </td>
                  <td><?php echo $no_image; ?></td>
                  <td>
                  <a href="page_edit.php?id=<?php echo $row['id']; ?>">Edit</a> | <a href="page_view.php?id=<?php echo $row['id']; ?>">View</a> | <a href="javascript:void();" onclick="doConfirm('<?php echo $row['id']; ?>');">Delete
                  </a>
                  </td>
                </tr>
              <?php $i++;
                  }
              }
               ?>

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
    function doConfirm(id)
    {

        var ok = confirm("Are you sure to Delete?")
        if (ok)
        {
            location.href='page_list.php?id='+id;
        }
    }
</script>
<script src="js/jquery.min.js"></script> 
<script src="js/jquery.ui.custom.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/jquery.uniform.js"></script> 
<script src="js/select2.min.js"></script> 
<script src="js/jquery.dataTables.min.js"></script> 
<script src="js/matrix.js"></script> 
<script src="js/matrix.tables.js"></script>
</body>
</html>