  <?php
session_start();
if (!$_SESSION['user_id']) {
    header('location:index.php');
}
include "header.php";
require_once("functions/user_list.php");
include "sidebar-menu.php";

$db = new Database();
if (isset($_POST['meetus'])) {
    $formsData = array(
    'starter' => $_POST['starter']
    );
    $result = $db->selectdata('dynamic_field', "WHERE id=1");
    if (empty($result)) {
        $result = $db->insert_data('dynamic_field', $formsData);
    } else {
        $result = $db->update('dynamic_field', $formsData, "WHERE id=1");
    }
}
if (isset($_POST['address'])) {
    $formsData = array(
  'silver' => $_POST['silver']
);
    $result = $db->selectdata('dynamic_field', "WHERE id=1");
    if (empty($result)) {
        $result = $db->insert_data('dynamic_field', $formsData);
    } else {
        $result = $db->update('dynamic_field', $formsData, "WHERE id=1");
    }
}

if (isset($_POST['adimg_first'])) {
    $form_data = array(
                    'bronze' => $_POST['bronze']
                        );
    $result = $db->selectdata('dynamic_field', "WHERE id=1");
    if (empty($result)) {
        $result = $db->insert_data('dynamic_field', $form_data);
    } else {
        $result = $db->update('dynamic_field', $form_data, "WHERE id=1");
    }
                
    if (!empty($result)) {
        $suc_msg = "Details updated successfully";
    } else {
        $err_msg = "Please try again";
    }
}



if (isset($_POST['adimg_scnd'])) {
    $form_data = array(
      'gold' => $_POST['gold']
          );
    $result = $db->selectdata('dynamic_field', "WHERE id=1");
    if (empty($result)) {
        $result = $db->insert_data('dynamic_field', $form_data);
    } else {
        $result = $db->update('dynamic_field', $form_data, "WHERE id=1");
    }
  
    if (!empty($result)) {
        $suc_msg = "Details updated successfully";
    } else {
        $err_msg = "Please try again";
    }
}


?>
<div id="content" class="">
<div id="content-header">
  <div id="breadcrumb"> <a href="index.php" title="Go to Home" class="tip-bottom">
    <i class="icon-home"></i> Home</a>  
   <a href="#" class="current">Dynamic field page_id</a> 
 </div>
  <!--<h1>Add Events</h1>-->
</div>
<div class="container-fluid">
  <!--<hr>-->
  <?php if (isset($suc_msg)) {
    echo $suc_msg;
} $suc_msg = ''; ?>
  <?php if (isset($err_msg)) {
    echo $err_msg;
} $err_msg = ''; ?>
  <div class="row-fluid">
    <div class="span12">
      <div class="widget-box">
        <div class="widget-title"> 
          <span class="icon"> 
            <i class="icon-align-justify"></i> 
          </span>
          <h5>Dynamic field page</h5>
        </div>
        <div class="widget-content nopadding col-sm-12">
          <form action="" method="post" class="form-horizontal" enctype='multipart/form-data'>
            <div class="control-group">
              <label class="control-label">Starter pack</label>
           <?php
                $dynamic_field = $db->selectdata('dynamic_field', "WHERE 1");

                if ($dynamic_field) {
                    $dynamic = $dynamic_field->fetch_assoc();
                }

                ?>
              <div class="controls">
              <input type="text" name="starter" value="<?php echo $dynamic['starter'];?>" placeholder="Starter">
              <input type="submit" name="meetus" class="btn btn-success" value="submit" />
              </div>              
            </div>
          </form>
        </div>
        <div class="widget-content nopadding col-sm-6">
          <form action="" method="post" class="form-horizontal" enctype='multipart/form-data'>   <div class="control-group">
              <label class="control-label">Bronze</label>
              <div class="controls">
                <input type="text" name="bronze" value="<?php echo $dynamic['bronze'];?>" placeholder="Starter">
                <input type="submit" name="adimg_first" class="btn btn-success" value="submit" />
              </div>              
            </div>            
          </form>
        </div>
         <div class="widget-content nopadding col-sm-6">
          <form action="" method="post" class="form-horizontal" enctype='multipart/form-data'>
            <div class="control-group">
              <label class="control-label">Gold</label>
              <div class="controls">
                <input type="text" name="gold" value="<?php echo $dynamic['gold'];?>" placeholder="Gold">
                <input type="submit" name="adimg_scnd" class="btn btn-success" value="submit" />                
              </div>
              
            </div>            
          </form>
        </div>
        <div class="widget-content nopadding col-sm-12">
        <form action="" method="post" class="form-horizontal" enctype='multipart/form-data'>
          <div class="control-group">
            <label class="control-label">Silver</label>       
            <div class="controls">
              <input type="text" name="silver" value="<?php echo $dynamic['silver'];?>" placeholder="Silver">
               <input type="submit" name="address" class="btn btn-success" value="submit" />
            </div>              
          </div>
        </form>
      </div>

       
       
</div>  
</div>
</div>

</div>
</div>




<script src="js/jquery.min.js"></script> 
<script src="js/jquery.ui.custom.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/bootstrap-colorpicker.js"></script> 
<script src="js/bootstrap-datepicker.js"></script> 
<script src="js/jquery.toggle.buttons.js"></script> 
<script src="js/masked.js"></script> 
<script src="js/jquery.uniform.js"></script> 
<script src="js/select2.min.js"></script> 
<script src="js/matrix.js"></script> 
<script src="js/matrix.form_common.js"></script> 
<script src="js/wysihtml5-0.3.0.js"></script> 
<script src="js/jquery.peity.min.js"></script> 
<script src="js/bootstrap-wysihtml5.js"></script> 
<script>
  $('.textarea_editor').wysihtml5();
  $('.textarea_editor1').wysihtml5();
</script>


</body>
</html>