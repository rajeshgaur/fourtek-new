<?php
$base_url = 'https://www.fourtek.com';
class Database
{
    public $host = "localhost";

    public $user = "fourtekDb";
    public $pass = "fourtekDb123";

    public $db = "fourtekDb";
    public $link;

    
    public function __construct()
    {
        $this->connect();
    }

    private function connect()
    {
        $this->link = new mysqli($this->host, $this->user, $this->pass, $this->db);
        mysqli_query($this->link, "SET SESSION sql_mode = ''");
        return $this->link;
    }

    public function select($query)
    {
        $result = $this->link->query($query);
        if ($result->num_rows>0) {
            return $result;
        } else {
            return false;
        }
    }


    public function selectdata($table_name, $where_clause)
    {
        $query = "SELECT * from ".$table_name;
        $query .= " ".$where_clause;
        $result = $this->link->query($query);


        if ($result->num_rows>0) {
            return $result;
        } else {
            return false;
        }
    }

    public function insert_data($table_name, $form_data)
    {
        $fields = array_keys($form_data);
        $sql = "INSERT INTO ".$table_name."
        (`".implode('`,`', $fields)."`)
        VALUES('".implode("','", $form_data)."')";
        //$sql;
        if ($this->link->query($sql)) {
            return true;
        } else {
            return false;
        }
    }
    public function update($table_name, $form_data, $where_clause='')
    {
        if (!empty($where_clause)) {
            $sql = "UPDATE ".$table_name." SET ";
            $sets = array();
            foreach ($form_data as $column => $value) {
                $sets[] = "`".$column."` = '".$value."'";
            }
            $sql .= implode(', ', $sets);
            $sql .= $where_clause;
            if ($this->link->query($sql)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    public function delete($table_name, $where_clause)
    {
        $sql = "DELETE FROM ". $table_name . ' ' . $where_clause;
        if ($this->link->query($sql) === true) {
            return true;
        } else {
            return false;
        }
    }

    public function generateSeoURL($string, $wordLimit = 0)
    {
        $separator = '-';
        
        if ($wordLimit != 0) {
            $wordArr = explode(' ', $string);
            $string = implode(' ', array_slice($wordArr, 0, $wordLimit));
        }

        $quoteSeparator = preg_quote($separator, '#');

        $trans = array(
            '&.+?;'                    => '',
            '[^\w\d _-]'            => '',
            '\s+'                    => $separator,
            '('.$quoteSeparator.')+'=> $separator
        );

        $string = strip_tags($string);
        foreach ($trans as $key => $val) {
            $string = preg_replace('#'.$key.'#i'.(UTF8_ENABLED ? 'u' : ''), $val, $string);
        }

        $string = strtolower($string);
        return trim(trim($string, $separator));
    }
}
