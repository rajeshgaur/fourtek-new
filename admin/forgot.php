<?php
session_start();
error_reporting(0);
require_once('connect.php');
require_once("functions/user_list.php");
$db = new Database();

if (isset($_POST['forgot'])) {
    $email = $_POST['email'];
    $check = $db->selectdata("admin", "WHERE email='$email'");
    $check2 = $check->num_rows;

    if ($check2 == 0) {
        $error = '<font size="2" style="padding-left:50px;" color="red">Sorry, Your emails doesnt exists in our record</font>';
    } else {
        $query = $db->selectdata("admin", "WHERE email='$email'");
        $r = $query->fetch_assoc();
        $usernam = $r['username'];
        $password = substr(md5(uniqid(rand(), 1)), 3, 10);
        $pass = md5($password); //encrypted version for database entry

        //send email
        $to = "$email";
        $subject = "Password Recovery";
        $body = "Hi $usernam , 
        you or someone else have requested your account details. 
        Here is your account information please keep this as you may need this at a later stage.
        Your username is $usernam and your password is $password. 
        Your password has been reset please login and change your password. 
        Regards,
        Easemymatch.";

        $lheaders= "From: <info@easemymatch.com> \r\n";
        $lheaders.= "Reply-To: info@easemymatch.com";

        if (mail($to, $subject, $body)) {
            $formData = array(
                'password' => $pass
            );
            $sql = $db->update('admin', $formData, "WHERE email='$email'");
            $rsent = true;
            $error = '<font size="2" style="padding-left:40px;" color="green">Please check your email.</font>';
        }
    }
}


?>

<!DOCTYPE html>
<html lang="en">
    
<head>
    <title>Ease My Match Admin</title><meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link rel="stylesheet" href="css/bootstrap-responsive.min.css" />
        <link rel="stylesheet" href="css/matrix-login.css" />
        <link href="font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>

    </head>
    <body>
        <div id="loginbox"> 
         
        <form  action="" class="form-vertical" method="post" style="display: block !important;">
            <?php if (isset($error)) {
    echo $error;
    $error = '';
} ?>
        <p class="normal_text">Enter your e-mail address below and we will send you instructions how to recover a password.</p>
        
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_lo"><i class="icon-envelope"></i></span>
                            <input type="email"  name="email" placeholder="E-mail address" />
                        </div>
                    </div>
               
                <div class="form-actions">
                    <span class="pull-left"><a href="index.php" class="flip-link btn btn-success" id="to-login">&laquo; Back to login</a></span>
                    <span class="pull-right">
                        <input class="btn btn-info btn-success" type="submit" name="forgot" value="Forgot password"> </span>
                </div>
            </form>
        </div>
        
        <script src="js/jquery.min.js"></script>  
        <script src="js/matrix.login.js"></script> 
    </body>

</html>