<?php
session_start();
if (!$_SESSION['user_id']) {
    header('location:index.php');
}
include "header.php";
require_once("functions/user_list.php");
include "sidebar-menu.php";

if (isset($_POST['submit'])) {
    $db = new Database();
    $title = $_POST['job_title'];
    $url = $db->generateSeoURL($title, 6);
    $table_name = $_POST['table_name'];
    $err = 0;

    if (empty(trim($title))) {
        $err = 1;
        $err_title = '<font size="1" color="red">Please enter title</font>';
    }
    if (empty(trim($_POST['description']))) {
        $err = 1;
        $err_desc = '<font size="1" color="red">Please enter description</font>';
    }
    // if (empty(trim($_POST['shortdesc']))) {
    //     $err = 1;
    //     $err_desc = '<font size="1" color="red">Please enter short description</font>';
    // }
    if (empty(trim($_POST['qualification']))) {
        $err = 1;
        $err_desc = '<font size="1" color="red">Please enter qualification</font>';
    }
    if (empty(trim($_POST['experience']))) {
        $err = 1;
        $err_desc = '<font size="1" color="red">Please enter experience</font>';
    }
    if (empty(trim($_POST['postdate']))) {
        $err = 1;
        $err_desc = '<font size="1" color="red">Please enter post date</font>';
    }
    if (empty(trim($_POST['location']))) {
        $err = 1;
        $err_desc = '<font size="1" color="red">Please enter location</font>';
    }
    if (empty(trim($_POST['numberopening']))) {
        $err = 1;
        $err_opening = '<font size="1" color="red">Please enter number of opening</font>';
    }
    
    if ($err == 0) {
        $form_data = array(
                'title' => addslashes($_POST['job_title']),
                //'short_desc' => addslashes($_POST['shortdesc']),
                'description' => addslashes($_POST['description']),
                'min_qualifica' => addslashes($_POST['qualification']),
                'num_of_post' => $_POST['numberopening'],
                'post_date' => $_POST['postdate'],
                'experience' => $_POST['experience'],
                'location' => $_POST['location']
                
                
            );
        $result = $db->insert_data('job_career', $form_data);
        if ($result) {
            $_SESSION['suc_msg'] = '<font size="1" color="green">Details saved successfully</font>';
            echo'<script type="text/javascript">window.location.href = "job-list.php";</script>';
        }
    }
}
?>
<div id="content" class="">
<div id="content-header">
  <div id="breadcrumb"> <a href="index.php" title="Go to Home" class="tip-bottom">
    <i class="icon-home"></i> Home</a>
   <a href="job-list.php" class="tip-bottom">Job requirement</a> 
   <a href="#" class="current">Add job</a> 
 </div> 
</div>
<div class="container-fluid">
  <!--<hr>-->
  <?php if (isset($suc_msg)) {
    echo $suc_msg;
} $suc_msg = ''; ?>
    <?php if (isset($err_msg)) {
    echo $err_msg;
} $err_msg = ''; ?>
  <div class="row-fluid">
    <div class="span12">
      <div class="widget-box">
        <div class="widget-title"> 
          <span class="icon"> 
            <i class="icon-align-justify"></i> 
          </span>
          <h5>Add job</h5>
        </div>
        <div class="widget-content nopadding col-sm-12">
          <form action="" method="post" class="form-horizontal" enctype='multipart/form-data'>
            
            <div class="control-group">
              <label class="control-label">Job title* :</label>
              <div class="controls">
        <input type="text" name="job_title" value="<?php if ($_POST['job_title']) {
    echo $_POST['job_title'];
} ?>" class="span10" placeholder="Job title" />
                <?php echo $err_title; ?>
              </div>
            </div>                
            

             <!-- <div class="control-group">
              <label class="control-label">Short description* :</label>
              <div class="controls">
                 <textarea name="shortdesc" class="span10" rows="6"><?php// if ($_POST['shortdesc']) { echo $_POST['shortdesc']; } ?></textarea>
                 <?php //echo $err_desc;?>
              </div>
            </div>  -->
            <div class="control-group">
              <label class="control-label">Description* :</label>
              <div class="controls">
                 <textarea name="description" class="textarea_editor span10" rows="6"><?php if ($_POST['description']) {
    echo $_POST['description'];
} ?>
                  </textarea>
                 <?php echo $err_desc; ?>
              </div>
            </div> 
          <div class="control-group">
              <label class="control-label">Minimum qualification* :</label>
              <div class="controls">
                 <textarea name="qualification" class="textarea_editor span10" rows="6"><?php if ($_POST['qualification']) {
    echo $_POST['qualification'];
} ?></textarea>
                 <?php echo $err_desc; ?>
              </div>
            </div> 

            <div class="control-group">
              <label class="control-label">No Of Opening* :</label>
              <div class="controls">
        <input type="text" name="numberopening" value="<?php if ($_POST['numberopening']) {
    echo $_POST['numberopening'];
} ?>" class="span10" placeholder="No of opening" />
                <?php echo $err_opening; ?>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Posted date* :</label>
              <div class="controls">
        <input type="date" name="postdate" value="<?php if ($_POST['postdate']) {
    echo $_POST['postdate'];
} ?>" class="span10" placeholder="Posted date" />
                <?php echo $err_title; ?>
              </div>
            </div>
             <div class="control-group">
              <label class="control-label">Experience* :</label>
              <div class="controls">
        <input type="text" name="experience" value="<?php if ($_POST['experience']) {
    echo $_POST['experience'];
} ?>" class="span10" placeholder="Experience" />
                <?php echo $err_title; ?>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Location* :</label>
              <div class="controls">
        <input type="text" name="location" value="<?php if ($_POST['location']) {
    echo $_POST['location'];
} ?>" class="span10" placeholder="Location" />
                <?php echo $err_title; ?>
              </div>
            </div>

            <div class="form-actions">
              <input type="submit" class="btn btn-success" name="submit" value="submit" />
            </div>
          </form>
        </div>
      </div>
  
</div>
</div>

</div>
</div>


<!--Footer-part-->
<!--<div class="row-fluid">
  <div id="footer" class="span12"> 2013 &copy; Matrix Admin. Brought to you by <a href="http://themedesigner.in">Themedesigner.in</a> </div>
</div>-->
<!--end-Footer-part--> 
<script src="js/jquery.min.js"></script> 
<script src="js/jquery.ui.custom.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/bootstrap-colorpicker.js"></script> 
<script src="js/bootstrap-datepicker.js"></script> 
<script src="js/jquery.toggle.buttons.js"></script> 
<script src="js/masked.js"></script> 
<script src="js/jquery.uniform.js"></script> 
<script src="js/select2.min.js"></script> 
<script src="js/matrix.js"></script> 
<script src="js/matrix.form_common.js"></script> 
<script src="js/wysihtml5-0.3.0.js"></script> 
<script src="js/jquery.peity.min.js"></script> 
<script src="js/bootstrap-wysihtml5.js"></script> 
<script>
  $('.textarea_editor').wysihtml5();
</script>
</body>
</html>
