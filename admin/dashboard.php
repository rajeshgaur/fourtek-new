<?php
//error_reporting(0);
session_start();
if (!$_SESSION['user_id']) {
    header('location:index.php');
}
include "header.php";
require_once("functions/user_list.php");
include "sidebar-menu.php";

?>
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
  </div>
</div>

<!--end-main-container-part-->


<?php

include "footer.php";
?>
