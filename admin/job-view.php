<?php
session_start();
if (!$_SESSION['user_id']) {
    header('location:index.php');
}
include "header.php";
require_once("functions/user_list.php");
include "sidebar-menu.php";

$id = $_REQUEST['id'];

?>
  
<div id="content">
<div id="content-header">
  <div id="breadcrumb"> <a href="dashboard.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
  <a href="job-list.php" class="tip-bottom">Job list</a> 
  <a href="#" class="current">View job</a> 
</div>
  <!--<h1>Common Form Elements</h1>-->
</div>
<div class="container-fluid">
  <!--<hr>-->
  <?php if (isset($suc_msg)) {
    echo $suc_msg;
} $suc_msg = ''; ?>
  <?php if (isset($err_msg)) {
    echo $err_msg;
} $err_msg = ''; ?>
  <div class="row-fluid">
    <div class="span12">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>View job</h5>
        </div>

        <?php $db = new Database();
        $result = $db->selectdata("job_career", "where id = '$id'");
        while ($row = $result->fetch_array()) {
            ?>

        <div class="widget-content nopadding">
          <form action="" method="post" class="form-horizontal">
            <input type="hidden" name="table_name" value="blog" />
            <div class="control-group">
              <label class="control-label">Job title* :</label>
              <div class="controls">
       <?php if ($row['title']) {
                echo $row['title'];
            } ?>
               
              </div>
            </div> 
             <!-- <div class="control-group">
              <label class="control-label">Shor description* :</label>
              <div class="controls">
                 <?php //if ($row['short_desc']) { echo $row['short_desc']; }?>
                 
              </div>
            </div>  -->
            <div class="control-group">
              <label class="control-label">Description* :</label>
              <div class="controls">
                 <?php if ($row['description']) {
                echo $row['description'];
            } ?>
                 
              </div>
            </div> 
          <div class="control-group">
              <label class="control-label">Minimum qualification* :</label>
              <div class="controls">
                 <?php if ($row['min_qualifica']) {
                echo $row['min_qualifica'];
            } ?>
                 
              </div>
            </div> 

            <div class="control-group">
              <label class="control-label">No Of Opening* :</label>
              <div class="controls">
       <?php if ($row['num_of_post']) {
                echo $row['num_of_post'];
            } ?>
                
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Posted date* :</label>
              <div class="controls">
        <?php if ($row['post_date']) {
                echo $row['post_date'];
            } ?>   
              </div>
            </div>
             <div class="control-group">
              <label class="control-label">Experience* :</label>
              <div class="controls">
        <?php if ($row['experience']) {
                echo $row['experience'];
            } ?>
                
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Location* :</label>
              <div class="controls">
        <?php if ($row['location']) {
                echo $row['location'];
            } ?>
                
              </div>
            </div>


           
          </form>
        </div>
        <?php
        } ?>
      </div>
      
    </div>
    
  </div>
 
</div></div>
<!--Footer-part-->
<div class="row-fluid">
  <div id="footer" class="span12"> 2013 &copy; Matrix Admin. Brought to you by <a href="http://themedesigner.in">Themedesigner.in</a> </div>
</div>
  
<!--end-Footer-part-->
<script src="js/jquery.min.js"></script> 
<script src="js/jquery.ui.custom.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/bootstrap-colorpicker.js"></script> 
<script src="js/bootstrap-datepicker.js"></script> 
<script src="js/jquery.toggle.buttons.js"></script> 
<script src="js/masked.js"></script> 
<script src="js/jquery.uniform.js"></script> 
<script src="js/select2.min.js"></script> 
<script src="js/matrix.js"></script> 
<script src="js/matrix.form_common.js"></script> 
<script src="js/wysihtml5-0.3.0.js"></script> 
<script src="js/jquery.peity.min.js"></script> 
<script src="js/bootstrap-wysihtml5.js"></script> 

 
<script>
	$('.textarea_editor').wysihtml5();
</script>
</body>
</html>