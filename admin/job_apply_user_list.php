 <!DOCTYPE html>
 <html> 
 <body>
 <?php
session_start();
if (!$_SESSION['user_id']) {
    header('location:index.php');
}
include "header.php";
require_once("functions/user_list.php");
include "sidebar-menu.php";
$db = new Database();
?>
<script src="js/jquery.min.js"></script> 
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="dashboard.php" title="Go to Home" class="tip-bottom">
      <i class="icon-home"></i> Home</a>
    <a href="javascript:;" class="current">Users Listing</a> 
    </div>   
  </div>
  <div class="container-fluid">  
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>Users Listing</h5>          
          </div>
          <div class="widget-content nopadding table-responsive">          
            <div class="table-responsive">
            <table class="table table-bordered data-table">
              <thead>
                <tr>                  
                  <th>S.no</th>
                  <th>First Name</th>
                  <th>Last Name</th>
                  <th>Mobile</th>                 
                  <th>Email Id</th>
                  <th>Post for Apply</th>
                  <th>Experience</th>
                  <!--<th>City</th>
                  <th>State</th>-->
                  <th>Current Location</th>
                  <th>CTC</th>
                  <th>CV</th>
                </tr>
              </thead>
              <tbody>
              <?php
              $result = $db->selectdata('job_apply', 'WHERE 1');
              $i=1;
              if (!empty($result)) {
                  while ($row = $result->fetch_array()) {
                      ?>
               <tr class="gradeX" id="changeme<?php echo $i; ?>">                 
                  <td class=""><?php echo $i; ?></td>
                  <td><?php echo $row['first_name']; ?></td>
                  <td><?php echo $row['last_name']; ?></td>
                  <td><?php echo $row['mobile_number']; ?></td>                  
                  <td><?php echo $row['email']; ?></td>
                  <td><?php echo $row['post_apply']; ?></td>
                  <td><?php echo $row['total_exp']; ?></td>
                  <!--<td><?php echo $row['city']; ?></td>
                  <td><?php echo $row['state']; ?></td>-->
                  <td><?php echo $row['address_two']; ?></td>
                  <td><?php echo $row['address_one']; ?></td>
                  
                  <td> <a href="../upload/<?php echo $row['resume_name']; ?>" download>CV Download</a></td>
                </tr>
                <?php $i++;
                  }
              }?>
              </tbody>
            </table>
          </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>




<script src="js/jquery.min.js"></script> 
<script src="js/jquery.ui.custom.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/jquery.uniform.js"></script> 
<script src="js/select2.min.js"></script> 
<script src="js/jquery.dataTables.min.js"></script> 
<script src="js/matrix.js"></script> 
<script src="js/matrix.tables.js"></script>
</body>
</html>