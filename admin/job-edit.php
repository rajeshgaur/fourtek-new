<?php
session_start();
if (!$_SESSION['user_id']) {
    header('location:index.php');
}
include "header.php";
require_once("functions/user_list.php");
include "sidebar-menu.php";

$id = $_REQUEST['id'];

if (isset($_POST['submit'])) {
    $err_msg = '';
    $db = new Database();
    $title = $_POST['job_title'];
    $err = 0;
    if (empty(trim($title))) {
        $err = 1;
        $err_title = '<font size="1" color="red">Please enter title</font>';
    }
    if (empty(trim($_POST['description']))) {
        $err = 1;
        $err_desc = '<font size="1" color="red">Please enter description</font>';
    }
    // if (empty(trim($_POST['shortdesc']))) {
    //     $err = 1;
    //     $err_shortdesc = '<font size="1" color="red">Please enter short description</font>';
    // }
    if (empty(trim($_POST['qualification']))) {
        $err = 1;
        $err_qualif = '<font size="1" color="red">Please enter qualification</font>';
    }
    if (empty(trim($_POST['experience']))) {
        $err = 1;
        $err_experience = '<font size="1" color="red">Please enter experience</font>';
    }
    if (empty(trim($_POST['postdate']))) {
        $err = 1;
        $err_date = '<font size="1" color="red">Please enter post date</font>';
    }
    if (empty(trim($_POST['location']))) {
        $err = 1;
        $err_location = '<font size="1" color="red">Please enter location</font>';
    }
    if ($err == 0) {
        $form_data = array(
          'title' => addslashes($_POST['job_title']),
          //'short_desc' => addslashes($_POST['shortdesc']),
          'description' => addslashes($_POST['description']),
          'min_qualifica' => addslashes($_POST['qualification']),
          'num_of_post' => $_POST['numberopening'],
          'post_date' => $_POST['postdate'],
          'experience' => $_POST['experience'],
          'location' => $_POST['location']
        );
       
        if (empty($err_msg)) {
            $result = $db->update('job_career', $form_data, "where id='$id'");
            if (!empty($result)) {
                //$suc_msg = '<font size="1" color="green">Blog details updated successfully</font>';
                $_SESSION['suc_msg'] = '<font size="1" color="green">Blog details updated successfully</font>';
                echo'<script type="text/javascript">window.location.href = "job-list.php";</script>';
            } else {
                $err_msg = '<font size="1" color="red">Please try again</font>';
            }
        }
    }
}
?>
  
<div id="content">
<div id="content-header">
  <div id="breadcrumb"> <a href="index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
  <a href="job-list.php" class="tip-bottom">Job list</a> 
   <a href="#" class="current">Edit job</a> 
</div>
  
</div>
<div class="container-fluid">
 
  <?php if (isset($suc_msg)) {
    echo $suc_msg;
} $suc_msg = ''; ?>
  <?php if (isset($err_msg)) {
    echo $err_msg;
} $err_msg = ''; ?>
  <div class="row-fluid">
    <div class="span12">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Edit job</h5>
        </div>

        <?php $db = new Database();
        $result = $db->selectdata("job_career", "where id= '$id'");
        while ($row = $result->fetch_array()) {
            ?>

        <div class="widget-content nopadding">
          <form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
            <input type="hidden" name="table_name" value="blog" />
            <div class="control-group">
              <label class="control-label">Job title* :</label>
              <div class="controls">
          <input type="text" name="job_title" value="<?php if ($row['title']) {
                echo $row['title'];
            } ?>" class="span10" placeholder="Job title" />
                <?php echo $err_title; ?>
              </div>
            </div> 
             <!-- <div class="control-group">
              <label class="control-label">Shor description* :</label>
              <div class="controls">
                 <textarea name="shortdesc" class="span10" rows="6"><?php if ($row['short_desc']) {
                echo $row['short_desc'];
            } ?></textarea>
                 <?php echo $err_shortdesc; ?>
              </div>
            </div>  -->
            <div class="control-group">
              <label class="control-label">Description* :</label>
              <div class="controls">
                 <textarea name="description" class="span10" rows="6"><?php if ($row['description']) {
                echo $row['description'];
            } ?>
                  </textarea>
                 <?php echo $err_desc; ?>
              </div>
            </div> 
          <div class="control-group">
              <label class="control-label">Minimum qualification* :</label>
              <div class="controls">
                 <textarea name="qualification" class="span10" rows="6"><?php if ($row['min_qualifica']) {
                echo $row['min_qualifica'];
            } ?></textarea>
                 <?php echo $err_qualif; ?>
              </div>
            </div> 

            <div class="control-group">
              <label class="control-label">No Of Opening* :</label>
              <div class="controls">
        <input type="text" name="numberopening" value="<?php if ($row['num_of_post']) {
                echo $row['num_of_post'];
            } ?>" class="span10" placeholder="No of opening" />
                <?php echo $err_title; ?>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Posted date* :</label>
              <div class="controls">
        <input type="date" name="postdate" value="<?php if ($row['post_date']) {
                echo $row['post_date'];
            } ?>" class="span10" placeholder="Posted date" />
                <?php echo $err_date; ?>
              </div>
            </div>
             <div class="control-group">
              <label class="control-label">Experience* :</label>
              <div class="controls">
        <input type="text" name="experience" value="<?php if ($row['experience']) {
                echo $row['experience'];
            } ?>" class="span10" placeholder="Experience" />
                <?php echo $err_experience; ?>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Location* :</label>
              <div class="controls">
        <input type="text" name="location" value="<?php if ($row['location']) {
                echo $row['location'];
            } ?>" class="span10" placeholder="Location" />
                <?php echo $err_location; ?>
              </div>
            </div>
            
            <div class="form-actions">
              <input type="submit" class="btn btn-success" name="submit" value="submit" />
            </div>
          </form>
        </div>
        <?php
        } ?>
      </div>
      
    </div>
    
  </div>
 
</div></div>
<!--Footer-part-->
<div class="row-fluid">
  <!--<div id="footer" class="span12"> 2013 &copy; Matrix Admin. Brought to you by <a href="http://themedesigner.in">Themedesigner.in</a> </div>-->
</div>
  
<!--end-Footer-part-->
<script src="js/jquery.min.js"></script> 
<script src="js/jquery.ui.custom.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/bootstrap-colorpicker.js"></script> 
<script src="js/bootstrap-datepicker.js"></script> 
<script src="js/jquery.toggle.buttons.js"></script> 
<script src="js/masked.js"></script> 
<script src="js/jquery.uniform.js"></script> 
<script src="js/select2.min.js"></script> 
<script src="js/matrix.js"></script> 
<script src="js/matrix.form_common.js"></script> 
<script src="js/wysihtml5-0.3.0.js"></script> 
<script src="js/jquery.peity.min.js"></script> 
<script src="js/bootstrap-wysihtml5.js"></script> 

 
<script>
	$('.textarea_editor').wysihtml5();
</script>
</body>
</html>