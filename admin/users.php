<?php
session_start();
if (!$_SESSION['user_id']) {
    header('location:index.php');
}
include "header.php";
require_once("functions/user_list.php");
include "sidebar-menu.php";


if (!empty($_REQUEST['del_user'])) {
    $userId = $_REQUEST['del_user'];
    $db = new Database();
    $result = $db->delete('users', "WHERE id = '$userId'");
    $result = $db->delete('users_profile', "WHERE user_id = '$userId'");
    $result = $db->delete('notification', "WHERE sender_id = '$userId'");
    $result = $db->delete('notification', "WHERE receiver_id = '$userId'");
    if (!empty($result)) {
        $suc_msg = "Record deleted successfully";
    } else {
        $err_msg = "Please try again";
    }
}
if (!empty($_REQUEST['user_act'])) {
    $userId = $_REQUEST['user_act'];
    $db = new Database();
    $form_data = array(
      'status' => 1
  );
    $result = $db->update('users', $form_data, "where id='$userId'");
    $users_profile = $db->update('users_profile', $form_data, "where user_id='$userId'");
    if ($result) {
        $usersDATA = $db->selectdata('users', "where id='$userId'");
        $res = $usersDATA->fetch_assoc();
        $email = $res['email'];
        $username = $res['username'];
        $subject = 'Easemymatch Account approve';
        $headers = "From: info@easemymatch.com \r\n";
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
        $message = '<html><head></head><body><table style="max-width:600px;min-width:240px;border:1px solid #dcdcdc" border="0" cellspacing="0" cellpadding="0" align="center"><tbody><tr><td colspan="4" style="font-size:11px!important;font-family:Arial;color:black;padding-top:10px">
</td></tr><tr><td style="border-bottom:1px solid #ededed"><table cellspacing="0" cellpadding="0" border="0" width="100%"><tbody><tr><td width="373" height="52" style="padding-left:10px"><div><img border="0" align="left" vspace="0" hspace="0" style="max-width:120px;width:inherit" alt="easemymatch.com" src="http://easemymatch.com/img/logo.png"></div></td></tr></tbody></table></td></tr><tr><td>
<table style="border-spacing:0px 10px;max-width:600px;min-width:240px;font-family:Arial,Helvetica,sans-serif;font-size:12px" border="0" cellspacing="0" cellpadding="0"><tbody><tr><td width="20"></td>
<td valign="middle" height="35"> Dear '.$username.' </td>
<td width="20" height="25"></td></tr><tr><td width="20"></td><td>Your easemymatch account has been activated.Please Login into Easemymatch.com to choose your perfect Partner.</td>
<td width="20"></td></tr><tr><td width="20"></td><td></td><td width="20" height="25"></td>
</tr><tr><td width="20"></td><td>If you have any issue, please get in touch with customer service immediately on <a href="mailto:info@easemymatch.com" target="_blank">info@easemymatch.com</a> or 9354225779.
</td><td width="20"></td></tr></tbody></table></td></tr><tr><td width="600">
<table style="font-family:Arial,Helvetica,sans-serif;font-size:12px" border="0" cellspacing="0" cellpadding="0" width="100%"><tbody><tr><td width="20px" height="10"></td>
<td style="color:#4a4a4a"></td></tr><tr><td width="600"></td></tr><tr><td colspan="2" width="600"><table style="max-width:600px;min-width:240px" border="0" cellspacing="0" cellpadding="0"><tbody><tr><td> </td></tr></tbody></table></td></tr><tr></tr><tr><td valign="top" height="30"><table style="max-width:600px;min-width:240px" border="0" cellspacing="0" cellpadding="0"><tbody><tr><td width="20"></td>
<td style="font-family:Arial,Helvetica,sans-serif;font-size:12px">Wish you success in your search.</td></tr></tbody></table></td></tr><tr><td valign="top"><table style="max-width:600px;min-width:240px" border="0" cellspacing="0" cellpadding="0"><tbody><tr><td width="20"></td><td style="font-family:Arial,Helvetica,sans-serif;font-size:12px">Warm Regards,
<br><b style="color:#c4161c">easemymatch</b><span style="font-size:1px"> </span><b color="#00000">.com Team</b> </td></tr></tbody></table></td></tr><tr><td height="15"></td>
</tr><tr><td align="center" valign="top" height="20"><font face="Tahoma, Geneva, sans-serif" style="font-size:12px">Got any Questions?</font><a href="mailto:info@easemymatch.com" style="color:#0f529d" target="_blank">info@easemymatch.com </a> </td></tr>
<tr><td height="25"></td></tr></tbody></table></td></tr></tbody></table></body></html>';

        try {
            mail($email, $subject, $message, $headers);
        } catch (Exception $e) {
            echo $e;
        }
    }
}
if (!empty($_REQUEST['user_inact'])) {
    $userId = $_REQUEST['user_inact'];
    $db = new Database();
    $form_data = array(
        'status' => 0
    );
    $result = $db->update('users', $form_data, "where id='$userId'");
}
?>

<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">-->
 <script src="js/jquery.min.js"></script> 
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="dashboard.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> 
    <a href="users.php" class="current">Users</a> 
    <a href="#" class="current">Users Listing</a> 
    </div>
    <!--<h1>Users Listing</h1>-->
  </div>
  <div class="container-fluid">
    <!--<hr>-->
    <?php if (isset($suc_msg)) {
    echo $suc_msg;
} $suc_msg = ''; ?>
    <?php if (isset($err_msg)) {
    echo $err_msg;
} $err_msg = ''; ?>
    <div class="row-fluid">
      <div class="span12">
        
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>Users Listing</h5>
            <?php
            //$query_user = "SELECT u.id, u.username, u.email, up.user_id, up.height, up.religion, up.maritalstatus, up.follower, up.manglik_status, up.have_childern, up.living_with_parent, up.body_type, up.physical_status, up.drink, up.mothertounge, up.colour, up.weight, up.blood_group, up.diet, up.smoke, up.occupation, up.occupation_descr, up.annual_income, up.fathers_occupation, up.mothers_occupation, up.brother_count, up.sister_count, up.about_family, up.family_type, up.how_many_brother_married, up.how_many_sister_married
            //FROM users u, users_profile up
            //WHERE u.id = up.user_id";
            //$query = urlencode($query_user);
            ?>
            <!--<div><a href="<?php echo $base_url; ?>admin/ExcelSheetDownload.php?table_name=users&str=<?php echo $query; ?>">Download CSV</a></div>-->
          </div>
          <div class="widget-content nopadding table-responsive">
          <form method="post" action="<?php echo $base_url; ?>admin/ExcelSheetDownload.php?table_name=users&str=<?php echo $query; ?>">
            <div class="table-responsive">
            <?php $db = new Database();
              $admin_id = $_SESSION['user_id'];
              $result_admin = $db->selectdata("admin", "where id = '$admin_id' ");
              if ($result_admin) {
                  $res_admin = $result_admin->fetch_assoc();
                  $admin_loc = $res_admin['admin_location'];
                  $admin_type = $res_admin['type'];
              }
              if ($admin_type == 'super') {
                  $query = "SELECT u.id, u.username, u.email, u.profile_id, u.mobile_number, u.create_dates, u.status, up.user_id, up.height, up.religion,up.facebook_id, up.create_profile_for, up.dateofbirth, up.gender, up.caste, up.city, up.state, up.country, up.parent_together_country, up.parent_together_state,  up.parent_together_city, up.highest_degree, up.ug_degree, up.organization, up.about_me, up.dietary_habit, up.own_pets, up.own_house, up.own_car, up.residential_status, up.langauge_speak,
              up.subcaste, up.maritalstatus, up.have_childern, up.follower, up.manglik_status, up.have_childern, up.living_with_parent, up.body_type, up.physical_status, up.drink, up.mothertounge, up.colour, up.weight, up.blood_group, up.diet, up.smoke, up.occupation, up.occupation_descr, up.annual_income, up.fathers_occupation, up.mothers_occupation, up.nature_handicap, up.brother_count, up.sister_count, up.gothra, up.maternal_gothra, up.family_status, up.family_income, up.about_family, up.family_type, up.how_many_brother_married, up.how_many_sister_married
                FROM users u, users_profile up
                WHERE u.id = up.user_id ORDER BY u.id DESC";
              } else {
                  $query = "SELECT u.id, u.username, u.email, u.profile_id, u.mobile_number, u.create_dates, u.status, up.user_id, up.height, up.religion,up.facebook_id,  up.create_profile_for, up.dateofbirth, up.gender, up.caste, up.city, up.state, up.country, up.parent_together_country, up.parent_together_state,  up.parent_together_city, up.highest_degree, up.ug_degree, up.organization, up.about_me, up.dietary_habit, up.own_pets, up.own_house, up.own_car, up.residential_status, up.langauge_speak,
              up.subcaste, up.maritalstatus, up.have_childern, up.follower, up.manglik_status, up.have_childern, up.living_with_parent, up.body_type, up.physical_status, up.drink, up.mothertounge, up.colour, up.weight, up.blood_group, up.diet, up.smoke, up.occupation, up.occupation_descr, up.annual_income, up.fathers_occupation, up.mothers_occupation, up.nature_handicap, up.brother_count, up.sister_count, up.gothra, up.maternal_gothra, up.family_status, up.family_income, up.about_family, up.family_type, up.how_many_brother_married, up.how_many_sister_married
                FROM users u, users_profile up
                WHERE u.id = up.user_id AND up.city =  '".$admin_loc."' ORDER BY u.id DESC";
              }
              //$query = "SELECT u.id, u.username, u.email, u.mobile_number, u.status, up.user_id, up.height, up.religion, up.create_profile_for, up.dateofbirth, up.gender, up.caste, up.city, up.state, up.country, up.parent_together_country, up.parent_together_state,  up.parent_together_city, up.highest_degree, up.ug_degree, up.organization, up.about_me, up.dietary_habit, up.own_pets, up.own_house, up.own_car, up.residential_status, up.langauge_speak, up.subcaste, up.maritalstatus, up.have_childern, up.follower, up.manglik_status, up.have_childern, up.living_with_parent, up.body_type, up.physical_status, up.drink, up.mothertounge, up.colour, up.weight, up.blood_group, up.diet, up.smoke, up.occupation, up.occupation_descr, up.annual_income, up.fathers_occupation, up.mothers_occupation, up.nature_handicap, up.brother_count, up.sister_count, up.gothra, up.maternal_gothra, up.family_status, up.family_income, up.about_family, up.family_type, up.how_many_brother_married, up.how_many_sister_married FROM users u, users_profile up WHERE u.id = up.user_id ORDER BY u.id ASC";
              ?>
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>Select</th>
                  <th>S.no</th>
                  <th>Profile Id</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Mobile</th>
                  <th>Facebook id</th>
                  <th>Height</th>
                  <th>Religion</th>
                  <th>Profile Created for</th>
                  <th>DOB</th>
                  <th>Gender</th>
                  <th>Caste</th>
                  <!--<th>Subcaste</th>-->
                  <th>Country</th>
                  <th>State</th>
                  <th>City</th>
                  <th>Marital Status</th>
                  <th>Follower</th>
                  <th>Manglik Status</th>
                  <th>Have Children</th>
                  <th>Living with</th>
                  <th>Parent Country</th>
                  <th>Parent State</th>
                  <th>Parent City</th>
                  <th>Highest degree</th>
                  <th>Degree</th>
                  <th>Organisation</th>
                  <th>About me</th>
                  <th>Body type</th>
                  <th>Physical status</th>
                  <th>Drink</th>
                  <th>Mother Tongue</th>
                  <th>Color</th>
                  <th>Weight</th>
                  <th>Blood Group</th>
                  <th>Smoke</th>
                  <th>Dietary Habit</th>
                  <th>Pets</th>
                  <th>House</th>
                  <th>Car</th>
                  <th>Residential status</th>
                  <th>Language Known</th>
                  <th>Handicap</th>
                  <th>Occupation</th>
                  <th>Occupation Desc</th>
                  <th>Annual Income</th>
                  <th>Fathers Occupation</th>
                  <th>Mother Occupation</th>
                  <th>Brother</th>
                  <th>Sister</th>
                  <th>Gotra</th>
                  <th>Maternal Gotra</th>
                  <th>Family status</th>
                  <th>Family income</th>
                  <th>About family</th>
                  <th>Family type</th>
                  <th>No of brother married</th>
                  <th>No of sister married</th>
                  <th>Registration Date</th>
                  <th>Admin Feedback</th>
                  <?php if ($admin_type == 'super') {
                  ?>
                  <th>Action</th>
                  <?php
              } ?>
                </tr>
              </thead>
              <tbody>
            

              <?php
              $result = $db->select($query);
              $i=1;
              if (!empty($result)) {
                  while ($row = $result->fetch_array()) {
                      $casteId = $row['caste'];
                      $subcasteId = $row['subcaste'];
                      $queryCaste = "SELECT * FROM `caste` WHERE `id` = '".$casteId."' ";
                      $result_caste = $db->select($queryCaste);
                      if (!empty($result_caste)) {
                          $row_caste = $result_caste->fetch_array();
                          $caste_name = $row_caste['name'];
                      } else {
                          $caste_name = '';
                      }
                      $querySubCaste = "SELECT * FROM `caste` WHERE `id` = '".$subcasteId."' ";
                      $result_sub_caste = $db->select($querySubCaste);
                      if (!empty($result_sub_caste)) {
                          $row_sub_caste = $result_sub_caste->fetch_array();
                          $sub_caste_name = $row_sub_caste['name'];
                      } else {
                          $sub_caste_name = '';
                      } ?>
               <tr class="gradeX" id="changeme<?php echo $i; ?>">
                  <td><input type="checkbox" name="select[]" id="test<?php echo $i; ?>" value="<?php echo $row['id']; ?>"></td>
                  <td class=""><?php echo $i; ?></td>
                  <td><?php echo $row['profile_id']; ?></td>
                  <td><?php echo $row['username']; ?></td>
                  <td><?php echo $row['email']; ?></td>
                  <td><?php echo $row['mobile_number']; ?></td>
                  <td><?php echo $row['facebook_id']; ?></td>
                  <td><?php echo $row['height']; ?></td>
                  <td><?php echo $row['religion']; ?></td>
                  <td><?php echo $row['create_profile_for']; ?></td>
                  <td>
                  <?php if (empty($row['dateofbirth'])) {
                          echo '';
                      } else {
                          echo date('d-M-Y', strtotime($row['dateofbirth'])) ;
                      } ?>
                    
                  </td>
                  <td><?php echo $row['gender']; ?></td>
                  <td><?php echo $caste_name; ?></td>
                  <!--<td><?php echo $row['subcaste']; ?></td>-->
                  <?php
                  $country_id = $row['country'];
                      $result_country = $db->selectdata("countries", "where countryID =  '$country_id' ");
                      if ($result_country) {
                          $row_country = $result_country->fetch_assoc();
                          $countryName = $row_country['countryName'];
                      } ?>
                  <td><?php echo $countryName; ?></td>
                  <?php
                  $state_id = $row['state'];
                      $result_state = $db->selectdata("states", "where stateID =  '$state_id' ");
                      if ($result_state) {
                          $row_state = $result_state->fetch_assoc();
                          $stateName = $row_state['stateName'];
                      } ?>
                  <td><?php echo $stateName; ?></td>
                  <?php
                  $city_id = $row['city'];
                      $result_city = $db->selectdata("cities", "where cityID =  '$city_id' ");
                      if ($result_city) {
                          $row_city = $result_city->fetch_assoc();
                          $cityName = $row_city['cityName'];
                      } ?>
                  <td><?php echo $cityName; ?></td>
                  <td><?php echo $row['maritalstatus']; ?></td>
                  <td><?php echo $row['follower']; ?></td>
                  <td><?php echo $row['manglik_status']; ?></td>
                  <td><?php echo $row['have_childern']; ?></td>
                  <td><?php echo $row['living_with_parent']; ?></td>
                  <?php
                  $country_idd = $row['parent_together_country'];
                      $result_countryy = $db->selectdata("countries", "where countryID =  '$country_idd' ");
                      if ($result_countryy) {
                          $row_countryy = $result_countryy->fetch_assoc();
                          $countryNamee = $row_countryy['countryName'];
                      } ?>
                  <td><?php echo $countryNamee; ?></td>
                  <?php
                  $state_idd = $row['parent_together_state'];
                      $result_statee = $db->selectdata("states", "where stateID =  '$state_idd' ");
                      if ($result_statee) {
                          $row_statee = $result_statee->fetch_assoc();
                          $stateNamee = $row_statee['stateName'];
                      } ?>
                  <td><?php echo $stateNamee; ?></td>
                  <?php
                  $city_idd = $row['parent_together_city'];
                      $result_cityy = $db->selectdata("cities", "where cityID =  '$city_idd' ");
                      if ($result_cityy) {
                          $row_cityy = $result_cityy->fetch_assoc();
                          $cityNamee = $row_cityy['cityName'];
                      } ?>
                  <td><?php echo $cityNamee; ?></td>
                  <td><?php echo $row['highest_degree']; ?></td> 
                  <td><?php echo $row['ug_degree']; ?></td>
                  <td><?php echo $row['organization']; ?></td>
                  <td><?php //echo $row['about_me'];?>
                 <?php $description = $row['about_me'];
                      $limit = 50;
                      if (strlen($description)<=$limit) {
                          echo $description;
                      } else {
                          $text = substr($description, 0, $limit) .'...';
                          echo $text;
                      } ?></td>
                  <td><?php echo $row['body_type']; ?></td>
                  <td><?php echo $row['physical_status']; ?></td>
                  <td><?php echo $row['drink']; ?></td>
                  <td><?php echo $row['mothertounge']; ?></td>
                  <td><?php echo $row['colour']; ?></td>
                  <td><?php echo $row['weight']; ?></td>
                  <td><?php echo $row['blood_group']; ?></td>
                  <td><?php echo $row['smoke']; ?></td>
                  <td><?php echo $row['dietary_habit']; ?></td>
                  <td><?php echo $row['own_pets']; ?></td>
                  <td><?php echo $row['own_house']; ?></td>
                  <td><?php echo $row['own_car']; ?></td>
                  <td><?php echo $row['residential_status']; ?></td>

                  <?php
                  $lang = json_decode($row['langauge_speak']);
                      $langg = implode(', ', $lang); ?>
                  <td><?php echo $langg; ?></td>
                  <td><?php echo $row['nature_handicap']; ?></td>
                  <td><?php echo $row['occupation']; ?></td>
                  <td><?php echo $row['occupation_descr']; ?></td>
                  <td><?php if (empty($row['annual_income'])) {
                          echo "No Income";
                      } else {
                          echo $row['annual_income'] . " Lakh";
                      } ?></td>
                  <td><?php echo $row['fathers_occupation']; ?></td>
                  <td><?php echo $row['mothers_occupation']; ?></td>
                  <td><?php echo $row['brother_count']; ?></td>
                  <td><?php echo $row['sister_count']; ?></td>
                  <td><?php echo $row['gothra']; ?></td>
                  <td><?php echo $row['maternal_gothra']; ?></td>
                  <td><?php echo $row['family_status']; ?></td>
                  <td><?php if (empty($row['family_income'])) {
                          echo "No Income";
                      } else {
                          echo $row['family_income'] . " Lakh";
                      } ?></td>
                  <!-- <td><?php //echo $row['about_family'];?></td> -->
                  <td><?php //echo $row['about_me'];?>
                 <?php $about_family = $row['about_family'];
                      $limits = 50;
                      if (strlen($about_family)<=$limits) {
                          echo $about_family;
                      } else {
                          $texts = substr($about_family, 0, $limits) .'...';
                          echo $texts;
                      } ?></td>
                  <td><?php echo $row['family_type']; ?></td>
                  <td><?php echo $row['how_many_brother_married']; ?></td>
                  <td><?php echo $row['how_many_sister_married']; ?></td>
                  <?php
                  $datt = strtotime($row['create_dates']); ?>
                  <td><?php echo date('d-m-Y', $datt); ?></td>
                  <?php $usersIDSED = $row['id'];
                      $usersfeed = $db->selectdata('admin_feedback', "where user_id='$usersIDSED'");
                      if ($usersfeed) {
                          $color = "green";
                      } else {
                          $color = "red";
                      } ?>
                  <td><a style="color:<?php echo $color; ?>" href="admin_feedback.php?id=<?php echo $row['id']; ?>">Feedback</a></td>
                  
                  <?php if ($admin_type == 'super') {
                          ?>
                  <td><!-- <a href="edit.php?id=<?php echo $row['id']; ?>">Edit</a> | <a href="view_user.php?id=<?php echo $row['id']; ?>">View</a> | --> <a href="javascript:void();" onclick="doConfirm('<?php echo $row['id']; ?>');">Delete</a> | <a href="javascript:void();" onclick="doActivate('<?php echo $row['id']; ?>');"><?php if ($row['status']==1) {
                              echo '<font size="2" color="green">Active</font>';
                          } else {
                              echo "Active";
                          } ?></a> | <a href="javascript:void();" onclick="doInactivate('<?php echo $row['id']; ?>');"><?php if ($row['status']==0) {
                              echo '<font size="2" color="red">Inactive</font>';
                          } else {
                              echo "Inactive";
                          } ?> </a></td>
                  <?php
                      } ?>
                </tr>
                <script>
$('#test<?php echo $i; ?>').change(function(){
    if($(this).is(":checked")) {
        $('#changeme<?php echo $i; ?>').addClass('red');
    } else {
        $('#changeme<?php echo $i; ?>').removeClass('red');
    }
});
</script>
              <?php $i++;
                  }
              }?>
              

              </tbody>
            </table>
          </div>
            <input type="submit" value="Download CSV" name="submit"> 
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>




<script>
    function doConfirm(id)
    {
        var ok = confirm("Are you sure you want to Delete?")
        if (ok)
        {
            location.href='users.php?del_user='+id;
        }
    }
    function doActivate(id)
    {
        var ok = confirm("Are you sure you want to Activate?")
        if (ok)
        {
            location.href='users.php?user_act='+id;
        }
    }
    function doInactivate(id)
    {
        var ok = confirm("Are you sure you want to Inactivate?")
        if (ok)
        {
            location.href='users.php?user_inact='+id;
        }
    }
</script>
<style>
 .red {background:#efefef !important;color:#000;}
</style>




<!--Footer-part-->
<div class="row-fluid">
  <!--<div id="footer" class="span12"> 2013 &copy; Matrix Admin. Brought to you by <a href="http://themedesigner.in">Themedesigner.in</a> </div>-->
</div>
<!--end-Footer-part-->

<script src="js/jquery.ui.custom.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/jquery.uniform.js"></script> 
<script src="js/select2.min.js"></script> 
<script src="js/jquery.dataTables.min.js"></script> 
<script src="js/matrix.js"></script> 
<script src="js/matrix.tables.js"></script>
</body>
</html>