<?php
session_start();
if (!$_SESSION['user_id']) {
    header('location:index.php');
}
include "header.php";
require_once("functions/user_list.php");
include "sidebar-menu.php";
$db = new Database();
$id = $_REQUEST['id'];

if (isset($_POST['submit'])) {
    $title = $_POST['title'];
    $err = 0;
    if (empty(trim($title))) {
        $err = 1;
        $err_title = '<font size="1" color="red">Please enter title</font>';
    }
    if (empty(trim($_POST['about-client']))) {
        $err = 1;
        $about = '<font size="1" color="red">Please enter about client</font>';
    }
    if (empty(trim($_POST['overview']))) {
        $err = 1;
        $overview = '<font size="1" color="red">Please enter project overview</font>';
    }
    if (empty(trim($_POST['statment']))) {
        $err = 1;
        $statment = '<font size="1" color="red">Please identifying the problem statement</font>';
    }
    if (empty(trim($_POST['benifits']))) {
        $err = 1;
        $benifits = '<font size="1" color="red">Please enter client benifits</font>';
    }
    if (empty($_FILES['bimage'])) {
        $err = 1;
        $bimage = '<font size="1" color="red">Please enter banner image</font>';
    }
    if (empty($_FILES['pimage'])) {
        $err = 1;
        $pimage = '<font size="1" color="red">Please enter profile image</font>';
    }
    if (empty($_POST['banner-text'])) {
        $err = 1;
        $banner = '<font size="1" color="red">Please enter banner text</font>';
    }
    if ($err == 0) {
        $target_dir = "uploads/";
        if (empty($_FILES["pimage"]["name"])) {
            $pimage = $_POST['oldpimage'];
        } else {
            $pimage = md5(uniqid()) . $_FILES["pimage"]["name"];
            $pimage_files = $target_dir . $pimage;
            strtolower(pathinfo($pimage_files, PATHINFO_EXTENSION));
            move_uploaded_file($_FILES["pimage"]["tmp_name"], $pimage_files);
        }
                
        if (empty($_FILES["bimage"]["name"])) {
            $bimage = $_POST['oldbimage'];
        } else {
            $bimage = md5(uniqid()) . $_FILES["bimage"]["name"];
            $bimage_file = $target_dir . $bimage;
            strtolower(pathinfo($bimage_file, PATHINFO_EXTENSION));
            move_uploaded_file($_FILES["bimage"]["tmp_name"], $bimage_file);
        }
        $form_data = array(
                  'title' => addslashes($_POST['title']),
                  'about_client' => addslashes($_POST['about-client']),
                  'overview' => addslashes($_POST['overview']),
                  'benifit' => addslashes($_POST['benifits']),
                  'statment' => addslashes($_POST['statment']),
                  'banner-text' => addslashes($_POST['banner-text']),
                  'pimage' => $pimage,
                  'bimage' => $bimage
                );
        $result = $db->update('case_study', $form_data, "WHERE id='$id'");
        if ($result) {
            $_SESSION['suc_msg'] = '<font size="1" color="green">Details saved successfully</font>';
            echo'<script type="text/javascript">window.location.href = "case-study-list.php";</script>';
        }
    }
}
?>
  
<div id="content">
<div id="content-header">
  <div id="breadcrumb"> <a href="index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
  <a href="case-study-list.php" class="tip-bottom">Case study list</a> 
   <a href="#" class="current">Edit Case study</a> 
</div>
  
</div>
<div class="container-fluid">
 
  <?php if (isset($suc_msg)) {
    echo $suc_msg;
} $suc_msg = ''; ?>
  <?php if (isset($err_msg)) {
    echo $err_msg;
} $err_msg = ''; ?>
  <div class="row-fluid">
    <div class="span12">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Edit job</h5>
        </div>

        <?php $db = new Database();
        $result = $db->selectdata("case_study", "where id= '$id'");
        while ($row = $result->fetch_assoc()) {
            ?>

<div class="widget-content nopadding col-sm-12">
          <form action="" method="post" class="form-horizontal" enctype='multipart/form-data'>
            
            <div class="control-group">
              <label class="control-label">Title* :</label>
              <div class="controls">
        <input type="text" name="title" value="<?php if ($row['title']) {
                echo $row['title'];
            } ?>" class="span10" placeholder="title" />
                <?php echo $err_title; ?>
              </div>
            </div>                
            
            <div class="control-group">
              <label class="control-label">Banner text* :</label>
              <div class="controls">
                 <textarea name="banner-text" class="span10" rows="6"><?php if ($row['banner-text']) {
                echo $row['banner-text'];
            } ?></textarea>
                 <?php echo $banner; ?>
              </div>
            </div>
             <div class="control-group">
              <label class="control-label">About Client* :</label>
              <div class="controls">
                 <textarea name="about-client" class="span10" rows="6"><?php if ($row['about_client']) {
                echo $row['about_client'];
            } ?></textarea>
                 <?php echo $about; ?>
              </div>
            </div> 
            <div class="control-group">
              <label class="control-label">Project Overview* :</label>
              <div class="controls">
                 <textarea name="overview" class="span10" rows="6"><?php if ($row['overview']) {
                echo $row['overview'];
            } ?></textarea>
                 <?php echo $overview; ?>
              </div>
            </div> 
            <div class="control-group">
              <label class="control-label">Identifying The Problem Statement* :</label>
              <div class="controls">
                 <textarea name="statment" class="span10" rows="6"><?php if ($row['statment']) {
                echo $row['statment'];
            } ?></textarea>
                 <?php echo $statment; ?>
              </div>
            </div> 
            <div class="control-group">
              <label class="control-label">Client Benefits* :</label>
              <div class="controls">
                 <textarea name="benifits" class="span10" rows="6"><?php if ($row['benifit']) {
                echo $row['benifit'];
            } ?></textarea>
                 <?php echo $benifits; ?>
              </div>
            </div>

            <div class="control-group">
              <label class="control-label">Banner Image* :</label>
              <div class="controls">
                 <input type="file"  name="bimage"/>
                 <input type="hidden" name="oldbimage" value="<?php echo $row['bimage']; ?>">
                 <div class="img-circle">
                <?php if (!empty($row['bimage'])) {
                $bimages = $row['bimage'];
                echo '<img src="uploads/'.$bimages.'"  width="80px;" height="80px;" >';
            } ?>
            </div>
                 <?php echo $bimage; ?>
              </div>
            </div>

            <div class="control-group">
              <label class="control-label">Profile Image* :</label>
              <div class="controls">
                 <input type="file"  name="pimage"/>
                 <input type="hidden" name="oldpimage" value="<?php echo $row['pimage']; ?>">
                 <div class="img-circle">
                <?php if (!empty($row['pimage'])) {
                $imagee = $row['pimage'];
                echo '<img src="uploads/'.$imagee.'"  width="80px;" height="80px;" >';
            } ?>
            </div>
                 <?php echo $pimage; ?>
              </div>
            </div>

            <div class="form-actions">
              <input type="submit" class="btn btn-success" name="submit" value="submit" />
            </div>
          </form>
        </div>
        <?php
        } ?>
      </div>
      
    </div>
    
  </div>
 
</div></div>
<!--Footer-part-->
<div class="row-fluid">
  <!--<div id="footer" class="span12"> 2013 &copy; Matrix Admin. Brought to you by <a href="http://themedesigner.in">Themedesigner.in</a> </div>-->
</div>
  
<!--end-Footer-part-->
<script src="js/jquery.min.js"></script> 
<script src="js/jquery.ui.custom.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/bootstrap-colorpicker.js"></script> 
<script src="js/bootstrap-datepicker.js"></script> 
<script src="js/jquery.toggle.buttons.js"></script> 
<script src="js/masked.js"></script> 
<script src="js/jquery.uniform.js"></script> 
<script src="js/select2.min.js"></script> 
<script src="js/matrix.js"></script> 
<script src="js/matrix.form_common.js"></script> 
<script src="js/wysihtml5-0.3.0.js"></script> 
<script src="js/jquery.peity.min.js"></script> 
<script src="js/bootstrap-wysihtml5.js"></script> 

 
<script>
	$('.textarea_editor').wysihtml5();
</script>
</body>
</html>
