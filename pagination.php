<?php
      $limit = 3;
      /*How may adjacent page links should be shown on each side of the current page link.*/
      $adjacents = 2;
      $sql = "SELECT COUNT(*) 'total_rows' FROM `posts`";
      $res = mysqli_fetch_object(mysqli_query($con, $sql));
      $total_rows = $res->total_rows;
      $total_pages = ceil($total_rows / $limit);
      
      
      if (isset($_GET['page']) && $_GET['page'] != "") {
          $page = $_GET['page'];
          $offset = $limit * ($page-1);
      } else {
          $page = 1;
          $offset = 0;
      }
      $query  = "select * from `posts` limit $offset, $limit";
      $result = mysqli_query($con, $query);
      if (mysqli_num_rows($result) > 0) {
          while ($row = mysqli_fetch_object($result)) {
              echo '<h1 class="post-title"><a href="'.$row->link.'" target="_blank">'.$row->title.'</a></h1>';
              echo '<p>'.$row->content.'</p>';
          }
      }
      //Here we generates the range of the page numbers which will display.
      if ($total_pages <= (1+($adjacents * 2))) {
          $start = 1;
          $end   = $total_pages;
      } else {
          if (($page - $adjacents) > 1) {
              if (($page + $adjacents) < $total_pages) {
                  $start = ($page - $adjacents);
                  $end   = ($page + $adjacents);
              } else {
                  $start = ($total_pages - (1+($adjacents*2)));
                  $end   = $total_pages;
              }
          } else {
              $start = 1;
              $end   = (1+($adjacents * 2));
          }
      }
?>

For pagination==========

<?php if ($total_pages > 1) {
    ?>
          <ul class="pagination pagination-sm justify-content-center">
            <!-- Link of the first page -->
            <li class='page-item <?php ($page <= 1 ? print 'disabled' : '')?>'>
              <a class='page-link' href='MTB-php-pagination-with-bootstrap/?page=1'><<</a>
            </li>
            <!-- Link of the previous page -->
            <li class='page-item <?php ($page <= 1 ? print 'disabled' : '')?>'>
              <a class='page-link' href='MTB-php-pagination-with-bootstrap/?page=<?php ($page>1 ? print($page-1) : print 1)?>'><</a>
            </li>
            <!-- Links of the pages with page number -->
            <?php for ($i=$start; $i<=$end; $i++) {
        ?>
            <li class='page-item <?php ($i == $page ? print 'active' : '')?>'>
              <a class='page-link' href='MTB-php-pagination-with-bootstrap/?page=<?php echo $i; ?>'><?php echo $i; ?></a>
            </li>
            <?php
    } ?>
            <!-- Link of the next page -->
            <li class='page-item <?php ($page >= $total_pages ? print 'disabled' : '')?>'>
              <a class='page-link' href='MTB-php-pagination-with-bootstrap/?page=<?php ($page < $total_pages ? print($page+1) : print $total_pages)?>'>></a>
            </li>
            <!-- Link of the last page -->
            <li class='page-item <?php ($page >= $total_pages ? print 'disabled' : '')?>'>
              <a class='page-link' href='MTB-php-pagination-with-bootstrap/?page=<?php echo $total_pages; ?>'>>>                      
              </a>
            </li>
          </ul>
       <?php
} ?>