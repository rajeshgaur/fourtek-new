<?php error_reporting(1); 
require_once("admin/functions/user_list.php");
$db = new Database();


require_once "./vendor/autoload.php";

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

if(isset($_POST['submit']))
{
  //$_POST=array();
  
  if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])){
    //your site secret key
    $secret = '6LcfIYMUAAAAAJVBnF2cjuzCdSremj4GUx8fRJGY';
    //get verify response data
    $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
    $responseData = json_decode($verifyResponse);
    if($responseData->success){
$name = $_POST['FirstName'];
$visitor_email = $_POST['email'];
$phone = $_POST['phone'];
$message = $_POST['message'];

//Create a new PHPMailer instance 
$mail = new PHPMailer;
$mail->isSMTP();
$mail->SMTPDebug = 0;
$mail->Host = 'smtp.gmail.com';
$mail->Port = 587;
$mail->SMTPSecure = 'tls';
$mail->SMTPAutoTLS = false;
$mail->SMTPAuth = true;
$mail->Username = "adwords@fourtek.com";
$mail->Password = "Fourtek@0101";
$mail->setFrom($visitor_email, 'Fourtek Contact Us');
$mail->addAddress('himanshu.fourtek@gmail.com');
$mail->addAddress('arpit@fourtek.com');
$mail->addAddress('nakul@fourtek.com');
$mail->addAddress('nitisha@fourtek.com');
$mail->addAddress('manoj@fourtek.com');
$mail->Subject = 'Fourtek Contact Us';

$mail->msgHTML('<html><head> </head><body> <table border="1" cellpadding="5">
        <tr> <td colspan="2"><strong>Your Name</strong></td>
          <td colspan="2">'.$_POST["name"].'</td>
        </tr>
        <tr>
          <td colspan="2"><strong>Your Email</strong></td>
          <td colspan="2">'.$_POST["email"].'</td>
        </tr>
        <tr>
          <td colspan="2"><strong>Your Phone</strong></td>
          <td colspan="2">'.$_POST["mobile"].'</td>
        </tr>       
        <tr>
          <td colspan="2"><strong>Comment</strong></td>
          <td colspan="2">'.$_POST["comment"].'</td>
        </tr>
      </table>
      </body>
      </html>');
      if (!$mail->send()) {
        $errmsg = "Mailer Error: " . $mail->ErrorInfo;
       } else {  
        $succmsg = "<div class='success-msg' style='color:green;'>Your Message has been Sent Successfully.</div>";
        $form_data = array(
        'name' => $_POST['name'],
        'email' => $_POST['email'],
        'mobile' => $_POST['mobile'],
        'comment' => $_POST['comment']
          );
        $result = $db->insert_data('contact_us_form', $form_data); 
       }
    } else { $errmsg = 'Robot verification failed, please try again.'; }
  } else {
    $errmsg = 'Please click on the reCAPTCHA box.';
  }
} else {
  $errmsg = '';
  $succmsg = '';
}


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Contact us for quality web, app development & out of box digital marketing. Share your ideas with us and we will transform into results.">
    <meta name="keywords" content="app development, digital marketing, web development, contact us">
    <title>Contact Us for Web, App Development and Digital Marketing - Fourtek</title>
    <link rel="canonical" href="https://www.fourtek.com/contact-us">
    <link rel="shortcut icon" type="image/icon" href="images/fav.ico" />
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Pattaya|Poppins:300,300i,400" rel="stylesheet">
    <link href="css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
    <link href="css/animate.min.css" rel="stylesheet" type="text/css">
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link href="css/responsive.css" rel="stylesheet" type="text/css">
    <script src="js/jquery.min.js"></script>
    <?php include "google-code.php";?>
  </head>
<style>
  header{background: url(images/contact-us.jpg) !important; background-size: cover !important;  background-repeat: no-repeat;background-attachment: fixed !important;}
</style>
 <body id="page-top" class="inner-page">
      <?php include 'include/menu.php' ; ?>
  
    <header class="masthead video">
      <div class="container h-100">
        <div class="row h-100">
          <div class="col-12 my-auto text-center text-white">          
            <h1>Contact Us</h1>
            <p>We ain’t let you hit an absurdly long phone menu to contact us or let your email to remain in the inbox abyss. We are here to listen from you. We are here to serve you!</p>
          </div>
        </div>
      </div>
    </header>

<section class="breadcrumb-block">
<div class="container">
  <div class="breadcrumb">
    <a class="breadcrumb-item" href="index.php">Home</a>
    <span class="breadcrumb-item active">Contact Us</span>
  </div>
</div>
</section>

    <!-- About Section -->
    <section class="about-sections contact-forms">
      <div class="container">
        <div class="row"> 
        <div class="col-sm-7">
        <div class="wow fadeIn text-center">
          <h2><span class="contact-head">We’re All Ears: Talk to Us Anytime</span></h2>
          <p>We are an organization built on four pillars: (1) Trust (2) Transparency (3) Quality (4) Devotion. We provide you with the most suitable solutions for your business along with robust maintenance and support. We are committed to serve you with more information, answer your questions and solve your problems with our experienced and robust communication support.
</p>
        </div>
         <h4 style="margin-bottom:20px;">Let's Start the Project</h4>
         <p style="color:red;"><?php echo $errmsg;?></p>
         <p style="color:green;"><?php echo $succmsg;?></p>
         <form class="form" role="form"  action="" method="POST">
            <div class="form-group">
                <input type="text" id="name" class="form-control rounded-0" name="name"  required="" value="<?php //echo $_POST['name']?>" placeholder="Your Name *" required="">
            </div>
            <div class="form-group">
                <input type="email" class="form-control rounded-0" name="email" required="" value="<?php //echo $_POST['email']?>" placeholder="Your Email *" required="">
            </div>
            <div class="form-group">
                <input type="text" class="form-control rounded-0" name="mobile" required="" minlength="10" maxlength="15" value="<?php //echo $_POST['mobile']?>" pattern="^[0-9]*$" placeholder="Your Phone Number *" required="">
            </div>
            <div class="form-group">
                <textarea class="form-control rounded-0" name="comment" rows="8" placeholder="Type your comment here"><?php //echo $_POST['comment']?></textarea>
            </div>

<div class="row">
  <div class="col-md-6">
  <div class="g-recaptcha" data-sitekey="6LcfIYMUAAAAAGI1bmHjGaNkvJo4xnzzijcTarIe"></div>
  </div>
  <div class="col-md-12 contact-form-btn" style="">
  <div class="form-group">
            
            <button type="submit" name="submit" class="btn-contact">Send Message</button>
        </div>
  </div>

<div class="clearfix"></div>

</div>



             </form>
             </div>

  
        <div class="col-sm-5">
        <div class="wrapper center-block">
  <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
  <div class="panel panel-default">
    <div class="panel-heading active" role="tab" id="headingOne">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
        <strong>Corporate Address</strong>
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse show" role="tabpanel" aria-labelledby="headingOne">
    <div class="panel-body">
    <p>In case of any query, inquiry or suggestion, feel free to contact us on given contact number or email address. You may also drop us a mail at the mentioned address.</p>
     <ul class="panel-address">
     <li><i class="fa fa-map-marker" aria-hidden="true"></i> B86, Sector 60, Noida ( U.P.) 201301 India</li>
     <li><i class="fa fa-phone" aria-hidden="true"></i> +91-120-428-4526, +91-120-415-1299</li>
     <li><i class="fa fa-envelope" aria-hidden="true"></i> info@fourtek.com</li>
     </ul>
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingTwo">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          New Delhi
        </a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
    <div class="panel-body">
    <p>Find out how  Fourtek can help your business succeed online. Please call us or fill the form below and we will get back to you.</p>
     <ul class="panel-address">
     <li><i class="fa fa-map-marker" aria-hidden="true"></i> B-31/A, Street No.2 Jagat Puri, Delhi-110051.</li>
     <li><i class="fa fa-phone" aria-hidden="true"></i> +91-120-428-4526, +91-120-415-1299</li>
     <li><i class="fa fa-envelope" aria-hidden="true"></i> info@fourtek.com</li>
     </ul>
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingThree">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
        Andhra Pradesh
        </a>
      </h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
    <div class="panel-body">
    <p>Find out how  Fourtek can help your business succeed online. Please call us or fill the form below and we will get back to you.</p>
     <ul class="panel-address">
     <li><i class="fa fa-map-marker" aria-hidden="true"></i>D.No 45-7-11 Gudimetlavari Street, Jagannaikpur,</li>
       <li> East Godavari Dist, Andhra Pradesh, India - 533002.</li>
     <li><i class="fa fa-phone" aria-hidden="true"></i> +91-120-428-4526, +91-120-415-1299</li>
     <li><i class="fa fa-envelope" aria-hidden="true"></i> info@fourtek.com</li>
     </ul>
      </div>
    </div>
    <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingFour">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseTwo">
        Japan
        </a>
      </h4>
    </div>
    <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
    <div class="panel-body">
    <p>Find out how  Fourtek can help your business succeed online. Please call us or fill the form below and we will get back to you.</p>
     <ul class="panel-address">
     <li><i class="fa fa-map-marker" aria-hidden="true"></i> 502 4-44-25 Honcho, Funabashi, </li>
     <li>Chibha Prefecture 273-0005 Japan</li>
     <li><i class="fa fa-envelope" aria-hidden="true"></i> info@fourtek.com</li>
     </ul>
      </div>
    </div>
  </div>
  </div>
</div>
</div>
</div>
</div> 
</div>
</section>
<section class="breadcrumb-block">
<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d224238.50972621195!2d77.329146!3d28.577968!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x9f1b0b68a91d0992!2sDigital+Marketing+Company+In+India%2C+Mobile+App+Developers%2C+Web+Development+-+Fourtek+IT+Solutions!5e0!3m2!1sen!2sin!4v1454498355262" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</section>
<?php include 'include/footer.php' ;?>
<script>
  $(document).ready(function(){
     $('.panel-collapse').on('show.bs.collapse', function () {
    $(this).siblings('.panel-heading').addClass('active');
  });

  $('.panel-collapse').on('hide.bs.collapse', function () {
    $(this).siblings('.panel-heading').removeClass('active');
  });
  });
  </script>
  </body>
</html>
