=== Digital Services ===
Contributors: wpdigipro
Tags: one-column, two-columns,left-sidebar,grid-layout,custom-logo,custom-colors,custom-menu,featured-images,translation-ready,blog,featured-image-header,footer-widgets,full-width-template,theme-options,threaded-comments
Requires PHP: 5.2.4
Requires at least: 4.0
Tested up to: 4.9.7
Stable tag: 1.0.0
License: GNU General Public License v3 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html

Short description. Digital Services is a WordPress theme developed mainly for freelancer, digital agencies and companies who offer digital services like website designing, website development, internet marketing, SEO etc.

== Description ==
Digital Services is a WordPress theme developed mainly for freelancer, digital agencies and companies who offer digital services like website designing, website development, internet marketing, SEO etc. Digital Services theme is a mobile responsive WordPress theme so your website looks amazing in all the mobile devices, Macs and PCs. Digital Services WordPress theme is fully managed by theme customizer and hence you can easily change colors, logo etc from WordPress customizer settings. To make your website a fully functional membership website you can buy and add WPDigiPro WordPress plugin (available at WPDigiPro.com). You can integrate PayPal to receive Payments and automatically membership area will be created for your clients from where they will be able to raise their tickets, access to your digital products etc. Check more details of WPDigiPro at WPDigiPro.com. Step by Step documentation of Digital Services WordPress theme along with dummy data is here: https://wpdigipro.com/documentation/digital-services-wordpress-theme/

== Copyright ==
Digital Services WordPress Theme, Copyright 2018 WPDigiProThemes
Digital Services is distributed under the terms of the GNU GPL

== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.


== Frequently Asked Questions ==

= How can we add our own logo and other customizations? =

Go to Appearance => Customize menu for logo and other customization needs.

== Changelog ==

= 1.0.0 - 12 July 2018 =
* Theme Submited on Wordpress.org

= 1.0.1 - 3 Aug 2018 =
* Misc Changes.

= 1.0.2 - 14 Sept 2018 =
* Customizer file updated.Fixed code and theme css.

= 1.0.3 - 23 Nov 2018 =
* Customizer color option set.
* Misc changes.

= 1.0.4 - 24 Nov 2018 =
* Misc changes.

= 1.0.5 - 24 Nov 2018 =
* Misc changes.

= 1.0.6 - 27 Nov 2018 =
* TGM updated.
* Misc changes.

== Upgrade Notice ==

= 1.0.6 =
* TGM updated.
* Misc changes.

= 1.0.2 =
* Customizer file updated.Fixed code and theme css.

== Resources ==
* Poppins - //fonts.googleapis.com/css?family=Poppins, MIT 
* bootstrap.js © 2011-2016 Twitter, Inc., MIT  Version 3.3.7
* bootstrap.css © 2011-2016 Twitter, Inc., MIT Version 3.3.7
* font-awesome.css © @fontawesome, MIT Version 4.6.1
* font-awesome font © @fontawesome, MIT Version 4.6.1
* owl.carousel © 2016-2018 David Deutsch, MIT Version 2.2.1
* TGM Plugin Activation ,GNU GPL Version 2.6.1

* https://www.pexels.com/photo/low-angle-view-of-spiral-staircase-against-black-background-247676/ © Pixabay , CC0
* https://www.pexels.com/photo/computers-cup-desk-gadgets-221011/ © Pixabay , CC0
* https://www.pexels.com/photo/man-in-white-shirt-using-tablet-computer-shallow-focus-photography-207582/ © Pixabay , CC0
