<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <!-- SEO Meta Tags -->
    <meta name="description" content="Juno multipurpose HTML landing page pack helps you easily create websites for businesses, mobile apps, events, training courses and many more.">
    <meta name="author" content="Inovatik">

    <!-- OG Meta Tags to improve the way the post looks when you share the page on LinkedIn, Facebook, Google+ -->
	<meta property="og:site_name" content="" /> <!-- website name -->
	<meta property="og:site" content="" /> <!-- website link -->
	<meta property="og:title" content=""/> <!-- title shown in the actual shared post -->
	<meta property="og:description" content="" /> <!-- description shown in the actual shared post -->
	<meta property="og:image" content="" /> <!-- image link, make sure it's jpg -->
	<meta property="og:url" content="" /> <!-- where do you want your post to link to -->
	<meta property="og:type" content="article" />

    <!-- Website Title -->
    <title> Fourtek Academy</title>
    
    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,400i,700,700i&amp;subset=latin-ext" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/fontawesome-all.css" rel="stylesheet">
    <link href="css/swiper.css" rel="stylesheet">
	<link href="css/magnific-popup.css" rel="stylesheet">
	<link href="css/styles.css" rel="stylesheet">
	
	<!-- Favicon  -->
    <link rel="icon" href="images/fav.ico">
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-107580501-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-107580501-1');
</script>

</head>
<body data-spy="scroll" data-target=".fixed-top">
    
    <!-- Preloader -->
	<div class="spinner-wrapper">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>
    <!-- end of preloader -->
    

    <!-- Navbar -->
    <nav class="navbar navbar-expand-md navbar-dark navbar-custom fixed-top">
        <!-- Text Logo - Use this if you don't have a graphic logo -->
        <!-- <a class="navbar-brand logo-text page-scroll" href="index.php">Juno</a> -->

        <!-- Image Logo -->
        <a class="navbar-brand logo-image" href="index.php"><img src="images/logo.png" alt="Fourtek Academy"></a>
         <ul class="navbar-nav ml-auto">
              <li class="nav-item">
 <a class="nav-link page-scroll" href="#duration"><i class="fa fa-phone" aria-hidden="true"></i> +91-9899846974
</a>
                </li>
                <li class="nav-item">
 <a class="nav-link page-scroll" href="mailto:info@fourtek.com"><i class="fa fa-envelope" aria-hidden="true"></i> info@fourtek.com
</a>
                </li>
            

               

               
            </ul>
        <!-- Mobile Menu Toggle Button -->
        
    </nav> <!-- end of navbar -->
    <!-- end of navbar -->


    <!-- Header -->
    <header id="header" class="header">
        <div class="header-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 offset-lg-6">
                        
                        <h1 class="top-heading-text">PHP Course in Noida</h1>
						<p class="heading-text">For the <em style="color: #af1f25;">Techie</em> inside you</p>
                        
                       
                    </div> <!-- end of col -->
                </div> <!-- end of row -->
            </div> <!-- end of container -->
        </div> <!-- end of header-content -->
    </header> <!-- end of header -->
    <!-- end of header -->


    

    <!-- Overview -->
    <div class="basic-1" id="freetrail">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 padding-top60">
                    <h2 class="">OVERVIEW</h2>
					<hr class="line-heading">
                    <p class="text-center p-large">Transform your Career with Best PHP Workshop Training in Noida with 100% Practical and Live Project Implementation, Learn from Expert Developers and Instructors.</p>
					<p class="text-center p-large">Fourtek Academy is a learning home to many students worldwide who believe in investing in their knowledge to better their future. We wish to provide various online technical courses to the learners to skill up and become future ready.
This PHP course is a part of our mission for better skillsets to the candidates 

</p>
<p class="text-center p-large">Especially curated by our technical head </p>
                   
                </div> <!-- end of col -->
				 <div class="col-lg-6">
                    <!-- Signup Form -->
                <div class="form-container lightbox-form">
                    <form id="signupForm" data-toggle="validator" data-focus="false" method="post" action="">
                        <div class="form-group">
                            <input type="text" class="form-control-input" id="sfirstname" name="slastname" required>
                            <label class="label-control" for="sfirstname">First name</label>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control-input" id="slastname" name="slastname" required>
                            <label class="label-control" for="slastname">Last name</label>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control-input" id="semail" name="semail" required>
                            <label class="label-control" for="semail">Email</label>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control-input" id="sphone" name="sphone" required>
                            <label class="label-control" for="sphone">Phone</label>
                            <div class="help-block with-errors"></div>
                        </div>

                       <!-- <div class="form-group checkbox">

                        
                            <input type="checkbox" id="sterms" value="Agreed-to-Terms" name="sterms" required>I agree to Fourtek Academy <a class="white" href="#">Privacy Policy</a> and <a class="white" href="#">Terms & Conditions</a>
                            <div class="help-block with-errors"></div>
                        </div>-->
                        <div class="form-group">
                            <button type="submit" class="form-control-submit-button">Free Trail Now</button>
                        </div>
                        <div class="form-message">
                            <div id="smsgSubmit" class="h3 text-center hidden"></div>
                        </div>
                    </form>
                </div> <!-- end of form-container -->
                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of basic-1 -->
    <!-- end of overview -->


   

    <!-- Details -->
    <div id="details" class="tabs">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>COURSE DETAILS</h2>
                    <hr class="line-heading">
<p class="text-center">PHP an open source server. Side programming is the language that is designed for web development, but it is also used as a general programming programming language. It is a singular tool for creating dimensions and interactive web pages. PHP development services include web portals, e-commerce, social networking websites, CMS applications and website maintenance and applications.</p>
                    <!-- Tabs Links -->
                    <ul class="nav nav-tabs" id="junoTabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="nav-tab-1" data-toggle="tab" href="#tab-1" role="tab" aria-controls="tab-1" aria-selected="true">Week 1</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="nav-tab-2" data-toggle="tab" href="#tab-2" role="tab" aria-controls="tab-2" aria-selected="false">Week 2</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="nav-tab-3" data-toggle="tab" href="#tab-3" role="tab" aria-controls="tab-3" aria-selected="false">Week 3</a>
                        </li>
						 <li class="nav-item">
                            <a class="nav-link" id="nav-tab-4" data-toggle="tab" href="#tab-4" role="tab" aria-controls="tab-4" aria-selected="false">Week 4</a>
                        </li>
                    </ul>
                    <!-- end of tabs links -->
                    
                    <!-- Tabs Content -->
                    <div class="tab-content" id="junoTabsContent">
                        
                        <!-- Tab 1 -->
                        <div class="tab-pane fade show active" id="tab-1" role="tabpanel" aria-labelledby="tab-1">
                            <div class="row">
							
							  <div class="col-lg-6">
                                    <div class="image-container">
                                        <img class="img-fluid" src="images/features-tab-1.jpg" alt="alternative">
                                    </div> <!-- end of image-container -->
                                </div> <!-- end of col -->
                                <div class="col-lg-3">
                                        
                                    <!-- Accordion -->
                                 <div class="text-container">
                                       <h3>OVERVIEW</h3>
                                        <ul class="list-unstyled li-space-lg">
                                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Variables  </div>
                                            </li>
                                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Operators</div>
                                            </li>
                                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Conditional Statements</div>
                                            </li>
											 <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Loops</div>
                                            </li>
											 <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Functions</div>
                                            </li>
											 
                                        </ul>
                                        

                                    </div> <!-- end of text-container -->

                                </div> <!-- end of col -->
                              
                            <div class="col-lg-3">
                                        
                                    <!-- Accordion -->
                                 <div class="text-container">
                                       <h3>&nbsp;</h3>
                                        <ul class="list-unstyled li-space-lg">
                                          
                                             <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Strings</div>
                                            </li>
                                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Arrays</div>
                                            </li>
                                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Classes and Inheritance</div>
                                            </li>
                                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Exception Handling</div>
                                            </li>
                                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">POST / GETS</div>
                                            </li>
                                        </ul>
                                        

                                    </div> <!-- end of text-container -->

                                </div> <!-- end of col -->
                              
                            </div> <!-- end of row -->

                        </div> <!-- end of tab-pane -->
                        <!-- end of tab 1 -->

                        <!-- Tab 2 -->
                        <div class="tab-pane fade" id="tab-2" role="tabpanel" aria-labelledby="tab-2">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="image-container">
                                        <img class="img-fluid" src="images/features-tab-2.jpg" alt="alternative">
                                    </div> <!-- end of image-container -->
                                </div> <!-- end of col -->
<<<<<<< HEAD
                                <div class="col-lg-3">
=======
                                <div class="col-lg-4">
>>>>>>> 40a0b6877cf5a61178a744081e3c0aadab54e54d
                                    <div class="text-container">
                                        <h3>PROGRAMS</h3>
                                        <ul class="list-unstyled li-space-lg">
                                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Student Mark List with Grade  </div>
                                            </li>
                                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Lucky Draw with Coupon Code</div>
                                            </li>
                                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">ATM PROGRAM</div>
                                            </li>
                                             <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">POWER BILL GENERATION</div>
                                            </li>
                                             <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Generate Table base on given number</div>
                                            </li>
                                             <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Reverse of a Number</div>
                                            </li>
                                            
                                             <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Strings</div>
                                            </li>
                                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Sum of the Number</div></li>
<li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Armstrong Number</div></li>

                                                    <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Prime Number</div></li>


                                        </ul>
                                    </div> <!-- end of text-container -->
                                </div> <!-- end of col -->
                                <div class="col-lg-3">
                                    <div class="text-container">
                                        <h3>&nbsp;</h3>
                                        <ul class="list-unstyled li-space-lg">
                                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Pyramid Programs</div></li>

                                        </ul>
                                    </div> <!-- end of text-container -->
                                </div> <!-- end of col -->
                                <div class="col-lg-4">
                                    <div class="text-container">
                                        <h3>&nbsp;</h3>
                                        <ul class="list-unstyled li-space-lg">
                                            
<li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Matrix Multiplication Using Array</div></li>
<li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Biggest and Smallest Numbers Using Array</div></li>
<li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Getting length of string</div></li>
<li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Reversing a string</div></li>
<li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Counting of the number of words in  a string</div></li>
<li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Converting string into UPPERCASE/LOWERCASE</div></li>
<li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Comparing Strings
</div></li>
                                        </ul>
                                    </div> <!-- end of text-container -->
                                </div> <!-- end of col -->
                            </div> <!-- end of row -->
                        </div> <!-- end of tab-pane -->
                        <!-- end of tab 2 -->

                        <!-- Tab 3 -->
                        <div class="tab-pane fade" id="tab-3" role="tabpanel" aria-labelledby="tab-3">
                            <div class="row">
							 <div class="col-lg-6">
                                    <div class="image-container">
                                        <img class="img-fluid" src="images/features-tab-3.jpg" alt="alternative">
                                    </div> <!-- end of image-container -->
                                </div> <!-- end of col -->
                                <div class="col-lg-6">
                                    <div class="text-container">
                                        <h3>DATABASE</h3>
                                        
                                        <ul class="list-unstyled li-space-lg">
                                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Database: Create , Select   and   Drop</div></li>
<li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Table: Create ,Select , Insert, Update and Delete</div></li>
<li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Where Clause</div></li>
<li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">And    &  Or  Clause</div></li>
<li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Order</div></li>
<li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Group</div></li>
<li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Sorting
</div></li>
                                        </ul>
                                       
                                    </div> <!-- end of text-container -->
                                </div> <!-- end of col -->
                               
                            </div> <!-- end of row -->
                        </div> <!-- end of tab-pane -->
                        <!-- end of tab 3 -->
<!-- Tab 3 -->
                        <div class="tab-pane fade" id="tab-4" role="tabpanel" aria-labelledby="tab-4">
                            <div class="row">
							 <div class="col-lg-6">
                                    <div class="image-container">
                                        <img class="img-fluid" src="images/features-tab-4.jpg" alt="alternative">
                                    </div> <!-- end of image-container -->
                                </div> <!-- end of col -->
                                <div class="col-lg-6">
                                    <div class="text-container">
                                        <h3>PROJECTS</h3>
                                        
                                        <ul class="list-unstyled li-space-lg">
                                            <li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Registration /  Login / View Profile / Edit Profile  / Logout</div></li>
<li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Taxi Booking System</div></li>
<li class="media">
                                                <i class="fas fa-square"></i>
                                                <div class="media-body">Gold Loan System </div></li>


                                        </ul>
                                       
                                    </div> <!-- end of text-container -->
                                </div> <!-- end of col -->
                               
                            </div> <!-- end of row -->
                        </div> <!-- end of tab-pane -->
                        <!-- end of tab 3 -->
                    </div> <!-- end of tab-content -->
                    <!-- end of tabs content -->

                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of tabs -->
    <!-- end of details -->


    <!-- Benefits 1 -->
	<div id="benefits" class="basic-2">
        <div class="area-1">
        </div><!-- end of area-1 on same line and no space between comments to eliminate margin white space --><div class="area-2">
            <div class="text-container">
                <h3>Fourtek Academy Training Will Help</h3>
                <ul class="list-unstyled li-space-lg">
                    <li class="media">
                        <i class="fas fa-check"></i>
                        <div class="media-body">100 hrs of Learning + Working Experience.</div>
                    </li>
                    <li class="media">
                        <i class="fas fa-check"></i>
                        <div class="media-body">Our Technical Head Personally Guiding  and Grooming You.
</div>
                    </li>
                    <li class="media">
                        <i class="fas fa-check"></i>
                        <div class="media-body">1 Day Free Demo Class.
</div>
                    </li>
                    <li class="media">
                        <i class="fas fa-check"></i>
                        <div class="media-body">Certification on Completion.</div>
                    </li>
					<li class="media">
                        <i class="fas fa-check"></i>
                        <div class="media-body">Practice at your home with ease, as we provide full query handling support anytime from anywhere.
</div>
                    </li>
					
					<li class="media">
                        <i class="fas fa-check"></i>
                        <div class="media-body">3 In hand live projects 
</div>
                    </li>
					
                </ul>
            </div> <!-- end of text-container -->
        </div> <!-- end of area-2 -->
    </div> <!-- end of basic-2 -->
    <!-- end of benefits 1 -->



<!-- Pricing -->
    <div id="pricing" class="cards-2">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>PRICING</h2>
                    <hr class="line-heading">
                </div> <!-- end of col -->
            </div> <!-- end of row -->
            <div class="row">
                <div class="col-lg-6">
                    <div class="text-container">
                        <h3>Never Before</h3>
                        <p>This course is created to make you a real asset in programming industry with a price which was never before available for such quality experience </p>
                        
                    </div> <!-- end of text-container -->
                </div> <!-- end of col -->
                <div class="col-lg-6">
                    
                    <!-- Card -->
                    <div class="card first">
                        <div class="card-body">
                            <h3 class="card-title">Full Course - <span class="price">Rs.10,000</span></h3>
                            <p>Kick start your career</p>
                            <ul class="list-unstyled li-space-lg">
                                <li class="media">
                                    <i class="fas fa-square"></i>
                                    <div class="media-body">Student Friendly Price</div>
                                </li>
                                <li class="media">
                                    <i class="fas fa-square"></i>
                                    <div class="media-body">3 Hours Session per day</div>
                                </li>
                                <li class="media">
                                    <i class="fas fa-square"></i>
                                    <div class="media-body">One Time Payment</div>
                                </li>
                            </ul>
                        </div>
                        <div class="btn-wrapper">
                            <a class="btn-outline-reg page-scroll" href="#freetrail">Free Trail Now</a>
                        </div>
                    </div>
                    <!-- end of card -->

                    <!-- Card -->
                   
    
                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of cards-2 -->
    <!-- end of pricing -->


 



 

    <!-- Contact -->
    <div id="contact" class="form-2">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>CONTACT</h2>
                    <ul class="list-unstyled li-space-lg">
                        <li class="address">Don't hesitate to give us a call or send us a contact form message</li>
                        <li><i class="fas fa-map-marker-alt"></i>B86, Sector 60, Noida ( Uttar Pradesh) 201301 India</li>
                        <li><i class="fas fa-phone"></i><a class="green" href="tel:003024630820">+91-9899846974</a></li>
                        <li><i class="fas fa-envelope"></i><a class="green" href="mailto:office@juno.com">info@fourtek.com</a></li>
                    </ul>
                </div> <!-- end of col -->
            </div> <!-- end of row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="">
                       <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3502.9118313218682!2d77.36430031508193!3d28.602421732429697!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390ce5703efa0ac1%3A0x9f1b0b68a91d0992!2sFourtek%20IT%20Solutions%20Private%20Limited!5e0!3m2!1sen!2sin!4v1569245466963!5m2!1sen!2sin" width="100%" height="300" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                    </div>
                </div> <!-- end of col -->
                
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of form-2 -->
    <!-- end of contact -->

<div class="copyright"><div class="container"><div class="row"><div class="col-lg-12">                   <p class="p-small">Copyright © 2019 <a href="https://www.fourtek.com/" target="_blank">Fourtek It Solutions</a> all right reserved</p></div> </div></div> </div>



    	
    <!-- Scripts -->
    <script src="js/jquery.min.js"></script> <!-- jQuery for Bootstrap's JavaScript plugins -->
    <script src="js/popper.min.js"></script> <!-- Popper tooltip library for Bootstrap -->
    <script src="js/bootstrap.min.js"></script> <!-- Bootstrap framework -->
    <script src="js/jquery.easing.min.js"></script> <!-- jQuery Easing for smooth scrolling between anchors -->
    <script src="js/jquery.countdown.min.js"></script> <!-- The Final Countdown plugin for jQuery -->
    <script src="js/swiper.min.js"></script> <!-- Swiper for image and text sliders -->
    <script src="js/jquery.magnific-popup.js"></script> <!-- Magnific Popup for lightboxes -->
    <script src="js/validator.min.js"></script> <!-- Validator.js - Bootstrap plugin that validates forms -->
    <script src="js/scripts.js"></script> <!-- Custom scripts -->
</body>

<!-- Mirrored from inovatik.com/juno-multipurpose-landing-page-pack/05-training-course/index.php by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 23 Sep 2019 09:39:44 GMT -->
</html>
