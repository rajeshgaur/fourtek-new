<?php
error_reporting(0);
ob_start();
require_once "./vendor/autoload.php";

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

if (isset($_POST['submit'])) {
    //$_POST=array();
    if (isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])) {
        //your site secret key
        $secret = '6LcfIYMUAAAAAJVBnF2cjuzCdSremj4GUx8fRJGY';
        //get verify response data
        $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
        $responseData = json_decode($verifyResponse);
        if ($responseData->success) {
            $name = $_POST['name'];
            $visitor_email = $_POST['email'];
            $quote = $_POST['quote'];
            $message = $_POST['comment'];
            //Create a new PHPMailer instance
            $mail = new PHPMailer;
            $mail->isSMTP();
            $mail->SMTPDebug = 0;
            $mail->Host = 'smtp.gmail.com';
            $mail->Port = 587;
            $mail->SMTPSecure = 'tls';
            $mail->SMTPAutoTLS = false;
            $mail->SMTPAuth = true;
            $mail->Username = "adwords@fourtek.com";
            $mail->Password = "Fourtek@0101";
            $mail->setFrom($visitor_email, $quote);
            //$mail->addAddress('surendra@fourtek.com');
            $mail->addAddress('arpit@fourtek.com');
            $mail->addAddress('nakul@fourtek.com');
            $mail->addAddress('manoj@fourtek.com');
            $mail->addAddress('nitisha@fourtek.com');
            $mail->Subject = $quote;
            $mail->msgHTML('<html><head> </head><body> <table border="1" cellpadding="5">
        <tr> <td colspan="2"><strong>Name</strong></td>
          <td colspan="2">'.$_POST["name"].'</td>
        </tr>
        <tr>
          <td colspan="2"><strong>Email</strong></td>
          <td colspan="2">'.$_POST["email"].'</td>
        </tr>
        <tr>
          <td colspan="2"><strong>Mobile</strong></td>
          <td colspan="2">'.$_POST["mobile"].'</td>
        </tr>  
        <tr>
          <td colspan="2"><strong>Request Quote</strong></td>
          <td colspan="2">'.$_POST["quote"].'</td>
        </tr>     
        <tr>
          <td colspan="2"><strong>Description</strong></td>
          <td colspan="2">'.$_POST["comment"].'</td>
        </tr>
      </table>
      </body>
      </html>');
            if (!$mail->send()) {
                $errmsg = "Mailer Error: " . $mail->ErrorInfo;
            } else {
                $succmsg = "<div class='success-msg' style='color:green;'>Your Message has been Sent Successfully.</div>";
                //header('Location: thanks.php');
        //exit();
        
        // $form_data = array(
        // 'name' => $_POST['name'],
        // 'email' => $_POST['email'],
        // 'mobile' => $_POST['mobile'],
        // 'comment' => $_POST['comment']
        //   );
        // $result = $db->insert_data('digital_contact_form', $form_data);
            }
        } else {
            $errmsg = 'Robot verification failed, please try again.';
        }
    } else {
        $errmsg = 'Please click on the reCAPTCHA box.';
    }
} else {
    $errmsg = '';
    $succmsg = '';
}

?>

<!-- Modal -->
<div class="order-now-form modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header request-quote-form">
        <h5 class="modal-title" id="exampleModalLabel">Request a Quote</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

          <form action="" method="post">  
             <div class="row">  
              
               <div class="col-sm-6 col-md-6 form-group"> 
                      <span class="field-name">Name*</span>                
                   <input type="text" name="name" class="form-control" placeholder="Name" value="<?php //echo $_POST['name']?>" required="">
               </div>

               <div class="col-sm-6 col-md-6 form-group">
                   <span class="field-name">Mobile*</span>                 
                   <input type="text" name="mobile" minlength="10" maxlength="15" pattern="^[0-9]*$" class="form-control" placeholder="Mobile" value="<?php //echo $_POST['mobile']?>" required="">
               </div> 
               <div class="col-sm-6 col-md-6 form-group"> 
                   <span class="field-name">Email Id*</span>                
                   <input type="email" name="email" class="form-control" placeholder="Email Id" value="<?php //echo $_POST['email']?>" required="">
               </div> 
               <div class="col-sm-6 col-md-6 form-group"> 
                   <span class="field-name">Select Quote*</span> 
                   <select class="form-control" name="quote" required>
                   <option value="">Please Select</option>
                   <option value="Custom E-commerce Development" <?php //if($_POST['quote'] == 'Custom E-commerce Development'){ echo 'selected="selected"';}?>>Custom E-commerce Development</option>
                   <option value="Customer Relationship Management" <?php //if($_POST['quote'] == 'Customer Relationship Management'){ echo 'selected="selected"';}?>>Customer Relationship Management</option>
                   <option value="Custom Website Designing" <?php //if($_POST['quote'] == 'Custom Website Designing'){ echo 'selected="selected"';}?>>Custom Website Designing</option>
                   <option value="Devnagri" <?php //if($_POST['quote'] == 'Devnagri'){ echo 'selected="selected"';}?>>Devnagri</option>
                   <option value="Digital Marketing Service" <?php //if($_POST['quote'] == 'Digital Marketing Service'){ echo 'selected="selected"';}?>>Digital Marketing Service</option>
                   <option value="E-commerce Development Services" <?php //if($_POST['quote'] == 'E-commerce Development Services'){ echo 'selected="selected"';}?>>E-commerce Development Services</option> 
                   <option value="Enterprise Resource Planning" <?php //if($_POST['quote'] == 'Enterprise Resource Planning'){ echo 'selected="selected"';}?>>Enterprise Resource Planning</option>
                   <option value="Game Development" <?php //if($_POST['quote'] == 'Game Development'){ echo 'selected="selected"';}?>>Game Development</option>
                   <option value="Human Resource Management" <?php //if($_POST['quote'] == 'Human Resource Management'){ echo 'selected="selected"';}?>>Human Resource Management</option> 
                   <option value="iOS Application Development" <?php //if($_POST['quote'] == 'iOS Application Development'){ echo 'selected="selected"';}?>>iOS Application Development</option> 
                   <option value="Magento Development" <?php //if($_POST['quote'] == 'Magento Development'){ echo 'selected="selected"';}?>>Magento Development</option> 
                   <option value="Mobile App Development Services" <?php //if($_POST['quote'] == 'Mobile App Development Services'){ echo 'selected="selected"';}?>>Mobile App Development Services</option>
                   <option value="Online Reputation Management Service" <?php //if($_POST['quote'] == 'Online Reputation Management Service'){ echo 'selected="selected"';}?>>Online Reputation Management Service</option>
                   <option value="PHP Web Development" <?php //if($_POST['quote'] == 'PHP Web Development'){ echo 'selected="selected"';}?>>PHP Web Development</option>
                   <option value="Salesforce Development" <?php //if($_POST['quote'] == 'Salesforce Development'){ echo 'selected="selected"';}?>>Salesforce Development</option>
                   <option value="Web Design & Development" <?php //if($_POST['quote'] == 'Web Design & Development'){ echo 'selected="selected"';}?>>Web Design & Development</option> 
                   <option value="Website Redesigning Service" <?php //if($_POST['quote'] == 'Website Redesigning Service'){ echo 'selected="selected"';}?> >Website Redesigning Service</option> 
                                    
                   </select>               
                   
               </div> 

               <div class="col-sm-12 col-md-12 form-group">
                   <span class="field-name">Description</span>                 
                   <textarea class="form-control" name="comment" placeholder="Message" style="min-height:100px"><?php //echo $_POST['comment']?></textarea>
               </div>


            </div>  
             
            <p><?php echo $succmsg;?></p>
            <p><?php echo $errmsg;?></p>

            <div class="row">
  <div class="col-md-6">
  <div class="g-recaptcha" data-sitekey="6LcfIYMUAAAAAGI1bmHjGaNkvJo4xnzzijcTarIe"></div>
  </div>
  <div class="col-md-6" style="text-align:right;">
  <div class="submit-query">
                <input type="submit" name="submit" value="Submit" class="request-submit">
              </div>
  </div>

<div class="clearfix"></div>

</div>
              

              <br>            
            </form>

      </div>
    </div>
  </div>
</div>
<script src="js/jquery.min.js"></script>
<script>
jQuery(document).ready(function(){
  var success = "<?php echo $succmsg;?>";
  
if(success){
window.location = "http://www.fourtek.com/thanks.php?msg=success";
}
});
</script>
