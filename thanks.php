<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Looking for India's best digital marketing company? Get connected to the Fourtek, providing most reliable web & mobile app development services in Delhi/Noida.">
    <title>Top Digital marketing , Website & Mobile  App Development company in Delhi, India </title>
    <link rel="shortcut icon" type="image/icon" href="images/fav.ico" />
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Pattaya|Poppins:300,300i,400" rel="stylesheet">
    <link href="css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
    <link href="css/animate.min.css" rel="stylesheet" type="text/css">
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link href="css/responsive.css" rel="stylesheet" type="text/css">
    <?php include "google-code.php";?>
  </head>


<style>
  header{background: url(images/about-bg.jpg) !important; background-size: cover !important;  background-repeat: no-repeat;background-attachment: fixed !important;}
</style>


  <body id="page-top" class="inner-page">
      <?php include 'include/menu.php' ; ?>
  
    <header class="masthead video">
      <div class="container h-100">
        <div class="row h-100">
          <div class="col-12 my-auto text-center text-white">          
            <h1 class="wow fadeInDown">Thank You</h1>
            <p><strong>Discovering, Designing & Deploying Experience That Matters </strong><br>
With more 09 years of experience, we are producing ‘just extraordinary’ experiences for brands that are as H.O.T (High Order Thinking) as they are efficacious.</p>
          </div>
        </div>
      </div>
    </header>

<section class="breadcrumb-block">
<div class="container">
  <div class="breadcrumb">
    <a class="breadcrumb-item" href="index.php">Home</a>
    <span class="breadcrumb-item active">Thank You</span>
  </div>
</div>
</section>

<section>
  <div class="container">
    <div class="col-md-7 offset-md-2 thankyou">
      <div class="likeIcon"><img src="images/likeIcon.png"></div>
      <h2>Thank You</h2>
     <!-- <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </p>-->
    </div>
  </div>
</section>












   <?php include 'include/footer.php' ;?>

  </body>
</html>
