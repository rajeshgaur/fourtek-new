
<style type="text/css">
 .brand-color-element.second{
    width: 40px !important;
    height: 40px !important;
}
.widget-launcher .widget-launcher-icon.open {
    width: 25px;
    height: 25px;
}
</style>
    <!-- Start of HubSpot Embed Code -->
     <script type="text/javascript" id="hs-script-loader" async defer src="js/hubspot/5797952.js"></script>
    <!-- End of HubSpot Embed Code -->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-107580501-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-107580501-1');
</script> 
<!-- Facebook Pixel Code -->
<!-- <script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '2175297652501549');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=2175297652501549&ev=PageView&noscript=1"
/></noscript> -->
<!--End Facebook Pixel Code -->
<!-- <script src="js/hubspot/api.js" async defer></script> -->
<meta property="og:title" content="Fourtek - India #1 IT Solution & Digital Marketing Company">
<meta property="og:site_name" content="Fourtek">
<meta property="og:url" content="https://www.fourtek.com/">
<meta property="og:description" content="Drive Conversions and Increase ROI with Fourtek. Quote Now">
<meta property="og:type" content="website">
<meta property="og:image" content="https://www.fourtek.com/images/Fourtek-OG-image.jpg">
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@fourtek">
<meta name="twitter:title" content="Fourtek - India #1 IT Solution & Digital Marketing Company">
<meta name="twitter:description" content="Drive Conversions and Increase ROI with Fourtek. Quote Now">
<meta name="twitter:image" content="https://www.fourtek.com/images/Fourtek-OG-image.jpg">


