<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="At fourtek we give utmost importance to the clients’ privacy. For more than a decade, our client service department is ensuring that no data shall pass to any irrelevant party though us">
    <title>Our Clients - Fourtek </title>
    <link rel="canonical" href="https://www.fourtek.com/our-clients">
    <link rel="shortcut icon" type="image/icon" href="images/fav.ico" />
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Pattaya|Poppins:300,300i,400" rel="stylesheet">
    <link href="css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
    <link href="css/animate.min.css" rel="stylesheet" type="text/css">
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link href="css/responsive.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="css/layout.css">
    <?php include "google-code.php";?>
  </head>


<style>
  header{background: url(images/happy-client-banners.jpg) !important; background-size: cover !important;  background-repeat: no-repeat;background-attachment: fixed !important;}
</style>


  <body id="page-top" class="inner-page">
      <?php include 'include/menu.php' ; ?>
  
    <header class="masthead video">
      <div class="container h-100">
        <div class="row h-100">
          <div class="col-12 my-auto text-center text-white">          
            <h1 class="wow fadeInDown">Our Clients</h1>
            <p>We have catered to the needs of multiple brands. Our tenaciousness towards our passion for developing brilliant solutions for fulfilling our clients’ need has led our path brighten and helped us to step by step achieve the success. And, we’re gracefully and continuously stepping ahead on this path. </p>
          </div>
        </div>
      </div>
    </header>

	<section class="breadcrumb-block">
	<div class="container">
	  <div class="breadcrumb">
		<a class="breadcrumb-item" href="index.php">Home</a>
		<span class="breadcrumb-item active">Our Clients</span>
	  </div>
	</div>
	</section>


	<div class="container-fuild">

		<ul id="filters" class="clearfix">
			<!-- <li><span class="filter " data-filter=".mobile, .web">All</span></li> -->
			<li><span class="filter active" data-filter=".web">Web Sites Development</span></li>
			<li><span class="filter" data-filter=".government">Government Projects</span></li>	
			<li><span class="filter" data-filter=".dm">Digital Marketing</span></li>
			<li><span class="filter" data-filter=".mobile">Mobile</span></li>
			
		</ul>

		<div id="portfoliolist">
			
			<div class="portfolio government" data-cat="government">
				<div class="portfolio-wrapper">				
					<img src="images/desktop1.png" alt="" />
					<div class="label">
						<div class="label-text">
							<a class="text-title">IRCTC</a>
							<span class="text-category">Government</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>	

			<div class="portfolio government" data-cat="government">
				<div class="portfolio-wrapper">
					<img src="images/desktop5.png" alt="" />
					<div class="label">
						<div class="label-text">
							<a class="text-title">CERC</a>
							<span class="text-category">Government</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>		

			<div class="portfolio web" data-cat="web">
				<div class="portfolio-wrapper">			
					<img src="images/desktop10.png" alt="" />
					<div class="label">
						<div class="label-text">
							<a class="text-title">Prestige Smart Kitchen</a>
							<span class="text-category">Development</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>	

			<div class="portfolio web dm" data-cat="web dm">
				<div class="portfolio-wrapper">			
					<img src="images/desktop11.png" alt="" />
					<div class="label">
						<div class="label-text">
							<a class="text-title">Grey Orange</a>
							<span class="text-category">Development</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>		

			<div class="portfolio web" data-cat="web">
				<div class="portfolio-wrapper">			
					<img src="images/desktop16.png" alt="" />
					<div class="label">
						<div class="label-text">
							<a class="text-title">Ease My Match</a>
							<span class="text-category">Development</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>


			<div class="portfolio web" data-cat="web">
				<div class="portfolio-wrapper">			
					<img src="images/desktop17.png" alt="" />
					<div class="label">
						<div class="label-text">
							<a class="text-title">Skipper</a>
							<span class="text-category">Development</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>	


			<div class="portfolio web" data-cat="web">
				<div class="portfolio-wrapper">			
					<img src="images/desktop18.png" alt="" />
					<div class="label">
						<div class="label-text">
							<a class="text-title">CV Logistics</a>
							<span class="text-category">Development</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>			
			

			<div class="portfolio 	" data-cat="dm">
				<div class="portfolio-wrapper">			
					<img src="images/desktop22.png" alt="" />
					<div class="label">
						<div class="label-text">
							<a class="text-title">Pick Go</a>
							<span class="text-category">Development</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>		

			<div class="portfolio web dm" data-cat="web dm">
				<div class="portfolio-wrapper">			
					<img src="images/desktop2.png" alt="" />
					<div class="label">
						<div class="label-text">
							<a class="text-title">United Cooker</a>
							<span class="text-category">Development</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>		
			
			<div class="portfolio web dm" data-cat="web dm">
				<div class="portfolio-wrapper">						
					<img src="images/desktop3.png" alt="" />
					<div class="label">
						<div class="label-text">
							<a class="text-title">Mr.Cook</a>
							<span class="text-category">Development</span>						
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>				
			
			<div class="portfolio dm" data-cat="dm">
				<div class="portfolio-wrapper">			
					<img src="images/desktop4.png" alt="" />
					<div class="label">
						<div class="label-text">
							<a class="text-title">Phiten Japan</a>
							<span class="text-category">Development</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>	
								
			
			<div class="portfolio web" data-cat="web">
				<div class="portfolio-wrapper">			
					<img src="images/desktop6.png" alt="" />
					<div class="label">
						<div class="label-text">
							<a class="text-title">Monaca</a>
							<span class="text-category">Development</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>	
			
			<div class="portfolio web" data-cat="web">
				<div class="portfolio-wrapper">			
					<img src="images/desktop7.png" alt="" />
					<div class="label">
						<div class="label-text">
							<a class="text-title">Vatika Group</a>
							<span class="text-category">Development</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>	
			
			<div class="portfolio web dm" data-cat="web dm">
				<div class="portfolio-wrapper">			
					<img src="images/desktop8.png" alt="" />
					<div class="label">
						<div class="label-text">
							<a class="text-title">Trueform</a>
							<span class="text-category">Development</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>																																							
			
			<div class="portfolio web" data-cat="web">
				<div class="portfolio-wrapper">			
					<img src="images/desktop9.png" alt="" />
					<div class="label">
						<div class="label-text">
							<a class="text-title">Chat Work</a>
							<span class="text-category">Development</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>														
			
				

																

			<div class="portfolio web dm" data-cat="web dm">
				<div class="portfolio-wrapper">			
					<img src="images/desktop12.png" alt="" />
					<div class="label">
						<div class="label-text">
							<a class="text-title">India App Stores</a>
							<span class="text-category">Development</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>	


			<div class="portfolio web" data-cat="web">
				<div class="portfolio-wrapper">			
					<img src="images/desktop13.png" alt="" />
					<div class="label">
						<div class="label-text">
							<a class="text-title">Ayu Milk</a>
							<span class="text-category">Development</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>	

			<div class="portfolio web" data-cat="web">
				<div class="portfolio-wrapper">			
					<img src="images/desktop14.png" alt="" />
					<div class="label">
						<div class="label-text">
							<a class="text-title">Fashion and you</a>
							<span class="text-category">Development</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>			

			<div class="portfolio web" data-cat="web">
				<div class="portfolio-wrapper">			
					<img src="images/desktop15.png" alt="" />
					<div class="label">
						<div class="label-text">
							<a class="text-title">Kalyan Silk</a>
							<span class="text-category">Development</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>	

			
			<div class="portfolio web" data-cat="web">
				<div class="portfolio-wrapper">			
					<img src="images/desktop19.png" alt="" />
					<div class="label">
						<div class="label-text">
							<a class="text-title">Pilgrim Packages</a>
							<span class="text-category">Development</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>	
			
			
			<div class="portfolio web" data-cat="web">
				<div class="portfolio-wrapper">			
					<img src="images/desktop20.png" alt="" />
					<div class="label">
						<div class="label-text">
							<a class="text-title">Urban Glamstar</a>
							<span class="text-category">Development</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>	


			<div class="portfolio web" data-cat="web">
				<div class="portfolio-wrapper">			
					<img src="images/desktop21.png" alt="" />
					<div class="label">
						<div class="label-text">
							<a class="text-title">Hash Tag Author</a>
							<span class="text-category">Development</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>				
			

			<div class="portfolio mobile" data-cat="mobile">
				<div class="portfolio-wrapper">			
					<img src="images/iphone1.png" alt="" />
					<div class="label">
						<div class="label-text">
							<a class="text-title">UBI Cabs</a>
							<span class="text-category">iPhone Application</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>

			<div class="portfolio mobile" data-cat="mobile">
				<div class="portfolio-wrapper">			
					<img src="images/android.png" alt="" />
					<div class="label">
						<div class="label-text">
							<a class="text-title">Ayu Milk</a>
							<span class="text-category">Android Application</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>
			
			<div class="portfolio mobile" data-cat="mobile">
				<div class="portfolio-wrapper">			
					<img src="images/iphone2.png" alt="" />
					<div class="label">
						<div class="label-text">
							<a class="text-title">Parlor – Talk, Meet & Connect Instantly</a>
							<span class="text-category">iPhone Application</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>			
			
			<div class="portfolio mobile" data-cat="mobile">
				<div class="portfolio-wrapper">			
					<img src="images/iphone3.png" alt="" />
					<div class="label">
						<div class="label-text">
							<a class="text-title">Family Locator</a>
							<span class="text-category">iPhone Application</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>				
			
			
			<div class="portfolio mobile" data-cat="mobile">
				<div class="portfolio-wrapper">			
					<img src="images/iphone4.png" alt="" />
					<div class="label">
						<div class="label-text">
							<a class="text-title">Line Birds</a>
							<span class="text-category">iPhone Application</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>				
			

			<div class="portfolio mobile" data-cat="mobile">
				<div class="portfolio-wrapper">			
					<img src="images/iphone5.png" alt="" />
					<div class="label">
						<div class="label-text">
							<a class="text-title">Pharoah JUMP</a>
							<span class="text-category">iPhone Application</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>	


			
			<div class="portfolio mobile" data-cat="mobile">
				<div class="portfolio-wrapper">			
					<img src="images/android2.png" alt="" />
					<div class="label">
						<div class="label-text">
							<a class="text-title">Chat Work Video, audio, text sharing apps</a>
							<span class="text-category">Android Application</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>			
			
			<div class="portfolio mobile" data-cat="mobile">
				<div class="portfolio-wrapper">			
					<img src="images/android3.png" alt="" />
					<div class="label">
						<div class="label-text">
							<a class="text-title">Duck hunter revolution</a>
							<span class="text-category">Android Application</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>			


			<div class="portfolio mobile" data-cat="mobile">
				<div class="portfolio-wrapper">			
					<img src="images/iphone7.png" alt="" />
					<div class="label">
						<div class="label-text">
							<a class="text-title">Real Estate by Trulia – Homes for Sale, Apartments for Rent & Open Houses</a>
							<span class="text-category">iPhone Application</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>
			
			
			<div class="portfolio mobile" data-cat="mobile">
				<div class="portfolio-wrapper">			
					<img src="images/iphone8.png" alt="" />
					<div class="label">
						<div class="label-text">
							<a class="text-title">Scan Square</a>
							<span class="text-category">iPhone Application</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>	


			<div class="portfolio mobile" data-cat="mobile">
				<div class="portfolio-wrapper">			
					<img src="images/iphone9.png" alt="" />
					<div class="label">
						<div class="label-text">
							<a class="text-title">Habit Tracker</a>
							<span class="text-category">iPhone Application</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>	


			<div class="portfolio mobile" data-cat="mobile">
				<div class="portfolio-wrapper">			
					<img src="images/iphone6.png" alt="" />
					<div class="label">
						<div class="label-text">
							<a class="text-title">Chat Work Video, audio, text sharing apps</a>
							<span class="text-category">iPhone Application</span>
						</div>
						<div class="label-bg"></div>
					</div>
				</div>
			</div>			
			
			
		</div>
		
	</div><!-- container -->

	<div class="clearfix"></div>
<br/><br/>

   <?php include 'include/footer.php' ;?>
   

	
	<script type="text/javascript" src="js/jquery.mixitup.min.js"></script>
	
	<script type="text/javascript">
	$(function () {
		
		var filterList = {
		
			init: function () {

				$('#portfoliolist').mixItUp({
  				selectors: {
    			  target: '.portfolio',
    			  filter: '.filter'	
    		  },
    		  load: {
      		  filter: '.web'  
      		}     
				});								
			
			}

		};
		
		// Run the show!
		filterList.init();
		
		
	});	
	</script>

  </body>
</html>
