<?php
session_start();
error_reporting(0);
require_once("admin/functions/user_list.php");
$db = new Database();
$job_id = $_REQUEST['id'];
$result = $db->selectdata("job_career", "where id='$job_id'");
if ($result) {
    $row = $result->fetch_assoc();
}?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">

    <title>Career</title>
    <link rel="shortcut icon" type="image/icon" href="images/fav.ico" />
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Pattaya|Poppins:300,300i,400" rel="stylesheet">
    <link href="css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
    <link href="css/animate.min.css" rel="stylesheet" type="text/css">
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link href="css/responsive.css" rel="stylesheet" type="text/css">
    <?php include "google-code.php";?>
  </head>


<style>
  header{background: url(images/career-detail-banner.jpg) !important; background-size: cover !important;  background-repeat: no-repeat;background-attachment: fixed !important;}
</style>


  <body id="page-top" class="inner-page">
      <?php include 'include/menu.php' ; ?>
  
    <header class="masthead video">
      <div class="container h-100">
        <div class="row h-100">
          <div class="col-12 my-auto text-center text-white">          
            <h1>WE ARE HIRING</h1>
            <p>A formal career path and development process exists in Fourtek IT Solutions Pvt Ltd. All career paths have an underlying management structure. Promotions within each career path are aligned with the needs of the organization. </p>
          </div>
        </div>
      </div>
    </header>

<section class="breadcrumb-block">
<div class="container">
  <div class="breadcrumb">
    <a class="breadcrumb-item" href="index.php">Home</a>
    <span class="breadcrumb-item active">Career</span>
  </div>
</div>
</section>


    <!-- About Section -->
    <!-- <section class="about-sections">
      <div class="container">
        <div class="wow fadeIn text-center">
          <h2><span>Raising the Bar </span> – WE NEED A BETTER BRAND OF BRAIN.</h2><br/>
        </div>
      
       <div class="row"> 
        <div class="col-sm-6"><img src="images/career-img.jpg" alt=""> </div>
        <div class="col-sm-6">
           <p>A formal career path and development process exists in Fourtek IT Solutions Pvt Ltd. All career paths have an underlying management structure. Promotions within each career path are aligned with the needs of the organization. It is important to know that regardless of where you work in our organization or where you are in your career, you will have a variety of opportunities to develop your potential. At Fourtek you wll find mentors to guide you in your career. You will only get better every day with managers who provide you constructive feedback and track your progress.</p>
        </div>

        <div class="col-sm-12">
          <p class="text-center">If you would like to explore working with us, please email your resume to <a href="mailto:hr@fourtek.com">hrd@fourtek.com</a> / <a href="mailto:info@fourtek.com">info@fourtek.com</a></p>
        </div>

      
     </div> 
  </div>
</section> -->


  <section id="career-details" class="recent-job">
    <div class="container">
       <div class="row">
        <div class="col-sm-12 col-md-12">
          <div class="card ">
            <div class="card-body">
              <h6 class="card-title"><img src="images/edit.png" alt=""> Job Title:</h6>
              <?php echo $row['title'];?>
              <hr/>
              
              <h6><img src="images/edit.png" alt=""> Job Description:</h6> 
                <?php echo $row['description'];?>
              <hr/>
               <h6><img src="images/edit.png" alt=""> Minimum qualification:</h6> 
                <?php echo $row['min_qualifica'];?>
                <hr/>
             <div class="row">   

               <div class="col-sm-12 col-md-3">                 
                   <img src="images/people.png" alt=""> No Of Opening- <?php echo $row['num_of_post'];?>
               </div>

               <div class="col-sm-12 col-md-3">                
                    <img src="images/calender.png" alt=""> Posted date : 
                    <?php
                    $date = $row['post_date'];
                    $Dates = strtotime($date);
                    echo date('d-m-Y', $Dates);
                      ?>            
               </div>

               <div class="col-sm-12 col-md-3">                 
                   <img src="images/bulb.png" alt=""> Experience - <?php echo $row['experience'];?>               
               </div> 

               <div class="col-sm-12 col-md-3">                 
                   <img src="images/bulb.png" alt=""> Location - <?php echo $row['location'];?>             
               </div>                

            </div>  

              <br/>
              <a href="career-form.php?job=<?php echo $row['title'];?>#career-form" class="btn btn-primary">Apply Now</a>
              <a href="career.php" class="btn btn-primary back-btn">BACK</a>
            </div>
          </div>
        </div>




       </div>
       <br/> <br/> <br/> <br/> <br/>
    </div>
  </section>


   <?php include 'include/footer.php' ;?>

  </body>
</html>