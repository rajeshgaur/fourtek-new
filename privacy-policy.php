<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="At fourtek we give utmost importance to the clients’ privacy. For more than a decade, our client service department is ensuring that no data shall pass to any irrelevant party though us.">

    <title>Privacy Policy - Fourtek IT Solutions</title>
    <link rel="canonical" href="https://www.fourtek.com/privacy-policy">
    <link rel="shortcut icon" type="image/icon" href="images/fav.ico" />
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Pattaya|Poppins:300,300i,400" rel="stylesheet">
    <link href="css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
    <link href="css/animate.min.css" rel="stylesheet" type="text/css">
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link href="css/responsive.css" rel="stylesheet" type="text/css">
    <?php include "google-code.php";?>
  </head>


<style>
  header{background: url(images/privacypolicy.jpg) !important; background-size: cover !important;  background-repeat: no-repeat;background-attachment: fixed !important;}
</style>


  <body id="page-top" class="inner-page">
      <?php include 'include/menu.php' ; ?>
  
    <header class="privacy-banner masthead video">
      <div class="container h-100">
        <div class="row h-100">
          <div class="col-12 my-auto text-center text-white">          
            <h1 class="wow fadeInDown">Privacy Policy</h1>
          </div>
        </div>
      </div>
    </header>

<section class="breadcrumb-block">
<div class="container">
  <div class="breadcrumb">
    <a class="breadcrumb-item" href="index.php">Home</a>
    <span class="breadcrumb-item active">Privacy Policy</span>
  </div>
</div>
</section>


    <!-- About Section -->
    <section class="about-sections privacypolicycontainer">
      <div class="container">
        <h2><span>Introduction:</span></h2>
		<span class="line-blue"></span>
        <p>Fourtek is committed to render the complete protection to the privacy of its website visitors. We collect your data and process it to offer and improve our services. Your utilization of our service is considered as your consent for gathering and using your data according to our policy.</p>
        <ul class="privacypolicy-list">
          <li>In this policy “we”, “our” and “us” refer to the <b> Fourtek IT Solutions Pvt. Ltd.</b> /or its applicable affiliates (if any).</li>
          <li>This policy applies where we act as the data controller to the personal data of the visitors of our website.</li>
          <li>We use cookies and will be asking your consent to these cookies when you will visit our website.
In order to prevent manipulation or destruction of your personal data along with avoiding access to your data by the unauthorized person, we keep our website protected with help of technical and organizational security measures.</li>
        </ul>
	
		
		<hr />


       <h2><span> Types of </span> Data Collected</h2>
		<span class="line-blue"></span>
		
         <strong>Personal Data:</strong>
         <p>While you would be using our services, we may ask you to render us your particular personal data to identify or contact you. Data we may require:</p>
          <ul class="privacypolicy-list">
             <li>Email Address</li>
             <li>Contact Number</li>
             <li>First and Last name</li>
          </ul>
			

          <strong>Usage Data</strong>
           <p>We may collect data regarding the way you access the services (usage data). This data may include information related to the computer’s IP address, the services page of our website you visit, date and time of your visit, the total time spent on particular pages, identifier etc.</p>
          

          <strong>Tracking Data & Cookies</strong>
          <p>We use the cookies and relatable tracking technologies for tracking your activities on our services and possess specific data.</p>
          <p>You are authorized to decline the cookies or to specify whenever the cookies are being sent to you. Besides this, by not confirming the cookies, you may not be able to use some specific part of our services.</p>


          <strong>Cookies Used by Us (Examples):</strong>
           <span class="spanclas"> Security Cookies </span>
           <ul class="privacypolicy-list">
            <li>The usage of these cookies are for ensuring the security
Session Cookies-</li>
            <li>The usage of these cookies are for running our services</li>
          </ul>

          <strong>Data Transfer & Disclosure</strong>
          <span class="spanclas">Transfer</span>
          <p>It is to keep in your mind that we may transmit your data (including personal data) to –and managed on- computers, which are placed outside of your residence, country or state.</p>

          <ul class="privacypolicy-list">
            <li>If residing out of the Indian territory and consent to share data with us, please keep it in your mind that we do transfer the data; including personal data to India to process it here.
Moreover, we take steps ahead gradually and rationally for ensuring that your data is completely handled securely in accordance with our policy.</li>
            <li>We don’t share your personal data with any other country or company unless they’re having a proper security control for making sure the safety of your personal and related data. </li>
          </ul>
   
         <strong>Disclosure:</strong>
          <p>We might reveal your personal information preserving the belief that the action is quite significant to</p>

          <ul class="privacypolicy-list">
            <li>Defending and safeguarding property or rights of the company Adhering to a legal obligation</li>
			<li>Safeguarding the personal security of the public or services users</li>
            <li>Investigating or preventing suspicious wrongdoing in relation to the service Protecting the legal liability</li>
          </ul>

        <strong>Data Security</strong>
          <p>Your data is for sure indispensable for us yet you need to keep it in your mind that no electronic storage or process of internet transmission comes with 100 percent security. We follow all the commercially accepted methodologies for safeguarding your personal data but we can’t come with a complete assurance of security.</p> 

        <strong>Service Providers</strong>
         <p>We may appoint individuals and third parties {service providers} on our behalf for providing our services and organize service-based tasks or support us to assess how the service is being executed.</p>



  <strong>Analytics</strong>
       <p>Third-party service providers can be hired by us to monitor and analyze the utilization of our service.</p>

  <strong>Google Analytics</strong>
         <p>Google Analytics, a web analytics service offered by Google, reports and tracks the traffic of the website. It not only executes the data collected for monitoring and tracking the service utilization but also shares this data with other services of Google. And further this data may be utilized by Google for personalizing and contextualizing its own advertising networks’.
For getting more details about Google’s privacy policy, visit terms & privacy page of Google <a href="https://policies.google.com/privacy?hl=e" target="_blank">https://policies.google.com/privacy?hl=e</a>
</p>

     <strong>Other websites Links</strong>
         <p>The services offered by us may consist of links of other websites, not running by us. If clicking on the link of the third party website, you will be directed to their website. We don’t keep any control and take any kind of responsibility for privacy policies, content or practices of them. So, we firmly recommend reviewing the privacy policy of each site you visit.</p>      

     <strong>Privacy of Children</strong>
         <p>Our service does not entertain anyone below 18 years i.e. children.
We knowingly don’t collect identifiable data from anyone below 18 years. In case any guardian finds out that their children have shared their personal data with us, they must contact us on the given address or email id. We surely will take the necessary steps to eliminate that information from our servers immediately.</p>  

     <strong>Alteration To The Privacy Policy</strong>
         <p>This policy can be modified or updated at any time. After updating the privacy policy page, we will notify you through emailers.</p>  

     <strong>Contact Us</strong>
         <p>In case of any questions regarding our privacy policy, the practices of our site or your dealings with our website or services, feel free to contact us at <a href="mailto:info@fourtek.com">info@fourtek.com</a> or visit us at:<br/>
          <span class="spanclas">Address:</span>
          <b> Fourtek IT Solutions Pvt. Ltd.</b><br/>
B-86, Block B, Sector 60, Noida, Uttar Pradesh 201301


</p> 


      </div>
    </section>

 




   <?php include 'include/footer.php' ;?>

  </body>
</html>
