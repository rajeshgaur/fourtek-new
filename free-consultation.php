<?php

error_reporting(1);
require_once("admin/functions/user_list.php");
$db = new Database();


require_once "./vendor/autoload.php";

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

if (isset($_POST['submit'])) {
    //$_POST=array();
    if (isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])) {
        //your site secret key
        $secret = '6LcfIYMUAAAAAJVBnF2cjuzCdSremj4GUx8fRJGY';
        //get verify response data
        $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
        $responseData = json_decode($verifyResponse);
        if ($responseData->success) {
            $name = $_POST['FirstName'];
            $visitor_email = $_POST['email'];
            $phone = $_POST['phone'];
            $message = $_POST['message'];

            //Create a new PHPMailer instance
            $mail = new PHPMailer;
            $mail->isSMTP();
            $mail->SMTPDebug = 0;
            $mail->Host = 'smtp.gmail.com';
            $mail->Port = 587;
            $mail->SMTPSecure = 'tls';
            $mail->SMTPAutoTLS = false;
            $mail->SMTPAuth = true;
            $mail->Username = "adwords@fourtek.com";
            $mail->Password = "Fourtek@0101";
            $mail->setFrom($visitor_email, 'Free Client Consultation');
            $mail->addAddress('arpit@fourtek.com');
            $mail->addAddress('manoj@fourtek.com');
            $mail->addAddress('nakul@fourtek.com');
            $mail->addAddress('nitisha@fourtek.com');
            //$mail->addAddress('sharmagaurav@fourtek.com');
            $mail->Subject = 'Free Client Consultation';
            $mail->msgHTML('<html><head> </head><body> <table border="1" cellpadding="5">
<tr> <td colspan="2"><strong>Name</strong></td>
  <td colspan="2">'.$_POST["name"].'</td>
</tr>
<tr>
  <td colspan="2"><strong>Email</strong></td>
  <td colspan="2">'.$_POST["email"].'</td>
</tr>
<tr>
  <td colspan="2"><strong>Phone</strong></td>
  <td colspan="2">'.$_POST["mobile"].'</td>
</tr>
<tr>
  <td colspan="2"><strong>Consultation</strong></td>
  <td colspan="2">'.$_POST["consultation"].'</td>
</tr>
<tr>
  <td colspan="2"><strong>Company</strong></td>
  <td colspan="2">'.$_POST["company"].'</td>
</tr>       
<tr>
  <td colspan="2"><strong>Comment</strong></td>
  <td colspan="2">'.$_POST["comment"].'</td>
</tr>
</table>
</body>
</html>');
            if (!$mail->send()) {
                $errmsg = "Mailer Error: " . $mail->ErrorInfo;
            } else {
                $succmsg = "<div class='success-msg' style='color:green;'>Your Message has been Sent Successfully.</div>";
                $form_data = array(
    'name' => $_POST['name'],
    'email' => $_POST['email'],
    'mobile' => $_POST['mobile'],
    'comment' => $_POST['comment'],
    'consultation' => $_POST['consultation'],
    'company' => $_POST['company']
      );
                $result = $db->insert_data('freeconsultation', $form_data);
            }
        } else {
            $errmsg = 'Robot verification failed, please try again.';
        }
    } else {
        $errmsg = 'Please click on the reCAPTCHA box.';
    }
} else {
    $errmsg = '';
    $succmsg = '';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <title>Fourtek</title>
    <link rel="shortcut icon" type="image/icon" href="images/fav.ico" />
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Pattaya|Poppins:300,300i,400" rel="stylesheet">
    <link href="css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
    <link href="css/animate.min.css" rel="stylesheet" type="text/css">
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link href="css/responsive.css" rel="stylesheet" type="text/css">
    <script src="js/jquery.min.js"></script>
    <?php include "google-code.php";?>
  </head>
<style>
  header{background: url(images/contact-us.jpg) !important; background-size: cover !important;  background-repeat: no-repeat;background-attachment: fixed !important;}
</style>
 <body id="page-top" class="inner-page">
      <?php include 'include/menu.php' ; ?>
  
    <header class="masthead video feeconsulation-header">
      <div class="container h-100">
        <div class="row h-100">
          <div class="col-12 my-auto text-center text-white">          
            <h1>Free Client Consultation</h1>
          </div>
        </div>
      </div>
    </header>

<section class="breadcrumb-block">
<div class="container">
  <div class="breadcrumb">
    <a class="breadcrumb-item" href="index.php">Home</a>
    <span class="breadcrumb-item active">Free Client Consultation</span>
  </div>
</div>
</section>

    <!-- About Section -->
    <section class="consultancy-sections">
      <div class="container">
        <div class="row"> 
        
        <div class="col-sm-5">
        <div class="wrapper center-block">
  <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
  <div class="panel panel-default">
    <div class="panel-heading active" role="tab" id="headingOne">
      <h4 class="panel-title">
        <a><p>You will get a free digital marketing consultation</p></a>
      </h4>

      <h4 class="panel-title">
        <a> <p>You will get strategies that have been proven to work</p></a>
      </h4>

      <h4 class="panel-title">
        <a> <p>You will get suggestions on improving your website and conversion rate</p></a>
      </h4>

      <h4 class="panel-title">
         <a><p>You will get a website audit to identify areas of technical improvement</p></a>
      </h4>

      <h4 class="panel-title">
         <a><p>You will get a keyword report indicating search engine opportunities</p></a>
      </h4>

      <h4 class="panel-title">
       <a> <p>You will get a report on generating traffic through additional sources</p></a>
      </h4>

      <h4 class="panel-title">
        <a> <p>You will get a single point of contact for all your digital marketing needs</p></a>
      </h4>

    </div>

  </div>
  

</div>
</div>
</div>




<div class="col-sm-7">
<h4 style="margin-bottom:20px;">Let's Start the Project</h4>
<p style="color:red;"><?php echo $errmsg;?></p>
<p style="color:green;"><?php echo $succmsg;?></p>
<form class="form" role="form"  action="" method="POST">
  <div class="form-group">
      <input type="text" class="form-control rounded-0" name="name"  required="" placeholder="Your Name *" value="<?php //echo $_POST['name']?>" >
  </div>
  <div class="form-group">
      <input type="email" class="form-control rounded-0" name="email" required="" placeholder="Your Email *" value="<?php //echo $_POST['email']?>" >
  </div>
  <div class="form-group">
      <input type="phone" class="form-control rounded-0" name="mobile" required="" placeholder="Your Phone Number *" minlength="10" maxlength="15" pattern="^[0-9]*$"  value="<?php //echo $_POST['mobile']?>">
  </div>


  <div class="form-group">
  <select class="form-control"  name="consultation" required>
    <option value="">Please Select Consultation *</option>
  <option value="Consultation Interest" <?php //if($_POST['consultation'] == 'Consultation Interest'){ echo 'selected="selected"';}?>>Consultation Interest</option>
  <option value="Website Designing" <?php //if($_POST['consultation'] == 'Website Designing'){ echo 'selected="selected"';}?>>Website Designing</option>
  <option value="Website Development" <?php //if($_POST['consultation'] == 'Website Development'){ echo 'selected="selected"';}?>>Website Development</option>
  <option value="Application Development" <?php //if($_POST['consultation'] == 'Application Development'){ echo 'selected="selected"';}?>>Application Development </option>
  <option value="Game Development" <?php //if($_POST['consultation'] == 'Game Development'){ echo 'selected="selected"';}?>>Game Development</option>
  <option value="Digital Marketing" <?php //if($_POST['consultation'] == 'Digital Marketing'){ echo 'selected="selected"';}?>>Digital Marketing</option>
  <option value="SEO" <?php //if($_POST['consultation'] == 'SEO'){ echo 'selected="selected"';}?>>SEO</option>
  <option value="SEM" <?php //if($_POST['consultation'] == 'SEM'){ echo 'selected="selected"';}?>>SEM</option>
  <option value="Content Marketing" <?php //if($_POST['consultation'] == 'Content Marketing'){ echo 'selected="selected"';}?>>Content Marketing</option>
  <option value="Email Marketing" <?php //if($_POST['consultation'] == 'Email Marketing'){ echo 'selected="selected"';}?>>Email Marketing</option>
  <option value="Social Media Management" <?php //if($_POST['consultation'] == 'Social Media Management'){ echo 'selected="selected"';}?>>Social Media Management</option>
  </select>
  </div>
  <div class="form-group">
      <input type="text" class="form-control rounded-0" name="company" required="" placeholder="Your Company name *" value="<?php //echo $_POST['company']?>">
  </div>


  <div class="form-group">
      <textarea class="form-control rounded-0" name="comment" rows="8" placeholder="Additional Information / Comments"><?php //echo $_POST['comment']?></textarea>
  </div>
 
  <div class="row">
  <div class="col-md-6">
  <div class="g-recaptcha" data-sitekey="6LcfIYMUAAAAAGI1bmHjGaNkvJo4xnzzijcTarIe"></div>
  </div>
  <div class="col-md-6 send-messages">
  <div class="form-group">
            
            <button type="submit" name="submit" class="btn-contact">Send Message</button>
        </div>
  </div>

<div class="clearfix"></div>

</div>
      </form>

    </div>






</div> 
</div>
</section>
<?php include('include/footer.php'); ?>

  </body>
</html>
