-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 22, 2018 at 06:31 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.0.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fourtek`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(8) NOT NULL,
  `username` varchar(40) NOT NULL,
  `forgot_pass_identity` varchar(255) NOT NULL,
  `password` varchar(60) NOT NULL,
  `email` varchar(60) NOT NULL,
  `mobile_number` varchar(20) NOT NULL,
  `secondary_number` varchar(20) NOT NULL,
  `address` varchar(250) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `description` longtext NOT NULL,
  `profile_image` varchar(250) NOT NULL,
  `subadmin_rights` varchar(250) NOT NULL,
  `type` varchar(20) NOT NULL,
  `status` int(11) NOT NULL,
  `admin_location` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `forgot_pass_identity`, `password`, `email`, `mobile_number`, `secondary_number`, `address`, `create_date`, `description`, `profile_image`, `subadmin_rights`, `type`, `status`, `admin_location`) VALUES
(1, 'admin', '', 'e6e061838856bf47e1de730719fb2609', 'bhupesh@fourtek.com', '0', '0', '', '2018-07-16 06:56:22', '', '', '', 'super', 1, ''),
(2, 'sdfsdfsdfsd', '', 'test123', 'admin@admin.com', '8986866161', '46169949494949', 'admin', '2018-04-27 04:24:07', 'dfgdfgdgdf<br>', '', '[\"blog\",\"events\"]', 'admin', 0, ''),
(3, 'sdfsdfsdfsd', '', 'gdfgdgd', 'admin@admin.com', '8986866161', '46169949494949', 'admin', '2018-04-27 04:24:13', 'dfgdgdf<br>', '', '[\"blog\",\"events\"]', 'admin', 0, ''),
(4, 'sdfsdfsdfsd', '', 'test123', 'admin@admin.com', '8986866161', '46169949494949', 'admin', '2018-04-27 04:24:16', 'sdffdfdf<br>', '', '[\"blog\",\"events\"]', 'admin', 0, ''),
(5, 'hello', '', 'hello12345', 'hello@admin.com', '8899663322', '558866223399', 'hello', '2018-04-27 04:24:20', 'hello<br>', '', '[\"blog\",\"events\"]', 'admin', 0, ''),
(6, 'testtt', '', '7b064dad507c266a161ffc73c53dcdc5', 'testt@gmail.com', '7896541263', '', 'aa', '0000-00-00 00:00:00', 'saf<br>', '', '[\"blog\",\"events\"]', 'admin', 0, ''),
(8, 'asdsafd', '', '24437656b2b84575068210effb02cece', 'safdsadf@gmail.com', 'safsd', 'sdfsaf', 'asfsadf', '2018-04-26 11:58:49', 'sadfsfd<br>', '', '', 'admin', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `contact_us_form`
--

CREATE TABLE `contact_us_form` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `comment` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact_us_form`
--

INSERT INTO `contact_us_form` (`id`, `name`, `email`, `mobile`, `comment`) VALUES
(1, 'Surendra Tomar', 'surendra@fourtek.com', '8855663322', 'Hello Test form'),
(2, 'Surendra Tomar', 'surendra@fourtek.com', '8855663322', 'Hello Test form');

-- --------------------------------------------------------

--
-- Table structure for table `digital_contact_form`
--

CREATE TABLE `digital_contact_form` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `comment` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `digital_contact_form`
--

INSERT INTO `digital_contact_form` (`id`, `name`, `email`, `mobile`, `comment`) VALUES
(1, 'Surendra', 'surendra@fourtek.com', '8855662233', 'Hello test contact form');

-- --------------------------------------------------------

--
-- Table structure for table `dynamic_field`
--

CREATE TABLE `dynamic_field` (
  `id` int(11) NOT NULL,
  `starter` varchar(255) NOT NULL,
  `bronze` varchar(255) NOT NULL,
  `gold` varchar(255) NOT NULL,
  `silver` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dynamic_field`
--

INSERT INTO `dynamic_field` (`id`, `starter`, `bronze`, `gold`, `silver`) VALUES
(1, '1230', '5000', '6000', '8000');

-- --------------------------------------------------------

--
-- Table structure for table `job_apply`
--

CREATE TABLE `job_apply` (
  `id` int(5) NOT NULL,
  `first_name` varchar(250) NOT NULL,
  `last_name` varchar(250) NOT NULL,
  `post_apply` varchar(250) NOT NULL,
  `total_exp` varchar(250) NOT NULL,
  `email` varchar(40) NOT NULL,
  `mobile_number` varchar(50) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `address_one` varchar(255) NOT NULL,
  `address_two` varchar(255) NOT NULL,
  `resume_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_apply`
--

INSERT INTO `job_apply` (`id`, `first_name`, `last_name`, `post_apply`, `total_exp`, `email`, `mobile_number`, `city`, `state`, `address_one`, `address_two`, `resume_name`) VALUES
(93, 'surendra', 'tomar', 'php', '2', 'surendra@fourtek.com', '8855447799', 'noida', 'up', 'noida', 'noida', '63de7cec3b5ec91610e1c407e8abf385.xls');

-- --------------------------------------------------------

--
-- Table structure for table `job_career`
--

CREATE TABLE `job_career` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `short_desc` text NOT NULL,
  `description` text NOT NULL,
  `min_qualifica` text NOT NULL,
  `num_of_post` varchar(255) NOT NULL,
  `post_date` varchar(255) NOT NULL,
  `experience` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_career`
--

INSERT INTO `job_career` (`id`, `title`, `short_desc`, `description`, `min_qualifica`, `num_of_post`, `post_date`, `experience`, `location`, `create_date`) VALUES
(19, 'Automation Tester', 'Create automation tests for platforms and services  <br/>team.', '<p> - Collaborate with project management and developers to define and implement innovative solutions for the project direction, visuals and user experience</p>\r\n<p> - Execute all visual design stages from concept to final hand-off to development team</p>\r\n<p> - Conceptualize original ideas that bring simplicity and user friendliness to complex design roadblocks</p>\r\n<p> - Create wireframes, storyboards, user flows, process flows and site-maps to effectively communicate interaction and design ideas</p>\r\n<p> - Present and defend designs and key milestone deliverables to peers and executive level stakeholders</p>\r\n<p> - Conduct user research and evaluate user feedback</p>\r\n<p> - Establish and promote design guidelines, best practices and standards</p>                                                      ', '<p>- B.Tech/B.E. (Computer Science/IT/Electronics)</p><br><p>- MCA</p><br><p>- Computer diploma in development with 3+ years of experience compulsory</p><br>', '02', '2018-09-29', '2-4 Years', 'Noida Sector 60', '2018-09-28 09:02:25');

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(6) NOT NULL,
  `txnid` varchar(20) NOT NULL,
  `payment_amount` decimal(7,2) NOT NULL,
  `payment_status` varchar(25) NOT NULL,
  `itemid` varchar(25) NOT NULL,
  `createdtime` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_us_form`
--
ALTER TABLE `contact_us_form`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `digital_contact_form`
--
ALTER TABLE `digital_contact_form`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dynamic_field`
--
ALTER TABLE `dynamic_field`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_apply`
--
ALTER TABLE `job_apply`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_career`
--
ALTER TABLE `job_career`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `contact_us_form`
--
ALTER TABLE `contact_us_form`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `digital_contact_form`
--
ALTER TABLE `digital_contact_form`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `dynamic_field`
--
ALTER TABLE `dynamic_field`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `job_apply`
--
ALTER TABLE `job_apply`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;

--
-- AUTO_INCREMENT for table `job_career`
--
ALTER TABLE `job_career`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
