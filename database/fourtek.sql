-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 28, 2018 at 02:04 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.0.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fourtek`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(8) NOT NULL,
  `username` varchar(40) NOT NULL,
  `forgot_pass_identity` varchar(255) NOT NULL,
  `password` varchar(60) NOT NULL,
  `email` varchar(60) NOT NULL,
  `mobile_number` varchar(20) NOT NULL,
  `secondary_number` varchar(20) NOT NULL,
  `address` varchar(250) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `description` longtext NOT NULL,
  `profile_image` varchar(250) NOT NULL,
  `subadmin_rights` varchar(250) NOT NULL,
  `type` varchar(20) NOT NULL,
  `status` int(11) NOT NULL,
  `admin_location` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `forgot_pass_identity`, `password`, `email`, `mobile_number`, `secondary_number`, `address`, `create_date`, `description`, `profile_image`, `subadmin_rights`, `type`, `status`, `admin_location`) VALUES
(1, 'admin', '', 'e6e061838856bf47e1de730719fb2609', 'bhupesh@fourtek.com', '0', '0', '', '2018-07-16 06:56:22', '', '', '', 'super', 1, ''),
(2, 'sdfsdfsdfsd', '', 'test123', 'admin@admin.com', '8986866161', '46169949494949', 'admin', '2018-04-27 04:24:07', 'dfgdfgdgdf<br>', '', '[\"blog\",\"events\"]', 'admin', 0, ''),
(3, 'sdfsdfsdfsd', '', 'gdfgdgd', 'admin@admin.com', '8986866161', '46169949494949', 'admin', '2018-04-27 04:24:13', 'dfgdgdf<br>', '', '[\"blog\",\"events\"]', 'admin', 0, ''),
(4, 'sdfsdfsdfsd', '', 'test123', 'admin@admin.com', '8986866161', '46169949494949', 'admin', '2018-04-27 04:24:16', 'sdffdfdf<br>', '', '[\"blog\",\"events\"]', 'admin', 0, ''),
(5, 'hello', '', 'hello12345', 'hello@admin.com', '8899663322', '558866223399', 'hello', '2018-04-27 04:24:20', 'hello<br>', '', '[\"blog\",\"events\"]', 'admin', 0, ''),
(6, 'testtt', '', '7b064dad507c266a161ffc73c53dcdc5', 'testt@gmail.com', '7896541263', '', 'aa', '0000-00-00 00:00:00', 'saf<br>', '', '[\"blog\",\"events\"]', 'admin', 0, ''),
(8, 'asdsafd', '', '24437656b2b84575068210effb02cece', 'safdsadf@gmail.com', 'safsd', 'sdfsaf', 'asfsadf', '2018-04-26 11:58:49', 'sadfsfd<br>', '', '', 'admin', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE `banner` (
  `id` int(11) NOT NULL,
  `promotion_title` varchar(250) NOT NULL,
  `promotion_content` varchar(250) NOT NULL,
  `name` varchar(250) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`id`, `promotion_title`, `promotion_content`, `name`, `status`) VALUES
(6, 'Free Matrimonial Site', '- Free Registration.<br>\r\n\r\n- See each others contact information for free<br>\r\n\r\n- Free Personalized Messages with your prospects.', '84ecf2c347a7e009bc47d3131b604cc1SM566324.jpg', 1),
(8, 'Matrimonial Meetups', '- Meeting up with marriage minded singles near you.<br>\r\n- Meetups on the basis of age, caste and income group.', '13f566021c4bd4f7899fbb7283386385banner133.jpg', 1),
(9, 'Trusted Matrimony Portal', '100% email and mobile verified profile<br>\r\n\r\n100% Facebook verified profile.', 'f24ca2fc05a3cdc4c4ff54d318529f5235e62151a0f9ec359c0b06eb6798ad20caring.jpg', 1),
(15, 'Spiritual Following', 'Choose match according to your spiritual following like Radha Soami, AOL, Sadguru, Osho etc.', '9bb61b3485bc79f56d81aeb2f80b24f420416540f5f4a6d856542bf51136212fsunset-1.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `blog_comment`
--

CREATE TABLE `blog_comment` (
  `id` int(11) NOT NULL,
  `blog_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `comment_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog_comment`
--

INSERT INTO `blog_comment` (`id`, `blog_id`, `comment`, `user_id`, `status`, `comment_date`) VALUES
(8, 18, 'This is test comment easemymatch<br>', 77, 0, '2018-08-30 12:35:45');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `cat_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `cat_name`) VALUES
(4, 'Personality Development'),
(5, 'Learning'),
(7, 'Relationships'),
(9, 'Motivational'),
(10, 'Meetup');

-- --------------------------------------------------------

--
-- Table structure for table `dynamic_field`
--

CREATE TABLE `dynamic_field` (
  `id` int(11) NOT NULL,
  `meetup_tagline` longtext NOT NULL,
  `blog_add_first_img` varchar(255) NOT NULL,
  `blog_add_scnd_img` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `number` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dynamic_field`
--

INSERT INTO `dynamic_field` (`id`, `meetup_tagline`, `blog_add_first_img`, `blog_add_scnd_img`, `address`, `number`, `email`) VALUES
(1, 'Singles Matrimonial Meetups basis on your criteria to select the life partner', '84b817e70a495839e515a2c92addb983Comp-1_1.gif', 'bf1df807c2063b0bea0d21993964e1c9Final Second Image (1).jpg', 'Singles Matrimonial Meetups basis on your criteria to select the life partner', '9354225779', 'contactus@easemymatch.com');

-- --------------------------------------------------------

--
-- Table structure for table `job_career`
--

CREATE TABLE `job_career` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `short_desc` text NOT NULL,
  `description` text NOT NULL,
  `min_qualifica` text NOT NULL,
  `num_of_post` varchar(255) NOT NULL,
  `post_date` varchar(255) NOT NULL,
  `experience` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_career`
--

INSERT INTO `job_career` (`id`, `title`, `short_desc`, `description`, `min_qualifica`, `num_of_post`, `post_date`, `experience`, `location`, `create_date`) VALUES
(19, 'Automation Tester', 'Create automation tests for platforms and services  <br/>team.', '<p> - Collaborate with project management and developers to define and implement innovative solutions for the project direction, visuals and user experience</p>\r\n<p> - Execute all visual design stages from concept to final hand-off to development team</p>\r\n<p> - Conceptualize original ideas that bring simplicity and user friendliness to complex design roadblocks</p>\r\n<p> - Create wireframes, storyboards, user flows, process flows and site-maps to effectively communicate interaction and design ideas</p>\r\n<p> - Present and defend designs and key milestone deliverables to peers and executive level stakeholders</p>\r\n<p> - Conduct user research and evaluate user feedback</p>\r\n<p> - Establish and promote design guidelines, best practices and standards</p>                                                      ', '<p>- B.Tech/B.E. (Computer Science/IT/Electronics)</p><br><p>- MCA</p><br><p>- Computer diploma in development with 3+ years of experience compulsory</p><br>', '02', '2018-09-29', '2-4 Years', 'Noida Sector 60', '2018-09-28 09:02:25'),
(20, 'Automation Tester', 'Create automation tests for platforms and services  <br/>team.', '<p> - Collaborate with project management and developers to define and implement innovative solutions for the project direction, visuals and user experience</p><br><p> - Execute all visual design stages from concept to final hand-off to development team</p><br><p> - Conceptualize original ideas that bring simplicity and user friendliness to complex design roadblocks</p><br><p> - Create wireframes, storyboards, user flows, process flows and site-maps to effectively communicate interaction and design ideas</p><br><p> - Present and defend designs and key milestone deliverables to peers and executive level stakeholders</p><br><p> - Conduct user research and evaluate user feedback</p><br><p> - Establish and promote design guidelines, best practices and standards</p><br>                                                      ', '<p>- B.Tech/B.E. (Computer Science/IT/Electronics)</p><br><p>- MCA</p><br><p>- Computer diploma in development with 3+ years of experience compulsory</p><br>', '02', '2018-09-29', '2-4 Years', 'Noida Sector 60', '2018-09-28 09:02:18');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `title` varchar(250) NOT NULL,
  `page_url` varchar(250) NOT NULL,
  `heading` text NOT NULL,
  `description` text NOT NULL,
  `images` varchar(250) NOT NULL,
  `status` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `page_id`, `title`, `page_url`, `heading`, `description`, `images`, `status`, `create_date`) VALUES
(1, 1, 'About us', 'about-us', '<h3><br></h3>', '<blockquote><h3>\"Marriages are\r\nmade in heaven and consummated on Earth\"</h3></blockquote>Someone, somewhere, is made for you. You are seeking him and he is seeking you. In everyone\'s life there comes a phase where they want to choose their life partner, and that time all they need is a trustworthy platform to find that special person who is interested to commit and embark with them in a new journey of life with trust and confidence.Ease My Match team is here to help you to make your dream come true.<div><br></div><div>We have met and interviewed many people who are searching partner for long time with no luck, and then we decided to dig in root causes and come up with platform to ease search for perfect match.</div><div><br></div><div><div><b>Problem</b>&nbsp;:- No profile verification and allowed to create any number of account with different email id\'s. If one profile is blocked any one can create multiple profile with new email id\'s.</div><div><b><b>Ease My Match</b>&nbsp;</b>:-&nbsp;&nbsp;We are doing manual verification on the basis of facebook and other parameters. If we found anybody\'s facebook account and other information are not trustworthy, we won\'t allow any account creation.</div><div><br></div><div><b>Problem</b>&nbsp;:-&nbsp;Few people are using these sites for dating and for fraud with no marriage intention. If anyone report against that person then generally other&nbsp; websites asks for proof.</div><div><b>Ease My Match:-&nbsp;</b>&nbsp;If while search any profile found with diverse intentions,&nbsp; click&nbsp;&nbsp;\"report abuse\" button and we will take action to completely revoke that&nbsp;from our website.<br></div><div><br></div><div><b>Problem</b>&nbsp;:- Meeting to a person needs lots of efforts e.g.&nbsp; send interest, accept interest, parents discussion, boy and girl discussion and then the real part of meeting. It is very long and time consuming process.<br></div><div><b><b>Ease My Match</b></b>&nbsp;:-&nbsp; We will scan and organize regular matrimonial meetup\'s&nbsp; in all cities considering preference criteria\'s.</div><div><br></div><div><b>Problem :-&nbsp;</b>Some people are blocked in life due to some bitter past experiences and it causes blocking of love energy to flow to their system.</div><div><b><b>Ease My Match</b>&nbsp;</b>&nbsp;:- We will provide counselling and also help them to clean blockages and overcome fears.</div><div><br></div><div><b>Problem</b>&nbsp;:- Few people prefers to get married in their own following(e.g. radha soami,&nbsp;brahma kumaris, AOL,&nbsp;etc), however, today no platform matches the profile considering following.</div><div><b><b>Ease My Match</b>&nbsp;</b>:- You can select option for your following and start searching your partner in that interest.</div><div><br></div><div>Experience the beautiful journey with Ease my Match.</div><div><br></div><div>We wish you luck for your soulmate search.<div></div></div></div><div><br></div>', 'ac5fa9a642c1950486cac97034e1434da19f724f2d89b28329d36e736198711fabout-feature.png', 1, '2018-07-13 06:48:18');

-- --------------------------------------------------------

--
-- Table structure for table `photos`
--

CREATE TABLE `photos` (
  `id` int(10) NOT NULL,
  `cust_id` int(10) NOT NULL,
  `pic1` varchar(25) NOT NULL,
  `pic2` varchar(40) NOT NULL,
  `pic3` varchar(40) NOT NULL,
  `pic4` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `photos`
--

INSERT INTO `photos` (`id`, `cust_id`, `pic1`, `pic2`, `pic3`, `pic4`) VALUES
(27, 6, 'img.jpg', 'picture.jpg', 'picture-2.jpg', 'user.png'),
(28, 7, 'banner_img_3@2x.png', 'article_img_2.jpg', 'banner_img_5@2x.png', 'article_img_1.jpg'),
(29, 12, 'article_img_1.jpg', 'article_img_2.jpg', 'banner_img_2.png', 'banner_img_2.png'),
(30, 13, 'team-13.jpg', 'thumb-intro.jpg', 'avatar-1.jpg', '1.jpg'),
(31, 14, '1.jpg', 'img-1.jpg', 'avatar-1.jpg', 'team-13.jpg'),
(32, 15, 'DL.png', 'process.png', 'deep.png', 'deep.png');

-- --------------------------------------------------------

--
-- Table structure for table `profile_images`
--

CREATE TABLE `profile_images` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `image_name` varchar(255) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile_images`
--

INSERT INTO `profile_images` (`id`, `user_id`, `image_name`, `time`) VALUES
(11, 77, 'I4SZS8ymoWt.jpg', '2018-08-30 07:29:29'),
(12, 77, 'imgpsh_fullsize2.jpeg', '2018-09-04 10:45:27');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(5) NOT NULL,
  `reset_key` varchar(250) NOT NULL,
  `profile_id` varchar(250) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(40) NOT NULL,
  `email` varchar(40) NOT NULL,
  `mobile_number` varchar(50) NOT NULL,
  `show_name` int(11) NOT NULL,
  `show_mobile` int(11) NOT NULL,
  `show_email` int(11) NOT NULL,
  `register_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `reward_point` int(11) NOT NULL,
  `hide_profile` int(11) NOT NULL,
  `hide_profile_date_from` date NOT NULL,
  `delete_profile` int(11) NOT NULL,
  `delete_reason` varchar(255) NOT NULL,
  `other_reason` varchar(255) NOT NULL,
  `interest_request` int(11) NOT NULL,
  `request_accept` int(11) NOT NULL,
  `event_alert` int(11) NOT NULL,
  `promotional_mails` int(11) NOT NULL,
  `meetup_alert` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `create_dates` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `reset_key`, `profile_id`, `username`, `password`, `email`, `mobile_number`, `show_name`, `show_mobile`, `show_email`, `register_date`, `reward_point`, `hide_profile`, `hide_profile_date_from`, `delete_profile`, `delete_reason`, `other_reason`, `interest_request`, `request_accept`, `event_alert`, `promotional_mails`, `meetup_alert`, `status`, `create_dates`) VALUES
(92, '', 'EMM00092', 'swati', 'e10adc3949ba59abbe56e057f20f883e', 'swati@fourtek.com', '7060584707', 0, 0, 0, '2018-09-05 13:11:08', 0, 0, '0000-00-00', 0, '', '', 0, 0, 0, 0, 0, 0, '2018-09-05 13:11:08');

-- --------------------------------------------------------

--
-- Table structure for table `users_profile`
--

CREATE TABLE `users_profile` (
  `id` int(10) NOT NULL,
  `user_id` int(5) NOT NULL,
  `blocked_user` varchar(255) NOT NULL,
  `report_abuse` varchar(255) NOT NULL,
  `height` varchar(20) NOT NULL,
  `religion` varchar(250) NOT NULL,
  `create_profile_for` varchar(250) NOT NULL,
  `dateofbirth` varchar(250) NOT NULL,
  `gender` varchar(250) NOT NULL,
  `caste` varchar(250) NOT NULL,
  `subcaste` varchar(250) NOT NULL,
  `city` varchar(250) NOT NULL,
  `state` varchar(250) NOT NULL,
  `country` varchar(250) NOT NULL,
  `maritalstatus` varchar(250) NOT NULL,
  `follower` varchar(250) NOT NULL,
  `manglik_status` varchar(255) NOT NULL,
  `have_childern` varchar(255) NOT NULL,
  `living_with_parent` varchar(255) NOT NULL,
  `parent_together_country` varchar(255) NOT NULL,
  `parent_together_state` varchar(250) NOT NULL,
  `parent_together_city` varchar(250) NOT NULL,
  `highest_degree` varchar(250) NOT NULL,
  `ug_degree` varchar(250) NOT NULL,
  `organization` varchar(250) NOT NULL,
  `about_me` longtext NOT NULL,
  `body_type` text NOT NULL,
  `physical_status` text NOT NULL,
  `drink` varchar(250) NOT NULL,
  `mothertounge` text NOT NULL,
  `colour` varchar(250) NOT NULL,
  `weight` varchar(250) NOT NULL,
  `blood_group` varchar(250) NOT NULL,
  `diet` varchar(250) NOT NULL,
  `images` varchar(250) NOT NULL,
  `smoke` varchar(250) NOT NULL,
  `dietary_habit` varchar(250) NOT NULL,
  `own_pets` varchar(250) NOT NULL,
  `own_house` varchar(250) NOT NULL,
  `own_car` varchar(250) NOT NULL,
  `residential_status` varchar(250) NOT NULL,
  `langauge_speak` varchar(250) NOT NULL,
  `nature_handicap` varchar(250) NOT NULL,
  `occupation` text NOT NULL,
  `occupation_descr` text NOT NULL,
  `annual_income` varchar(250) NOT NULL,
  `fathers_occupation` varchar(250) NOT NULL,
  `mothers_occupation` varchar(250) NOT NULL,
  `brother_count` varchar(250) NOT NULL,
  `sister_count` varchar(250) NOT NULL,
  `gothra` varchar(250) NOT NULL,
  `maternal_gothra` varchar(250) NOT NULL,
  `family_status` varchar(250) NOT NULL,
  `family_income` varchar(250) NOT NULL,
  `about_family` longtext NOT NULL,
  `family_type` varchar(255) NOT NULL,
  `how_many_brother_married` varchar(250) NOT NULL,
  `how_many_sister_married` varchar(250) NOT NULL,
  `photo_visibilty` int(11) NOT NULL,
  `profile_update_status` int(11) NOT NULL,
  `profilecreationdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `facebook_id` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_profile`
--

INSERT INTO `users_profile` (`id`, `user_id`, `blocked_user`, `report_abuse`, `height`, `religion`, `create_profile_for`, `dateofbirth`, `gender`, `caste`, `subcaste`, `city`, `state`, `country`, `maritalstatus`, `follower`, `manglik_status`, `have_childern`, `living_with_parent`, `parent_together_country`, `parent_together_state`, `parent_together_city`, `highest_degree`, `ug_degree`, `organization`, `about_me`, `body_type`, `physical_status`, `drink`, `mothertounge`, `colour`, `weight`, `blood_group`, `diet`, `images`, `smoke`, `dietary_habit`, `own_pets`, `own_house`, `own_car`, `residential_status`, `langauge_speak`, `nature_handicap`, `occupation`, `occupation_descr`, `annual_income`, `fathers_occupation`, `mothers_occupation`, `brother_count`, `sister_count`, `gothra`, `maternal_gothra`, `family_status`, `family_income`, `about_family`, `family_type`, `how_many_brother_married`, `how_many_sister_married`, `photo_visibilty`, `profile_update_status`, `profilecreationdate`, `facebook_id`, `status`) VALUES
(174, 92, '', '', '5.6', 'Hinduism', 'Self', '2000-09-01', 'Female', '', '', '', '', 'HM', 'Never Married', '', '', '', 'Together', '', '', '', 'B.Arch', '', '', 'This is the text data.This is the text data.This is the text data.This is the text data.This is the text data.', '', '', '', 'Hindi', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Not working', '', '2', '', '', '', '', '', '', '', '', '', '', '', '', 0, 2, '2018-09-05 13:11:55', '', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_comment`
--
ALTER TABLE `blog_comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dynamic_field`
--
ALTER TABLE `dynamic_field`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_career`
--
ALTER TABLE `job_career`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `photos`
--
ALTER TABLE `photos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cust_id` (`cust_id`);

--
-- Indexes for table `profile_images`
--
ALTER TABLE `profile_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_profile`
--
ALTER TABLE `users_profile`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cust_id` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `blog_comment`
--
ALTER TABLE `blog_comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `dynamic_field`
--
ALTER TABLE `dynamic_field`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `job_career`
--
ALTER TABLE `job_career`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `photos`
--
ALTER TABLE `photos`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `profile_images`
--
ALTER TABLE `profile_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;

--
-- AUTO_INCREMENT for table `users_profile`
--
ALTER TABLE `users_profile`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=175;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
