-- MySQL dump 10.13  Distrib 5.7.33, for Linux (x86_64)
--
-- Host: localhost    Database: empirede_empiredelhi
-- ------------------------------------------------------
-- Server version	5.7.33-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `annexure`
--

DROP TABLE IF EXISTS `annexure`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `annexure` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `particular` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `file` varchar(255) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `annexure`
--

LOCK TABLES `annexure` WRITE;
/*!40000 ALTER TABLE `annexure` DISABLE KEYS */;
INSERT INTO `annexure` VALUES (1,'Name & Adress of contractor','Name & Adress of contractor','2015-11-03-2101015706Name & Adress of contractor.pdf','2015-11-03'),(2,'Name & Adress of where the contract work','Name & Adress of where the contract work','2015-11-03-1453662037Name & Adress of where the contract work.pdf','2015-11-03'),(3,'Name & Email.ID of the Incharge','Name & Email.ID of the Incharge','2015-11-03-1325955671Name & Email.ID of the Incharge.pdf','2015-11-03'),(4,'name and address of estblisment.','name and address of estblisment.','2015-11-03-1681758907name and address of estblisment..pdf','2015-11-03'),(5,'Name of Web site','Name of Web site','2015-11-03-1895069436Name of Web site.pdf','2015-11-03'),(6,'Form No V','From Principal employer','2015-11-03-62469383Form No V.pdf','2015-11-03'),(7,'Notice of commencement compation of contract work rule 25(2)','Form NO VI','2015-11-03-428659845Form No IV.pdf','2015-11-03'),(8,'Form NO IV Rule 21(1) VII Rule 29(2)','Application Form','2015-11-03-103073251Form No IV.pdf','2015-11-03'),(9,'Agreement with P.E along with rates','P.O','2015-11-03-1458593462PO.pdf','2015-11-03'),(10,'letter of extension of contract period in case of renewal','mail copy','2015-11-03-789591029Amendment Mail.pdf','2015-11-03'),(11,'Affidivate that wages are being disbursed thought account payee cheque ECS','Affidivate','2015-11-03-21292281Affidavit Minimum wages.pdf','2015-11-03'),(12,'PF code number','Registration no ','2015-11-03-1264039294PF Registration.pdf','2015-11-03'),(13,'Esic Code No','Registration no','2015-11-03-1328054805ESIC Registration.pdf','2015-11-03'),(14,'Labour welfare funds & Half Years returns','Received copy','2015-11-03-845176058Labour Welfare Fund July 2014 to Dec 2014.pdf','2015-11-03'),(17,'FORM V','FORM','2017-12-19-1551798899FORM5.pdf','2017-12-19'),(18,'affidivate','AFFIDIVATE','2017-12-19-1149147952AFFIDIVATE.pdf','2017-12-19'),(19,'affidivate','AFFIDIVATE','2017-12-19-929576385AFFIDIVATE for LabourLicence-2.pdf','2017-12-19'),(20,'Affidivate','AFFIDIVATE','2017-12-19-2031580686UNDERTAKING.pdf','2017-12-19'),(21,'Labour licence fess','Recept','2017-12-19-613391999RevenueApplicationReciept.pdf','2017-12-19'),(22,'Renewal fees of Labour licence','PO no 4600004608 till 31.12.2021','2021-01-15-1908299478LabourApplicationReciept.pdf','2021-01-15'),(23,'FORM V','PO 4600004608 till 31.12.2021','2021-01-15-1035312068form no 05.pdf','2021-01-15');
/*!40000 ALTER TABLE `annexure` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empire_users`
--

DROP TABLE IF EXISTS `empire_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empire_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8 NOT NULL,
  `pwd` varchar(1023) CHARACTER SET utf8 NOT NULL,
  `opwd` varchar(1023) CHARACTER SET utf8 NOT NULL,
  `status` smallint(1) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empire_users`
--

LOCK TABLES `empire_users` WRITE;
/*!40000 ALTER TABLE `empire_users` DISABLE KEYS */;
INSERT INTO `empire_users` VALUES (1,'0011Empire','611475fee0a771185fcc0abadce62b1c','0',1,'2016-03-18 10:02:28');
/*!40000 ALTER TABLE `empire_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employee_data`
--

DROP TABLE IF EXISTS `employee_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employee_data` (
  `id` int(25) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `category_file` varchar(255) DEFAULT NULL,
  `type` varchar(255) NOT NULL,
  `filepath` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=152 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee_data`
--

LOCK TABLES `employee_data` WRITE;
/*!40000 ALTER TABLE `employee_data` DISABLE KEYS */;
INSERT INTO `employee_data` VALUES (4,'2015-10-19-1609613680list of enforcment project (Empire Enterpises Ltd).pdf','employess','uploads','2015-10-19-1609613680list of enforcment project (Empire Enterpises Ltd).pdf','2015-10-19 05:18:55'),(8,'2015-10-19-405757103Mustrol cum wages sheet month of june 2015.pdf','mustrolwagessheet','uploads','2015-10-19-405757103Mustrol cum wages sheet month of june 2015.pdf','2015-10-19 05:21:40'),(11,'2015-10-19-1984244272mustrool cum wages sheet month of Sep 2015.pdf','mustrolwagessheet','uploads','2015-10-19-1984244272mustrool cum wages sheet month of Sep 2015.pdf','2015-10-19 05:24:08'),(12,'2015-10-19-165250983ESC Sheet month of April 2015.pdf','escsreport','uploads','2015-10-19-165250983ESC Sheet month of April 2015.pdf','2015-10-19 05:24:47'),(13,'2015-10-19-140265278ECS sheet for month of may 2015.pdf','escsreport','uploads','2015-10-19-140265278ECS sheet for month of may 2015.pdf','2015-10-19 05:25:24'),(14,'2015-10-19-392729352ECS Report  for the month of june 2015.pdf','escsreport','uploads','2015-10-19-392729352ECS Report  for the month of june 2015.pdf','2015-10-19 05:26:08'),(15,'2015-10-19-1901981517ECS report month of july 2015 andhra bank.pdf','escsreport','uploads','2015-10-19-1901981517ECS report month of july 2015 andhra bank.pdf','2015-10-19 05:26:29'),(16,'2015-10-19-1926169086ECS Report  FOR the MONTH OF AUG 2015.pdf','escsreport','uploads','2015-10-19-1926169086ECS Report  FOR the MONTH OF AUG 2015.pdf','2015-10-19 05:26:47'),(17,'2015-10-19-510605014ESC  for the month of september 2015.pdf','escsreport','uploads','2015-10-19-510605014ESC  for the month of september 2015.pdf','2015-10-19 05:33:44'),(18,'2015-10-19-1692893756PF Challan-Apr15.pdf','pfchallan','uploads','2015-10-19-1692893756PF Challan-Apr15.pdf','2015-10-19 05:34:17'),(19,'2015-10-19-634410804PF MAY- 2015.pdf','pfchallan','uploads','2015-10-19-634410804PF MAY- 2015.pdf','2015-10-19 05:35:13'),(20,'2015-10-19-2124491781PF JUNE_ 2015.pdf','pfchallan','uploads','2015-10-19-2124491781PF JUNE_ 2015.pdf','2015-10-19 05:36:02'),(21,'2015-10-19-302412899PF Challan-July15.pdf','pfchallan','uploads','2015-10-19-302412899PF Challan-July15.pdf','2015-10-19 05:36:58'),(22,'2015-10-19-1056324345PF AUG 2015.pdf','pfchallan','uploads','2015-10-19-1056324345PF AUG 2015.pdf','2015-10-19 05:39:22'),(23,'2015-10-19-970640437ESIC paid copy april 2015.pdf','esicchallan','uploads','2015-10-19-970640437ESIC paid copy april 2015.pdf','2015-10-19 05:39:54'),(24,'2015-10-19-604358940ESIC MAy 2015.pdf','esicchallan','uploads','2015-10-19-604358940ESIC MAy 2015.pdf','2015-10-19 05:40:17'),(25,'2015-10-19-1008293109ESI Challan-June15 Bank Copy (1).pdf','esicchallan','uploads','2015-10-19-1008293109ESI Challan-June15 Bank Copy (1).pdf','2015-10-19 05:45:24'),(26,'2015-10-19-1751440171ESI Challan-July15 Bank Copy (1).pdf','esicchallan','uploads','2015-10-19-1751440171ESI Challan-July15 Bank Copy (1).pdf','2015-10-19 05:46:00'),(27,'2015-10-19-413751362ESI AUG 2015.pdf','esicchallan','uploads','2015-10-19-413751362ESI AUG 2015.pdf','2015-10-19 05:46:29'),(28,'2015-10-19-1008556972Labour Licence MMG Project(1).pdf','laboutllicense','uploads','2015-10-19-1008556972Labour Licence MMG Project(1).pdf','2015-10-19 05:47:16'),(29,'2015-10-19-1863126877Labour Licence-Enf Project.pdf','laboutllicense','uploads','2015-10-19-1863126877Labour Licence-Enf Project.pdf','2015-10-19 05:47:57'),(30,'2015-10-19-1286669961Labour Welfare Fund January 2015 to June 2015.pdf','labourwelfre','uploads','2015-10-19-1286669961Labour Welfare Fund January 2015 to June 2015.pdf','2015-10-19 05:48:50'),(33,'2015-11-27-1268069635list of enforcment project (Empire Enterpises Ltd).pdf','employess','uploads','2015-11-27-1268069635list of enforcment project (Empire Enterpises Ltd).pdf','2015-11-27 05:33:38'),(34,'2015-11-27-1933706389ECS Report month of OCT-2015.pdf','escsreport','uploads','2015-11-27-1933706389ECS Report month of OCT-2015.pdf','2015-11-27 06:54:08'),(35,'2015-11-27-415515321Mustrol cum wages sheet month of April 2015.pdf','mustrolwagessheet','uploads','2015-11-27-415515321Mustrol cum wages sheet month of April 2015.pdf','2015-11-26 19:35:50'),(36,'2015-11-27-344693196Mustrol cum wages sheet month of May 2015.pdf','mustrolwagessheet','uploads','2015-11-27-344693196Mustrol cum wages sheet month of May 2015.pdf','2015-11-26 19:36:12'),(38,'2015-11-27-1715005454mustrool cum wagesh sheet month of july 2015.pdf','mustrolwagessheet','uploads','2015-11-27-1715005454mustrool cum wagesh sheet month of july 2015.pdf','2015-11-26 19:37:08'),(39,'2015-11-27-889921751Mustrool cum wages sheet month of Aug 2015.pdf','mustrolwagessheet','uploads','2015-11-27-889921751Mustrool cum wages sheet month of Aug 2015.pdf','2015-11-26 19:37:25'),(40,'2015-11-27-1593628673mustrool cum wages sheet month of Sep 2015.pdf','mustrolwagessheet','uploads','2015-11-27-1593628673mustrool cum wages sheet month of Sep 2015.pdf','2015-11-26 19:37:45'),(41,'2015-11-27-489240212ESC Sheet month of April 2015.pdf','escsreport','uploads','2015-11-27-489240212ESC Sheet month of April 2015.pdf','2015-11-26 19:38:13'),(42,'2015-11-27-1031653345ECS sheet for month of may 2015.pdf','escsreport','uploads','2015-11-27-1031653345ECS sheet for month of may 2015.pdf','2015-11-26 19:38:42'),(43,'2015-11-27-1781228940ECS Report  for the month of june 2015.pdf','escsreport','uploads','2015-11-27-1781228940ECS Report  for the month of june 2015.pdf','2015-11-26 19:39:22'),(44,'2015-11-27-117099642ECS report month of july 2015 andhra bank.pdf','escsreport','uploads','2015-11-27-117099642ECS report month of july 2015 andhra bank.pdf','2015-11-26 19:39:45'),(45,'2015-11-27-658945027ECS Report  FOR the MONTH OF AUG 2015.pdf','escsreport','uploads','2015-11-27-658945027ECS Report  FOR the MONTH OF AUG 2015.pdf','2015-11-26 19:40:28'),(46,'2015-11-27-31597993ESC  for the month of september 2015.pdf','escsreport','uploads','2015-11-27-31597993ESC  for the month of september 2015.pdf','2015-11-26 19:40:50'),(47,'2015-11-27-162831031ECS Report month of OCT-2015.pdf','escsreport','uploads','2015-11-27-162831031ECS Report month of OCT-2015.pdf','2015-11-26 19:41:29'),(48,'2015-11-27-341599656PF Challan-Apr15.pdf','pfchallan','uploads','2015-11-27-341599656PF Challan-Apr15.pdf','2015-11-26 19:42:27'),(49,'2015-11-27-1363978224PF MAY- 2015.pdf','pfchallan','uploads','2015-11-27-1363978224PF MAY- 2015.pdf','2015-11-26 19:43:07'),(50,'2015-11-27-2133464162PF JUNE_ 2015.pdf','pfchallan','uploads','2015-11-27-2133464162PF JUNE_ 2015.pdf','2015-11-26 19:43:46'),(52,'2015-11-27-658273862PF AUG 2015.pdf','pfchallan','uploads','2015-11-27-658273862PF AUG 2015.pdf','2015-11-26 19:44:58'),(53,'2015-11-27-18571367PF Challan-Sep15.pdf','pfchallan','uploads','2015-11-27-18571367PF Challan-Sep15.pdf','2015-11-26 19:45:47'),(54,'2015-11-27-1548961188ESIC paid copy april 2015.pdf','esicchallan','uploads','2015-11-27-1548961188ESIC paid copy april 2015.pdf','2015-11-26 19:46:23'),(58,'2015-11-27-713295124ESI AUG 2015.pdf','esicchallan','uploads','2015-11-27-713295124ESI AUG 2015.pdf','2015-11-26 19:48:45'),(60,'2015-11-27-1035645003Labour Welfare Fund January 2015 to June 2015.pdf','labourwelfre','uploads','2015-11-27-1035645003Labour Welfare Fund January 2015 to June 2015.pdf','2015-11-26 19:49:57'),(61,'2015-11-27-24288037list of mmg project (Empire Enterprises Ltd).pdf','employess','uploads','2015-11-27-24288037list of mmg project (Empire Enterprises Ltd).pdf','2015-11-26 19:51:12'),(67,'2016-03-02-1744572391salary sheet month of nov 2015.pdf','escsreport','uploads','2016-03-02-1744572391salary sheet month of nov 2015.pdf','2016-03-02 09:09:31'),(68,'2016-03-02-2120287635salary monht of dec 2015.pdf','escsreport','uploads','2016-03-02-2120287635salary monht of dec 2015.pdf','2016-03-02 09:10:10'),(69,'2016-03-02-1869701229SALARY DEC 2015 Empire Enterpriseds Ltd.pdf','escsreport','uploads','2016-03-02-1869701229SALARY DEC 2015 Empire Enterpriseds Ltd.pdf','2016-03-02 09:10:28'),(70,'2016-03-02-2136152857SALARY jan 2016 Empire Enterpriseds Ltd.pdf','escsreport','uploads','2016-03-02-2136152857SALARY jan 2016 Empire Enterpriseds Ltd.pdf','2016-03-02 09:13:02'),(71,'2016-03-02-957180667salary sheet month of oct 2015.pdf','mustrolwagessheet','uploads','2016-03-02-957180667salary sheet month of oct 2015.pdf','2016-03-02 09:14:16'),(72,'2016-03-02-235573145salary sheet month of nov 2015.pdf','mustrolwagessheet','uploads','2016-03-02-235573145salary sheet month of nov 2015.pdf','2016-03-02 09:14:37'),(73,'2016-03-02-383353183Saley shhet month DEC 2015.pdf','mustrolwagessheet','uploads','2016-03-02-383353183Saley shhet month DEC 2015.pdf','2016-03-02 09:15:59'),(74,'2016-03-02-2045381310Salary sheet month of Jan 2016.pdf','mustrolwagessheet','uploads','2016-03-02-2045381310Salary sheet month of Jan 2016.pdf','2016-03-02 09:16:31'),(75,'2016-03-02-12921074081011512015891_remittance_confirm_slip.pdf','pfchallan','uploads','2016-03-02-12921074081011512015891_remittance_confirm_slip.pdf','2016-03-02 09:20:54'),(76,'2016-03-02-1665883158PF challan month of oct 2015.pdf','pfchallan','uploads','2016-03-02-1665883158PF challan month of oct 2015.pdf','2016-03-02 09:21:27'),(77,'2016-03-02-1918594610PF Challan-Nov15 paid copy.pdf','pfchallan','uploads','2016-03-02-1918594610PF Challan-Nov15 paid copy.pdf','2016-03-02 09:22:34'),(78,'2016-03-02-1375838788PF challan month of dec 2015 paid copy.pdf','pfchallan','uploads','2016-03-02-1375838788PF challan month of dec 2015 paid copy.pdf','2016-03-02 09:23:15'),(79,'2016-03-02-418006187PF Challan paid copy month of jan 2016.pdf','pfchallan','uploads','2016-03-02-418006187PF Challan paid copy month of jan 2016.pdf','2016-03-02 09:24:58'),(80,'2016-03-02-1999748092PF Challan paid copy month of jan 2016.pdf','pfchallan','uploads','2016-03-02-1999748092PF Challan paid copy month of jan 2016.pdf','2016-03-02 09:25:21'),(81,'2016-03-02-2107305662ESI Challan-Nov15.pdf','esicchallan','uploads','2016-03-02-2107305662ESI Challan-Nov15.pdf','2016-03-02 09:25:50'),(82,'2016-03-02-97525160ESI Challan-Nov15 paid copy.pdf','esicchallan','uploads','2016-03-02-97525160ESI Challan-Nov15 paid copy.pdf','2016-03-02 09:26:25'),(83,'2016-03-02-943196967ESI challan month of dec 2015 paid copy.pdf','esicchallan','uploads','2016-03-02-943196967ESI challan month of dec 2015 paid copy.pdf','2016-03-02 09:27:05'),(84,'2016-03-02-418788699ESIC challan paid copy month of jan 2016.pdf','esicchallan','uploads','2016-03-02-418788699ESIC challan paid copy month of jan 2016.pdf','2016-03-02 09:28:38'),(85,'2016-03-18-703833315salary transfer list for the monh of feb 2016.pdf','mustrolwagessheet','uploads','2016-03-18-703833315salary transfer list for the monh of feb 2016.pdf','2016-03-18 07:45:30'),(86,'2016-03-18-1206854380salary transfer list for the monh of feb 2016.pdf','escsreport','uploads','2016-03-18-1206854380salary transfer list for the monh of feb 2016.pdf','2016-03-18 07:46:52'),(87,'2016-03-18-1884532023salary sheet monht of feb 2016.pdf','mustrolwagessheet','uploads','2016-03-18-1884532023salary sheet monht of feb 2016.pdf','2016-03-18 07:47:07'),(88,'2016-03-18-1610794429PF CHALLAN - FEB -2016 (1).pdf','pfchallan','uploads','2016-03-18-1610794429PF CHALLAN - FEB -2016 (1).pdf','2016-03-18 07:48:02'),(89,'2016-03-18-31890244ESIC CHALLAN - FEB -2016.pdf','esicchallan','uploads','2016-03-18-31890244ESIC CHALLAN - FEB -2016.pdf','2016-03-18 07:48:33'),(90,'2016-05-10-923339988Salary sheet month of march 2016 in pdf.pdf','mustrolwagessheet','uploads','2016-05-10-923339988Salary sheet month of march 2016 in pdf.pdf','2016-05-10 07:47:26'),(91,'2016-05-10-614730640ECS salary sheet  bank march 2016.pdf','escsreport','uploads','2016-05-10-614730640ECS salary sheet  bank march 2016.pdf','2016-05-10 07:47:46'),(92,'2016-05-10-830772648PF CHALLAN - MARCH - 2016.pdf','pfchallan','uploads','2016-05-10-830772648PF CHALLAN - MARCH - 2016.pdf','2016-05-10 07:49:47'),(93,'2016-05-10-1111619774ESIC CHALLAN - MARCH - 2016 (2).pdf','esicchallan','uploads','2016-05-10-1111619774ESIC CHALLAN - MARCH - 2016 (2).pdf','2016-05-10 07:50:10'),(94,'2016-07-04-656374767ECR MONTH OF  APRIL 2016.pdf','escsreport','uploads','2016-07-04-656374767ECR MONTH OF  APRIL 2016.pdf','2016-07-03 21:53:03'),(95,'2016-07-04-1375962596PF PAID CHALLAN - APRIL 2016.pdf','pfchallan','uploads','2016-07-04-1375962596PF PAID CHALLAN - APRIL 2016.pdf','2016-07-03 21:53:47'),(96,'2016-07-04-947422559ESIC Paid challan - April 2016.pdf','esicchallan','uploads','2016-07-04-947422559ESIC Paid challan - April 2016.pdf','2016-07-03 21:54:13'),(97,'2016-07-04-1101224296SALARY TRANSFER LIST month of may 2016.pdf','escsreport','uploads','2016-07-04-1101224296SALARY TRANSFER LIST month of may 2016.pdf','2016-07-03 21:54:37'),(98,'2016-07-04-241566192salary sheet month of April 2016.pdf','mustrolwagessheet','uploads','2016-07-04-241566192salary sheet month of April 2016.pdf','2016-07-03 21:55:44'),(99,'2016-07-04-162898097salary month of may 2016.pdf','mustrolwagessheet','uploads','2016-07-04-162898097salary month of may 2016.pdf','2016-07-03 21:56:07'),(100,'2016-07-27-1775182743salary sheet month of june 2016.pdf','mustrolwagessheet','uploads','2016-07-27-1775182743salary sheet month of june 2016.pdf','2016-07-26 21:21:16'),(101,'2016-07-27-641207135PF Challan-June16 PAID.pdf','pfchallan','uploads','2016-07-27-641207135PF Challan-June16 PAID.pdf','2016-07-26 21:22:14'),(102,'2016-07-27-1082684959ESI Challan-June16 PAID.pdf','esicchallan','uploads','2016-07-27-1082684959ESI Challan-June16 PAID.pdf','2016-07-26 21:24:45'),(103,'2016-07-27-243543051ESIC CHALLAN MAY-2016 paid copy.pdf','esicchallan','uploads','2016-07-27-243543051ESIC CHALLAN MAY-2016 paid copy.pdf','2016-07-26 21:25:25'),(104,'2017-12-21-1308057099Esic challan month of jan 2017.pdf','esicchallan','uploads','2017-12-21-1308057099Esic challan month of jan 2017.pdf','2017-12-20 20:17:17'),(105,'2017-12-21-590155559ESI Challan-MAY 2017.pdf','esicchallan','uploads','2017-12-21-590155559ESI Challan-MAY 2017.pdf','2017-12-20 20:19:14'),(106,'2017-12-21-1360083091ESIC june2017.pdf','esicchallan','uploads','2017-12-21-1360083091ESIC june2017.pdf','2017-12-20 20:19:39'),(107,'2017-12-21-508300076ESIC Challan Month of July 2017.pdf','esicchallan','uploads','2017-12-21-508300076ESIC Challan Month of July 2017.pdf','2017-12-20 20:20:27'),(108,'2017-12-21-545326556ESIC aug  2017.pdf','esicchallan','uploads','2017-12-21-545326556ESIC aug  2017.pdf','2017-12-20 20:21:23'),(109,'2021-01-15-1201117343document.pdf','employess','uploads','2021-01-15-1201117343document.pdf','2021-01-15 01:23:58'),(110,'2021-01-15-1823060433MMG 2 4600004608  Empire.pdf','detailpo','uploads','2021-01-15-1823060433MMG 2 4600004608  Empire.pdf','2021-01-15 01:26:11'),(111,'2021-01-15-93212508document.pdf','employess','uploads','2021-01-15-93212508document.pdf','2021-01-15 01:26:18'),(112,'2021-01-15-1223449521wages sheet month of April 2019.pdf','mustrolwagessheet','uploads','2021-01-15-1223449521wages sheet month of April 2019.pdf','2021-01-15 01:28:19'),(113,'2021-01-15-990183475Wages sheet month of May-2020.pdf','mustrolwagessheet','uploads','2021-01-15-990183475Wages sheet month of May-2020.pdf','2021-01-15 01:33:52'),(114,'2021-01-15-644997649Wages sheet month of JUNE 2020 in pdf.pdf','mustrolwagessheet','uploads','2021-01-15-644997649Wages sheet month of JUNE 2020 in pdf.pdf','2021-01-15 01:34:07'),(115,'2021-01-15-325097733Wages Sheet month of july 2020.pdf','mustrolwagessheet','uploads','2021-01-15-325097733Wages Sheet month of july 2020.pdf','2021-01-15 01:34:22'),(116,'2021-01-15-1234351201Wages Sheet month of Aug-2020.pdf','mustrolwagessheet','uploads','2021-01-15-1234351201Wages Sheet month of Aug-2020.pdf','2021-01-15 01:34:38'),(117,'2021-01-15-2026751453Wages sheet month of Sep 2020.pdf','mustrolwagessheet','uploads','2021-01-15-2026751453Wages sheet month of Sep 2020.pdf','2021-01-15 01:34:50'),(118,'2021-01-15-1807038020WAGES SHEET MONTH OF OCT-2020.pdf','mustrolwagessheet','uploads','2021-01-15-1807038020WAGES SHEET MONTH OF OCT-2020.pdf','2021-01-15 01:35:03'),(119,'2021-01-15-1778888295wages sheet month of Nov-2020.pdf','mustrolwagessheet','uploads','2021-01-15-1778888295wages sheet month of Nov-2020.pdf','2021-01-15 01:36:40'),(120,'2021-01-15-1490044550Bank transfer month of April 2020 verified copy.pdf','escsreport','uploads','2021-01-15-1490044550Bank transfer month of April 2020 verified copy.pdf','2021-01-15 01:37:12'),(121,'2021-01-15-682026232Bank transfer month of May 2020 verified copy.pdf','escsreport','uploads','2021-01-15-682026232Bank transfer month of May 2020 verified copy.pdf','2021-01-15 01:37:29'),(122,'2021-01-15-1315495585Bank transfer month of june 2020.pdf','escsreport','uploads','2021-01-15-1315495585Bank transfer month of june 2020.pdf','2021-01-15 01:37:40'),(123,'2021-01-15-1624833732Bank transfer month of July 2020.pdf','escsreport','uploads','2021-01-15-1624833732Bank transfer month of July 2020.pdf','2021-01-15 01:37:52'),(124,'2021-01-15-619510012Bank transfer monht of Aug 2020.pdf','escsreport','uploads','2021-01-15-619510012Bank transfer monht of Aug 2020.pdf','2021-01-15 01:38:05'),(125,'2021-01-15-1948962881Bank Transfer month of SEP 2020.pdf','escsreport','uploads','2021-01-15-1948962881Bank Transfer month of SEP 2020.pdf','2021-01-15 01:38:23'),(126,'2021-01-15-962539082Bank transfer Salary month of OCT-2020.pdf','escsreport','uploads','2021-01-15-962539082Bank transfer Salary month of OCT-2020.pdf','2021-01-15 01:38:32'),(127,'2021-01-15-1563335498Bank Transfer Month Of Nov-2020.pdf','escsreport','uploads','2021-01-15-1563335498Bank Transfer Month Of Nov-2020.pdf','2021-01-15 01:38:42'),(128,'2021-01-15-1925395164PF challan month of APril 2020.pdf','pfchallan','uploads','2021-01-15-1925395164PF challan month of APril 2020.pdf','2021-01-15 01:39:11'),(129,'2021-01-15-1023529201PF challan month of MAY 2020.pdf','pfchallan','uploads','2021-01-15-1023529201PF challan month of MAY 2020.pdf','2021-01-15 01:39:16'),(130,'2021-01-15-722027954ECR month of june 2020.pdf','pfchallan','uploads','2021-01-15-722027954ECR month of june 2020.pdf','2021-01-15 01:39:52'),(131,'2021-01-15-148142369PF challan month of July 2020.pdf','pfchallan','uploads','2021-01-15-148142369PF challan month of July 2020.pdf','2021-01-15 01:40:01'),(132,'2021-01-15-334846157PF challan month of Aug-2020.pdf','pfchallan','uploads','2021-01-15-334846157PF challan month of Aug-2020.pdf','2021-01-15 01:40:12'),(133,'2021-01-15-997181104PF challan month of Sep 2020.pdf','pfchallan','uploads','2021-01-15-997181104PF challan month of Sep 2020.pdf','2021-01-15 01:40:21'),(134,'2021-01-15-560110469PF challan month of OCT-2020.pdf','pfchallan','uploads','2021-01-15-560110469PF challan month of OCT-2020.pdf','2021-01-15 01:40:31'),(135,'2021-01-15-1216740048PF challan month of NOV-2020.pdf','pfchallan','uploads','2021-01-15-1216740048PF challan month of NOV-2020.pdf','2021-01-15 01:40:43'),(136,'2021-01-15-1496579855ESIC challan month of April 2019.pdf','esicchallan','uploads','2021-01-15-1496579855ESIC challan month of April 2019.pdf','2021-01-15 01:41:03'),(137,'2021-01-15-257764163ESIC Challan month of May 2020..pdf','esicchallan','uploads','2021-01-15-257764163ESIC Challan month of May 2020..pdf','2021-01-15 01:41:16'),(138,'2021-01-15-1210139052ESIC challan month of june 2020.pdf','esicchallan','uploads','2021-01-15-1210139052ESIC challan month of june 2020.pdf','2021-01-15 01:41:24'),(139,'2021-01-15-566412461ESIC challan month of july 2020.pdf','esicchallan','uploads','2021-01-15-566412461ESIC challan month of july 2020.pdf','2021-01-15 01:41:46'),(140,'2021-01-15-780412329ESIC challan month of Aug 2020.pdf','esicchallan','uploads','2021-01-15-780412329ESIC challan month of Aug 2020.pdf','2021-01-15 01:41:55'),(141,'2021-01-15-1551636401ESIC challan month of SEP 2020.pdf','esicchallan','uploads','2021-01-15-1551636401ESIC challan month of SEP 2020.pdf','2021-01-15 01:42:06'),(142,'2021-01-15-329464447ESIC challan month of OCT-2020.pdf','esicchallan','uploads','2021-01-15-329464447ESIC challan month of OCT-2020.pdf','2021-01-15 01:42:17'),(143,'2021-01-15-1445686281Half yearly returns and Labour welfare funds challan till june 2020..pdf','labourwelfre','uploads','2021-01-15-1445686281Half yearly returns and Labour welfare funds challan till june 2020..pdf','2021-01-15 01:45:52'),(144,'2021-01-15-1286460751LabourApplicationReciept.pdf','laboutllicense','uploads','2021-01-15-1286460751LabourApplicationReciept.pdf','2021-01-15 01:50:39'),(145,'2021-01-15-1405338840Labour License 4600004608.pdf','laboutllicense','uploads','2021-01-15-1405338840Labour License 4600004608.pdf','2021-01-15 01:51:43'),(146,'2021-01-15-372110103Datails of Manpower MMG project.pdf','employess','uploads','2021-01-15-372110103Datails of Manpower MMG project.pdf','2021-01-15 01:56:40'),(147,'2021-01-28-2082374638Wages sheet month of Dec-2020.pdf','mustrolwagessheet','uploads','2021-01-28-2082374638Wages sheet month of Dec-2020.pdf','2021-01-28 12:01:00'),(148,'2021-01-28-304534741Bank transfer month of Dec-2020.pdf','escsreport','uploads','2021-01-28-304534741Bank transfer month of Dec-2020.pdf','2021-01-28 12:01:20'),(149,'2021-01-28-957283136PF challan monht of Dec-2020.pdf','pfchallan','uploads','2021-01-28-957283136PF challan monht of Dec-2020.pdf','2021-01-28 12:01:40'),(150,'2021-01-28-1349070116ESIC challan month of NOV-2020.pdf','esicchallan','uploads','2021-01-28-1349070116ESIC challan month of NOV-2020.pdf','2021-01-28 12:10:19'),(151,'2021-01-28-1751533909ESIC Challan month of DEC-2020.pdf','esicchallan','uploads','2021-01-28-1751533909ESIC Challan month of DEC-2020.pdf','2021-01-28 12:10:36');
/*!40000 ALTER TABLE `employee_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employee_pdf`
--

DROP TABLE IF EXISTS `employee_pdf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employee_pdf` (
  `id` int(25) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(1023) CHARACTER SET utf8 NOT NULL,
  `category_file` varchar(255) DEFAULT NULL,
  `type` varchar(255) NOT NULL,
  `filepath` varchar(2047) CHARACTER SET utf8 DEFAULT NULL,
  `date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee_pdf`
--

LOCK TABLES `employee_pdf` WRITE;
/*!40000 ALTER TABLE `employee_pdf` DISABLE KEYS */;
INSERT INTO `employee_pdf` VALUES (4,'4.jpg','mustrol','','uploads/4.jpg','2014-06-02 12:36:53'),(6,'','mustrol','',NULL,'2014-06-02 13:26:08'),(7,'Vita Agro - Administrator.pdf','mustrol','','uploads/7.pdf','2014-06-02 13:30:18'),(8,'Vita Agro - Administrator.pdf','mustrol','','uploads/8.pdf','2014-06-02 13:32:54'),(98,'PF JUNE_ 2015.pdf','pfchallan','','uploads/PF JUNE_ 2015.pdf','2015-09-17 06:12:49'),(99,'Mustrol cum wages sheet month of April 2015.pdf','employess','','uploads/Mustrol cum wages sheet month of April 2015.pdf','2015-09-23 06:31:51');
/*!40000 ALTER TABLE `employee_pdf` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `login`
--

DROP TABLE IF EXISTS `login`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `login`
--

LOCK TABLES `login` WRITE;
/*!40000 ALTER TABLE `login` DISABLE KEYS */;
INSERT INTO `login` VALUES (1,1);
/*!40000 ALTER TABLE `login` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-02-08  4:08:44
